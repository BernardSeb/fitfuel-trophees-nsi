# FitFuel Trophees NSI

### Protocole d'utilisation :
- [ ] Système d'exploitation : Windows
- [ ] Ecran : Il est préférable d'utiliser un écran 1080p avec une mise à l'échelle à 100% pour une utilisation optimale. Si vous utilisez un ordinateur portable il est possible qu'une partie des interfaces soit rogner (Régler la mise à l'échelle de Windows à 100% peut corriger ce problème).
- [ ] WebCam / caméra externe : uniquement si vous souhaitez utiliser la section "tracking" vidéo.

### Démarrage de l'application :
Pour lancer l'application, il suffit d'exécuter le fichier ui_MainPage.py

### Configuration :
Au lancement de l'application, vous pouvez cliquer sur l'icône 'Paramètres' pour saisir toutes les informations nécessaires au fonctionnement optimal de l'application.

### Page Nutrition :
- [ ] Recommandations de repas hebdomadaires : Sur le côté gauche de la page, vous trouverez des recommandations pour les repas de la semaine. Cliquez sur chaque repas pour obtenir une liste détaillée des instructions relatives à la préparation du plat en question.
- [ ] Références de recettes en ligne : Si vous préférez, vous pouvez également explorer d'autres recettes en ligne. Elles sont référencées à droite de l'écran, ce qui vous permet d'y accéder directement dans votre navigateur.

### Page sur la forme physique : 
- [ ] Séries d'exercices indépendants : Sur le côté droit, vous trouverez diverses séries d'exercices que vous pouvez effectuer de manière autonome, selon vos préférences.
- [ ] Séances "tracking" vidéo : La partie gauche de la page fournit des recommandations d'exercices en fonction de votre niveau répartis sur plusieurs jours. En cliquant sur le jour souhaité vous ouvrirez une fenêtre affichant une vidéo démontrant la technique correcte. La caméra s'active ensuite pour s'assurer de la bonne exécution de vos mouvements Un chronomètre permet de suivre votre progression, ainsi qu'un compteur de points.
- [ ] Nerd Stats : Cette fonctionalité vous permet d'afficher ou de masquer les interprétations algorithmiques de différents points du corps pendant l'exercice, vous donnant ainsi un aperçu détaillé de vos performances. Cette fonctionnalité n'est visible que l'orsque la caméra est en marche.

### Contribution : 
Nous avons intégralement rédigé le code de FitFuel par nous-même. Cependant, il reste important de créditer Corentin Remaud, nous avons utilisé son [API](https://github.com/remaudcorentin-dev/python-marmiton), pour interagir avec Marmiton . Toutefois, le code exploitant cette librairie est entièrement rédigé par nos soins.

### Contact: 
Pour nous contacter vous pouvez nous envoyer un mail sur l'adresse mail suivante : fitfuel.officiel@gmail.com
