def spaghetti_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "bœuf haché", "tomates concassées", "tomate concentrée", "origan", "basilic", "sucre", "spaghetti"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon", 1*multiplicateur_arrondi, "gousse d'ail émincée", 300*multiplicateur_arrondi, "g de bœuf haché", 400*multiplicateur_arrondi, "g de tomates concassées en conserve", 2*multiplicateur_arrondi, "cuillère à soupe de tomate concentrée", "1/2 cuillère à café d'origan", "1/2 cuillère à café de basilic", "1/2 cuillère à café de sucre", 100*multiplicateur_arrondi, "g de spaghetti"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon et l'ail, faire revenir jusqu'à ce qu'ils soient translucides.", "ÉTAPE 3\nAjouter la viande hachée et faire cuire jusqu'à ce qu'elle soit dorée.", "ÉTAPE 4\nAjouter les tomates concassées, la tomate concentrée, l'origan, le basilic, le sucre, le sel et le poivre.", "ÉTAPE 5\nLaisser mijoter à feu doux pendant 20-30 minutes.", "ÉTAPE 6\nPendant ce temps, cuire les spaghetti selon les instructions sur l'emballage.", "ÉTAPE 7\nServir la sauce bolognaise sur les spaghetti chauds."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

spaghetti_bolognaise
def poulet_roti_pommes_terre_roties(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "paprika", "thym", "huile d'olive", "pommes de terre", "poulet entier"]
    ingredient = ["sel", "poivre", "paprika", "thym", 4*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 800*multiplicateur_arrondi, "g de pommes de terre", 1*multiplicateur_arrondi, "poulet entier"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nÉplucher les pommes de terre et les couper en quartiers.", "ÉTAPE 3\nDans un grand plat allant au four, mélanger les pommes de terre avec l'huile d'olive, le sel, le poivre, le paprika et le thym.", "ÉTAPE 4\nPlacer le poulet au centre du plat sur les pommes de terre.", "ÉTAPE 5\nAssaisonner le poulet avec du sel, du poivre, du paprika et du thym.", "ÉTAPE 6\nEnfourner et cuire pendant environ 1 heure ou jusqu'à ce que le poulet soit bien cuit et que les pommes de terre soient dorées et tendres.", "ÉTAPE 7\nRetirer du four et laisser reposer le poulet pendant quelques minutes avant de le découper.", "ÉTAPE 8\nServir le poulet rôti avec les pommes de terre rôties."]
    temps_de_preparation = 15
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_pommes_terre_roties
def steak_grille_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steak de bœuf", "ail", "brocoli", "poivron rouge", "poivron jaune", "oignon", "sauce soja"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "steak de bœuf", 2*multiplicateur_arrondi, "g d'ail émincé", 1*multiplicateur_arrondi, "tête de brocoli coupée en petits morceaux", 1*multiplicateur_arrondi, "poivron rouge émincé", 1*multiplicateur_arrondi, "poivron jaune émincé", 1*multiplicateur_arrondi, "oignon émincé", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 2\nAssaisonner le steak de bœuf avec du sel et du poivre.", "ÉTAPE 3\nFaire griller le steak dans la poêle pendant 3-4 minutes de chaque côté pour une cuisson à point.", "ÉTAPE 4\nRetirer le steak de la poêle et le laisser reposer pendant quelques minutes avant de le trancher.", "ÉTAPE 5\nDans la même poêle, ajouter l'ail émincé, le brocoli, les poivrons et l'oignon.", "ÉTAPE 6\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 7\nAssaisonner les légumes avec de la sauce soja.", "ÉTAPE 8\nServir le steak tranché avec les légumes sautés."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_grille_legumes_sautes
def lasagnes_e_pinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "épinards", "ail", "fromage ricotta", "fromage mozzarella", "fromage parmesan", "sauce tomate", "feuilles de lasagne"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", 200*multiplicateur_arrondi, "g d'épinards frais", 2*multiplicateur_arrondi, "gousses d'ail émincées", 250*multiplicateur_arrondi, "g de fromage ricotta", 200*multiplicateur_arrondi, "g de fromage mozzarella râpé", 50*multiplicateur_arrondi, "g de fromage parmesan râpé", 400*multiplicateur_arrondi, "g de sauce tomate", 200*multiplicateur_arrondi, "g de feuilles de lasagne"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (thermostat 6).", "ÉTAPE 2\nDans une poêle, faire chauffer l'huile d'olive et faire revenir l'ail émincé.", "ÉTAPE 3\nAjouter les épinards frais dans la poêle et les faire cuire jusqu'à ce qu'ils soient tendres.", "ÉTAPE 4\nDans un bol, mélanger les épinards cuits avec la ricotta, le sel et le poivre.", "ÉTAPE 5\nDans un plat à gratin, étaler une couche de sauce tomate au fond.", "ÉTAPE 6\nDisposer une couche de feuilles de lasagne par-dessus la sauce tomate.", "ÉTAPE 7\nÉtaler une couche du mélange épinards-ricotta sur les feuilles de lasagne.", "ÉTAPE 8\nSaupoudrer de mozzarella râpée sur le mélange épinards-ricotta.", "ÉTAPE 9\nRépéter les couches jusqu'à ce que tous les ingrédients soient utilisés, en terminant par une couche de sauce tomate et de fromage parmesan râpé sur le dessus.", "ÉTAPE 10\nCouvrir le plat de lasagnes d'une feuille de papier d'aluminium et cuire au four pendant 30 minutes.", "ÉTAPE 11\nRetirer le papier d'aluminium et poursuivre la cuisson pendant encore 10-15 minutes, jusqu'à ce que le dessus soit doré et croustillant.", "ÉTAPE 12\nLaisser reposer pendant quelques minutes avant de servir."]
    temps_de_preparation = 30
    temps_de_cuisson = 45
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

lasagnes_e_pinards_fromage
def curry_poulet_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet", "oignon", "ail", "gingembre", "curry en poudre", "lait de coco", "tomates", "poivrons", "riz basmati"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 500*multiplicateur_arrondi, "g de poulet coupé en dés", 1*multiplicateur_arrondi, "oignon émincé", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillère à soupe de curry en poudre", 400*multiplicateur_arrondi, "ml de lait de coco", 400*multiplicateur_arrondi, "g de tomates concassées en conserve", 1*multiplicateur_arrondi, "poivron coupé en dés", 200*multiplicateur_arrondi, "g de riz basmati cuit"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon émincé, l'ail émincé et le gingembre râpé dans la poêle et faire revenir jusqu'à ce qu'ils soient tendres.", "ÉTAPE 3\nAjouter les dés de poulet dans la poêle et les faire cuire jusqu'à ce qu'ils soient dorés de tous les côtés.", "ÉTAPE 4\nSaupoudrer de curry en poudre sur le poulet et mélanger pour bien enrober.", "ÉTAPE 5\nAjouter le lait de coco, les tomates concassées et les dés de poivron dans la poêle.", "ÉTAPE 6\nPorter à ébullition, puis réduire le feu et laisser mijoter pendant 20-25 minutes, jusqu'à ce que le poulet soit cuit et la sauce épaissie.", "ÉTAPE 7\nPendant ce temps, cuire le riz basmati selon les instructions sur l'emballage.", "ÉTAPE 8\nServir le curry de poulet chaud sur le riz basmati cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

curry_poulet_riz_basmati
def pizza_variete_garnitures(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâte à pizza", "sauce tomate", "mozzarella", "garnitures au choix (ex: champignons, poivrons, oignons, olives, jambon, pepperoni)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "pâte à pizza", 200*multiplicateur_arrondi, "ml de sauce tomate", 200*multiplicateur_arrondi, "g de mozzarella râpée", "garnitures au choix"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 220°C (thermostat 7-8).", "ÉTAPE 2\nÉtaler la pâte à pizza sur une plaque de cuisson légèrement huilée.", "ÉTAPE 3\nÉtaler la sauce tomate sur la pâte à pizza, en laissant un bord libre autour.", "ÉTAPE 4\nAjouter la mozzarella râpée sur la sauce tomate.", "ÉTAPE 5\nAjouter les garnitures de votre choix sur la pizza.", "ÉTAPE 6\nAssaisonner avec du sel et du poivre, selon votre goût.", "ÉTAPE 7\nArroser d'un filet d'huile d'olive sur les garnitures.", "ÉTAPE 8\nCuire au four préchauffé pendant 12-15 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante et que le fromage soit fondu et doré.", "ÉTAPE 9\nRetirer du four et laisser refroidir légèrement avant de couper et de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_variete_garnitures
def saumon_cuit_four_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filet de saumon", "quinoa", "légumes au choix (ex: brocoli, carottes, poivrons)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "filet de saumon", 75*multiplicateur_arrondi, "g de quinoa", "légumes au choix"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (thermostat 6).", "ÉTAPE 2\nAssaisonner le filet de saumon avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 3\nPlacer le filet de saumon sur une plaque de cuisson recouverte de papier sulfurisé.", "ÉTAPE 4\nCuire au four préchauffé pendant 15-20 minutes, ou jusqu'à ce que le saumon soit cuit à votre goût.", "ÉTAPE 5\nPendant ce temps, cuire le quinoa selon les instructions sur l'emballage.", "ÉTAPE 6\nFaire cuire les légumes de votre choix à la vapeur, à la poêle ou au four.", "ÉTAPE 7\nServir le filet de saumon cuit au four avec le quinoa cuit et les légumes."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_cuit_four_quinoa
def wraps_legumes_grilles_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "tortillas de blé entier", "légumes pour grillades (ex: poivrons, courgettes, oignons)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet", 4*multiplicateur_arrondi, "tortillas de blé entier", "légumes pour grillades"]
    preparation = ["ÉTAPE 1\nAssaisonner les poitrines de poulet avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 2\nFaire griller les poitrines de poulet sur un gril préchauffé pendant 6-8 minutes de chaque côté, ou jusqu'à ce qu'elles soient cuites à votre goût.", "ÉTAPE 3\nPendant ce temps, faire griller les légumes sur le gril jusqu'à ce qu'ils soient tendres et légèrement dorés.", "ÉTAPE 4\nCouper les poitrines de poulet cuites en lanières.", "ÉTAPE 5\nChauffer les tortillas de blé entier dans une poêle ou au micro-ondes selon les instructions sur l'emballage.", "ÉTAPE 6\nÉtaler les légumes grillés et les lanières de poulet sur les tortillas chaudes.", "ÉTAPE 7\nRouler les tortillas fermement pour former des wraps.", "ÉTAPE 8\nCouper les wraps en deux et servir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_legumes_grilles_poulet
def riz_frit_legumes_crevettes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "riz cuit", "crevettes décortiquées", "légumes pour wok (ex: poivrons, carottes, pois mange-tout)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 300*multiplicateur_arrondi, "g de riz cuit", 200*multiplicateur_arrondi, "g de crevettes décortiquées", "légumes pour wok"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter les crevettes décortiquées dans le wok et les faire sauter jusqu'à ce qu'elles soient roses et opaques.", "ÉTAPE 3\nRetirer les crevettes du wok et réserver.", "ÉTAPE 4\nAjouter les légumes pour wok dans le wok et les faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 5\nAjouter le riz cuit dans le wok avec les légumes et mélanger pour bien combiner.", "ÉTAPE 6\nAjouter les crevettes réservées dans le wok avec le riz et les légumes.", "ÉTAPE 7\nAssaisonner avec du sel et du poivre, selon votre goût, et mélanger pour bien enrober.", "ÉTAPE 8\nContinuer à cuire jusqu'à ce que le tout soit bien chaud.", "ÉTAPE 9\nServir le riz frit aux légumes et crevettes chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

riz_frit_legumes_crevettes
def sandwich_poulet_grille_avocat_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "avocat", "fromage (ex: cheddar, suisse)", "pain de blé entier", "laitue ou épinards"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet", 1*multiplicateur_arrondi, "avocat mûr en tranches", 1*multiplicateur_arrondi, "tranches de fromage (ex: cheddar, suisse)", 2*multiplicateur_arrondi, "tranches de pain de blé entier", "laitue ou épinards"]
    preparation = ["ÉTAPE 1\nAssaisonner les poitrines de poulet avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 2\nFaire griller les poitrines de poulet sur un gril préchauffé pendant 6-8 minutes de chaque côté, ou jusqu'à ce qu'elles soient cuites à votre goût.", "ÉTAPE 3\nPendant ce temps, préparer les autres ingrédients.", "ÉTAPE 4\nCouper les poitrines de poulet cuites en tranches épaisses.", "ÉTAPE 5\nTartiner une tranche de pain de blé entier de tranches d'avocat mûr.", "ÉTAPE 6\nDisposer les tranches de poulet grillé sur l'avocat.", "ÉTAPE 7\nAjouter une tranche de fromage sur le poulet.", "ÉTAPE 8\nAjouter de la laitue ou des épinards sur le fromage.", "ÉTAPE 9\nRecouvrir avec une deuxième tranche de pain de blé entier.", "ÉTAPE 10\nCouper le sandwich en deux et servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_grille_avocat_fromage
def salade_quinoa_pois_chiches_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "pois chiches en conserve", "légumes au choix (ex: concombre, tomates, poivrons)", "fromage feta émietté", "vinaigrette au choix"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 75*multiplicateur_arrondi, "g de quinoa cuit", 200*multiplicateur_arrondi, "g de pois chiches en conserve, rincés et égouttés", "légumes au choix", 50*multiplicateur_arrondi, "g de fromage feta émietté", "vinaigrette au choix"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les pois chiches rincés et égouttés, et les légumes au choix coupés en dés.", "ÉTAPE 2\nAjouter le fromage feta émietté dans le bol.", "ÉTAPE 3\nAssaisonner la salade avec du sel, du poivre et de l'huile d'olive, selon votre goût.", "ÉTAPE 4\nAjouter la vinaigrette de votre choix et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 5\nServir la salade de quinoa aux pois chiches et légumes comme plat principal ou en accompagnement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_pois_chiches_legumes
def wrap_vegetarien_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "haricots noirs en conserve", "maïs en conserve", "avocat", "tomates", "tortillas de blé entier"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de haricots noirs en conserve, rincés et égouttés", 200*multiplicateur_arrondi, "g de maïs en conserve, rincé et égoutté", 1*multiplicateur_arrondi, "avocat mûr en tranches", 1*multiplicateur_arrondi, "tomate coupée en dés", 4*multiplicateur_arrondi, "tortillas de blé entier"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les haricots noirs rincés et égouttés avec le maïs rincé et égoutté.", "ÉTAPE 2\nAjouter les tranches d'avocat et les dés de tomate dans le bol.", "ÉTAPE 3\nAssaisonner avec du sel, du poivre et de l'huile d'olive, selon votre goût, et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 4\nChauffer les tortillas de blé entier dans une poêle ou au micro-ondes selon les instructions sur l'emballage.", "ÉTAPE 5\nRépartir le mélange de haricots noirs, maïs, avocat et tomate sur les tortillas chaudes.", "ÉTAPE 6\nRouler les tortillas fermement pour former des wraps.", "ÉTAPE 7\nCouper les wraps en deux et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_haricots_noirs
def poulet_roti_patates_douces_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet entier", "patates douces", "légumes pour rôtir (ex: carottes, oignons, poivrons)", "herbes aromatiques (ex: thym, romarin)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "poulet entier", 400*multiplicateur_arrondi, "g de patates douces coupées en dés", "légumes pour rôtir", "herbes aromatiques"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nAssaisonner le poulet entier avec du sel, du poivre et de l'huile d'olive, en frottant bien la peau.", "ÉTAPE 3\nDisposer le poulet entier dans un plat allant au four.", "ÉTAPE 4\nDisposer les dés de patates douces et les légumes pour rôtir autour du poulet dans le plat.", "ÉTAPE 5\nAjouter les herbes aromatiques sur le poulet et les légumes.", "ÉTAPE 6\nCuire au four préchauffé pendant 1 heure à 1 heure 15 minutes, ou jusqu'à ce que le poulet soit doré et que les patates douces et les légumes soient tendres.", "ÉTAPE 7\nRetirer le poulet du four et laisser reposer pendant quelques minutes avant de découper et de servir avec les patates douces et les légumes."]
    temps_de_preparation = 15
    temps_de_cuisson = 75
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_patates_douces_legumes
def pates_bolognaise_viande_hachee(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande hachée (boeuf ou dinde)", "oignon", "ail", "sauce tomate", "pâtes (ex: spaghetti, penne)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de viande hachée (boeuf ou dinde)", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 400*multiplicateur_arrondi, "ml de sauce tomate", 200*multiplicateur_arrondi, "g de pâtes (ex: spaghetti, penne)"]
    preparation = ["ÉTAPE 1\nFaire chauffer l'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché et l'ail émincé dans la poêle et faire revenir jusqu'à ce qu'ils soient tendres.", "ÉTAPE 3\nAjouter la viande hachée dans la poêle et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à votre goût.", "ÉTAPE 4\nAjouter la sauce tomate dans la poêle et laisser mijoter pendant 10-15 minutes pour développer les saveurs.", "ÉTAPE 5\nPendant ce temps, faire cuire les pâtes selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 6\nÉgoutter les pâtes cuites et les ajouter à la sauce tomate dans la poêle.", "ÉTAPE 7\nMélanger les pâtes avec la sauce pour bien les enrober.", "ÉTAPE 8\nServir les pâtes à la bolognaise chaudes."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_bolognaise_viande_hachee
def salade_poulet_cesar_croûtons_parmesan(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "laitue romaine", "croûtons", "fromage parmesan râpé", "sauce césar"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 200*multiplicateur_arrondi, "g de laitue romaine coupée en lanières", "croûtons", 50*multiplicateur_arrondi, "g de fromage parmesan râpé", "sauce césar"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet cuites avec la laitue romaine coupée en lanières.", "ÉTAPE 2\nAjouter les croûtons et le fromage parmesan râpé dans le bol.", "ÉTAPE 3\nAssaisonner avec du sel, du poivre et de l'huile d'olive, selon votre goût.", "ÉTAPE 4\nAjouter la sauce césar dans le bol et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 5\nServir la salade de poulet césar garnie de plus de croûtons et de fromage parmesan râpé, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar_croûtons_parmesan
def steak_boeuf_grille_pommes_terre_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steak de bœuf (ex: entrecôte, faux-filet)", "pommes de terre", "légumes pour rôtir (ex: carottes, courgettes, oignons)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "steak de bœuf (ex: entrecôte, faux-filet)", 300*multiplicateur_arrondi, "g de pommes de terre coupées en quartiers", "légumes pour rôtir"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner le steak de bœuf avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 3\nGriller le steak de bœuf sur le gril préchauffé pendant 4-6 minutes de chaque côté, ou jusqu'à ce qu'il atteigne la cuisson désirée.", "ÉTAPE 4\nPendant ce temps, faire rôtir les quartiers de pommes de terre et les légumes pour rôtir au four préchauffé à 200°C (thermostat 6-7) pendant 20-25 minutes, ou jusqu'à ce qu'ils soient tendres et dorés.", "ÉTAPE 5\nRetirer le steak de bœuf grillé du gril et le laisser reposer pendant quelques minutes avant de servir avec les pommes de terre rôties et les légumes."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_grille_pommes_terre_legumes
def salade_thon_haricots_blancs_vinaigrette(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "thon en conserve", "haricots blancs en conserve", "tomates cerises", "oignon rouge", "vinaigrette au choix"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "boîte de thon en conserve, égouttée", 200*multiplicateur_arrondi, "g de haricots blancs en conserve, rincés et égouttés", "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge finement tranché", "vinaigrette au choix"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon égoutté, les haricots blancs rincés et égouttés, les tomates cerises coupées en deux et l'oignon rouge finement tranché.", "ÉTAPE 2\nAssaisonner la salade avec du sel, du poivre et de l'huile d'olive, selon votre goût.", "ÉTAPE 3\nAjouter la vinaigrette de votre choix dans le bol et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 4\nServir la salade de thon avec haricots blancs accompagnée de croûtons ou de pain grillé, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs_vinaigrette
def poelee_crevettes_legumes_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "crevettes décortiquées", "légumes pour sauté (ex: poivrons, brocoli, champignons)", "ail", "riz basmati"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de crevettes décortiquées", "légumes pour sauté", 2*multiplicateur_arrondi, "gousses d'ail émincées", 150*multiplicateur_arrondi, "g de riz basmati cuit"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter les crevettes décortiquées dans la poêle et les faire sauter jusqu'à ce qu'elles soient roses et opaques.", "ÉTAPE 3\nRetirer les crevettes du wok et réserver.", "ÉTAPE 4\nAjouter les légumes pour sauté dans le wok et les faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 5\nAjouter l'ail émincé dans le wok avec les légumes et cuire pendant 1 à 2 minutes de plus.", "ÉTAPE 6\nAjouter les crevettes réservées dans le wok avec les légumes et l'ail.", "ÉTAPE 7\nAssaisonner avec du sel et du poivre, selon votre goût, et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 8\nServir la poêlée de crevettes et légumes chaud avec du riz basmati cuit."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_legumes_riz_basmati
def burger_dinde_frites_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "patties de dinde", "pain à burger", "fromage (ex: cheddar)", "laitue", "tomates", "oignon rouge", "patates douces"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "patties de dinde", 2*multiplicateur_arrondi, "pains à burger", 2*multiplicateur_arrondi, "tranches de fromage (ex: cheddar)", "feuilles de laitue", "tranches de tomates", 1*multiplicateur_arrondi, "oignon rouge tranché", 200*multiplicateur_arrondi, "g de patates douces coupées en frites"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 220°C (thermostat 7-8).", "ÉTAPE 2\nDisposer les frites de patates douces sur une plaque de cuisson tapissée de papier sulfurisé.", "ÉTAPE 3\nArroser les frites de patates douces avec de l'huile d'olive et les assaisonner avec du sel et du poivre.", "ÉTAPE 4\nCuire au four préchauffé pendant 20-25 minutes, ou jusqu'à ce que les frites de patates douces soient dorées et croustillantes.", "ÉTAPE 5\nPendant ce temps, faire cuire les patties de dinde dans une poêle chauffée à feu moyen jusqu'à ce qu'elles soient cuites à votre goût.", "ÉTAPE 6\nAssembler les burgers en plaçant les patties de dinde sur les pains à burger, puis en ajoutant une tranche de fromage, des feuilles de laitue, des tranches de tomates et des tranches d'oignon rouge.", "ÉTAPE 7\nServir les burgers de dinde chauds avec les frites de patates douces croustillantes."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

burger_dinde_frites_patates_douces
def poulet_roti_legumes_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet entier", "légumes pour rôtir (ex: carottes, poivrons, courgettes)", "quinoa"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "poulet entier", "légumes pour rôtir", 75*multiplicateur_arrondi, "g de quinoa cru"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nAssaisonner le poulet entier avec du sel, du poivre et de l'huile d'olive, en frottant bien la peau.", "ÉTAPE 3\nDisposer le poulet entier dans un plat allant au four.", "ÉTAPE 4\nDisposer les légumes pour rôtir autour du poulet dans le plat.", "ÉTAPE 5\nCuire au four préchauffé pendant 1 heure à 1 heure 15 minutes, ou jusqu'à ce que le poulet soit doré et que les légumes soient tendres.", "ÉTAPE 6\nPendant ce temps, faire cuire le quinoa selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 7\nÉgoutter le quinoa cuit et le servir avec le poulet rôti et les légumes."]
    temps_de_preparation = 15
    temps_de_cuisson = 75
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_legumes_quinoa
def wraps_saumon_avocat_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de saumon", "avocat", "concombre", "tomates", "tortillas de blé", "sauce au yaourt grec"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "avocat mûr en tranches", 1*multiplicateur_arrondi, "concombre coupé en lanières", 1*multiplicateur_arrondi, "tomates coupées en tranches", 4*multiplicateur_arrondi, "tortillas de blé", "sauce au yaourt grec"]
    preparation = ["ÉTAPE 1\nFaire chauffer l'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 2\nAssaisonner les filets de saumon avec du sel et du poivre, puis les faire cuire dans la poêle chaude pendant 3 à 4 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à votre goût.", "ÉTAPE 3\nPendant ce temps, chauffer les tortillas de blé dans une poêle ou au micro-ondes selon les instructions sur l'emballage.", "ÉTAPE 4\nÉtaler une cuillerée de sauce au yaourt grec sur chaque tortilla de blé.", "ÉTAPE 5\nDisposer les tranches d'avocat, les lanières de concombre, les tranches de tomates et les filets de saumon cuits sur chaque tortilla de blé.", "ÉTAPE 6\nAssaisonner avec du sel et du poivre supplémentaires, si désiré.", "ÉTAPE 7\nRouler les tortillas fermement pour former des wraps.", "ÉTAPE 8\nCouper les wraps en deux et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_saumon_avocat_legumes
def lasagnes_boeuf_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande de boeuf hachée", "épinards frais", "sauce tomate", "feuilles de lasagne", "fromage ricotta", "fromage mozzarella râpé"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 300*multiplicateur_arrondi, "g de viande de boeuf hachée", 200*multiplicateur_arrondi, "g d'épinards frais", 400*multiplicateur_arrondi, "ml de sauce tomate", 6*multiplicateur_arrondi, "feuilles de lasagne", 200*multiplicateur_arrondi, "g de fromage ricotta", 150*multiplicateur_arrondi, "g de fromage mozzarella râpé"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (thermostat 6).", "ÉTAPE 2\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 3\nAjouter la viande de boeuf hachée dans la poêle et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à votre goût.", "ÉTAPE 4\nAjouter les épinards frais dans la poêle avec la viande de boeuf et les faire cuire jusqu'à ce qu'ils soient ramollis.", "ÉTAPE 5\nAjouter la sauce tomate dans la poêle avec la viande de boeuf et les épinards, et laisser mijoter pendant quelques minutes pour développer les saveurs.", "ÉTAPE 6\nPendant ce temps, faire cuire les feuilles de lasagne dans une grande casserole d'eau bouillante salée selon les instructions sur l'emballage.", "ÉTAPE 7\nÉgoutter les feuilles de lasagne cuites et les disposer dans un plat à gratin.", "ÉTAPE 8\nÉtaler une couche de mélange de viande, épinards et sauce tomate sur les feuilles de lasagne dans le plat à gratin.", "ÉTAPE 9\nRépéter les couches avec le reste des feuilles de lasagne et du mélange de viande, épinards et sauce tomate.", "ÉTAPE 10\nTerminer avec une couche de fromage ricotta et saupoudrer de fromage mozzarella râpé sur le dessus.", "ÉTAPE 11\nCuire au four préchauffé pendant 30-35 minutes, ou jusqu'à ce que le fromage soit doré et bouillonne."]
    temps_de_preparation = 30
    temps_de_cuisson = 40
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

lasagnes_boeuf_epinards_fromage
def salade_pates_grecque_poulet_grille(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "pâtes courtes (ex: penne)", "concombre", "tomates cerises", "olives noires", "oignon rouge", "fromage feta"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet", 200*multiplicateur_arrondi, "g de pâtes courtes (ex: penne)", 1*multiplicateur_arrondi, "concombre coupé en dés", 150*multiplicateur_arrondi, "g de tomates cerises coupées en deux", 50*multiplicateur_arrondi, "g d'olives noires dénoyautées", 1*multiplicateur_arrondi, "oignon rouge finement tranché", 100*multiplicateur_arrondi, "g de fromage feta émietté"]
    preparation = ["ÉTAPE 1\nDans une grande casserole d'eau bouillante salée, faire cuire les pâtes selon les instructions sur l'emballage.", "ÉTAPE 2\nPendant ce temps, faire chauffer l'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 3\nAssaisonner les filets de poulet avec du sel et du poivre, puis les faire cuire dans la poêle chaude pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à votre goût.", "ÉTAPE 4\nRetirer les filets de poulet de la poêle et les laisser reposer pendant quelques minutes avant de les trancher en lanières.", "ÉTAPE 5\nDans un grand bol, mélanger les pâtes cuites, le concombre coupé en dés, les tomates cerises coupées en deux, les olives noires dénoyautées, l'oignon rouge finement tranché et le fromage feta émietté.", "ÉTAPE 6\nAjouter les lanières de poulet grillé dans le bol avec les autres ingrédients.", "ÉTAPE 7\nAssaisonner la salade avec du sel, du poivre et de l'huile d'olive, selon votre goût, et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 8\nServir la salade de pâtes grecque avec poulet grillé garnie de persil frais haché, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_pates_grecque_poulet_grille
def chili_con_carne_riz_brun(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande de boeuf hachée", "oignon", "poivron rouge", "haricots rouges en conserve", "tomates en conserve", "poudre de chili", "cumin", "riz brun"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 300*multiplicateur_arrondi, "g de viande de boeuf hachée", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 400*multiplicateur_arrondi, "g de haricots rouges en conserve, rincés et égouttés", 400*multiplicateur_arrondi, "g de tomates en conserve concassées", 2*multiplicateur_arrondi, "cuillères à café de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin en poudre", 200*multiplicateur_arrondi, "g de riz brun cuit"]
    preparation = ["ÉTAPE 1\nFaire chauffer l'huile d'olive dans une grande casserole à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché dans la casserole et le faire sauter jusqu'à ce qu'il soit tendre et translucide.", "ÉTAPE 3\nAjouter la viande de boeuf hachée dans la casserole avec l'oignon et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à votre goût.", "ÉTAPE 4\nAjouter le poivron rouge coupé en dés dans la casserole avec la viande de boeuf et l'oignon, et le faire cuire jusqu'à ce qu'il soit tendre.", "ÉTAPE 5\nAjouter les haricots rouges rincés et égouttés, les tomates en conserve concassées, la poudre de chili et le cumin en poudre dans la casserole avec les autres ingrédients.", "ÉTAPE 6\nLaisser mijoter le chili à feu doux pendant 20-30 minutes, en remuant de temps en temps, pour développer les saveurs.", "ÉTAPE 7\nPendant ce temps, faire cuire le riz brun selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 8\nÉgoutter le riz cuit et le servir chaud avec le chili con carne garni de coriandre fraîche hachée, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_con_carne_riz_brun
def poulet_curry_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "oignon", "ail", "gingembre frais", "poudre de curry", "lait de coco", "tomates en dés", "riz basmati"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en dés", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe de poudre de curry", 400*multiplicateur_arrondi, "ml de lait de coco", 400*multiplicateur_arrondi, "g de tomates en dés en conserve", 200*multiplicateur_arrondi, "g de riz basmati cuit"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché dans la poêle et le faire sauter jusqu'à ce qu'il soit tendre et translucide.", "ÉTAPE 3\nAjouter les dés de poulet dans la poêle avec l'oignon et les faire cuire jusqu'à ce qu'ils soient dorés.", "ÉTAPE 4\nAjouter l'ail émincé, le gingembre frais râpé et la poudre de curry dans la poêle avec le poulet et l'oignon, et faire cuire pendant 1 à 2 minutes de plus, en remuant constamment.", "ÉTAPE 5\nAjouter le lait de coco et les tomates en dés dans la poêle avec le poulet et les autres ingrédients.", "ÉTAPE 6\nLaisser mijoter le curry à feu doux pendant 15-20 minutes, en remuant de temps en temps, jusqu'à ce que la sauce épaississe et que le poulet soit cuit à votre goût.", "ÉTAPE 7\nPendant ce temps, faire cuire le riz basmati selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 8\nÉgoutter le riz cuit et le servir chaud avec le curry de poulet garni de coriandre fraîche hachée, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_curry_riz_basmati
def brochettes_crevettes_grillees_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "crevettes décortiquées", "poivrons", "oignon", "courgettes", "citron", "herbes fraîches (ex: persil, basilic)"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de crevettes décortiquées", 1*multiplicateur_arrondi, "poivron coupé en morceaux", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", "tranches de citron", "herbes fraîches (ex: persil, basilic) pour garnir"]
    preparation = ["ÉTAPE 1\nSi vous utilisez des brochettes en bois, les faire tremper dans de l'eau pendant au moins 30 minutes avant de les utiliser pour éviter qu'elles ne brûlent sur le gril.", "ÉTAPE 2\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 3\nEnfiler les crevettes décortiquées, les morceaux de poivron, les quartiers d'oignon et les rondelles de courgette sur les brochettes, en alternant les ingrédients.", "ÉTAPE 4\nArroser les brochettes d'huile d'olive et les assaisonner avec du sel et du poivre.", "ÉTAPE 5\nPlacer les brochettes sur le gril préchauffé et les faire cuire pendant 2 à 3 minutes de chaque côté, ou jusqu'à ce que les crevettes soient roses et les légumes soient tendres et légèrement carbonisés.", "ÉTAPE 6\nServir les brochettes de crevettes grillées chaudes avec des tranches de citron et des herbes fraîches hachées pour garnir."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

brochettes_crevettes_grillees_legumes
def salade_quinoa_pois_chiches_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "pois chiches en conserve", "concombre", "tomates cerises", "poivron rouge", "oignon rouge", "persil frais", "menthe fraîche", "vinaigre de cidre", "miel"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 100*multiplicateur_arrondi, "g de quinoa cru", 200*multiplicateur_arrondi, "g de pois chiches en conserve, rincés et égouttés", 1*multiplicateur_arrondi, "concombre coupé en dés", 150*multiplicateur_arrondi, "g de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "oignon rouge finement tranché", "persil frais haché", "menthe fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire fine pour éliminer l'amertume.", "ÉTAPE 2\nDans une casserole, porter 2 fois le volume d'eau salée à ébullition, ajouter le quinoa et laisser mijoter à feu moyen-doux pendant 12 à 15 minutes, ou jusqu'à ce que les germes blancs apparaissent.", "ÉTAPE 3\nPendant ce temps, préparer les légumes en les coupant en dés et en les tranchant finement selon les instructions.", "ÉTAPE 4\nDans un grand bol, mélanger le quinoa cuit, les pois chiches rincés et égouttés, les dés de concombre, les tomates cerises coupées en deux, les dés de poivron rouge, les tranches d'oignon rouge, le persil frais haché et la menthe fraîche hachée.", "ÉTAPE 5\nDans un petit bol, mélanger l'huile d'olive, le vinaigre de cidre et le miel pour faire la vinaigrette.", "ÉTAPE 6\nVerser la vinaigrette sur la salade de quinoa et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 7\nServir la salade de quinoa aux pois chiches et légumes garnie de plus de persil frais haché et de menthe fraîche hachée, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_pois_chiches_legumes
def steak_saumon_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de saumon", "patates douces", "ail en poudre", "paprika", "romarin frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de saumon", 2*multiplicateur_arrondi, "patates douces coupées en dés", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1*multiplicateur_arrondi, "cuillère à café de paprika", "branches de romarin frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nDans un grand bol, mélanger les dés de patates douces avec de l'huile d'olive, de l'ail en poudre, du paprika, du sel et du poivre.", "ÉTAPE 3\nÉtaler les dés de patates douces assaisonnées sur une plaque de cuisson tapissée de papier sulfurisé, en veillant à ce qu'ils soient répartis uniformément.", "ÉTAPE 4\nPlacer les filets de saumon sur la même plaque de cuisson avec les patates douces, en les espaçant légèrement.", "ÉTAPE 5\nAssaisonner les filets de saumon avec du sel, du poivre et du romarin frais haché.", "ÉTAPE 6\nCuire au four préchauffé pendant 15-20 minutes, ou jusqu'à ce que les patates douces soient tendres et dorées et que le saumon soit cuit à votre goût.", "ÉTAPE 7\nServir les steaks de saumon grillés avec les patates douces rôties garnies de romarin frais haché, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_saumon_patates_douces
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "laitue romaine", "tomates cerises", "croûtons", "parmesan", "jus de citron", "ail", "moutarde de Dijon", "anchois en conserve"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 1*multiplicateur_arrondi, "tête de laitue romaine, lavée et déchirée en morceaux", 100*multiplicateur_arrondi, "g de tomates cerises coupées en deux", 50*multiplicateur_arrondi, "g de croûtons", 30*multiplicateur_arrondi, "g de parmesan râpé", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "filets d'anchois en conserve égouttés"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à votre goût.", "ÉTAPE 3\nRetirer les lanières de poulet de la poêle et les laisser reposer pendant quelques minutes avant de les trancher en morceaux.", "ÉTAPE 4\nDans un grand bol, mélanger la laitue romaine déchirée en morceaux avec les tomates cerises coupées en deux, les croûtons et le parmesan râpé.", "ÉTAPE 5\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron frais, l'ail émincé, la moutarde de Dijon et les filets d'anchois égouttés écrasés à la fourchette.", "ÉTAPE 6\nVerser la vinaigrette sur la salade de laitue romaine et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 7\nAjouter les morceaux de poulet cuits sur le dessus de la salade et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def poulet_farcis_fromage_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "épinards frais", "fromage à la crème", "fromage râpé (ex: mozzarella)", "ail"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 100*multiplicateur_arrondi, "g d'épinards frais hachés", 50*multiplicateur_arrondi, "g de fromage à la crème", 50*multiplicateur_arrondi, "g de fromage râpé (ex: mozzarella)", 2*multiplicateur_arrondi, "gousses d'ail émincées"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 3\nAjouter les épinards frais hachés dans la poêle chaude et les faire sauter pendant 2 à 3 minutes, ou jusqu'à ce qu'ils soient flétris.", "ÉTAPE 4\nRetirer les épinards de la poêle et les égoutter pour enlever tout excès de liquide.", "ÉTAPE 5\nDans un bol, mélanger les épinards sautés avec le fromage à la crème, le fromage râpé et l'ail émincé pour faire la farce.", "ÉTAPE 6\nInciser les poitrines de poulet sur le côté pour former une poche sans les couper complètement.", "ÉTAPE 7\nFarcir chaque poitrine de poulet avec le mélange d'épinards et de fromage.", "ÉTAPE 8\nPlacer les poitrines de poulet farcies sur une plaque de cuisson tapissée de papier sulfurisé et les assaisonner avec du sel et du poivre.", "ÉTAPE 9\nCuire au four préchauffé pendant 25-30 minutes, ou jusqu'à ce que le poulet soit cuit à travers et que le fromage soit fondu et doré.", "ÉTAPE 10\nServir les poitrines de poulet farcies chaudes avec des légumes cuits à la vapeur ou une salade verte."]
    temps_de_preparation = 20
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_farcis_fromage_epinards
def penne_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande de boeuf hachée", "oignon", "ail", "carottes", "côtes de céleri", "tomates en dés", "concentré de tomate", "bouillon de boeuf", "herbes de Provence", "penne"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 300*multiplicateur_arrondi, "g de viande de boeuf hachée", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "carotte finement hachée", 1*multiplicateur_arrondi, "côte de céleri finement hachée", 400*multiplicateur_arrondi, "g de tomates en dés en conserve", 2*multiplicateur_arrondi, "cuillères à soupe de concentré de tomate", 200*multiplicateur_arrondi, "ml de bouillon de boeuf", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence", 200*multiplicateur_arrondi, "g de penne cuites"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché dans la poêle et le faire sauter jusqu'à ce qu'il soit tendre et translucide.", "ÉTAPE 3\nAjouter l'ail émincé dans la poêle avec l'oignon et le faire cuire pendant 1 à 2 minutes de plus, en remuant constamment.", "ÉTAPE 4\nAjouter la viande de boeuf hachée dans la poêle avec l'oignon et l'ail et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à votre goût.", "ÉTAPE 5\nAjouter la carotte finement hachée et la côte de céleri finement hachée dans la poêle avec la viande de boeuf et les autres ingrédients.", "ÉTAPE 6\nAjouter les tomates en dés en conserve, le concentré de tomate, le bouillon de boeuf et les herbes de Provence dans la poêle avec les autres ingrédients.", "ÉTAPE 7\nLaisser mijoter la sauce à feu doux pendant 20-30 minutes, en remuant de temps en temps, pour développer les saveurs.", "ÉTAPE 8\nPendant ce temps, faire cuire les penne selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 9\nÉgoutter les penne cuites et les servir chaudes avec la sauce bolognaise garnie de parmesan râpé, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

penne_bolognaise
def sandwich_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "pain ciabatta", "avocat", "tomate", "feuilles de laitue", "mayonnaise", "moutarde de Dijon"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet désossés et sans peau", 1*multiplicateur_arrondi, "pain ciabatta coupé en deux", 1*multiplicateur_arrondi, "avocat tranché", 1*multiplicateur_arrondi, "tomate tranchée", "feuilles de laitue", "mayonnaise", "moutarde de Dijon"]
    preparation = ["ÉTAPE 1\nPréchauffer une poêle grill à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les filets de poulet avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 3\nGriller les filets de poulet assaisonnés sur la poêle grill préchauffée pendant 6 à 8 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits et dorés.", "ÉTAPE 4\nRetirer les filets de poulet grillés de la poêle et les laisser reposer pendant quelques minutes avant de les trancher en morceaux.", "ÉTAPE 5\nPendant ce temps, préparer les ingrédients pour le sandwich en tranchant le pain ciabatta en deux et en tranchant l'avocat et la tomate.", "ÉTAPE 6\nTartiner une moitié du pain ciabatta avec de la mayonnaise et l'autre moitié avec de la moutarde de Dijon.", "ÉTAPE 7\nDisposer les feuilles de laitue sur la moitié du pain ciabatta tartinée de mayonnaise.", "ÉTAPE 8\nDisposer les tranches de tomate sur les feuilles de laitue, puis les tranches d'avocat sur les tomates.", "ÉTAPE 9\nDisposer les tranches de poulet grillé sur les tranches d'avocat, puis couvrir avec l'autre moitié du pain ciabatta tartinée de moutarde de Dijon.", "ÉTAPE 10\nCouper le sandwich en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_avocat
def pizza_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâte à pizza", "sauce tomate", "mozzarella", "courgettes", "aubergines", "poivrons", "oignon rouge", "tomates cerises", "basilic frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "pâte à pizza", 4*multiplicateur_arrondi, "cuillères à soupe de sauce tomate", 150*multiplicateur_arrondi, "g de mozzarella râpée", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 100*multiplicateur_arrondi, "g de tomates cerises coupées en deux", "feuilles de basilic frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à la température recommandée sur l'emballage de la pâte à pizza.", "ÉTAPE 2\nSur une surface légèrement farinée, étaler la pâte à pizza en forme de cercle.", "ÉTAPE 3\nTransférer la pâte à pizza sur une plaque de cuisson tapissée de papier sulfurisé.", "ÉTAPE 4\nÉtaler la sauce tomate sur la pâte à pizza en laissant une bordure libre autour des bords.", "ÉTAPE 5\nRépartir uniformément la mozzarella râpée sur la sauce tomate.", "ÉTAPE 6\nDisposer les rondelles de courgette, les rondelles d'aubergine, les lanières de poivron, les rondelles d'oignon rouge et les moitiés de tomates cerises sur la pizza.", "ÉTAPE 7\nArroser les légumes avec de l'huile d'olive, assaisonner avec du sel et du poivre, puis garnir de feuilles de basilic frais.", "ÉTAPE 8\nCuire la pizza au four préchauffé pendant 15-20 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante et que le fromage soit fondu et doré.", "ÉTAPE 9\nRetirer la pizza du four, la laisser refroidir légèrement, puis la couper en parts et servir chaud."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_legumes_grilles
def poulet_roti_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet entier", "pommes de terre", "carottes", "oignon", "ail", "thym frais", "romarin frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "poulet entier (environ 1,5 kg)", 500*multiplicateur_arrondi, "g de pommes de terre coupées en quartiers", 300*multiplicateur_arrondi, "g de carottes coupées en morceaux", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 4*multiplicateur_arrondi, "gousses d'ail entières", "branches de thym frais", "branches de romarin frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nAssaisonner le poulet entier avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 3\nDisposer le poulet dans un plat allant au four et le garnir avec les quartiers de pommes de terre, les morceaux de carottes, les quartiers d'oignon, les gousses d'ail entières, le thym frais et le romarin frais.", "ÉTAPE 4\nEnfourner le plat au four préchauffé et laisser cuire pendant 1 heure à 1 heure 15 minutes, ou jusqu'à ce que le poulet soit doré et bien cuit et que les légumes soient tendres et dorés.", "ÉTAPE 5\nRetirer le poulet rôti du four et le laisser reposer pendant quelques minutes avant de le découper en morceaux.", "ÉTAPE 6\nServir le poulet rôti chaud avec les légumes rôtis garnis de thym frais et de romarin frais, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 75
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_legumes
def chili_con_carne(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "viande de boeuf hachée", "oignon", "poivron rouge", "poivron vert", "ail", "piments jalapeños", "haricots rouges en conserve", "tomates en dés", "concentré de tomate", "bouillon de boeuf", "poudre de chili", "cumin en poudre", "paprika", "origan séché", "coriandre fraîche"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 500*multiplicateur_arrondi, "g de viande de boeuf hachée", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "poivron vert coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "piments jalapeños hachés (facultatif)", 400*multiplicateur_arrondi, "g de haricots rouges en conserve, rincés et égouttés", 400*multiplicateur_arrondi, "g de tomates en dés en conserve", 2*multiplicateur_arrondi, "cuillères à soupe de concentré de tomate", 200*multiplicateur_arrondi, "ml de bouillon de boeuf", 2*multiplicateur_arrondi, "cuillères à café de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin en poudre", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "coriandre fraîche pour garnir"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché dans la casserole chaude et le faire sauter jusqu'à ce qu'il soit tendre et translucide.", "ÉTAPE 3\nAjouter les dés de poivron rouge et de poivron vert dans la casserole avec l'oignon et les faire cuire pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres.", "ÉTAPE 4\nAjouter l'ail émincé dans la casserole avec les autres légumes et le faire cuire pendant 1 à 2 minutes de plus, en remuant constamment.", "ÉTAPE 5\nAjouter la viande de boeuf hachée dans la casserole avec les légumes et l'ail et la faire cuire jusqu'à ce qu'elle soit dorée et cuite à votre goût.", "ÉTAPE 6\nAjouter les piments jalapeños hachés, les haricots rouges rincés et égouttés, les tomates en dés en conserve, le concentré de tomate, le bouillon de boeuf, la poudre de chili, le cumin en poudre, le paprika et l'origan séché dans la casserole avec la viande et les légumes.", "ÉTAPE 7\nPorter le mélange à ébullition, puis réduire le feu et laisser mijoter à découvert pendant 20-30 minutes, en remuant de temps en temps, pour développer les saveurs.", "ÉTAPE 8\nServir le chili con carne chaud garni de coriandre fraîche hachée, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_con_carne
def pates_carbonara(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "spaghetti", "huile d'olive", "lardons fumés", "ail", "oeufs", "parmesan râpé", "persil frais"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "g de spaghetti", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 100*multiplicateur_arrondi, "g de lardons fumés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "oeufs", 50*multiplicateur_arrondi, "g de parmesan râpé", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nFaire cuire les spaghetti dans une grande casserole d'eau bouillante salée selon les instructions sur l'emballage jusqu'à ce qu'ils soient al dente.", "ÉTAPE 2\nPendant ce temps, dans une grande poêle, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 3\nAjouter les lardons fumés dans la poêle chaude et les faire cuire jusqu'à ce qu'ils soient dorés et croustillants.", "ÉTAPE 4\nAjouter l'ail émincé dans la poêle avec les lardons et les faire cuire pendant 1 à 2 minutes de plus, en remuant constamment.", "ÉTAPE 5\nDans un bol, battre les oeufs avec le parmesan râpé et du sel et du poivre.", "ÉTAPE 6\nÉgoutter les spaghetti cuits et les ajouter à la poêle avec les lardons et l'ail, en réservant une tasse d'eau de cuisson.", "ÉTAPE 7\nRetirer la poêle du feu et verser le mélange d'oeufs et de parmesan sur les spaghetti chauds dans la poêle, en remuant rapidement pour enrober les pâtes dans la sauce crémeuse.", "ÉTAPE 8\nAjouter un peu d'eau de cuisson réservée si nécessaire pour obtenir une consistance plus crémeuse.", "ÉTAPE 9\nServir les spaghetti à la carbonara chauds garnis de persil frais haché, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_carbonara
def steak_boeuf_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steaks de boeuf", "patates douces", "paprika", "ail en poudre", "thym frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "steaks de boeuf", 2*multiplicateur_arrondi, "patates douces coupées en dés", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", "branches de thym frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).", "ÉTAPE 2\nDans un grand bol, mélanger les dés de patates douces avec de l'huile d'olive, du sel, du poivre, du paprika et de l'ail en poudre.", "ÉTAPE 3\nDisposer les dés de patates douces assaisonnés sur une plaque de cuisson tapissée de papier sulfurisé en une seule couche.", "ÉTAPE 4\nEnfourner les patates douces dans le four préchauffé et les rôtir pendant 25-30 minutes, ou jusqu'à ce qu'elles soient tendres et dorées, en remuant à mi-cuisson.", "ÉTAPE 5\nPendant ce temps, assaisonner les steaks de boeuf avec du sel, du poivre et du thym frais haché.", "ÉTAPE 6\nFaire chauffer une poêle grill à feu moyen-élevé.", "ÉTAPE 7\nCuire les steaks de boeuf assaisonnés sur la poêle grill préchauffée pendant 3 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à votre goût.", "ÉTAPE 8\nRetirer les steaks de boeuf grillés de la poêle et les laisser reposer pendant quelques minutes avant de les servir avec les patates douces rôties."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_patates_douces
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "courgettes", "poivrons", "oignon rouge", "tomates cerises", "feuilles de basilic frais", "vinaigre de cidre", "miel", "moutarde de Dijon"]
    ingredient = ["sel", "poivre", 100*multiplicateur_arrondi, "g de quinoa sec", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 100*multiplicateur_arrondi, "g de tomates cerises coupées en deux", "feuilles de basilic frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide dans une passoire fine.", "ÉTAPE 2\nDans une casserole moyenne, porter 2 volumes d'eau salée à ébullition.", "ÉTAPE 3\nAjouter le quinoa rincé dans l'eau bouillante et laisser mijoter à feu moyen-doux pendant 15-20 minutes, ou jusqu'à ce que les graines de quinoa soient tendres et que l'eau soit absorbée.", "ÉTAPE 4\nPendant ce temps, préparer les légumes en les coupant en rondelles (courgette), en lanières (poivron) et en rondelles (oignon rouge), et en coupant les tomates cerises en deux.", "ÉTAPE 5\nDans un bol, mélanger les légumes préparés avec de l'huile d'olive, du sel et du poivre.", "ÉTAPE 6\nFaire griller les légumes assaisonnés sur une poêle grill ou sur un gril à feu moyen-élevé pendant 5 à 7 minutes de chaque côté, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.", "ÉTAPE 7\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre de cidre, le miel et la moutarde de Dijon.", "ÉTAPE 8\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés et la vinaigrette préparée.", "ÉTAPE 9\nGarnir la salade de quinoa aux légumes grillés de feuilles de basilic frais hachées avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def wraps_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "avocats", "tomates", "feuilles de laitue", "tortillas de blé"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 2*multiplicateur_arrondi, "avocats mûrs, épluchés, dénoyautés et tranchés", 2*multiplicateur_arrondi, "tomates coupées en tranches", 4*multiplicateur_arrondi, "feuilles de laitue", 4*multiplicateur_arrondi, "tortillas de blé"]
    preparation = ["ÉTAPE 1\nChauffer légèrement les tortillas de blé dans une poêle ou au micro-ondes pour les assouplir.", "ÉTAPE 2\nDans un grand bol, mélanger les lanières de poulet avec de l'huile d'olive, du sel et du poivre.", "ÉTAPE 3\nDisposer les tranches d'avocat, de tomate et les feuilles de laitue sur les tortillas de blé.", "ÉTAPE 4\nAjouter les lanières de poulet assaisonnées sur les légumes.", "ÉTAPE 5\nEnrouler les tortillas garnies fermement pour former des wraps.", "ÉTAPE 6\nCouper les wraps en deux ou en tranches avant de servir, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_poulet_avocat
def poulet_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "poivron rouge", "poivron jaune", "brocoli", "oignon", "ail", "gingembre frais", "sauce soja", "miel", "vinaigre de riz", "graines de sésame", "coriandre fraîche"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune coupé en lanières", 1*multiplicateur_arrondi, "tête de brocoli coupée en fleurettes", 1*multiplicateur_arrondi, "oignon émincé", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de riz", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées", "coriandre fraîche pour garnir"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une grande poêle à feu moyen-élevé.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 3\nRetirer le poulet cuit de la poêle et le mettre de côté.", "ÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire, puis ajouter les lanières de poivron rouge et jaune, les fleurettes de brocoli, l'oignon émincé, l'ail émincé et le gingembre frais râpé.", "ÉTAPE 5\nFaire sauter les légumes pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 6\nPendant ce temps, préparer la sauce en mélangeant la sauce soja, le miel et le vinaigre de riz dans un petit bol.", "ÉTAPE 7\nRemettre les lanières de poulet cuites dans la poêle avec les légumes sautés et verser la sauce préparée sur le tout.", "ÉTAPE 8\nRemuer pour enrober le poulet et les légumes dans la sauce et cuire encore quelques minutes jusqu'à ce que le tout soit bien chaud.", "ÉTAPE 9\nServir le poulet aux légumes sautés chaud garni de graines de sésame grillées et de coriandre fraîche hachée, si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_legumes_sautes
def poisson_riz_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poisson blanc", "riz basmati", "brocoli", "carottes", "citron", "ail", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poisson blanc", 150*multiplicateur_arrondi, "g de riz basmati", 1*multiplicateur_arrondi, "tête de brocoli coupée en fleurettes", 2*multiplicateur_arrondi, "carottes coupées en rondelles", "citron en tranches pour garnir", 2*multiplicateur_arrondi, "gousses d'ail émincées", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz basmati selon les instructions sur l'emballage.", "ÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé.", "ÉTAPE 3\nAssaisonner les filets de poisson blanc avec du sel, du poivre, de l'ail émincé et un filet d'huile d'olive.", "ÉTAPE 4\nDisposer les filets de poisson assaisonnés sur une plaque de cuisson tapissée de papier sulfurisé et garnir de tranches de citron.", "ÉTAPE 5\nFaire griller les filets de poisson sous le gril préchauffé pendant 6 à 8 minutes, ou jusqu'à ce qu'ils soient cuits à travers et que la chair soit opaque.", "ÉTAPE 6\nPendant ce temps, faire chauffer un peu d'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 7\nAjouter les fleurettes de brocoli et les rondelles de carottes dans la poêle chaude et les faire sauter pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 8\nServir les filets de poisson grillés chauds avec le riz basmati cuit et les légumes sautés garnis de persil frais haché, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_riz_legumes
def sandwich_boeuf_hache_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steak haché", "fromage cheddar", "pain à sandwich", "feuilles de laitue", "tomate", "oignon rouge", "cornichons"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "steak haché", 1*multiplicateur_arrondi, "tranches de fromage cheddar", 2*multiplicateur_arrondi, "tranches de pain à sandwich", 2*multiplicateur_arrondi, "feuilles de laitue lavées et séchées", 2*multiplicateur_arrondi, "tranches de tomate", 2*multiplicateur_arrondi, "tranches d'oignon rouge", 4*multiplicateur_arrondi, "cornichons en rondelles"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une poêle à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner le steak haché avec du sel et du poivre.", "ÉTAPE 3\nCuire le steak haché assaisonné dans la poêle chaude pendant 3 à 5 minutes de chaque côté, ou jusqu'à ce qu'il soit cuit à votre goût.", "ÉTAPE 4\nPendant ce temps, faire griller légèrement les tranches de pain à sandwich.", "ÉTAPE 5\nDisposer une tranche de fromage cheddar sur chaque tranche de pain grillé pour qu'elle fonde légèrement.", "ÉTAPE 6\nGarnir une tranche de pain grillé avec le steak haché cuit, les feuilles de laitue, les tranches de tomate, les tranches d'oignon rouge et les rondelles de cornichon.", "ÉTAPE 7\nRefermer le sandwich avec l'autre tranche de pain grillé.", "ÉTAPE 8\nCouper le sandwich en deux ou en quartiers avant de servir, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_boeuf_hache_fromage
def saumon_quinoa_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de saumon", "quinoa", "asperges", "poivron rouge", "courgette", "ail", "jus de citron", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de saumon", 100*multiplicateur_arrondi, "g de quinoa sec", 1*multiplicateur_arrondi, "bouquets d'asperges", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage.", "ÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé.", "ÉTAPE 3\nAssaisonner les filets de saumon avec du sel, du poivre et un filet d'huile d'olive.", "ÉTAPE 4\nDisposer les filets de saumon assaisonnés sur une plaque de cuisson tapissée de papier sulfurisé.", "ÉTAPE 5\nFaire griller les filets de saumon sous le gril préchauffé pendant 6 à 8 minutes, ou jusqu'à ce qu'ils soient cuits à travers et que la chair soit opaque.", "ÉTAPE 6\nPendant ce temps, faire chauffer un peu d'huile d'olive dans une poêle à feu moyen.", "ÉTAPE 7\nAjouter les asperges, les lanières de poivron rouge, les rondelles de courgette et l'ail émincé dans la poêle chaude et les faire sauter pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 8\nServir les filets de saumon grillés chauds avec le quinoa cuit et les légumes sautés garnis de persil frais haché."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_quinoa_legumes
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "laitue romaine", "croûtons", "parmesan râpé", "sauce César"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 4*multiplicateur_arrondi, "tasses de laitue romaine hachée", 1*multiplicateur_arrondi, "tasse de croûtons", 2*multiplicateur_arrondi, "cuillères à soupe de parmesan râpé", 4*multiplicateur_arrondi, "cuillères à soupe de sauce César"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen-élevé.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 3\nRetirer le poulet cuit de la poêle et le mettre de côté.", "ÉTAPE 4\nDans un grand bol, mélanger la laitue romaine hachée avec les croûtons, le parmesan râpé et la sauce César.", "ÉTAPE 5\nAjouter les lanières de poulet cuites dans le bol avec la salade César.", "ÉTAPE 6\nRemuer pour enrober le poulet et la salade dans la sauce César.", "ÉTAPE 7\nServir la salade de poulet César garnie de plus de parmesan râpé, si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def risotto_champignons(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "champignons", "oignon", "ail", "riz arborio", "vin blanc sec", "bouillon de légumes", "parmesan râpé", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de champignons tranchés", 1*multiplicateur_arrondi, "oignon finement haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "tasse de riz arborio", 1/4*multiplicateur_arrondi, "tasse de vin blanc sec", 2*multiplicateur_arrondi, "tasses de bouillon de légumes chaud", 1/2*multiplicateur_arrondi, "tasse de parmesan râpé", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une grande poêle à feu moyen.", "ÉTAPE 2\nAjouter les champignons tranchés dans la poêle chaude et les faire sauter pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient dorés.", "ÉTAPE 3\nAjouter l'oignon finement haché et les gousses d'ail émincées dans la poêle avec les champignons et les faire revenir pendant quelques minutes jusqu'à ce qu'ils soient tendres.", "ÉTAPE 4\nAjouter le riz arborio dans la poêle avec les légumes cuits et remuer pour enrober le riz d'huile d'olive.", "ÉTAPE 5\nVerser le vin blanc sec dans la poêle et remuer jusqu'à ce qu'il soit absorbé par le riz.", "ÉTAPE 6\nAjouter une louche de bouillon de légumes chaud dans la poêle avec le riz et remuer jusqu'à ce qu'il soit absorbé.", "ÉTAPE 7\nContinuer à ajouter le bouillon de légumes louche par louche, en remuant constamment, jusqu'à ce que le riz soit cuit al dente et crémeux.", "ÉTAPE 8\nRetirer la poêle du feu et incorporer le parmesan râpé dans le risotto.", "ÉTAPE 9\nServir le risotto aux champignons chaud garni de persil frais haché."]
    temps_de_preparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

risotto_champignons
def steak_boeuf_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steak de bœuf", "patates douces", "ail en poudre", "paprika", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "steak de bœuf épais", 2*multiplicateur_arrondi, "patates douces coupées en quartiers", 1/2*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1/2*multiplicateur_arrondi, "cuillère à café de paprika", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nDans un bol, mélanger l'huile d'olive avec du sel, du poivre, de l'ail en poudre et du paprika.", "ÉTAPE 3\nBadigeonner les steaks de bœuf des deux côtés avec ce mélange d'assaisonnement.", "ÉTAPE 4\nPlacer les quartiers de patates douces sur une plaque de cuisson tapissée de papier sulfurisé et les arroser de l'huile d'olive assaisonnée restante.", "ÉTAPE 5\nPlacer les steaks de bœuf assaisonnés sur la même plaque de cuisson que les patates douces.", "ÉTAPE 6\nCuire au four préchauffé pendant environ 25-30 minutes, ou jusqu'à ce que les steaks soient cuits à votre goût et que les patates douces soient tendres et dorées.", "ÉTAPE 7\nGarnir de persil frais haché avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_patates_douces
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "quinoa", "courgette", "aubergine", "poivron rouge", "tomates cerises", "oignon rouge", "persil frais", "vinaigrette balsamique"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 100*multiplicateur_arrondi, "g de quinoa sec", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "aubergine coupée en rondelles épaisses", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "tasses de tomates cerises", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", "persil frais pour garnir", "vinaigrette balsamique pour servir"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage.", "ÉTAPE 2\nPendant ce temps, préchauffer un gril à feu moyen-élevé.", "ÉTAPE 3\nAssaisonner les rondelles de courgette, d'aubergine et de poivron rouge avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 4\nGriller les légumes assaisonnés sur le gril préchauffé pendant 5 à 7 minutes de chaque côté, ou jusqu'à ce qu'ils soient tendres et légèrement carbonisés.", "ÉTAPE 5\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés, les tomates cerises et les quartiers d'oignon rouge.", "ÉTAPE 6\nGarnir de persil frais haché avant de servir et accompagner de vinaigrette balsamique."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def pates_sauce_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "bœuf haché", "oignon", "carotte", "céleri", "ail", "tomates en conserve", "bouillon de bœuf", "pâtes", "basilic frais", "parmesan râpé"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "g de bœuf haché", 1*multiplicateur_arrondi, "oignon finement haché", 1*multiplicateur_arrondi, "carotte finement hachée", 1*multiplicateur_arrondi, "branche de céleri finement hachée", 2*multiplicateur_arrondi, "gousses d'ail émincées", 400*multiplicateur_arrondi, "g de tomates en conserve concassées", 1*multiplicateur_arrondi, "tasse de bouillon de bœuf", 200*multiplicateur_arrondi, "g de pâtes cuites", "feuilles de basilic frais pour garnir", "parmesan râpé pour servir"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon finement haché, la carotte finement hachée, la branche de céleri finement hachée et les gousses d'ail émincées dans la poêle chaude et les faire revenir pendant 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres.", "ÉTAPE 3\nAjouter le bœuf haché dans la poêle avec les légumes cuits et faire cuire jusqu'à ce qu'il soit doré et cuit à travers.", "ÉTAPE 4\nAjouter les tomates en conserve concassées et le bouillon de bœuf dans la poêle avec la viande et les légumes cuits.", "ÉTAPE 5\nLaisser mijoter la sauce bolognaise pendant 15 à 20 minutes, en remuant de temps en temps, jusqu'à ce qu'elle épaississe légèrement.", "ÉTAPE 6\nPendant ce temps, faire cuire les pâtes selon les instructions sur l'emballage jusqu'à ce qu'elles soient al dente.", "ÉTAPE 7\nServir les pâtes cuites avec la sauce bolognaise garnie de feuilles de basilic frais et de parmesan râpé."]
    temps_de_preparation = 10
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_sauce_bolognaise
def poulet_roti_pommes_de_terre_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poulet entier", "pommes de terre", "carottes", "oignon", "ail", "thym frais", "romarin frais", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "poulet entier", 4*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 4*multiplicateur_arrondi, "carottes coupées en morceaux", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 4*multiplicateur_arrondi, "gousses d'ail écrasées", "quelques brins de thym frais", "quelques brins de romarin frais", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nDans un petit bol, mélanger l'huile d'olive avec du sel, du poivre, du thym frais haché et du romarin frais haché.", "ÉTAPE 3\nBadigeonner le poulet entier avec ce mélange d'assaisonnement, en s'assurant de bien enrober la peau.", "ÉTAPE 4\nPlacer le poulet assaisonné sur une grille dans un plat de cuisson et disposer les quartiers de pommes de terre autour du poulet.", "ÉTAPE 5\nDisposer également les carottes coupées en morceaux et les quartiers d'oignon dans le plat de cuisson avec le poulet et les pommes de terre.", "ÉTAPE 6\nFrotter les gousses d'ail écrasées sur la peau du poulet et les placer à l'intérieur de la cavité du poulet avec quelques brins de thym frais et de romarin frais.", "ÉTAPE 7\nCuire au four préchauffé pendant environ 1 heure à 1 heure 30 minutes, ou jusqu'à ce que le poulet soit bien cuit et que les pommes de terre et les légumes soient dorés et tendres.", "ÉTAPE 8\nGarnir de persil frais haché avant de servir."]
    temps_de_preparation = 20
    temps_de_cuisson = 90
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_pommes_de_terre_legumes
def burrito_poulet_haricots(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "haricots noirs en conserve", "oignon", "poivron rouge", "maïs en conserve", "tortillas", "fromage râpé", "coriandre fraîche", "salsa", "crème sure"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 1/2*multiplicateur_arrondi, "tasse de haricots noirs en conserve égouttés et rincés", 1/2*multiplicateur_arrondi, "oignon finement haché", 1/2*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1/4*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 2*multiplicateur_arrondi, "tortillas de blé", 1/2*multiplicateur_arrondi, "tasse de fromage râpé", "coriandre fraîche hachée pour garnir", "salsa pour servir", "crème sure pour servir"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen-élevé.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 3\nAjouter l'oignon finement haché et les lanières de poivron rouge dans la poêle avec le poulet cuit et les faire revenir pendant quelques minutes jusqu'à ce qu'ils soient tendres.", "ÉTAPE 4\nAjouter les haricots noirs égouttés et rincés et le maïs égoutté dans la poêle avec le mélange de poulet et de légumes.", "ÉTAPE 5\nLaisser mijoter le mélange pendant quelques minutes jusqu'à ce qu'il soit chaud et que les saveurs se mélangent.", "ÉTAPE 6\nRéchauffer les tortillas de blé selon les instructions sur l'emballage.", "ÉTAPE 7\nRépartir le mélange de poulet et de légumes chaud sur les tortillas réchauffées et saupoudrer de fromage râpé.", "ÉTAPE 8\nGarnir de coriandre fraîche hachée et servir avec de la salsa et de la crème sure."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

burrito_poulet_haricots
def salade_thon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "thon en conserve", "avocat", "concombre", "tomates cerises", "oignon rouge", "vinaigre balsamique", "miel", "moutarde de Dijon", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "boîte de thon en conserve égouttée", 1*multiplicateur_arrondi, "avocat coupé en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tasses de tomates cerises coupées en deux", 1/2*multiplicateur_arrondi, "oignon rouge finement tranché", 1/2*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1/2*multiplicateur_arrondi, "cuillère à café de miel", 1/2*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon en conserve égoutté avec les dés d'avocat, les dés de concombre, les tomates cerises coupées en deux et les tranches d'oignon rouge finement tranchées.", "ÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive avec du sel, du poivre, du vinaigre balsamique, du miel et de la moutarde de Dijon.", "ÉTAPE 3\nArroser la salade de thon et avocat de la vinaigrette préparée et bien mélanger pour enrober tous les ingrédients.", "ÉTAPE 4\nGarnir de persil frais haché avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_avocat

def sandwich_saumon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "tranches de pain de blé entier", "filets de saumon fumé", "avocat", "concombre", "oignon rouge", "fromage frais", "aneth frais"]
    ingredient = ["sel", "poivre", 2*multiplicateur_arrondi, "tranches de pain de blé entier", 100*multiplicateur_arrondi, "de filets de saumon fumé", 1/2*multiplicateur_arrondi, "avocat écrasé", 1/4*multiplicateur_arrondi, "concombre coupé en tranches fines", 1/4*multiplicateur_arrondi, "oignon rouge finement tranché", 2*multiplicateur_arrondi, "cuillères à soupe de fromage frais", "aneth frais pour garnir"]
    preparation = ["ÉTAPE 1\nSur une tranche de pain de blé entier, étaler uniformément le fromage frais.", "ÉTAPE 2\nDisposer les tranches de saumon fumé sur le fromage frais étalé.", "ÉTAPE 3\nGarnir le saumon fumé avec de l'avocat écrasé, des tranches de concombre et des tranches d'oignon rouge.", "ÉTAPE 4\nAssaisonner avec du sel et du poivre au goût.", "ÉTAPE 5\nSaupoudrer d'aneth frais haché avant de couvrir avec une deuxième tranche de pain de blé entier pour former un sandwich.", "ÉTAPE 6\nCouper le sandwich en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_saumon_avocat
def steak_boeuf_patates_douces_brocoli(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steaks de bœuf", "patates douces", "brocoli", "ail en poudre", "paprika", "thym séché", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "steaks de bœuf", 2*multiplicateur_arrondi, "patates douces coupées en dés", 2*multiplicateur_arrondi, "tasses de brocoli en fleurettes", 1/2*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1/2*multiplicateur_arrondi, "cuillère à café de paprika", 1/2*multiplicateur_arrondi, "cuillère à café de thym séché", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le grill à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les steaks de bœuf avec du sel, du poivre, de l'ail en poudre, du paprika et du thym séché.", "ÉTAPE 3\nDans un bol, mélanger les dés de patates douces avec de l'huile d'olive, du sel et du poivre.", "ÉTAPE 4\nSur une plaque de cuisson tapissée de papier sulfurisé, disposer les steaks de bœuf assaisonnés et les dés de patates douces en une seule couche.", "ÉTAPE 5\nGriller les steaks de bœuf et les patates douces au grill préchauffé pendant environ 10 minutes de chaque côté, ou jusqu'à ce que le bœuf soit cuit à la cuisson désirée et que les patates douces soient tendres et dorées.", "ÉTAPE 6\nPendant les dernières minutes de cuisson, ajouter les fleurettes de brocoli sur la plaque de cuisson et les faire griller jusqu'à ce qu'elles soient tendres.", "ÉTAPE 7\nGarnir de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_patates_douces_brocoli
def pates_pesto_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "pâtes", "basilic frais", "ail", "parmesan râpé", "pignons de pin", "jus de citron", "sel", "poivre"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 250*multiplicateur_arrondi, "de pâtes de votre choix", 2*multiplicateur_arrondi, "tasses de feuilles de basilic frais", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1/4*multiplicateur_arrondi, "tasse de parmesan râpé", 1/4*multiplicateur_arrondi, "tasse de pignons de pin grillés", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande casserole d'eau bouillante salée, cuire les pâtes selon les instructions sur l'emballage jusqu'à ce qu'elles soient al dente.", "ÉTAPE 2\nPendant ce temps, dans une poêle, chauffer l'huile d'olive à feu moyen-élevé.", "ÉTAPE 3\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 4\nDans un robot culinaire, mélanger les feuilles de basilic frais, l'ail émincé, le parmesan râpé, les pignons de pin grillés, le jus de citron, du sel et du poivre jusqu'à ce que le tout soit bien combiné.", "ÉTAPE 5\nÉgoutter les pâtes cuites et les réserver, en réservant une tasse d'eau de cuisson des pâtes.", "ÉTAPE 6\nDans la même casserole utilisée pour cuire les pâtes, ajouter les pâtes cuites et le pesto préparé.", "ÉTAPE 7\nAjouter un peu d'eau de cuisson des pâtes réservée au besoin pour obtenir une consistance lisse et crémeuse.", "ÉTAPE 8\nServir les pâtes au pesto avec les lanières de poulet grillées sur le dessus.", "ÉTAPE 9\nAssaisonner avec du sel et du poivre au goût."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pates_pesto_poulet
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "laitue romaine", "tomates cerises", "parmesan râpé", "croûtons à l'ail", "sauce césar"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 4*multiplicateur_arrondi, "tasses de laitue romaine hachée", 1*multiplicateur_arrondi, "tasses de tomates cerises coupées en deux", 1/4*multiplicateur_arrondi, "tasse de parmesan râpé", 1/2*multiplicateur_arrondi, "tasse de croûtons à l'ail", 1*multiplicateur_arrondi, "cuillère à soupe de sauce césar"]
    preparation = ["ÉTAPE 1\nDans une poêle, chauffer l'huile d'olive à feu moyen-élevé.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire cuire jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 3\nPendant ce temps, dans un grand bol, mélanger la laitue romaine hachée avec les tomates cerises coupées en deux.", "ÉTAPE 4\nAjouter les lanières de poulet cuites sur le dessus de la salade de laitue et de tomates cerises.", "ÉTAPE 5\nSaupoudrer de parmesan râpé et de croûtons à l'ail sur le dessus de la salade de poulet.", "ÉTAPE 6\nArroser de sauce césar et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 7\nServir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def poulet_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "quinoa", "courgette", "poivron rouge", "oignon rouge", "ail", "thym frais", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet", 1*multiplicateur_arrondi, "tasse de quinoa cru", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1/2*multiplicateur_arrondi, "oignon rouge coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "branches de thym frais", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nAssaisonner les filets de poulet avec du sel, du poivre et de l'huile d'olive.", "ÉTAPE 3\nDans un plat de cuisson, disposer les filets de poulet assaisonnés et les cuire au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce qu'ils soient dorés et cuits à travers.", "ÉTAPE 4\nPendant ce temps, cuire le quinoa selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 5\nDans un autre plat de cuisson, mélanger les dés de courgette, de poivron rouge et d'oignon rouge avec de l'ail émincé, du sel, du poivre et de l'huile d'olive.", "ÉTAPE 6\nDisposer les légumes préparés en une seule couche sur le plat de cuisson et les rôtir au four préchauffé pendant 15 à 20 minutes, ou jusqu'à ce qu'ils soient tendres et dorés.", "ÉTAPE 7\nServir les filets de poulet grillés avec le quinoa cuit et les légumes rôtis.", "ÉTAPE 8\nGarnir de thym frais et de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 40
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_quinoa_legumes_rotis
def wrap_poulet_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "tortillas de blé", "laitue", "tomates", "concombre", "oignon rouge", "mayonnaise", "moutarde", "miel"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 2*multiplicateur_arrondi, "tortillas de blé", 1*multiplicateur_arrondi, "tasse de laitue hachée", 1*multiplicateur_arrondi, "tasse de tomates coupées en dés", 1*multiplicateur_arrondi, "tasse de concombre coupé en dés", 1/4*multiplicateur_arrondi, "tasse d'oignon rouge finement tranché", 1/4*multiplicateur_arrondi, "tasse de mayonnaise", 1/4*multiplicateur_arrondi, "tasse de moutarde", 1/4*multiplicateur_arrondi, "cuillère à café de miel"]
    preparation = ["ÉTAPE 1\nDans une poêle, chauffer l'huile d'olive à feu moyen-élevé.", "ÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude et les faire réchauffer jusqu'à ce qu'elles soient chaudes.", "ÉTAPE 3\nPendant ce temps, réchauffer les tortillas de blé dans une poêle ou au micro-ondes selon les instructions sur l'emballage.", "ÉTAPE 4\nÉtaler une fine couche de mayonnaise sur chaque tortilla de blé réchauffée.", "ÉTAPE 5\nDisposer la laitue hachée, les tomates coupées en dés, le concombre coupé en dés et l'oignon rouge tranché sur le dessus de la mayonnaise étalée.", "ÉTAPE 6\nAjouter les lanières de poulet réchauffées sur les légumes préparés.", "ÉTAPE 7\nDans un petit bol, mélanger la moutarde avec le miel et enrober les lanières de poulet avec ce mélange.", "ÉTAPE 8\nEnrouler les tortillas de blé garnies pour former des wraps et les couper en deux avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_legumes
def pizza_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "pâte à pizza", "sauce tomate", "mozzarella", "courgette", "aubergine", "poivron rouge", "oignon rouge", "champignons", "basilic frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "pâte à pizza", 1/2*multiplicateur_arrondi, "tasse de sauce tomate", 200*multiplicateur_arrondi, "de mozzarella râpée", 1/2*multiplicateur_arrondi, "courgette coupée en tranches fines", 1/2*multiplicateur_arrondi, "aubergine coupée en tranches fines", 1/2*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1/4*multiplicateur_arrondi, "oignon rouge finement tranché", 1/2*multiplicateur_arrondi, "tasse de champignons tranchés", "feuilles de basilic frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à la température la plus élevée possible.", "ÉTAPE 2\nÉtaler la pâte à pizza sur une plaque de cuisson tapissée de papier sulfurisé.", "ÉTAPE 3\nÉtaler une fine couche de sauce tomate sur la pâte à pizza.", "ÉTAPE 4\nDisposer les tranches de courgette, d'aubergine, de poivron rouge, d'oignon rouge et les champignons tranchés sur le dessus de la sauce tomate.", "ÉTAPE 5\nSaupoudrer de mozzarella râpée sur les légumes préparés.", "ÉTAPE 6\nArroser d'un filet d'huile d'olive sur la pizza garnie.", "ÉTAPE 7\nCuire la pizza au four préchauffé pendant 12 à 15 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante et que le fromage soit fondu et doré.", "ÉTAPE 8\nGarnir de feuilles de basilic frais avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_legumes_fromage
def steak_hache_riz_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "steaks hachés", "riz", "brocoli", "carottes", "oignon", "ail", "sauce soja", "gingembre frais", "coriandre fraîche"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "steaks hachés", 1*multiplicateur_arrondi, "tasse de riz cuit", 1/2*multiplicateur_arrondi, "tasse de brocoli coupé en fleurettes", 1/2*multiplicateur_arrondi, "tasse de carottes coupées en rondelles", 1/4*multiplicateur_arrondi, "tasse d'oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1/4*multiplicateur_arrondi, "tasse de sauce soja", 1/2*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", "coriandre fraîche pour garnir"]
    preparation = ["ÉTAPE 1\nFaire chauffer l'huile d'olive dans une grande poêle à feu moyen-élevé.", "ÉTAPE 2\nAjouter les steaks hachés dans la poêle chaude et les faire cuire jusqu'à ce qu'ils soient dorés et cuits à travers.", "ÉTAPE 3\nPendant ce temps, dans une autre poêle, faire sauter les légumes (brocoli, carottes, oignon et ail) dans un peu d'huile d'olive jusqu'à ce qu'ils soient tendres.", "ÉTAPE 4\nDans un petit bol, mélanger la sauce soja avec le gingembre frais râpé.", "ÉTAPE 5\nAjouter le riz cuit dans la poêle avec les légumes sautés et verser le mélange de sauce soja dessus.", "ÉTAPE 6\nRemuer pour bien enrober le riz et les légumes avec la sauce soja aromatisée.", "ÉTAPE 7\nServir les steaks hachés cuits avec le riz et les légumes sautés.", "ÉTAPE 8\nGarnir de coriandre fraîche hachée avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_hache_riz_legumes_sautes
def poulet_four_pommes_terre_haricots_verts(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poulet", "pommes de terre", "haricots verts", "ail en poudre", "paprika", "thym frais", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poulet", 4*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 2*multiplicateur_arrondi, "tasses de haricots verts frais, équeutés", 1/2*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1/2*multiplicateur_arrondi, "cuillère à café de paprika", 2*multiplicateur_arrondi, "branches de thym frais", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nAssaisonner les filets de poulet avec du sel, du poivre, de l'ail en poudre et du paprika.", "ÉTAPE 3\nDans un plat de cuisson, disposer les filets de poulet assaisonnés et les cuire au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce qu'ils soient dorés et cuits à travers.", "ÉTAPE 4\nPendant ce temps, dans un grand bol, mélanger les quartiers de pommes de terre avec de l'huile d'olive, du sel, du poivre et des feuilles de thym frais.", "ÉTAPE 5\nDisposer les quartiers de pommes de terre assaisonnés sur une plaque de cuisson tapissée de papier sulfurisé et les rôtir au four préchauffé pendant 25 à 30 minutes, ou jusqu'à ce qu'ils soient dorés et croustillants.", "ÉTAPE 6\nDans la même plaque de cuisson que les pommes de terre, ajouter les haricots verts équeutés et les faire rôtir au four préchauffé pendant les 10 dernières minutes de cuisson, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.", "ÉTAPE 7\nServir les filets de poulet cuits avec les pommes de terre rôties et les haricots verts.", "ÉTAPE 8\nGarnir de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 45
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_four_pommes_terre_haricots_verts
def poisson_grille_quinoa_asperges(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "filets de poisson blanc", "quinoa", "asperges", "citron", "ail", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "filets de poisson blanc", 1*multiplicateur_arrondi, "tasse de quinoa cru", 1*multiplicateur_arrondi, "tasse d'asperges, bouts coupés", 1/2*multiplicateur_arrondi, "citron en tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé.", "ÉTAPE 3\nAssaisonner les filets de poisson avec du sel, du poivre, du jus de citron, de l'ail émincé et de l'huile d'olive.", "ÉTAPE 4\nGriller les filets de poisson assaisonnés sur le gril préchauffé pendant 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à travers.", "ÉTAPE 5\nDans une poêle, faire sauter les asperges coupées avec de l'huile d'olive, du sel, du poivre et de l'ail émincé jusqu'à ce qu'elles soient tendres et légèrement dorées.", "ÉTAPE 6\nServir les filets de poisson grillés avec le quinoa cuit et les asperges sautées.", "ÉTAPE 7\nGarnir de tranches de citron et de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_grille_quinoa_asperges
def chili_con_carne_riz(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "oignon", "ail", "boeuf haché maigre", "poivron rouge", "poivron vert", "haricots rouges", "tomates concassées en conserve", "cumin moulu", "paprika", "piment de Cayenne", "origan séché", "riz", "coriandre fraîche"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 400*multiplicateur_arrondi, "de boeuf haché maigre", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "poivron vert coupé en dés", 400*multiplicateur_arrondi, "de haricots rouges égouttés et rincés", 400*multiplicateur_arrondi, "de tomates concassées en conserve", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1/4*multiplicateur_arrondi, "cuillère à café de piment de Cayenne", 1/2*multiplicateur_arrondi, "cuillère à café d'origan séché", 1*multiplicateur_arrondi, "tasse de riz cuit", "coriandre fraîche pour garnir"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, faire chauffer l'huile d'olive à feu moyen.", "ÉTAPE 2\nAjouter l'oignon haché dans la casserole chaude et le faire revenir jusqu'à ce qu'il soit translucide.", "ÉTAPE 3\nAjouter l'ail émincé dans la casserole avec l'oignon et le faire sauter pendant une minute supplémentaire.", "ÉTAPE 4\nAjouter le boeuf haché maigre dans la casserole avec l'oignon et l'ail et le faire cuire jusqu'à ce qu'il soit doré et cuit à travers.", "ÉTAPE 5\nAjouter les poivrons coupés en dés dans la casserole avec la viande et les faire sauter jusqu'à ce qu'ils soient tendres.", "ÉTAPE 6\nAjouter les haricots rouges égouttés et rincés dans la casserole avec la viande et les poivrons.", "ÉTAPE 7\nAjouter les tomates concassées en conserve, le cumin moulu, le paprika, le piment de Cayenne et l'origan séché dans la casserole et bien mélanger.", "ÉTAPE 8\nLaisser mijoter le chili à feu doux pendant 20 à 30 minutes, en remuant de temps en temps.", "ÉTAPE 9\nPendant ce temps, faire cuire le riz selon les instructions sur l'emballage dans une casserole d'eau bouillante salée.", "ÉTAPE 10\nServir le chili con carne chaud sur le riz cuit.", "ÉTAPE 11\nGarnir de coriandre fraîche hachée avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_con_carne_riz
def sandwich_poulet_avocat_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "pain ciabatta", "avocat", "tomate", "fromage suisse", "mayonnaise", "moutarde de Dijon", "laitue", "oignon rouge"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 1*multiplicateur_arrondi, "pain ciabatta", 1*multiplicateur_arrondi, "avocat mûr, tranché", 1/2*multiplicateur_arrondi, "tomate tranchée", 2*multiplicateur_arrondi, "tranches de fromage suisse", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", 1/2*multiplicateur_arrondi, "tasse de laitue déchirée", 1/4*multiplicateur_arrondi, "oignon rouge tranché"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les poitrines de poulet avec du sel, du poivre et un peu d'huile d'olive.", "ÉTAPE 3\nGriller les poitrines de poulet assaisonnées sur le gril préchauffé pendant 6 à 8 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 4\nPendant ce temps, couper le pain ciabatta en deux et le toaster légèrement.", "ÉTAPE 5\nDans un petit bol, mélanger la mayonnaise avec la moutarde de Dijon.", "ÉTAPE 6\nÉtaler le mélange de mayonnaise et de moutarde sur les deux moitiés de pain ciabatta grillé.", "ÉTAPE 7\nDisposer les tranches d'avocat sur la moitié inférieure du pain ciabatta grillé.", "ÉTAPE 8\nPlacer une poitrine de poulet grillée sur les tranches d'avocat.", "ÉTAPE 9\nAjouter une tranche de fromage suisse sur le poulet grillé.", "ÉTAPE 10\nGarnir de tranches de tomate, de laitue déchirée et d'oignon rouge tranché.", "ÉTAPE 11\nRefermer le sandwich avec l'autre moitié de pain ciabatta grillé.", "ÉTAPE 12\nCouper le sandwich en deux et servir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_avocat_fromage
def wrap_thon_legumes_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "thon en conserve", "mayonnaise", "yaourt grec", "citron", "ciboulette fraîche", "wrap tortilla", "laitue", "concombre", "poivron rouge", "tomate", "avocat"]
    ingredient = ["sel", "poivre", 100*multiplicateur_arrondi, "de thon en conserve, égoutté", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise", 1*multiplicateur_arrondi, "cuillère à soupe de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1/2*multiplicateur_arrondi, "cuillère à soupe de ciboulette fraîche ciselée", 1*multiplicateur_arrondi, "wrap tortilla", 1*multiplicateur_arrondi, "feuille de laitue", 1*multiplicateur_arrondi, "concombre tranché", 1*multiplicateur_arrondi, "poivron rouge en lanières", 1*multiplicateur_arrondi, "tomate tranchée", 1*multiplicateur_arrondi, "avocat tranché"]
    preparation = ["ÉTAPE 1\nDans un bol moyen, mélanger le thon égoutté avec la mayonnaise, le yaourt grec, le jus de citron, la ciboulette fraîche ciselée, du sel et du poivre.", "ÉTAPE 2\nChauffer les wraps tortilla selon les instructions sur l'emballage.", "ÉTAPE 3\nSur chaque wrap, répartir la feuille de laitue.", "ÉTAPE 4\nÉtaler la préparation de thon sur la laitue.", "ÉTAPE 5\nDisposer les tranches de concombre, les lanières de poivron rouge, les tranches de tomate et les tranches d'avocat sur la préparation de thon.", "ÉTAPE 6\nRouler les wraps fermement et les couper en deux avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_thon_legumes_frais
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet grillé", "feuilles de laitue romaine", "croûtons", "parmesan", "sauce césar", "jus de citron", "huile d'olive", "moutarde de Dijon", "ail", "anchois", "œuf dur"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "de poulet grillé, tranché", 2*multiplicateur_arrondi, "tasses de feuilles de laitue romaine déchirées", 1/2*multiplicateur_arrondi, "tasse de croûtons", 1/4*multiplicateur_arrondi, "tasse de parmesan râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sauce césar", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1/2*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1/2*multiplicateur_arrondi, "gousse d'ail émincée", 1/2*multiplicateur_arrondi, "filet d'anchois écrasé", 1*multiplicateur_arrondi, "œuf dur haché"]
    preparation = ["ÉTAPE 1\nDans un petit bol, préparer la vinaigrette césar en mélangeant le jus de citron, l'huile d'olive, la moutarde de Dijon, l'ail émincé et l'anchois écrasé.", "ÉTAPE 2\nDans un grand bol, mélanger les feuilles de laitue romaine déchirées avec la moitié de la vinaigrette césar.", "ÉTAPE 3\nAjouter les croûtons et le parmesan râpé dans le bol de laitue romaine et bien mélanger.", "ÉTAPE 4\nRépartir la salade de laitue dans des assiettes individuelles.", "ÉTAPE 5\nDisposer les tranches de poulet grillé sur la salade de laitue.", "ÉTAPE 6\nArroser le poulet grillé avec le reste de la vinaigrette césar.", "ÉTAPE 7\nGarnir de tranches d'œuf dur hachées avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def sandwich_poulet_avocat_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "huile d'olive", "poitrines de poulet", "pain ciabatta", "avocat", "tomate", "fromage suisse", "mayonnaise", "moutarde de Dijon", "laitue", "oignon rouge"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 1*multiplicateur_arrondi, "pain ciabatta", 1*multiplicateur_arrondi, "avocat mûr, tranché", 1/2*multiplicateur_arrondi, "tomate tranchée", 2*multiplicateur_arrondi, "tranches de fromage suisse", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", 1/2*multiplicateur_arrondi, "tasse de laitue déchirée", 1/4*multiplicateur_arrondi, "oignon rouge tranché"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les poitrines de poulet avec du sel, du poivre et un peu d'huile d'olive.", "ÉTAPE 3\nGriller les poitrines de poulet assaisonnées sur le gril préchauffé pendant 6 à 8 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 4\nPendant ce temps, couper le pain ciabatta en deux et le toaster légèrement.", "ÉTAPE 5\nDans un petit bol, mélanger la mayonnaise avec la moutarde de Dijon.", "ÉTAPE 6\nÉtaler le mélange de mayonnaise et de moutarde sur les deux moitiés de pain ciabatta grillé.", "ÉTAPE 7\nDisposer les tranches d'avocat sur la moitié inférieure du pain ciabatta grillé.", "ÉTAPE 8\nPlacer une poitrine de poulet grillée sur les tranches d'avocat.", "ÉTAPE 9\nAjouter une tranche de fromage suisse sur le poulet grillé.", "ÉTAPE 10\nGarnir de tranches de tomate, de laitue déchirée et d'oignon rouge tranché.", "ÉTAPE 11\nRefermer le sandwich avec l'autre moitié de pain ciabatta grillé.", "ÉTAPE 12\nCouper le sandwich en deux et servir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_avocat_fromage

def wrap_thon_legumes_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "thon en conserve", "mayonnaise", "yaourt grec", "citron", "ciboulette fraîche", "wrap tortilla", "laitue", "concombre", "poivron rouge", "tomate", "avocat"]
    ingredient = ["sel", "poivre", 100*multiplicateur_arrondi, "de thon en conserve, égoutté", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise", 1*multiplicateur_arrondi, "cuillère à soupe de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1/2*multiplicateur_arrondi, "cuillère à soupe de ciboulette fraîche ciselée", 1*multiplicateur_arrondi, "wrap tortilla", 1*multiplicateur_arrondi, "feuille de laitue", 1*multiplicateur_arrondi, "concombre tranché", 1*multiplicateur_arrondi, "poivron rouge en lanières", 1*multiplicateur_arrondi, "tomate tranchée", 1*multiplicateur_arrondi, "avocat tranché"]
    preparation = ["ÉTAPE 1\nDans un bol moyen, mélanger le thon égoutté avec la mayonnaise, le yaourt grec, le jus de citron, la ciboulette fraîche ciselée, du sel et du poivre.", "ÉTAPE 2\nChauffer les wraps tortilla selon les instructions sur l'emballage.", "ÉTAPE 3\nSur chaque wrap, répartir la feuille de laitue.", "ÉTAPE 4\nÉtaler la préparation de thon sur la laitue.", "ÉTAPE 5\nDisposer les tranches de concombre, les lanières de poivron rouge, les tranches de tomate et les tranches d'avocat sur la préparation de thon.", "ÉTAPE 6\nRouler les wraps fermement et les couper en deux avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_thon_legumes_frais
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet grillé", "feuilles de laitue romaine", "croûtons", "parmesan", "sauce césar", "jus de citron", "huile d'olive", "moutarde de Dijon", "ail", "anchois", "œuf dur"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "de poulet grillé, tranché", 2*multiplicateur_arrondi, "tasses de feuilles de laitue romaine déchirées", 1/2*multiplicateur_arrondi, "tasse de croûtons", 1/4*multiplicateur_arrondi, "tasse de parmesan râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sauce césar", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1/2*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1/2*multiplicateur_arrondi, "gousse d'ail émincée", 1/2*multiplicateur_arrondi, "filet d'anchois écrasé", 1*multiplicateur_arrondi, "œuf dur haché"]
    preparation = ["ÉTAPE 1\nDans un petit bol, préparer la vinaigrette césar en mélangeant le jus de citron, l'huile d'olive, la moutarde de Dijon, l'ail émincé et l'anchois écrasé.", "ÉTAPE 2\nDans un grand bol, mélanger les feuilles de laitue romaine déchirées avec la moitié de la vinaigrette césar.", "ÉTAPE 3\nAjouter les croûtons et le parmesan râpé dans le bol de laitue romaine et bien mélanger.", "ÉTAPE 4\nRépartir la salade de laitue dans des assiettes individuelles.", "ÉTAPE 5\nDisposer les tranches de poulet grillé sur la salade de laitue.", "ÉTAPE 6\nArroser le poulet grillé avec le reste de la vinaigrette césar.", "ÉTAPE 7\nGarnir de tranches d'œuf dur hachées avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def riz_frit_legumes_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile de sésame", "ail", "gingembre frais", "poitrine de poulet", "sel", "poivre", "œufs", "petits pois surgelés", "carotte", "oignon", "riz cuit", "sauce soja"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 3*multiplicateur_arrondi, "d'ail émincé", 2*multiplicateur_arrondi, "de gingembre frais râpé", 200*multiplicateur_arrondi, "de poitrine de poulet coupée en dés", "sel", "poivre", 2*multiplicateur_arrondi, "œufs battus", 60*multiplicateur_arrondi, "de petits pois surgelés", 1*multiplicateur_arrondi, "carotte coupée en petits dés", 1*multiplicateur_arrondi, "oignon émincé", 300*multiplicateur_arrondi, "de riz cuit", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer l'huile de sésame à feu moyen.", "ÉTAPE 2\nAjouter l'ail émincé et le gingembre frais râpé dans la poêle chaude et faire sauter pendant environ 1 minute, ou jusqu'à ce qu'ils soient parfumés.", "ÉTAPE 3\nAjouter les dés de poulet dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés et cuits à travers.", "ÉTAPE 4\nAssaisonner le poulet avec du sel et du poivre selon votre goût.", "ÉTAPE 5\nPousser le poulet sur un côté de la poêle et verser les œufs battus dans l'autre côté.", "ÉTAPE 6\nRemuer les œufs jusqu'à ce qu'ils soient cuits et les mélanger avec le poulet.", "ÉTAPE 7\nAjouter les petits pois surgelés, les dés de carotte et l'oignon émincé dans la poêle et faire sauter jusqu'à ce que les légumes soient tendres.", "ÉTAPE 8\nAjouter le riz cuit dans la poêle et mélanger avec le reste des ingrédients.", "ÉTAPE 9\nVerser la sauce soja sur le riz frit et bien mélanger.", "ÉTAPE 10\nLaisser cuire encore quelques minutes, en remuant fréquemment, jusqu'à ce que le tout soit chaud et bien mélangé."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

riz_frit_legumes_poulet
def pizza_legumes_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine", "levure", "sel", "eau tiède", "huile d'olive", "sauce tomate", "fromage mozzarella", "poulet grillé", "poivron rouge", "champignons", "oignon", "basilic frais"]
    ingredient = ["sel", "poivre", 500*multiplicateur_arrondi, "de farine tout usage", 1*multiplicateur_arrondi, "cuillère à café de levure sèche active", 1/2*multiplicateur_arrondi, "cuillère à café de sel", 1/2*multiplicateur_arrondi, "tasse d'eau tiède", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1/2*multiplicateur_arrondi, "tasse de sauce tomate", 200*multiplicateur_arrondi, "de fromage mozzarella râpé", 200*multiplicateur_arrondi, "de poulet grillé coupé en dés", 1/2*multiplicateur_arrondi, "poivron rouge coupé en lanières", 2*multiplicateur_arrondi, "champignons tranchés", 1/4*multiplicateur_arrondi, "oignon rouge tranché", "feuilles de basilic frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 220°C (425°F).", "ÉTAPE 2\nDans un grand bol, mélanger la farine, la levure et le sel.", "ÉTAPE 3\nAjouter l'eau tiède et l'huile d'olive à la préparation de farine et bien mélanger jusqu'à ce que la pâte se rassemble.", "ÉTAPE 4\nPétrir la pâte sur une surface légèrement farinée pendant environ 5 minutes, ou jusqu'à ce qu'elle soit lisse et élastique.", "ÉTAPE 5\nDiviser la pâte en deux boules et les étaler sur des plaques à pizza légèrement graissées.", "ÉTAPE 6\nÉtaler la sauce tomate sur les fonds de pizza, en laissant une bordure libre.", "ÉTAPE 7\nGarnir les pizzas avec le fromage mozzarella râpé, les dés de poulet grillé, les lanières de poivron rouge, les champignons tranchés et les tranches d'oignon rouge.", "ÉTAPE 8\nAssaisonner les pizzas avec du sel, du poivre et des feuilles de basilic frais.", "ÉTAPE 9\nCuire au four préchauffé pendant environ 15 à 20 minutes, ou jusqu'à ce que la croûte soit dorée et le fromage fondu."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_legumes_poulet
def poulet_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet entier", "paprika", "ail en poudre", "pommes de terre", "carottes", "oignon", "huile d'olive"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "poulet entier", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 500*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 200*multiplicateur_arrondi, "carottes coupées en tronçons", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nAssaisonner le poulet avec du sel, du poivre, du paprika et de l'ail en poudre.", "ÉTAPE 3\nDisposer les quartiers de pommes de terre, les tronçons de carottes et les quartiers d'oignon dans un grand plat de cuisson.", "ÉTAPE 4\nArroser les légumes d'huile d'olive et assaisonner avec du sel et du poivre.", "ÉTAPE 5\nPlacer le poulet assaisonné sur les légumes dans le plat de cuisson.", "ÉTAPE 6\nRôtir au four préchauffé pendant environ 1 heure à 1 heure et demie, ou jusqu'à ce que le poulet soit bien cuit et que les légumes soient tendres.", "ÉTAPE 7\nRetirer le poulet et les légumes du four et laisser reposer quelques minutes avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 90
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_legumes_rotis
def steak_boeuf_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "steak de bœuf", "patates douces", "huile d'olive", "paprika", "ail en poudre", "romarin frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "steak de bœuf épais", 2*multiplicateur_arrondi, "patates douces coupées en tranches épaisses", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", "quelques brins de romarin frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les steaks de bœuf des deux côtés avec du sel, du poivre, du paprika et de l'ail en poudre.", "ÉTAPE 3\nArroser les tranches de patates douces avec de l'huile d'olive et les assaisonner avec du sel, du poivre et du romarin frais haché.", "ÉTAPE 4\nGriller les steaks de bœuf et les tranches de patates douces sur le gril préchauffé, en retournant une fois, jusqu'à ce qu'ils soient bien cuits.", "ÉTAPE 5\nRetirer les steaks et les patates douces du gril et laisser reposer quelques minutes avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

steak_boeuf_patates_douces
def quinoa_legumes_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "quinoa", "eau", "huile d'olive", "poulet", "oignon", "ail", "poivron rouge", "courgette", "petits pois surgelés", "jus de citron", "coriandre fraîche"]
    ingredient = ["sel", "poivre", 100*multiplicateur_arrondi, "de quinoa sec", 200*multiplicateur_arrondi, "ml d'eau", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 200*multiplicateur_arrondi, "de poulet coupé en dés", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "courgette coupée en dés", 100*multiplicateur_arrondi, "de petits pois surgelés", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "quelques brins de coriandre fraîche"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter l'eau à ébullition et ajouter le quinoa.", "ÉTAPE 2\nRéduire le feu, couvrir et laisser mijoter pendant environ 15 minutes, ou jusqu'à ce que le quinoa soit tendre et que l'eau soit absorbée.", "ÉTAPE 3\nPendant ce temps, chauffer l'huile d'olive dans une grande poêle à feu moyen.", "ÉTAPE 4\nAjouter le poulet coupé en dés à la poêle chaude et faire sauter jusqu'à ce qu'il soit doré et cuit à travers.", "ÉTAPE 5\nAjouter l'oignon haché, l'ail émincé, le poivron rouge coupé en dés, la courgette coupée en dés et les petits pois surgelés dans la poêle avec le poulet et faire sauter jusqu'à ce que les légumes soient tendres.", "ÉTAPE 6\nIncorporer le quinoa cuit dans la poêle avec les légumes et le poulet.", "ÉTAPE 7\nArroser le mélange de quinoa et de légumes avec du jus de citron frais et assaisonner avec du sel, du poivre et de la coriandre fraîche hachée avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

quinoa_legumes_poulet
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "thon en conserve", "haricots blancs en conserve", "tomate", "oignon rouge", "persil frais", "vinaigrette balsamique"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "de thon en conserve égoutté", 400*multiplicateur_arrondi, "de haricots blancs en conserve rincés et égouttés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1/4*multiplicateur_arrondi, "oignon rouge coupé en dés", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigrette balsamique"]
    preparation = ["ÉTAPE 1\nDans un grand bol, émietter le thon en conserve égoutté à la fourchette.", "ÉTAPE 2\nAjouter les haricots blancs en conserve rincés et égouttés dans le bol avec le thon émietté.", "ÉTAPE 3\nAjouter les dés de tomate, les dés d'oignon rouge et le persil frais haché dans le bol avec le mélange thon-haricots blancs.", "ÉTAPE 4\nArroser la salade de thon et haricots blancs avec de la vinaigrette balsamique et bien mélanger pour combiner tous les ingrédients.", "ÉTAPE 5\nAssaisonner la salade de thon et haricots blancs avec du sel et du poivre selon votre goût.", "ÉTAPE 6\nRéfrigérer la salade pendant au moins 30 minutes avant de servir pour permettre aux saveurs de se mélanger."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def pates_sauce_bolognaise(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "pâtes", "huile d'olive", "oignon", "ail", "viande hachée de bœuf", "tomates concassées en conserve", "sauce tomate", "origan séché", "basilic frais", "parmesan râpé"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "de pâtes", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 300*multiplicateur_arrondi, "de viande hachée de bœuf", 400*multiplicateur_arrondi, "de tomates concassées en conserve", 200*multiplicateur_arrondi, "de sauce tomate", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "quelques feuilles de basilic frais", 2*multiplicateur_arrondi, "cuillères à soupe de parmesan râpé"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, porter une grande quantité d'eau légèrement salée à ébullition.", "ÉTAPE 2\nAjouter les pâtes dans l'eau bouillante et cuire selon les instructions sur l'emballage, jusqu'à ce qu'elles soient al dente.", "ÉTAPE 3\nPendant ce temps, chauffer l'huile d'olive dans une grande poêle à feu moyen.", "ÉTAPE 4\nAjouter l'oignon haché et l'ail émincé dans la poêle chaude et faire sauter jusqu'à ce qu'ils soient translucides.", "ÉTAPE 5\nAjouter la viande hachée de bœuf dans la poêle avec l'oignon et l'ail et faire cuire jusqu'à ce qu'elle soit bien dorée et cuite à travers.", "ÉTAPE 6\nIncorporer les tomates concassées en conserve, la sauce tomate, l'origan séché et le basilic frais dans la poêle avec la viande hachée.", "ÉTAPE 7\nLaisser mijoter la sauce bolognaise pendant environ 10 minutes, en remuant de temps en temps.", "ÉTAPE 8\nÉgoutter les pâtes cuites et les mélanger avec la sauce bolognaise chaude.", "ÉTAPE 9\nServir les pâtes à la sauce bolognaise garnies de parmesan râpé."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous
pates_sauce_bolognaise
def wrap_poulet_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet", "poivron rouge", "poivron jaune", "courgette", "oignon", "tortillas de blé", "houmous", "feuilles de laitue"]
    ingredient = ["sel", "poivre", 200*multiplicateur_arrondi, "de poulet coupé en lanières", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1/2*multiplicateur_arrondi, "oignon rouge coupé en lanières", 2*multiplicateur_arrondi, "tortillas de blé", 2*multiplicateur_arrondi, "cuillères à soupe de houmous", 2*multiplicateur_arrondi, "feuilles de laitue lavées et séchées"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les lanières de poulet avec du sel et du poivre selon votre goût.", "ÉTAPE 3\nGriller les lanières de poulet sur le gril préchauffé, en les retournant une fois, jusqu'à ce qu'elles soient dorées et cuites à travers.", "ÉTAPE 4\nGriller également les lanières de poivron rouge, de poivron jaune, de courgette et d'oignon rouge sur le gril préchauffé jusqu'à ce qu'elles soient tendres et légèrement dorées.", "ÉTAPE 5\nChauffer les tortillas de blé sur le gril préchauffé pendant quelques secondes de chaque côté pour les réchauffer.", "ÉTAPE 6\nÉtaler une cuillerée de houmous sur chaque tortilla de blé.", "ÉTAPE 7\nDisposer quelques feuilles de laitue sur chaque tortilla de blé.", "ÉTAPE 8\nRépartir les lanières de poulet grillé et les légumes grillés sur les tortillas de blé.", "ÉTAPE 9\nRouler les tortillas de blé garnies de poulet et de légumes pour former des wraps.", "ÉTAPE 10\nCouper les wraps en deux et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_legumes_grilles
def poisson_grille_riz_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "filet de poisson blanc", "jus de citron", "ail", "huile d'olive", "riz", "brocoli", "carottes", "citron", "persil frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "filet de poisson blanc", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 100*multiplicateur_arrondi, "riz cuit", 100*multiplicateur_arrondi, "de brocoli cuit à la vapeur", 100*multiplicateur_arrondi, "de carottes cuites à la vapeur", "tranches de citron et feuilles de persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les filets de poisson blanc avec du sel, du poivre, du jus de citron frais et de l'ail émincé.", "ÉTAPE 3\nBadigeonner légèrement les filets de poisson avec de l'huile d'olive.", "ÉTAPE 4\nGriller les filets de poisson sur le gril préchauffé, en les retournant une fois, jusqu'à ce qu'ils soient cuits à travers et légèrement dorés sur les bords.", "ÉTAPE 5\nPendant ce temps, faire cuire le riz, le brocoli et les carottes à la vapeur jusqu'à ce qu'ils soient tendres.", "ÉTAPE 6\nServir les filets de poisson grillé avec le riz cuit à la vapeur, le brocoli et les carottes.", "ÉTAPE 7\nGarnir de tranches de citron frais et de feuilles de persil avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_grille_riz_legumes
def poulet_roti_pommes_terre_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet entier", "pommes de terre", "carottes", "oignon", "ail", "thym frais", "huile d'olive"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "poulet entier", 500*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 200*multiplicateur_arrondi, "carottes coupées en tronçons", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 2*multiplicateur_arrondi, "gousses d'ail émincées", 4*multiplicateur_arrondi, "branches de thym frais", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nAssaisonner le poulet avec du sel et du poivre à l'intérieur et à l'extérieur.", "ÉTAPE 3\nPlacer le poulet dans un grand plat de cuisson.", "ÉTAPE 4\nDans un bol, mélanger les quartiers de pommes de terre, les tronçons de carottes, les quartiers d'oignon, l'ail émincé et les branches de thym frais avec de l'huile d'olive.", "ÉTAPE 5\nRépartir le mélange de légumes autour du poulet dans le plat de cuisson.", "ÉTAPE 6\nRôtir au four préchauffé pendant environ 1 heure à 1 heure et demie, ou jusqu'à ce que le poulet soit bien cuit et que les légumes soient tendres.", "ÉTAPE 7\nRetirer le poulet et les légumes du four et laisser reposer quelques minutes avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 90
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_pommes_terre_legumes
def saumon_grille_couscous_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "filets de saumon", "huile d'olive", "couscous", "bouillon de légumes", "poivron rouge", "poivron jaune", "courgette", "oignon rouge", "jus de citron", "menthe fraîche"]
    ingredient = ["sel", "poivre", 2*multiplicateur_arrondi, "filets de saumon", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 200*multiplicateur_arrondi, "de couscous", 250*multiplicateur_arrondi, "ml de bouillon de légumes chaud", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "poivron jaune coupé en dés", 1*multiplicateur_arrondi, "courgette coupée en dés", 1/2*multiplicateur_arrondi, "oignon rouge coupé en dés", 2*multiplicateur_arrondi, "cuillères à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.", "ÉTAPE 2\nAssaisonner les filets de saumon avec du sel, du poivre et un peu d'huile d'olive.", "ÉTAPE 3\nGriller les filets de saumon sur le gril préchauffé, en les retournant une fois, jusqu'à ce qu'ils soient cuits à travers.", "ÉTAPE 4\nPendant ce temps, préparer le couscous en versant le bouillon de légumes chaud sur le couscous dans un grand bol, couvrir et laisser reposer pendant environ 5 minutes, ou jusqu'à ce que tout le liquide soit absorbé.", "ÉTAPE 5\nDans une grande poêle, chauffer un peu d'huile d'olive et faire sauter les dés de poivron rouge, de poivron jaune, de courgette et d'oignon rouge jusqu'à ce qu'ils soient tendres.", "ÉTAPE 6\nIncorporer le jus de citron frais et la menthe fraîche dans les légumes sautés.", "ÉTAPE 7\nServir les filets de saumon grillés avec le couscous et les légumes sautés.", "ÉTAPE 8\nGarnir de plus de menthe fraîche si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_couscous_legumes
def salade_poulet_cesar(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poulet", "laitue romaine", "parmesan", "pain", "ail", "huile d'olive", "jus de citron", "œuf", "anchois", "moutarde de Dijon", "vinaigre de vin rouge"]
    ingredient = ["sel", "poivre", 2*multiplicateur_arrondi, "de blancs de poulet cuits et coupés en dés", 1*multiplicateur_arrondi, "tête de laitue romaine hachée", 30*multiplicateur_arrondi, "de parmesan râpé", 1/2*multiplicateur_arrondi, "tasse de croûtons de pain", 1*multiplicateur_arrondi, "gousse d'ail émincée", 3*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "cuillères à soupe de jus de citron frais", 1*multiplicateur_arrondi, "œuf dur écrasé", 2*multiplicateur_arrondi, "anchois hachés", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à café de vinaigre de vin rouge"]
    preparation = ["ÉTAPE 1\nDans un grand saladier, mélanger les dés de poulet cuits, la laitue romaine hachée, le parmesan râpé et les croûtons de pain.", "ÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'ail émincé, l'huile d'olive, le jus de citron frais, l'œuf dur écrasé, les anchois hachés, la moutarde de Dijon et le vinaigre de vin rouge.", "ÉTAPE 3\nVerser la vinaigrette sur la salade de poulet César et mélanger pour bien enrober tous les ingrédients.", "ÉTAPE 4\nAssaisonner la salade de poulet César avec du sel et du poivre selon votre goût.", "ÉTAPE 5\nServir la salade de poulet César immédiatement, garnie de quartiers de citron frais si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar
def poulet_teriyaki_riz_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "filets de poulet", "sauce teriyaki", "riz", "mélange de légumes (carottes, poivrons, brocoli)", "huile d'olive", "ail", "gingembre frais", "sésame grillé"]
    ingredient = ["sel", "poivre", 4*multiplicateur_arrondi, "filets de poulet", 200*multiplicateur_arrondi, "ml sauce teriyaki", 200*multiplicateur_arrondi, "g riz non cuit", 200*multiplicateur_arrondi, "g mélange de légumes (carottes, poivrons, brocoli)", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sésame grillé"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz selon les instructions sur l'emballage dans une casserole d'eau bouillante salée, jusqu'à ce qu'il soit tendre et moelleux. Égoutter et réserver.", "ÉTAPE 2\nAssaisonner les filets de poulet avec du sel et du poivre selon votre goût.", "ÉTAPE 3\nDans une poêle, chauffer un peu d'huile d'olive à feu moyen. Ajouter l'ail émincé et le gingembre frais râpé dans la poêle chaude et faire sauter jusqu'à ce qu'ils soient parfumés.", "ÉTAPE 4\nAjouter les filets de poulet assaisonnés dans la poêle avec l'ail et le gingembre sautés. Faire cuire les filets de poulet jusqu'à ce qu'ils soient dorés des deux côtés et bien cuits.", "ÉTAPE 5\nRetirer les filets de poulet de la poêle et les trancher en lanières.", "ÉTAPE 6\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire et faire sauter le mélange de légumes jusqu'à ce qu'ils soient tendres mais encore croquants.", "ÉTAPE 7\nAjouter les lanières de poulet cuites et la sauce teriyaki dans la poêle avec les légumes sautés. Mélanger pour bien enrober les ingrédients de la sauce teriyaki.", "ÉTAPE 8\nServir le poulet teriyaki chaud sur un lit de riz cuit, garni de légumes sautés. Saupoudrer de sésame grillé avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_teriyaki_riz_legumes_sautes
def pizza_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "pâte à pizza", "sauce tomate", "mozzarella râpée", "légumes au choix (poivrons, champignons, oignons, olives)", "huile d'olive", "origan séché", "basilic frais"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "pâte à pizza", 200*multiplicateur_arrondi, "g sauce tomate", 200*multiplicateur_arrondi, "g mozzarella râpée", 200*multiplicateur_arrondi, "g légumes au choix (poivrons, champignons, oignons, olives)", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "origan séché", "basilic frais haché"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 220°C (425°F).", "ÉTAPE 2\nAbaisser la pâte à pizza sur une plaque à pâtisserie légèrement farinée.", "ÉTAPE 3\nÉtaler la sauce tomate sur la pâte à pizza abaisser, en laissant un bord d'environ 1 cm.", "ÉTAPE 4\nRépartir uniformément les légumes au choix sur la sauce tomate. Ajouter la mozzarella râpée sur les légumes.", "ÉTAPE 5\nArroser la pizza d'huile d'olive et saupoudrer d'origan séché.", "ÉTAPE 6\nCuire la pizza au four préchauffé pendant environ 15-20 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante et le fromage soit fondu et bouillonnant.", "ÉTAPE 7\nSaupoudrer de basilic frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pizza_legumes_fromage
def sandwich_bifteck_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "bifteck", "pain à sandwich", "fromage cheddar tranché", "laitue", "tomate", "oignon rouge", "mayonnaise", "moutarde"]
    ingredient = ["sel", "poivre", 1*multiplicateur_arrondi, "bifteck", 1*multiplicateur_arrondi, "pain à sandwich", 1*multiplicateur_arrondi, "tranches de fromage cheddar", "feuilles de laitue", "tranches de tomate", "tranches d'oignon rouge", "mayonnaise", "moutarde"]
    preparation = ["ÉTAPE 1\nAssaisonner le bifteck avec du sel et du poivre selon votre goût.", "ÉTAPE 2\nFaire chauffer une poêle à feu moyen-élevé. Ajouter le bifteck assaisonné dans la poêle chaude et faire cuire selon vos préférences de cuisson, environ 3-4 minutes de chaque côté pour une cuisson à point.", "ÉTAPE 3\nRetirer le bifteck de la poêle et le laisser reposer quelques minutes avant de trancher finement.", "ÉTAPE 4\nPendant ce temps, toaster légèrement les tranches de pain à sandwich.", "ÉTAPE 5\nÉtaler une fine couche de mayonnaise sur une tranche de pain à sandwich. Ajouter une tranche de fromage cheddar sur l'autre tranche de pain.", "ÉTAPE 6\nDisposer les tranches de bifteck sur la tranche de pain avec la mayonnaise. Garnir de feuilles de laitue, de tranches de tomate et d'oignon rouge.", "ÉTAPE 7\nRefermer le sandwich et couper en deux avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_bifteck_fromage
def poulet_grille_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "poitrines de poulet", "patates douces", "huile d'olive", "paprika", "ail en poudre", "romarin frais"]
    ingredient = ["sel", "poivre", 4*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 500*multiplicateur_arrondi, "g patates douces, pelées et coupées en cubes", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 2*multiplicateur_arrondi, "branches de romarin frais"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).", "ÉTAPE 2\nAssaisonner les poitrines de poulet avec du sel, du poivre, du paprika et de l'ail en poudre selon votre goût.", "ÉTAPE 3\nDans un grand bol, mélanger les cubes de patates douces avec de l'huile d'olive, du sel, du poivre et du romarin frais haché.", "ÉTAPE 4\nDisposer les poitrines de poulet assaisonnées sur une plaque de cuisson tapissée de papier sulfurisé. Répartir les cubes de patates douces autour du poulet.", "ÉTAPE 5\nCuire au four préchauffé pendant environ 25-30 minutes, ou jusqu'à ce que le poulet soit bien cuit et que les patates douces soient tendres et dorées.", "ÉTAPE 6\nServir les poitrines de poulet grillées avec les patates douces rôties, garnies de romarin frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_grille_patates_douces
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "quinoa", "courgettes", "poivrons rouges", "oignon rouge", "tomates cerises", "huile d'olive", "vinaigre de vin rouge", "miel", "moutarde de Dijon", "basilic frais"]
    ingredient = ["sel", "poivre", 100*multiplicateur_arrondi, "g quinoa non cuit", 2*multiplicateur_arrondi, "courgettes, tranchées en longueur", 2*multiplicateur_arrondi, "poivrons rouges, tranchés en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en quartiers", 100*multiplicateur_arrondi, "g tomates cerises, coupées en deux", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "cuillères à soupe de basilic frais haché"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide dans une passoire fine. Dans une casserole moyenne, porter 2 fois le volume d'eau salée à ébullition. Ajouter le quinoa et réduire le feu à doux. Couvrir et cuire jusqu'à ce que le quinoa soit tendre et que les germes se détachent, environ 15 minutes. Laisser refroidir.", "ÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé. Dans un bol moyen, mélanger les courgettes, les poivrons rouges et les quartiers d'oignon rouge avec de l'huile d'olive. Assaisonner de sel et de poivre selon votre goût.", "ÉTAPE 3\nGriller les légumes préparés sur le gril préchauffé jusqu'à ce qu'ils soient tendres et marqués, environ 5-7 minutes de chaque côté. Retirer du gril et laisser refroidir légèrement.", "ÉTAPE 4\nDans un grand bol, mélanger les légumes grillés avec le quinoa cuit et les tomates cerises coupées en deux.", "ÉTAPE 5\nDans un petit bol, fouetter ensemble l'huile d'olive, le vinaigre de vin rouge, le miel, la moutarde de Dijon, le sel et le poivre. Verser la vinaigrette sur la salade de quinoa et légumes et mélanger pour bien enrober.", "ÉTAPE 6\nGarnir la salade de quinoa aux légumes grillés de basilic frais haché avant de servir."]
    temps_de_preparation = 20
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "oeufs", "champignons", "fromage râpé", "huile d'olive"]
    ingredient = ["sel", "poivre", 4*multiplicateur_arrondi, "oeufs", 100*multiplicateur_arrondi, "g champignons tranchés", 50*multiplicateur_arrondi, "g fromage râpé (de votre choix)", 1*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive"]
    preparation = ["ÉTAPE 1\nChauffer une cuillère à soupe d'huile d'olive dans une poêle antiadhésive à feu moyen.", "ÉTAPE 2\nAjouter les champignons tranchés dans la poêle chaude et les faire sauter jusqu'à ce qu'ils soient dorés et tendres. Retirer les champignons de la poêle et réserver.", "ÉTAPE 3\nDans un grand bol, battre les œufs avec du sel et du poivre selon votre goût.", "ÉTAPE 4\nDans la même poêle, chauffer le reste de l'huile d'olive à feu moyen. Verser les œufs battus dans la poêle chaude.", "ÉTAPE 5\nLaisser les œufs cuire doucement jusqu'à ce qu'ils commencent à prendre sur les bords. Utiliser une spatule pour ramener les bords cuits vers le centre de la poêle.", "ÉTAPE 6\nLorsque l'omelette est presque prise mais encore légèrement baveuse sur le dessus, ajouter les champignons sautés et le fromage râpé sur une moitié de l'omelette.", "ÉTAPE 7\nPlier l'autre moitié de l'omelette sur le dessus des champignons et du fromage. Laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu et que l'omelette soit bien cuite.", "ÉTAPE 8\nTransférer l'omelette aux champignons et au fromage sur une assiette de service et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_fromage
def saumon_grille_quinoa_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "steaks de saumon", "quinoa", "mélange de légumes (courgettes, poivrons, oignons)", "huile d'olive", "ail en poudre", "jus de citron", "persil frais"]
    ingredient = ["sel", "poivre", 4*multiplicateur_arrondi, "steaks de saumon", 100*multiplicateur_arrondi, "g quinoa non cuit", 200*multiplicateur_arrondi, "g mélange de légumes (courgettes, poivrons, oignons)", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage dans une casserole d'eau bouillante salée, jusqu'à ce qu'il soit tendre. Égoutter et réserver.", "ÉTAPE 2\nPréchauffer le gril à feu moyen-élevé. Assaisonner les steaks de saumon avec du sel, du poivre et de l'ail en poudre.", "ÉTAPE 3\nDans un bol, mélanger les légumes avec de l'huile d'olive, du sel et du poivre. Répartir les légumes sur une plaque de cuisson tapissée de papier sulfurisé.", "ÉTAPE 4\nGriller les steaks de saumon assaisonnés et les légumes préparés sur le gril préchauffé pendant environ 4-5 minutes de chaque côté, ou jusqu'à ce que le saumon soit bien cuit et que les légumes soient tendres et légèrement dorés.", "ÉTAPE 5\nDans un petit bol, mélanger le jus de citron frais et le persil haché. Arroser le saumon grillé et les légumes avec ce mélange avant de servir.", "ÉTAPE 6\nServir les steaks de saumon grillés avec du quinoa cuit aux légumes, garnis de persil frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_quinoa_legumes
def salade_poulet_grille_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["sel", "poivre", "filets de poulet", "melon d'eau", "fraises", "feuilles de laitue", "feuilles de menthe", "vinaigre balsamique", "miel", "huile d'olive"]
    ingredient = ["sel", "poivre", 4*multiplicateur_arrondi, "filets de poulet", 1*multiplicateur_arrondi, "tasse de dés de melon d'eau", 1*multiplicateur_arrondi, "tasse de fraises fraîches, tranchées", 4*multiplicateur_arrondi, "tasses de feuilles de laitue mélangées", 1*multiplicateur_arrondi, "tasse de feuilles de menthe fraîche", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive"]
    preparation = ["ÉTAPE 1\nAssaisonner les filets de poulet avec du sel et du poivre selon votre goût. Faire cuire les filets de poulet assaisonnés sur un gril préchauffé à feu moyen-élevé jusqu'à ce qu'ils soient bien cuits et dorés, environ 6-8 minutes de chaque côté. Retirer du gril et laisser reposer quelques minutes avant de trancher.", "ÉTAPE 2\nDans un grand bol, mélanger les dés de melon d'eau et les fraises tranchées.", "ÉTAPE 3\nDans un petit bol, fouetter ensemble le vinaigre balsamique, le miel et l'huile d'olive pour faire la vinaigrette.", "ÉTAPE 4\nDans un grand saladier, disposer les feuilles de laitue mélangées et les feuilles de menthe fraîche.", "ÉTAPE 5\nAjouter les dés de melon d'eau et les fraises tranchées sur les feuilles de laitue dans le saladier.", "ÉTAPE 6\nArroser la salade de poulet grillé tranché avec la vinaigrette préparée juste avant de servir.", "ÉTAPE 7\nMélanger délicatement pour enrober les ingrédients de la vinaigrette. Servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_grille_fruits

tuplerepasdusoir_pour_la_prise_de_mas=[brochettes_crevettes_grillees_legumes,burger_dinde_frites_patates_douces,burrito_poulet_haricots,chili_con_carne,chili_con_carne_riz,chili_con_carne_riz_brun,curry_poulet_riz_basmati,lasagnes_boeuf_epinards_fromage,lasagnes_e_pinards_fromage,omelette_champignons_fromage,pates_bolognaise_viande_hachee,pates_carbonara,pates_pesto_poulet,pates_sauce_bolognaise,penne_bolognaise,pizza_legumes_fromage,pizza_legumes_grilles,pizza_legumes_poulet,pizza_variete_garnitures,poelee_crevettes_legumes_riz_basmati,poisson_grille_quinoa_asperges,poisson_grille_riz_legumes,poisson_riz_legumes,poulet_curry_riz_basmati,poulet_farcis_fromage_epinards,poulet_four_pommes_terre_haricots_verts,poulet_grille_patates_douces,poulet_legumes_rotis,poulet_legumes_sautes,poulet_quinoa_legumes_rotis,poulet_roti_legumes,poulet_roti_legumes_quinoa,poulet_roti_patates_douces_legumes,poulet_roti_pommes_de_terre_legumes,poulet_roti_pommes_terre_legumes,poulet_roti_pommes_terre_roties,poulet_teriyaki_riz_legumes_sautes,quinoa_legumes_poulet,risotto_champignons,riz_frit_legumes_crevettes,riz_frit_legumes_poulet,salade_pates_grecque_poulet_grille,salade_poulet_cesar,salade_poulet_grille_fruits,salade_quinoa_legumes_grilles,salade_quinoa_pois_chiches_legumes,salade_thon_avocat,salade_thon_haricots_blancs,salade_thon_haricots_blancs_vinaigrette,sandwich_bifteck_fromage,sandwich_boeuf_hache_fromage,sandwich_poulet_avocat,sandwich_poulet_avocat_fromage,sandwich_poulet_grille_avocat_fromage,sandwich_saumon_avocat,saumon_cuit_four_quinoa,saumon_grille_couscous_legumes,saumon_grille_quinoa_legumes,saumon_quinoa_legumes,spaghetti_bolognaise,steak_boeuf_grille_pommes_terre_legumes,steak_boeuf_patates_douces,steak_boeuf_patates_douces_brocoli,steak_grille_legumes_sautes,steak_hache_riz_legumes_sautes,steak_saumon_patates_douces,wrap_poulet_legumes,wrap_poulet_legumes_grilles,wrap_thon_legumes_frais,wrap_vegetarien_haricots_noirs,wraps_legumes_grilles_poulet,wraps_poulet_avocat,wraps_saumon_avocat_legumes]