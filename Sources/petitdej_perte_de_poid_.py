def salade_avocat_tomate(multiplicateur_arrondi):
    ingredient_pour_algo = ["vinaigrette", "sel", "poivre", "avocat", "tomate", "oignon rouge", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, " cuillère à soupe de vinaigrette", "sel", "poivre", 1*multiplicateur_arrondi, "avocat", 1*multiplicateur_arrondi, "tomate", 1*multiplicateur_arrondi, "oignon rouge", "jus de citron"]
    préparation = ["ÉTAPE 1\n Préparer la vinaigrette dans un petit bol.\nÉTAPE 2\n Couper l'avocat et la tomate en dés.\nÉTAPE 3\n Trancher finement l'oignon rouge.\nÉTAPE 4\n Arroser les avocats de jus de citron pour éviter qu'ils ne brunissent.\nÉTAPE 5\n Dans un grand bol, mélanger délicatement les dés d'avocat, de tomate et l'oignon rouge.\nÉTAPE 6\n Assaisonner avec la vinaigrette, le sel et le poivre selon votre goût.\nÉTAPE 7\n Servir immédiatement."]
    temps_de_préparation = 10
    temps_total = temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_total]
    return tous
salade_avocat_tomate
def frittata_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "épinards frais", "sel", "poivre", "œufs", "lait", "fromage râpé"]
    ingredient = [1*multiplicateur_arrondi, " cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, " tasse d'épinards frais", "sel", "poivre", 2*multiplicateur_arrondi, "œufs", 1*multiplicateur_arrondi, " cuillère à soupe de lait", 2*multiplicateur_arrondi, " cuillères à soupe de fromage râpé"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C (thermostat 6).\nÉTAPE 2\n Dans une poêle allant au four, faire chauffer l'huile d'olive.\nÉTAPE 3\n Ajouter les épinards frais et les faire cuire jusqu'à ce qu'ils soient flétris.\nÉTAPE 4\n Assaisonner les épinards avec du sel et du poivre.\nÉTAPE 5\n Dans un bol, battre les œufs avec le lait.\nÉTAPE 6\n Verser le mélange d'œufs sur les épinards dans la poêle.\nÉTAPE 7\n Saupoudrer de fromage râpé sur le dessus.\nÉTAPE 8\n Transférer la poêle au four et cuire la frittata pendant environ 10 minutes ou jusqu'à ce qu'elle soit bien prise et dorée sur le dessus.\nÉTAPE 9\n Servir chaud ou à température ambiante."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
frittata_epinards_fromage
def salade_concombre_tomate_basilic(multiplicateur_arrondi):
    ingredient_pour_algo = ["vinaigrette légère", "sel", "poivre", "concombre", "tomate", "feuilles de basilic frais"]
    ingredient = [1*multiplicateur_arrondi, " cuillère à soupe de vinaigrette légère", "sel", "poivre", 1*multiplicateur_arrondi, "concombre", 1*multiplicateur_arrondi, "tomate", 1*multiplicateur_arrondi, "feuilles de basilic frais"]
    préparation = ["ÉTAPE 1\n Préparer la vinaigrette légère dans un petit bol.\nÉTAPE 2\n Trancher finement le concombre et la tomate.\nÉTAPE 3\n Hacher finement les feuilles de basilic frais.\nÉTAPE 4\n Dans un grand bol, mélanger délicatement les tranches de concombre, de tomate et les feuilles de basilic.\nÉTAPE 5\n Assaisonner avec la vinaigrette légère, le sel et le poivre selon votre goût.\nÉTAPE 6\n Servir immédiatement."]
    temps_de_préparation = 10
    temps_total = temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_total]
    return tous
salade_concombre_tomate_basilic
def smoothie_vert_avocat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","avocat","épinards","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," avocat",1*multiplicateur_arrondi," poignée d'épinards",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un mixeur, ajoutez la banane pelée et coupée en morceaux, l'avocat pelé et dénoyauté, les épinards lavés et le lait d'amande.\nÉTAPE 2\nMixez le tout jusqu'à obtention d'une texture lisse et homogène.\nÉTAPE 3\nServez dans un grand verre et dégustez immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_vert_avocat_banane
def yaourt_grec_baies_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec","baies","graines de chia"]
    ingredient = [1*multiplicateur_arrondi," tasse de yaourt grec",1*multiplicateur_arrondi," tasse de baies mélangées (fraises, myrtilles, framboises, etc.)",1*multiplicateur_arrondi," cuillère à soupe de graines de chia"]
    préparation=["ÉTAPE 1\nDans un bol, ajoutez le yaourt grec.\nÉTAPE 2\nAjoutez les baies mélangées sur le dessus du yaourt.\nÉTAPE 3\nSaupoudrez de graines de chia.\nÉTAPE 4\nMélangez légèrement.\nÉTAPE 5\nServez immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
yaourt_grec_baies_chia
def salade_fruits_menthe(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits variés (fraises, mangue, kiwi, melon, etc.)","menthe fraîche"]
    ingredient = [1*multiplicateur_arrondi," tasse de fruits variés coupés en dés (fraises, mangue, kiwi, melon, etc.)",1*multiplicateur_arrondi," cuillère à soupe de feuilles de menthe fraîche ciselées"]
    préparation=["ÉTAPE 1\nCoupez les fruits en dés si ce n'est pas déjà fait.\nÉTAPE 2\nMélangez-les délicatement dans un saladier.\nÉTAPE 3\nSaupoudrez de feuilles de menthe fraîche ciselées.\nÉTAPE 4\nMélangez une dernière fois et servez."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
salade_fruits_menthe
def muffin_oeufs_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","fromage","lait","sel","poivre"]
    ingredient = [2*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais hachés",2*multiplicateur_arrondi," cuillères à soupe de fromage râpé",1*multiplicateur_arrondi," cuillère à soupe de lait", "sel", "poivre"]
    préparation=["ÉTAPE 1\nPréchauffez votre four à 180°C (thermostat 6).\nÉTAPE 2\nDans un bol, battez les œufs avec le lait, le sel et le poivre.\nÉTAPE 3\nAjoutez les épinards hachés et le fromage râpé, mélangez bien.\nÉTAPE 4\nVersez la préparation dans des moules à muffins préalablement graissés.\nÉTAPE 5\nEnfournez pendant environ 15-20 minutes jusqu'à ce que les muffins soient dorés et gonflés.\nÉTAPE 6\nLaissez refroidir légèrement avant de démouler et de déguster."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
muffin_oeufs_epinards_fromage
def smoothie_fruits_rouges_lait_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits rouges (fraises, framboises, myrtilles)","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," tasse de fruits rouges mélangés",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les fruits rouges et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez immédiatement dans un verre et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_fruits_rouges_lait_amande
def porridge_quinoa_noix_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa","lait","noix","cannelle"]
    ingredient = [0.25*multiplicateur_arrondi," tasse de quinoa non cuit",0.5*multiplicateur_arrondi," tasse de lait d'amande",2*multiplicateur_arrondi," cuillères à soupe de noix hachées",0.5*multiplicateur_arrondi," cuillère à café de cannelle"]
    préparation=["ÉTAPE 1\nRincez le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, portez le lait d'amande à ébullition.\nÉTAPE 3\nAjoutez le quinoa rincé et réduisez le feu à doux.\nÉTAPE 4\nLaissez mijoter pendant environ 15 minutes ou jusqu'à ce que le quinoa soit tendre et que le liquide soit absorbé.\nÉTAPE 5\nRetirez du feu et laissez reposer quelques minutes.\nÉTAPE 6\nAjoutez les noix hachées et la cannelle, mélangez bien.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
porridge_quinoa_noix_cannelle
def poelee_tofu_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu","légumes variés (poivrons, champignons, oignons, courgettes, etc.)","huile d'olive","sel","poivre","ail en poudre","paprika"]
    ingredient = [200*multiplicateur_arrondi,"g de tofu ferme, coupé en dés",1*multiplicateur_arrondi," tasse de légumes variés, coupés en dés (poivrons, champignons, oignons, courgettes, etc.)",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive", "sel", "poivre", "ail en poudre", "paprika"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les dés de tofu dans la poêle et faites-les dorer de tous les côtés pendant quelques minutes.\nÉTAPE 3\nAjoutez les légumes coupés en dés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAssaisonnez avec du sel, du poivre, de l'ail en poudre et du paprika selon votre goût.\nÉTAPE 5\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
poelee_tofu_legumes
def bol_fromage_cottage_peche(multiplicateur_arrondi):
    ingredient_pour_algo = ["fromage cottage","pêche"]
    ingredient = [1*multiplicateur_arrondi," tasse de fromage cottage",1*multiplicateur_arrondi," pêche, coupée en dés"]
    préparation=["ÉTAPE 1\nDans un bol, ajoutez le fromage cottage.\nÉTAPE 2\nAjoutez les dés de pêche sur le dessus du fromage cottage.\nÉTAPE 3\nMélangez légèrement.\nÉTAPE 4\nServez immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bol_fromage_cottage_peche
def pain_grille_seigle_fromage_frais_figue(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain de seigle","fromage frais","figue"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain de seigle grillée",1*multiplicateur_arrondi," cuillère à soupe de fromage frais",1*multiplicateur_arrondi," figue, coupée en tranches fines"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain de seigle jusqu'à ce qu'elle soit croustillante.\nÉTAPE 2\nÉtalez le fromage frais sur la tranche de pain grillée.\nÉTAPE 3\nDisposez les tranches de figue sur le dessus.\nÉTAPE 4\nServez immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_grille_seigle_fromage_frais_figue
def smoothie_ananas_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["ananas","lait de coco","yaourt grec","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse d'ananas frais coupé en dés",1*multiplicateur_arrondi," tasse de lait de coco",0.5*multiplicateur_arrondi," tasse de yaourt grec",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les dés d'ananas, le lait de coco, le yaourt grec et le miel.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et crémeuse.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait de coco pour ajuster la consistance.\nÉTAPE 4\nServez dans un grand verre et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_ananas_noix_coco
def salade_avocat_tomate_citron(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat","tomate","citron","sel","poivre","huile d'olive"]
    ingredient = [1*multiplicateur_arrondi," avocat, coupé en dés",1*multiplicateur_arrondi," tomate, coupée en dés",0.5*multiplicateur_arrondi," citron, jus", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un saladier, mélangez les dés d'avocat et de tomate.\nÉTAPE 2\nArrosez de jus de citron et d'huile d'olive.\nÉTAPE 3\nSalez et poivrez selon votre goût.\nÉTAPE 4\nMélangez délicatement et servez."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
salade_avocat_tomate_citron
def bol_yogourt_graines_lin_kiwi(multiplicateur_arrondi):
    ingredient_pour_algo = ["yogourt nature","graines de lin","kiwi"]
    ingredient = [1*multiplicateur_arrondi," tasse de yogourt nature",1*multiplicateur_arrondi," cuillère à soupe de graines de lin",1*multiplicateur_arrondi," kiwi, tranché"]
    préparation=["ÉTAPE 1\nDans un bol, versez le yogourt nature.\nÉTAPE 2\nSaupoudrez de graines de lin sur le dessus.\nÉTAPE 3\nAjoutez les tranches de kiwi sur le côté du bol.\nÉTAPE 4\nServez et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bol_yogourt_graines_lin_kiwi
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs","champignons","fromage","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de champignons tranchés",1*multiplicateur_arrondi," tasse de fromage râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les champignons tranchés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient dorés.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les champignons.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage râpé sur la moitié de l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_champignons_fromage
def smoothie_banane_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","amandes","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," banane congelée",0.25*multiplicateur_arrondi," tasse d'amandes",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane congelée, les amandes, le lait d'amande et le miel.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et crémeuse.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un grand verre et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_banane_amandes
def pain_grille_ble_entier_beurre_amande_pomme(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain de blé entier","beurre d'amande","pomme"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain de blé entier grillée",1*multiplicateur_arrondi," cuillère à soupe de beurre d'amande",1*multiplicateur_arrondi," pomme, tranchée"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain de blé entier jusqu'à ce qu'elle soit croustillante.\nÉTAPE 2\nÉtalez le beurre d'amande sur la tranche de pain grillée.\nÉTAPE 3\nDisposez les tranches de pomme sur le dessus.\nÉTAPE 4\nServez et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_grille_ble_entier_beurre_amande_pomme
def smoothie_baies_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies mélangées","épinards","banane","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," tasse de baies mélangées (fraises, myrtilles, framboises, etc.)",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," banane congelée",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les baies mélangées, les épinards frais, la banane congelée et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un grand verre et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_baies_epinards
def flocons_avoine_pomme_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","pomme","noix"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," pomme, coupée en dés",1*multiplicateur_arrondi," cuillère à soupe de noix hachées"]
    préparation=["ÉTAPE 1\nFaites cuire les flocons d'avoine selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, coupez la pomme en dés et hachez les noix.\nÉTAPE 3\nUne fois les flocons d'avoine cuits, ajoutez les dés de pomme et les noix hachées.\nÉTAPE 4\nMélangez bien et servez chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
flocons_avoine_pomme_noix
def omelette_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais hachés",2*multiplicateur_arrondi," cuillères à soupe de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les épinards hachés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient légèrement ramollis.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les épinards.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage feta émietté sur la moitié de l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_feta
def pancakes_banane_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","œufs","farine","pépites de chocolat","levure chimique","huile de coco"]
    ingredient = [1*multiplicateur_arrondi," banane écrasée",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de farine",2*multiplicateur_arrondi," cuillères à soupe de pépites de chocolat",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," cuillère à soupe d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la banane écrasée et les œufs jusqu'à obtenir un mélange homogène.\nÉTAPE 2\nAjoutez la farine et la levure chimique, mélangez bien.\nÉTAPE 3\nIncorporez délicatement les pépites de chocolat.\nÉTAPE 4\nFaites chauffer un peu d'huile de coco dans une poêle à feu moyen.\nÉTAPE 5\nVersez une petite louche de pâte dans la poêle et laissez cuire jusqu'à ce que des bulles se forment à la surface.\nÉTAPE 6\nRetournez le pancake et laissez cuire encore quelques instants de l'autre côté.\nÉTAPE 7\nRépétez l'opération avec le reste de la pâte.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_banane_chocolat
def smoothie_proteines_vanille_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["protéines en poudre à la vanille","lait d'amande","amandes"]
    ingredient = [1*multiplicateur_arrondi," dose de protéines en poudre à la vanille",1*multiplicateur_arrondi," tasse de lait d'amande",0.25*multiplicateur_arrondi," tasse d'amandes"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la dose de protéines en poudre à la vanille, le lait d'amande et les amandes.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un grand verre et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteines_vanille_amandes
def tartine_pain_complet_beurre_arachide_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet","beurre d'arachide","banane"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain complet grillée",1*multiplicateur_arrondi," cuillère à soupe de beurre d'arachide",1*multiplicateur_arrondi," banane, tranchée"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain complet jusqu'à ce qu'elle soit croustillante.\nÉTAPE 2\nÉtalez le beurre d'arachide sur la tranche de pain grillée.\nÉTAPE 3\nDisposez les tranches de banane sur le dessus.\nÉTAPE 4\nServez et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
tartine_pain_complet_beurre_arachide_banane
def smoothie_fruits_rouges_proteine_lactoserum(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits rouges surgelés","protéine de lactosérum en poudre","lait","banane"]
    ingredient = [1*multiplicateur_arrondi," tasse de fruits rouges surgelés (fraises, framboises, mûres, etc.)",1*multiplicateur_arrondi," dose de protéine de lactosérum en poudre",0.5*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," banane"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les fruits rouges surgelés, la protéine de lactosérum en poudre, le lait et la banane.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_fruits_rouges_proteine_lactoserum
def porridge_vanille_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait","vanille","amandes effilées","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait",0.5*multiplicateur_arrondi," cuillère à café d'extrait de vanille",1*multiplicateur_arrondi," cuillère à soupe d'amandes effilées grillées",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    préparation=["ÉTAPE 1\nDans une casserole, portez le lait à ébullition.\nÉTAPE 2\nAjoutez les flocons d'avoine et réduisez le feu à moyen-doux.\nÉTAPE 3\nLaissez mijoter pendant environ 5 minutes, en remuant de temps en temps, jusqu'à ce que le mélange épaississe.\nÉTAPE 4\nRetirez du feu et ajoutez l'extrait de vanille.\nÉTAPE 5\nVersez dans un bol et garnissez d'amandes effilées grillées et de miel.\nÉTAPE 6\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
porridge_vanille_amandes
def tartine_pain_complet_fromage_cottage_fraises(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet","fromage cottage","fraises"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain complet grillée",1*multiplicateur_arrondi," cuillère à soupe de fromage cottage",1*multiplicateur_arrondi," fraises, tranchées"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain complet jusqu'à ce qu'elle soit croustillante.\nÉTAPE 2\nÉtalez le fromage cottage sur la tranche de pain grillée.\nÉTAPE 3\nDisposez les tranches de fraises sur le dessus.\nÉTAPE 4\nServez et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
tartine_pain_complet_fromage_cottage_fraises
def smoothie_mangue_proteine_lactoserum(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue","protéine de lactosérum en poudre","lait","banane"]
    ingredient = [1*multiplicateur_arrondi," tasse de mangue congelée en dés",1*multiplicateur_arrondi," dose de protéine de lactosérum en poudre",0.5*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," banane"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la mangue congelée, la dose de protéine de lactosérum en poudre, le lait et la banane.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_mangue_proteine_lactoserum
def gaufres_vanille_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine","sucre","levure chimique","sel","lait","œufs","beurre","extrait de vanille","myrtilles"]
    ingredient = [1*multiplicateur_arrondi," tasse de farine",2*multiplicateur_arrondi," cuillères à soupe de sucre",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",0.25*multiplicateur_arrondi," cuillère à café de sel",0.75*multiplicateur_arrondi," tasse de lait",2*multiplicateur_arrondi," œufs",2*multiplicateur_arrondi," cuillères à soupe de beurre fondu",1*multiplicateur_arrondi," cuillère à café d'extrait de vanille",1*multiplicateur_arrondi," tasse de myrtilles"]
    préparation=["ÉTAPE 1\nPréchauffez le gaufrier selon les instructions du fabricant.\nÉTAPE 2\nDans un grand bol, mélangez la farine, le sucre, la levure chimique et le sel.\nÉTAPE 3\nDans un autre bol, battez ensemble le lait, les œufs, le beurre fondu et l'extrait de vanille.\nÉTAPE 4\nIncorporez les ingrédients liquides aux ingrédients secs et mélangez jusqu'à ce que la pâte soit lisse.\nÉTAPE 5\nVersez une portion de pâte dans le gaufrier chaud et ajoutez quelques myrtilles sur le dessus.\nÉTAPE 6\nFermez le gaufrier et faites cuire les gaufres selon les instructions du fabricant.\nÉTAPE 7\nRépétez avec le reste de la pâte.\nÉTAPE 8\nServez les gaufres chaudes avec plus de myrtilles sur le dessus et du sirop d'érable si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
gaufres_vanille_myrtilles
def smoothie_banane_chocolat_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","cacao en poudre","beurre d'arachide","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," banane congelée",1*multiplicateur_arrondi," cuillère à soupe de cacao en poudre non sucré",2*multiplicateur_arrondi," cuillères à soupe de beurre d'arachide",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane congelée, le cacao en poudre, le beurre d'arachide et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_banane_chocolat_beurre_arachide
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","champignons","fromage suisse","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de champignons tranchés",1*multiplicateur_arrondi," tranche de fromage suisse râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les champignons tranchés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient dorés.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les champignons.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage suisse râpé sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_champignons_fromage_suisse
def pancakes_proteine_lactoserum_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["protéine de lactosérum en poudre","lait","œufs","farine","myrtilles"]
    ingredient = [1*multiplicateur_arrondi," dose de protéine de lactosérum en poudre",0.75*multiplicateur_arrondi," tasse de lait",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de farine",0.5*multiplicateur_arrondi," tasse de myrtilles"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la protéine de lactosérum en poudre, le lait et les œufs.\nÉTAPE 2\nAjoutez progressivement la farine tout en continuant de mélanger jusqu'à ce que la pâte soit lisse.\nÉTAPE 3\nIncorporez délicatement les myrtilles à la pâte.\nÉTAPE 4\nFaites chauffer une poêle antiadhésive à feu moyen et versez une louche de pâte.\nÉTAPE 5\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez et faites cuire l'autre côté.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez chaud avec du sirop d'érable si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_proteine_lactoserum_myrtilles
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," oz de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les épinards frais dans la poêle et faites-les sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les épinards.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage feta émietté sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_fromage_feta
def smoothie_baies_banane_graines_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies mélangées surgelées","banane","lait d'amande","graines de chia"]
    ingredient = [1*multiplicateur_arrondi," tasse de baies mélangées surgelées",1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de graines de chia"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les baies mélangées surgelées, la banane, le lait d'amande et les graines de chia.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_baies_banane_graines_chia
def toast_avocat_oeufs_poches_piment_rouge(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","avocat","pain complet","piment rouge","vinaigre de cidre","sel"]
    ingredient = [2*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," avocat",1*multiplicateur_arrondi," tranche de pain complet grillée",1*multiplicateur_arrondi," piment rouge écrasé",1*multiplicateur_arrondi," cuillère à soupe de vinaigre de cidre", "sel"]
    préparation=["ÉTAPE 1\nFaites bouillir une casserole d'eau et ajoutez-y le vinaigre de cidre.\nÉTAPE 2\nCassez chaque œuf dans une tasse séparée.\nÉTAPE 3\nRemuez l'eau pour créer un tourbillon et versez délicatement les œufs un par un dans le centre du tourbillon.\nÉTAPE 4\nLaissez cuire pendant environ 3-4 minutes pour des œufs pochés à la perfection.\nÉTAPE 5\nPendant ce temps, écrasez l'avocat dans un bol et assaisonnez-le avec du sel.\nÉTAPE 6\nÉtalez l'avocat écrasé sur la tranche de pain grillée.\nÉTAPE 7\nUne fois les œufs pochés cuits, utilisez une écumoire pour les retirer délicatement de l'eau et égouttez-les sur du papier absorbant.\nÉTAPE 8\nDisposez les œufs pochés sur l'avocat écrasé et saupoudrez de piment rouge écrasé.\nÉTAPE 9\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
toast_avocat_oeufs_poches_piment_rouge
def smoothie_banane_beurre_amande_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","beurre d'amande","épinards frais","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," banane",2*multiplicateur_arrondi," cuillères à soupe de beurre d'amande",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane, le beurre d'amande, les épinards frais et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_banane_beurre_amande_epinards
def pain_perdu_myrtilles_sirop_derable(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain rassis","œufs","lait","myrtilles","sirop d'érable"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain rassis",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," tasse de lait",0.25*multiplicateur_arrondi," tasse de myrtilles",1*multiplicateur_arrondi," cuillère à soupe de sirop d'érable"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec le lait.\nÉTAPE 2\nTrempez les tranches de pain dans le mélange d'œufs et de lait jusqu'à ce qu'elles soient bien imbibées.\nÉTAPE 3\nFaites chauffer une poêle à feu moyen et faites-y cuire les tranches de pain imbibées des deux côtés jusqu'à ce qu'elles soient dorées.\nÉTAPE 4\nServez chaud avec des myrtilles fraîches et du sirop d'érable."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_perdu_myrtilles_sirop_derable
def omelette_poivrons_fromage_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","poivrons","fromage cheddar","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," poivron rouge coupé en dés",1*multiplicateur_arrondi," oz de fromage cheddar râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les dés de poivron rouge dans la poêle et faites-les sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les poivrons.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage cheddar râpé sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_poivrons_fromage_cheddar
def smoothie_banane_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","avocat","épinards frais","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," avocat",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane, l'avocat, les épinards frais et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_banane_avocat_epinards
def omelette_tomates_sechees_fromage_chevre(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","tomates séchées","fromage de chèvre","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",2*multiplicateur_arrondi," tomates séchées, égouttées et hachées",1*multiplicateur_arrondi," oz de fromage de chèvre émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les tomates séchées hachées dans la poêle et faites-les sauter pendant quelques minutes.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les tomates séchées.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage de chèvre émietté sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_tomates_sechees_fromage_chevre
def porridge_fruits_rouges_graines_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait","fruits rouges surgelés","graines de chia","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait",0.5*multiplicateur_arrondi," tasse de fruits rouges surgelés",1*multiplicateur_arrondi," cuillère à soupe de graines de chia",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    préparation=["ÉTAPE 1\nDans une casserole, mélangez les flocons d'avoine et le lait.\nÉTAPE 2\nFaites cuire à feu moyen en remuant fréquemment jusqu'à ce que le porridge épaississe.\nÉTAPE 3\nRetirez du feu et ajoutez les fruits rouges surgelés. Laissez reposer quelques minutes pour les décongeler.\nÉTAPE 4\nIncorporez les graines de chia et le miel.\nÉTAPE 5\nRemuez bien et laissez reposer quelques minutes avant de servir.\nÉTAPE 6\nAjoutez des fruits frais supplémentaires sur le dessus si désiré et dégustez chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
porridge_fruits_rouges_graines_chia
def omelette_champignons_bacon(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","champignons","bacon","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de champignons tranchés",2*multiplicateur_arrondi," tranches de bacon, coupées en morceaux", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les champignons tranchés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient dorés.\nÉTAPE 3\nAjoutez les morceaux de bacon dans la poêle avec les champignons et faites-les cuire jusqu'à ce qu'ils soient croustillants.\nÉTAPE 4\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 5\nVersez les œufs battus dans la poêle avec les champignons et le bacon.\nÉTAPE 6\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce qu'elle soit dorée et cuite à l'intérieur.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_champignons_bacon
def smoothie_epinards_mangue_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards frais","mangue","banane","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tasse de mangue congelée",1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les épinards frais, la mangue congelée, la banane et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_epinards_mangue_banane
def bowl_flocons_avoine_fruits_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait","fruits frais","noix","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait",0.5*multiplicateur_arrondi," tasse de fruits frais au choix",0.25*multiplicateur_arrondi," tasse de noix hachées",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez les flocons d'avoine avec le lait.\nÉTAPE 2\nLaissez reposer pendant quelques minutes jusqu'à ce que les flocons d'avoine aient absorbé le lait et soient ramollis.\nÉTAPE 3\nAjoutez les fruits frais coupés et les noix hachées sur le dessus du bol.\nÉTAPE 4\nArrosez de miel pour sucrer selon votre goût.\nÉTAPE 5\nDégustez chaud ou froid."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bowl_flocons_avoine_fruits_noix
def omelette_epinards_saumon_fume(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","saumon fumé","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tranche de saumon fumé, coupée en morceaux", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les épinards frais dans la poêle et faites-les sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les épinards.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nAjoutez les morceaux de saumon fumé sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce qu'elle soit dorée et cuite à l'intérieur.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_saumon_fume
def smoothie_avocat_epinards_baies(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat","épinards frais","baies mélangées surgelées","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," avocat",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tasse de baies mélangées surgelées",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez l'avocat, les épinards frais, les baies mélangées surgelées et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_avocat_epinards_baies
def pain_grille_avocats_oeufs_poches(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet","avocat","œufs","vinaigre de cidre","sel"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain complet",1*multiplicateur_arrondi," avocat",2*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," cuillère à soupe de vinaigre de cidre",1*multiplicateur_arrondi," pincée de sel"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain complet jusqu'à ce qu'elle soit dorée et croustillante.\nÉTAPE 2\nÉcrasez l'avocat dans un bol et assaisonnez-le avec une pincée de sel.\nÉTAPE 3\nFaites bouillir une casserole d'eau et ajoutez-y le vinaigre de cidre.\nÉTAPE 4\nCassez chaque œuf dans une tasse séparée.\nÉTAPE 5\nRemuez l'eau pour créer un tourbillon et versez délicatement les œufs un par un dans le centre du tourbillon.\nÉTAPE 6\nLaissez cuire pendant environ 3-4 minutes pour des œufs pochés à la perfection.\nÉTAPE 7\nÉtalez l'avocat écrasé sur la tranche de pain grillée.\nÉTAPE 8\nUtilisez une écumoire pour retirer délicatement les œufs pochés de l'eau et égouttez-les sur du papier absorbant.\nÉTAPE 9\nPlacez les œufs pochés sur l'avocat écrasé.\nÉTAPE 10\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_grille_avocats_oeufs_poches
def omelette_epinards_champignons_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","champignons","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",0.5*multiplicateur_arrondi," tasse de champignons tranchés",1*multiplicateur_arrondi," oz de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les épinards frais et les champignons tranchés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les épinards et les champignons.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage feta émietté sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_champignons_feta
def smoothie_beurre_cacahuete_banane_flocons_avoine(multiplicateur_arrondi):
    ingredient_pour_algo = ["beurre de cacahuète","banane","flocons d'avoine","lait d'amande"]
    ingredient = [2*multiplicateur_arrondi," cuillères à soupe de beurre de cacahuète",1*multiplicateur_arrondi," banane",2*multiplicateur_arrondi," cuillères à soupe de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez le beurre de cacahuète, la banane, les flocons d'avoine et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_beurre_cacahuete_banane_flocons_avoine
def pancakes_myrtilles_sirop_derable(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine","levure chimique","sucre","sel","œufs","lait","beurre fondu","myrtilles","sirop d'érable"]
    ingredient = [1*multiplicateur_arrondi," tasse de farine tout usage",1*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," cuillère à soupe de sucre",1*multiplicateur_arrondi," pincée de sel",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," cuillère à soupe de beurre fondu",0.5*multiplicateur_arrondi," tasse de myrtilles fraîches",1*multiplicateur_arrondi," cuillère à soupe de sirop d'érable"]
    préparation=["ÉTAPE 1\nDans un grand bol, mélangez la farine, la levure chimique, le sucre et le sel.\nÉTAPE 2\nDans un autre bol, battez l'œuf avec le lait et le beurre fondu.\nÉTAPE 3\nIncorporez les ingrédients liquides aux ingrédients secs et mélangez jusqu'à ce que la pâte soit homogène.\nÉTAPE 4\nAjoutez les myrtilles fraîches à la pâte et mélangez délicatement.\nÉTAPE 5\nFaites chauffer une poêle antiadhésive à feu moyen et versez une louche de pâte pour former chaque pancake.\nÉTAPE 6\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez les pancakes et laissez cuire de l'autre côté jusqu'à ce qu'ils soient dorés.\nÉTAPE 7\nServez chaud avec du sirop d'érable et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_myrtilles_sirop_derable
def oeufs_brouilles_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","légumes mélangés","fromage râpé","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse de légumes mélangés (poivrons, oignons, champignons, etc.)",1*multiplicateur_arrondi," tasse de fromage râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les légumes mélangés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les légumes.\nÉTAPE 5\nLaissez cuire en remuant continuellement jusqu'à ce que les œufs soient brouillés et bien cuits.\nÉTAPE 6\nSaupoudrez le fromage râpé sur les œufs brouillés et mélangez jusqu'à ce que le fromage soit fondu.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
oeufs_brouilles_legumes_fromage
def smoothie_banane_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","avocat","épinards frais","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," avocat",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane, l'avocat, les épinards frais et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_banane_avocat_epinards
def gaufres_proteinees_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'amande","poudre protéinée","œufs","lait d'amande","levure chimique","myrtilles"]
    ingredient = [1*multiplicateur_arrondi," tasse de farine d'amande",1*multiplicateur_arrondi," cuillère à soupe de poudre protéinée",2*multiplicateur_arrondi," œufs",0.25*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",0.5*multiplicateur_arrondi," tasse de myrtilles fraîches"]
    préparation=["ÉTAPE 1\nPréchauffez votre gaufrier selon les instructions du fabricant.\nÉTAPE 2\nDans un bol, mélangez la farine d'amande, la poudre protéinée et la levure chimique.\nÉTAPE 3\nAjoutez les œufs et le lait d'amande au mélange sec et mélangez jusqu'à obtenir une pâte lisse.\nÉTAPE 4\nIncorporez délicatement les myrtilles à la pâte.\nÉTAPE 5\nVersez une portion de pâte dans le gaufrier chaud et faites cuire selon les instructions du fabricant jusqu'à ce que les gaufres soient dorées et croustillantes.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez chaud avec du sirop d'érable ou du yaourt grec et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
gaufres_proteinees_myrtilles
def flocons_avoine_banane_cacahuete_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","banane","beurre de cacahuète","graines de chia","lait d'amande","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",0.5*multiplicateur_arrondi," banane écrasée",1*multiplicateur_arrondi," cuillère à soupe de beurre de cacahuète",1*multiplicateur_arrondi," cuillère à soupe de graines de chia",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans une casserole, combinez les flocons d'avoine, la banane écrasée, le beurre de cacahuète, les graines de chia et le lait d'amande.\nÉTAPE 2\nFaites cuire à feu moyen-doux pendant environ 5 à 7 minutes, en remuant fréquemment, jusqu'à ce que les flocons d'avoine soient cuits et que le mélange épaississe.\nÉTAPE 3\nRetirez du feu et versez dans un bol.\nÉTAPE 4\nSi désiré, arrosez de miel pour sucrer légèrement.\nÉTAPE 5\nServez chaud et dégustez."]
    temps_de_préparation = 2
    temps_de_cuisson = 7
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
flocons_avoine_banane_cacahuete_chia
def smoothie_proteine_myrtilles_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles","yaourt grec","poudre protéinée","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," tasse de yaourt grec",1*multiplicateur_arrondi," cuillère à soupe de poudre protéinée",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les myrtilles, le yaourt grec, la poudre protéinée, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à ce que le mélange soit lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajustez la consistance en ajoutant plus de lait d'amande.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteine_myrtilles_yaourt_grec
def toast_avocat_oeuf_plat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet","avocat","œufs","huile d'olive","sel","poivre"]
    ingredient = [1*multiplicateur_arrondi," tranche de pain complet",0.5*multiplicateur_arrondi," avocat écrasé",1*multiplicateur_arrondi," œuf",1*multiplicateur_arrondi," cuillère à café d'huile d'olive", "sel", "poivre"]
    préparation=["ÉTAPE 1\nFaites griller la tranche de pain complet jusqu'à ce qu'elle soit dorée et croustillante.\nÉTAPE 2\nÉtalez l'avocat écrasé sur la tranche de pain grillée.\nÉTAPE 3\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 4\nCassez l'œuf dans la poêle et faites cuire jusqu'à ce que le blanc soit pris et le jaune encore légèrement coulant.\nÉTAPE 5\nPlacez l'œuf cuit sur l'avocat écrasé.\nÉTAPE 6\nSalez et poivrez selon votre goût.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
toast_avocat_oeuf_plat
def omelette_epinards_poivrons_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","poivrons","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",0.5*multiplicateur_arrondi," tasse de poivrons coupés en dés",1*multiplicateur_arrondi," oz de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à soupe d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjoutez les épinards frais et les poivrons dans la poêle et faites-les sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus dans la poêle avec les épinards et les poivrons.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les bords commencent à se solidifier.\nÉTAPE 6\nSaupoudrez le fromage feta émietté sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques minutes jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServez chaud et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_poivrons_feta
def smoothie_mangue_banane_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue","banane","graines de chia","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," tasse de mangue coupée en dés",1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," cuillère à soupe de graines de chia",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la mangue, la banane, les graines de chia et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_mangue_banane_chia
def pancakes_chocolat_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine","levure chimique","sucre","sel","œufs","lait","beurre fondu","pépites de chocolat","noix hachées"]
    ingredient = [1*multiplicateur_arrondi," tasse de farine tout usage",1*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," cuillère à soupe de sucre",1*multiplicateur_arrondi," pincée de sel",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," cuillère à soupe de beurre fondu",1*multiplicateur_arrondi," tasse de pépites de chocolat",1*multiplicateur_arrondi," tasse de noix hachées"]
    préparation=["ÉTAPE 1\nDans un grand bol, mélangez la farine, la levure chimique, le sucre et le sel.\nÉTAPE 2\nDans un autre bol, battez l'œuf avec le lait et le beurre fondu.\nÉTAPE 3\nIncorporez les ingrédients liquides aux ingrédients secs et mélangez jusqu'à ce que la pâte soit homogène.\nÉTAPE 4\nAjoutez les pépites de chocolat et les noix hachées à la pâte et mélangez délicatement.\nÉTAPE 5\nFaites chauffer une poêle antiadhésive à feu moyen et versez une louche de pâte pour former chaque pancake.\nÉTAPE 6\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez les pancakes et laissez cuire de l'autre côté jusqu'à ce qu'ils soient dorés.\nÉTAPE 7\nServez chaud avec du sirop d'érable ou du yaourt grec et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_chocolat_noix
def pain_perdu_fruits_rouges_sirop_derable(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain","œufs","lait","cannelle","fruits rouges","sirop d'érable"]
    ingredient = [2*multiplicateur_arrondi," tranches de pain épaisse",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," cuillère à café de cannelle",1*multiplicateur_arrondi," tasse de fruits rouges frais ou surgelés",1*multiplicateur_arrondi," cuillère à soupe de sirop d'érable"]
    préparation=["ÉTAPE 1\nDans un bol peu profond, battez les œufs avec le lait et la cannelle.\nÉTAPE 2\nTrempez rapidement chaque tranche de pain dans le mélange d'œufs, en veillant à bien les enrober des deux côtés.\nÉTAPE 3\nFaites chauffer une poêle antiadhésive à feu moyen et faites-y cuire les tranches de pain trempées jusqu'à ce qu'elles soient dorées et croustillantes des deux côtés.\nÉTAPE 4\nServez chaud, garni de fruits rouges et arrosé de sirop d'érable.\nÉTAPE 5\nDégustez immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_perdu_fruits_rouges_sirop_derable
def bowl_cake_vanille_fruits_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","poudre protéinée à la vanille","levure chimique","œuf","lait d'amande","extrait de vanille","fruits frais (fraises, myrtilles, etc.)"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," cuillère à soupe de poudre protéinée à la vanille",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," œuf",0.25*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," cuillère à café d'extrait de vanille",1*multiplicateur_arrondi," tasse de fruits frais (fraises, myrtilles, etc.)"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez les flocons d'avoine, la poudre protéinée à la vanille et la levure chimique.\nÉTAPE 2\nAjoutez l'œuf, le lait d'amande et l'extrait de vanille au mélange sec et mélangez bien jusqu'à obtenir une pâte lisse.\nÉTAPE 3\nVersez la pâte dans un bol allant au micro-ondes et lissez le dessus.\nÉTAPE 4\nFaites cuire au micro-ondes pendant 2 à 3 minutes, ou jusqu'à ce que le cake soit bien gonflé et cuit.\nÉTAPE 5\nGarnissez de fruits frais et dégustez chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bowl_cake_vanille_fruits_frais
def smoothie_proteines_epinards_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["protéines en poudre","épinards","banane","amandes","lait d'amande"]
    ingredient = [1*multiplicateur_arrondi," cuillère à soupe de protéines en poudre (saveur au choix)",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," banane",0.25*multiplicateur_arrondi," tasse d'amandes",1*multiplicateur_arrondi," tasse de lait d'amande"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les protéines en poudre, les épinards frais, la banane, les amandes et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteines_epinards_amandes
def smoothie_proteine_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue","protéine en poudre à la vanille","lait de coco","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de mangue fraîche ou surgelée",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.5*multiplicateur_arrondi," tasse de lait de coco",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la mangue, la protéine en poudre à la vanille, le lait de coco et le lait d'amande.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi désiré, ajoutez un peu de miel pour sucrer légèrement.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteine_mangue_noix_coco
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards frais","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",1*multiplicateur_arrondi," cuillère à soupe de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjoutez les épinards frais dans la poêle et faites-les cuire jusqu'à ce qu'ils soient légèrement ramollis.\nÉTAPE 4\nVersez les œufs battus sur les épinards dans la poêle.\nÉTAPE 5\nLaissez cuire jusqu'à ce que les œufs soient presque pris, puis saupoudrez de fromage feta émietté sur un côté de l'omelette.\nÉTAPE 6\nPliez l'omelette en deux pour recouvrir le fromage feta et continuez à cuire jusqu'à ce que les œufs soient complètement pris et le fromage fondu.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_fromage_feta
def pancakes_patate_douce(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce","farine d'avoine","lait d'amande","œuf","cannelle","levure chimique","huile de coco"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de purée de patate douce cuite",0.25*multiplicateur_arrondi," tasse de farine d'avoine",0.25*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," cuillère à café de cannelle",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," cuillère à café d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la purée de patate douce cuite, la farine d'avoine, le lait d'amande, l'œuf, la cannelle et la levure chimique jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer l'huile de coco dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nVersez une louche de pâte dans la poêle chaude pour former chaque pancake.\nÉTAPE 4\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez les pancakes et laissez cuire de l'autre côté jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 5\nServez chaud avec du sirop d'érable ou de la compote de fruits et dégustez."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_patate_douce
def porridge_banane_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait d'amande","banane","amandes effilées","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," banane écrasée",1*multiplicateur_arrondi," cuillère à soupe d'amandes effilées",1*multiplicateur_arrondi," cuillère à café de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans une casserole, combinez les flocons d'avoine et le lait d'amande.\nÉTAPE 2\nPortez à ébullition, puis réduisez le feu et laissez mijoter en remuant de temps en temps jusqu'à ce que le mélange épaississe.\nÉTAPE 3\nRetirez du feu et incorporez la banane écrasée.\nÉTAPE 4\nVersez le porridge dans un bol et garnissez d'amandes effilées.\nÉTAPE 5\nSi désiré, arrosez de miel pour sucrer légèrement.\nÉTAPE 6\nDégustez chaud."]
    temps_de_préparation = 2
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
porridge_banane_amandes
def oeufs_brouilles_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","lait","poivrons","oignon","épinards","sel","poivre","huile d'olive"]
    ingredient = [2*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," cuillère à soupe de lait",0.5*multiplicateur_arrondi," tasse de poivrons coupés en dés",0.25*multiplicateur_arrondi," tasse d'oignon haché",0.5*multiplicateur_arrondi," tasse d'épinards frais", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du lait, du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjoutez les poivrons et les oignons dans la poêle et faites-les sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAjoutez les épinards dans la poêle et faites-les cuire jusqu'à ce qu'ils soient légèrement ramollis.\nÉTAPE 5\nVersez les œufs battus dans la poêle et remuez doucement jusqu'à ce qu'ils soient brouillés et cuits à votre goût.\nÉTAPE 6\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
oeufs_brouilles_legumes
def smoothie_baies_graines_lin(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies mélangées (fraises, framboises, myrtilles)","banane","graines de lin","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de baies mélangées fraîches ou surgelées",0.5*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," cuillère à soupe de graines de lin",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les baies mélangées, la banane, les graines de lin, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à ce que le mélange soit lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_baies_graines_lin
def smoothie_proteine_banane_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","protéine en poudre à la vanille","beurre d'arachide","lait d'amande","glace"]
    ingredient = [1*multiplicateur_arrondi," banane mûre",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",1*multiplicateur_arrondi," cuillère à soupe de beurre d'arachide",1*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," tasse de glace"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez la banane, la protéine en poudre à la vanille, le beurre d'arachide, le lait d'amande et la glace.\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteine_banane_beurre_arachide
def crepes_farine_avoine_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine","lait d'amande","œuf","banane","myrtilles","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de farine d'avoine",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," banane écrasée",0.25*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la farine d'avoine, le lait d'amande et l'œuf jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer une poêle antiadhésive à feu moyen et versez une louche de pâte dans la poêle chaude.\nÉTAPE 3\nCuire la crêpe jusqu'à ce qu'elle soit dorée des deux côtés.\nÉTAPE 4\nRépétez l'opération avec le reste de la pâte pour former plusieurs crêpes.\nÉTAPE 5\nGarnissez les crêpes de banane écrasée et de myrtilles.\nÉTAPE 6\nSi désiré, arrosez de miel pour sucrer légèrement.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
crepes_farine_avoine_fruits
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","champignons","fromage suisse","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de champignons tranchés",0.25*multiplicateur_arrondi," tasse de fromage suisse râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjoutez les champignons tranchés dans la poêle et faites-les sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 4\nVersez les œufs battus dans la poêle et remuez doucement jusqu'à ce qu'ils soient brouillés et cuits à votre goût.\nÉTAPE 5\nJuste avant que les œufs ne soient complètement cuits, saupoudrez de fromage suisse râpé sur un côté de l'omelette.\nÉTAPE 6\nPliez l'omelette en deux pour recouvrir le fromage et continuez à cuire jusqu'à ce que le fromage soit fondu.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_champignons_fromage_suisse
def flocons_avoine_fruits_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait d'amande","banane","myrtilles","noix","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," banane en tranches",0.25*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",0.25*multiplicateur_arrondi," tasse de noix concassées",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans une casserole, combinez les flocons d'avoine et le lait d'amande.\nÉTAPE 2\nPortez à ébullition, puis réduisez le feu et laissez mijoter en remuant de temps en temps jusqu'à ce que le mélange épaississe.\nÉTAPE 3\nRetirez du feu et incorporez les tranches de banane et les myrtilles.\nÉTAPE 4\nVersez le porridge dans un bol et garnissez de noix concassées.\nÉTAPE 5\nSi désiré, arrosez de miel pour sucrer légèrement.\nÉTAPE 6\nDégustez chaud."]
    temps_de_préparation = 2
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
flocons_avoine_fruits_noix
def smoothie_proteine_vanille_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec","protéine en poudre à la vanille","banane","lait d'amande","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de yaourt grec",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",1*multiplicateur_arrondi," banane mûre",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez le yaourt grec, la protéine en poudre à la vanille, la banane, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_proteine_vanille_yaourt_grec
def crepes_proteinees_farine_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'amande","protéine en poudre à la vanille","lait d'amande","œuf","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de farine d'amande",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," œuf",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la farine d'amande, la protéine en poudre à la vanille, le lait d'amande et l'œuf jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer une poêle antiadhésive à feu moyen et versez une louche de pâte dans la poêle chaude.\nÉTAPE 3\nCuire la crêpe jusqu'à ce qu'elle soit dorée des deux côtés.\nÉTAPE 4\nRépétez l'opération avec le reste de la pâte pour former plusieurs crêpes.\nÉTAPE 5\nSi désiré, arrosez de miel pour sucrer légèrement.\nÉTAPE 6\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
crepes_proteinees_farine_amande
def pain_grille_avocat_oeufs(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet","avocat","œufs","sel","poivre","piment rouge en flocons","huile d'olive"]
    ingredient = [2*multiplicateur_arrondi," tranches de pain complet",1*multiplicateur_arrondi," avocat mûr",2*multiplicateur_arrondi," œufs", "sel", "poivre", "piment rouge en flocons",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites griller les tranches de pain complet jusqu'à ce qu'elles soient croustillantes.\nÉTAPE 2\nPendant ce temps, écrasez l'avocat dans un bol avec du sel, du poivre et des piments rouges en flocons selon votre goût.\nÉTAPE 3\nFaites chauffer l'huile d'olive dans une poêle et faites cuire les œufs au plat ou selon votre préférence.\nÉTAPE 4\nÉtalez la purée d'avocat sur les tranches de pain grillé et déposez les œufs cuits dessus.\nÉTAPE 5\nAjoutez du sel, du poivre et des piments rouges en flocons supplémentaires si désiré.\nÉTAPE 6\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_grille_avocat_oeufs
def bowl_cake_proteine_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","protéine en poudre à la vanille","banane","œuf","lait d'amande","levure chimique","miel"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.5*multiplicateur_arrondi," banane écrasée",1*multiplicateur_arrondi," œuf",0.25*multiplicateur_arrondi," tasse de lait d'amande",0.25*multiplicateur_arrondi," cuillère à café de levure chimique",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez les flocons d'avoine, la protéine en poudre à la vanille, la banane écrasée, l'œuf, le lait d'amande, la levure chimique et le miel (si utilisé).\nÉTAPE 2\nRemuez jusqu'à obtenir une pâte homogène.\nÉTAPE 3\nFaites chauffer au micro-ondes pendant 2-3 minutes, ou jusqu'à ce que le cake soit gonflé et ferme au toucher.\nÉTAPE 4\nLaissez refroidir légèrement, puis démoulez dans un bol.\nÉTAPE 5\nGarnissez de fruits frais, de noix ou de graines si désiré.\nÉTAPE 6\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bowl_cake_proteine_vanille
def smoothie_epinards_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards frais","mangue","banane","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse d'épinards frais",0.5*multiplicateur_arrondi," tasse de mangue congelée",0.5*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez les épinards frais, la mangue, la banane, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_epinards_mangue
def toasts_avocat_saumon(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet","avocat","saumon fumé","citron","sel","poivre"]
    ingredient = [2*multiplicateur_arrondi," tranches de pain complet",1*multiplicateur_arrondi," avocat mûr",50*multiplicateur_arrondi," g de saumon fumé",1*multiplicateur_arrondi," quartier de citron", "sel", "poivre"]
    préparation=["ÉTAPE 1\nFaites griller les tranches de pain complet jusqu'à ce qu'elles soient croustillantes.\nÉTAPE 2\nPendant ce temps, écrasez l'avocat dans un bol avec du jus de citron, du sel et du poivre selon votre goût.\nÉTAPE 3\nTartinez généreusement les tranches de pain grillé avec la purée d'avocat.\nÉTAPE 4\nDéposez des tranches de saumon fumé sur le dessus.\nÉTAPE 5\nAjoutez un filet de jus de citron supplémentaire et un peu de poivre noir moulu si désiré.\nÉTAPE 6\nServez immédiatement et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
toasts_avocat_saumon
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards frais","fromage feta","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",30*multiplicateur_arrondi," g de fromage feta émietté", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjoutez les épinards frais dans la poêle et faites-les sauter jusqu'à ce qu'ils soient légèrement ramollis.\nÉTAPE 4\nVersez les œufs battus dans la poêle et remuez doucement jusqu'à ce qu'ils soient brouillés et cuits à votre goût.\nÉTAPE 5\nJuste avant que les œufs ne soient complètement cuits, saupoudrez de fromage feta émietté sur un côté de l'omelette.\nÉTAPE 6\nPliez l'omelette en deux pour recouvrir le fromage et continuez à cuire jusqu'à ce que le fromage soit fondu.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_fromage_feta
def smoothie_baies_proteine(multiplicateur_arrondi):
    ingredient_pour_algo = ["mélange de baies (fraises, myrtilles, framboises)","protéine en poudre à la vanille","banane","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de mélange de baies (fraises, myrtilles, framboises)",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.5*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, ajoutez le mélange de baies, la protéine en poudre à la vanille, la banane, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_baies_proteine
def pancakes_banane_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","œufs","flocons d'avoine","pépites de chocolat","cannelle","huile de coco"]
    ingredient = [1*multiplicateur_arrondi," banane mûre écrasée",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de flocons d'avoine",2*multiplicateur_arrondi," cuillères à soupe de pépites de chocolat", "pincée de cannelle",1*multiplicateur_arrondi," cuillère à café d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la banane écrasée et les œufs jusqu'à obtenir une texture lisse.\nÉTAPE 2\nAjoutez les flocons d'avoine, les pépites de chocolat et la cannelle au mélange et mélangez bien.\nÉTAPE 3\nFaites chauffer l'huile de coco dans une poêle antiadhésive à feu moyen.\nÉTAPE 4\nVersez environ 1/4 de tasse de pâte à pancakes dans la poêle chaude et faites cuire jusqu'à ce que des bulles se forment à la surface.\nÉTAPE 5\nRetournez les pancakes et faites cuire de l'autre côté jusqu'à ce qu'ils soient dorés et cuits à votre goût.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez chaud et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_banane_chocolat
def smoothie_avoine_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","myrtilles","banane","yaourt grec","lait d'amande","miel"]
    ingredient = [0.25*multiplicateur_arrondi," tasse de flocons d'avoine",0.5*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",0.5*multiplicateur_arrondi," banane",0.5*multiplicateur_arrondi," tasse de yaourt grec",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, combinez les flocons d'avoine, les myrtilles, la banane, le yaourt grec, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_avoine_myrtilles
def sandwich_oeufs_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","avocat","tranches de pain complet","tomate","feuilles de laitue","sel","poivre","huile d'olive"]
    ingredient = [2*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," avocat mûr",2*multiplicateur_arrondi," tranches de pain complet",1*multiplicateur_arrondi," tomate en tranches",1*multiplicateur_arrondi," feuilles de laitue", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nFaites cuire les œufs selon votre préférence (brouillés, au plat, etc.).\nÉTAPE 2\nÉcrasez l'avocat dans un bol et assaisonnez avec du sel et du poivre selon votre goût.\nÉTAPE 3\nFaites griller les tranches de pain complet.\nÉTAPE 4\nSur une tranche de pain grillé, étalez la purée d'avocat, puis disposez les œufs cuits, les tranches de tomate et les feuilles de laitue.\nÉTAPE 5\nAssaisonnez avec un peu d'huile d'olive, de sel et de poivre supplémentaires si désiré.\nÉTAPE 6\nCouvrir avec la deuxième tranche de pain grillé pour former un sandwich.\nÉTAPE 7\nServez immédiatement et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
sandwich_oeufs_avocat
def smoothie_bowl_banane_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane","beurre d'amande","lait d'amande","flocons d'avoine","miel","graines de chia","amandes effilées"]
    ingredient = [1*multiplicateur_arrondi," banane congelée",2*multiplicateur_arrondi," cuillères à soupe de beurre d'amande",0.5*multiplicateur_arrondi," tasse de lait d'amande",0.25*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," cuillère à soupe de miel",1*multiplicateur_arrondi," cuillère à soupe de graines de chia",1*multiplicateur_arrondi," cuillère à soupe d'amandes effilées"]
    préparation=["ÉTAPE 1\nDans un blender, mélangez la banane congelée, le beurre d'amande, le lait d'amande, les flocons d'avoine et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2\nVersez le smoothie dans un bol.\nÉTAPE 3\nGarnissez de graines de chia et d'amandes effilées.\nÉTAPE 4\nAjoutez des tranches de banane, si désiré.\nÉTAPE 5\nServez immédiatement et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_bowl_banane_beurre_amande
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","champignons","fromage suisse","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse de champignons tranchés",30*multiplicateur_arrondi," g de fromage suisse râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjoutez les champignons tranchés dans la poêle et faites-les cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 4\nVersez les œufs battus sur les champignons dans la poêle.\nÉTAPE 5\nSaupoudrez le fromage suisse râpé sur les œufs.\nÉTAPE 6\nLaissez cuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servez chaud.\nÉTAPE 8\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_champignons_fromage_suisse
def smoothie_bananes_epinards_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes","épinards frais","beurre d'arachide","lait d'amande","miel"]
    ingredient = [2*multiplicateur_arrondi," bananes congelées",1*multiplicateur_arrondi," tasse d'épinards frais",2*multiplicateur_arrondi," cuillères à soupe de beurre d'arachide",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, combinez les bananes congelées, les épinards frais, le beurre d'arachide, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un bol et dégustez."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_bananes_epinards_beurre_arachide
def pain_perdu_myrtilles_sirop_erable(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet","œufs","lait","myrtilles","sirop d'érable","cannelle","beurre"]
    ingredient = [2*multiplicateur_arrondi," tranches de pain complet épaisses",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de lait",0.5*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," cuillère à soupe de sirop d'érable (plus pour servir)", "pincée de cannelle",1*multiplicateur_arrondi," cuillère à soupe de beurre"]
    préparation=["ÉTAPE 1\nDans un bol peu profond, battez les œufs avec le lait, la cannelle et une cuillère à soupe de sirop d'érable.\nÉTAPE 2\nTrempez chaque tranche de pain dans le mélange d'œufs, en les retournant pour bien les enrober.\nÉTAPE 3\nFaites fondre le beurre dans une poêle à feu moyen.\nÉTAPE 4\nFaites cuire les tranches de pain trempées dans le beurre fondu jusqu'à ce qu'elles soient dorées des deux côtés.\nÉTAPE 5\nServez les tranches de pain perdu chaudes avec les myrtilles et un filet supplémentaire de sirop d'érable.\nÉTAPE 6\nDégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pain_perdu_myrtilles_sirop_erable
def omelette_bacon_champignons(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","bacon","champignons","fromage râpé","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",2*multiplicateur_arrondi," tranches de bacon",1*multiplicateur_arrondi," tasse de champignons tranchés",20*multiplicateur_arrondi," g de fromage râpé", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans une poêle, faites cuire les tranches de bacon jusqu'à ce qu'elles soient croustillantes.\nÉTAPE 2\nRetirez le bacon de la poêle et réservez-le sur une assiette recouverte de papier absorbant pour égoutter l'excès de graisse.\nÉTAPE 3\nDans la même poêle, ajoutez un peu d'huile d'olive si nécessaire, puis ajoutez les champignons tranchés et faites-les sauter jusqu'à ce qu'ils soient dorés.\nÉTAPE 4\nBattre les œufs dans un bol et assaisonner avec du sel et du poivre.\nÉTAPE 5\nVersez les œufs battus dans la poêle avec les champignons et faites cuire jusqu'à ce qu'ils soient presque pris.\nÉTAPE 6\nAjoutez le bacon cuit émietté sur un côté des œufs et saupoudrez de fromage râpé sur le dessus.\nÉTAPE 7\nPliez l'omelette en deux et laissez cuire encore quelques instants jusqu'à ce que le fromage soit fondu et que les œufs soient complètement cuits.\nÉTAPE 8\nServez chaud et dégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_bacon_champignons
def yaourt_grec_fruits_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec","fraises","banane","noix","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de yaourt grec",0.5*multiplicateur_arrondi," tasse de fraises tranchées",0.5*multiplicateur_arrondi," banane tranchée",1*multiplicateur_arrondi," cuillère à soupe de noix hachées",1*multiplicateur_arrondi," cuillère à café de miel"]
    préparation=["ÉTAPE 1\nDans un bol, versez le yaourt grec.\nÉTAPE 2\nGarnissez de fraises tranchées, de rondelles de banane et de noix hachées.\nÉTAPE 3\nArrosez de miel pour sucrer légèrement.\nÉTAPE 4\nServez immédiatement et dégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
yaourt_grec_fruits_noix
def crepes_farine_avoine_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine","œufs","lait","myrtilles","huile de coco"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de farine d'avoine",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de lait",0.25*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," cuillère à café d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez la farine d'avoine, les œufs et le lait jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer l'huile de coco dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nVersez une louche de pâte à crêpe dans la poêle chaude et étalez-la en une fine couche.\nÉTAPE 4\nDéposez quelques myrtilles sur le dessus de la crêpe.\nÉTAPE 5\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez la crêpe et faites cuire de l'autre côté jusqu'à ce qu'elle soit dorée.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez les crêpes chaudes avec des myrtilles supplémentaires et du sirop d'érable si désiré.\nÉTAPE 8\nDégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
crepes_farine_avoine_myrtilles
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue","lait de coco","yaourt grec","miel","graines de chia"]
    ingredient = [1*multiplicateur_arrondi," mangue mûre coupée en dés",0.5*multiplicateur_arrondi," tasse de lait de coco",0.5*multiplicateur_arrondi," tasse de yaourt grec",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)",1*multiplicateur_arrondi," cuillère à soupe de graines de chia"]
    préparation=["ÉTAPE 1\nDans un blender, combinez la mangue coupée en dés, le lait de coco, le yaourt grec et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait de coco pour ajuster la consistance.\nÉTAPE 4\nVersez le smoothie dans un bol et garnissez de graines de chia.\nÉTAPE 5\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_mangue_noix_coco
def toasts_avocat_oeufs_poches(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","avocat","tranches de pain complet","vinaigre","sel","poivre","piment rouge en flocons"]
    ingredient = [2*multiplicateur_arrondi," œufs extra-frais",1*multiplicateur_arrondi," avocat mûr",2*multiplicateur_arrondi," tranches de pain complet",1*multiplicateur_arrondi," cuillère à soupe de vinaigre blanc", "sel", "poivre",1*multiplicateur_arrondi," pincée de piment rouge en flocons"]
    préparation=["ÉTAPE 1\nFaites chauffer une casserole d'eau à feu moyen-vif jusqu'à ce qu'elle frémisse.\nÉTAPE 2\nCassez un œuf dans un petit bol ou une tasse.\nÉTAPE 3\nAjoutez le vinaigre à l'eau frémissante et remuez avec une cuillère pour créer un tourbillon.\nÉTAPE 4\nVersez délicatement l'œuf dans le tourbillon d'eau et laissez cuire pendant 3 à 4 minutes pour un œuf poché parfaitement cuit.\nÉTAPE 5\nRetirez l'œuf poché de l'eau avec une cuillère à égoutter et égouttez-le sur du papier absorbant.\nÉTAPE 6\nFaites griller les tranches de pain complet.\nÉTAPE 7\nÉcrasez l'avocat mûr et étalez-le sur les tranches de pain grillé.\nÉTAPE 8\nDisposez l'œuf poché sur l'avocat écrasé.\nÉTAPE 9\nAssaisonnez de sel, de poivre et de piment rouge en flocons.\nÉTAPE 10\nServez immédiatement et dégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
toasts_avocat_oeufs_poches
def smoothie_fraises_banane_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises","banane","beurre d'amande","lait d'amande","miel"]
    ingredient = [1*multiplicateur_arrondi," tasse de fraises fraîches ou surgelées",1*multiplicateur_arrondi," banane",2*multiplicateur_arrondi," cuillères à soupe de beurre d'amande",0.5*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de miel (facultatif)"]
    préparation=["ÉTAPE 1\nDans un blender, combinez les fraises, la banane, le beurre d'amande, le lait d'amande et le miel (si utilisé).\nÉTAPE 2\nMixez jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nSi nécessaire, ajoutez un peu plus de lait d'amande pour ajuster la consistance.\nÉTAPE 4\nServez dans un grand verre et dégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_fraises_banane_beurre_amande
def oeufs_brouilles_saumon_fume_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","saumon fumé","avocat","sel","poivre","huile d'olive"]
    ingredient = [3*multiplicateur_arrondi," œufs",50*multiplicateur_arrondi," g de saumon fumé",1*multiplicateur_arrondi," avocat", "sel", "poivre",1*multiplicateur_arrondi," cuillère à café d'huile d'olive"]
    préparation=["ÉTAPE 1\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 2\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nVersez les œufs battus dans la poêle chaude et remuez doucement avec une spatule jusqu'à ce qu'ils soient presque cuits.\nÉTAPE 4\nAjoutez le saumon fumé coupé en lamelles sur les œufs brouillés.\nÉTAPE 5\nLaissez cuire encore quelques instants jusqu'à ce que les œufs soient cuits à votre goût et que le saumon soit légèrement réchauffé.\nÉTAPE 6\nRetirez du feu et servez immédiatement avec des tranches d'avocat sur le dessus.\nÉTAPE 7\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
oeufs_brouilles_saumon_fume_avocat
def bowlcake_vanille_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","œuf","banane","lait","levure chimique","vanille liquide","amandes effilées"]
    ingredient = [3*multiplicateur_arrondi," cuillères à soupe de flocons d'avoine",1*multiplicateur_arrondi," œuf",0.5*multiplicateur_arrondi," banane écrasée",2*multiplicateur_arrondi," cuillères à soupe de lait",0.5*multiplicateur_arrondi," cuillère à café de levure chimique",0.5*multiplicateur_arrondi," cuillère à café de vanille liquide",1*multiplicateur_arrondi," cuillère à soupe d'amandes effilées"]
    préparation=["ÉTAPE 1\nDans un bol, mélangez les flocons d'avoine, l'œuf, la banane écrasée, le lait, la levure chimique et la vanille liquide.\nÉTAPE 2\nVersez le mélange dans un bol allant au micro-ondes et lissez le dessus avec une spatule.\nÉTAPE 3\nSaupoudrez d'amandes effilées sur le dessus.\nÉTAPE 4\nFaites cuire au micro-ondes pendant 2 à 3 minutes, jusqu'à ce que le bowlcake soit gonflé et ferme au toucher.\nÉTAPE 5\nLaissez refroidir quelques instants, puis démoulez le bowlcake sur une assiette.\nÉTAPE 6\nServez avec des toppings supplémentaires si désiré, comme des fruits frais, du yaourt grec ou du sirop d'érable.\nÉTAPE 7\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
bowlcake_vanille_amandes
def crepes_proteinees_vanille_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","œufs","lait d'amande","protéine en poudre à la vanille","myrtilles","huile de coco"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",2*multiplicateur_arrondi," œufs",0.25*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.25*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," cuillère à café d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un blender, mélangez les flocons d'avoine, les œufs, le lait d'amande et la protéine en poudre à la vanille jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer une poêle antiadhésive à feu moyen et ajoutez un peu d'huile de coco pour graisser légèrement la poêle.\nÉTAPE 3\nVersez environ 1/4 tasse de pâte à crêpes dans la poêle chaude.\nÉTAPE 4\nDéposez quelques myrtilles sur le dessus de la crêpe.\nÉTAPE 5\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez la crêpe et faites cuire de l'autre côté jusqu'à ce qu'elle soit dorée.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez les crêpes chaudes avec des myrtilles supplémentaires et du sirop d'érable si désiré.\nÉTAPE 8\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
crepes_proteinees_vanille_myrtilles
def smoothie_bowl_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue","lait de coco","banane","graines de chia","noix de coco râpée"]
    ingredient = [1*multiplicateur_arrondi," mangue congelée en dés",0.5*multiplicateur_arrondi," tasse de lait de coco",1*multiplicateur_arrondi," banane",1*multiplicateur_arrondi," cuillère à soupe de graines de chia",1*multiplicateur_arrondi," cuillère à soupe de noix de coco râpée"]
    préparation=["ÉTAPE 1\nDans un blender, combinez la mangue, le lait de coco et la banane jusqu'à obtenir une texture lisse et crémeuse.\nÉTAPE 2\nVersez le smoothie dans un bol.\nÉTAPE 3\nSaupoudrez de graines de chia et de noix de coco râpée sur le dessus.\nÉTAPE 4\nAjoutez des tranches de mangue fraîche ou d'autres fruits pour garnir.\nÉTAPE 5\nDégustez à la cuillère !"]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_bowl_mangue_noix_coco
def omelette_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs","épinards","fromage feta ou de chèvre","huile d'olive","sel","poivre"]
    ingredient = [3*multiplicateur_arrondi," œufs",1*multiplicateur_arrondi," tasse d'épinards frais",0.25*multiplicateur_arrondi," tasse de fromage feta ou de chèvre émietté",1*multiplicateur_arrondi," cuillère à café d'huile d'olive", "sel", "poivre"]
    préparation=["ÉTAPE 1\nFaites chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 2\nAjoutez les épinards dans la poêle et faites-les cuire jusqu'à ce qu'ils soient légèrement fanés.\nÉTAPE 3\nDans un bol, battez les œufs avec du sel et du poivre.\nÉTAPE 4\nVersez les œufs battus sur les épinards dans la poêle.\nÉTAPE 5\nLaissez cuire l'omelette pendant quelques minutes jusqu'à ce qu'elle soit presque prise.\nÉTAPE 6\nSaupoudrez le fromage émietté sur la moitié de l'omelette.\nÉTAPE 7\nUtilisez une spatule pour replier l'autre moitié de l'omelette sur le fromage.\nÉTAPE 8\nLaissez cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 9\nServez immédiatement et dégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
omelette_epinards_fromage
def porridge_banane_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","lait d'amande","banane","amandes effilées","miel","cannelle"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",1*multiplicateur_arrondi," tasse de lait d'amande",0.5*multiplicateur_arrondi," banane écrasée",1*multiplicateur_arrondi," cuillère à soupe d'amandes effilées",1*multiplicateur_arrondi," cuillère à café de miel",0.5*multiplicateur_arrondi," cuillère à café de cannelle"]
    préparation=["ÉTAPE 1\nDans une casserole, portez le lait d'amande à ébullition.\nÉTAPE 2\nAjoutez les flocons d'avoine et baissez le feu.\nÉTAPE 3\nRemuez continuellement pendant environ 5 minutes, jusqu'à ce que le porridge épaississe.\nÉTAPE 4\nRetirez du feu et ajoutez la banane écrasée, les amandes effilées, le miel et la cannelle.\nÉTAPE 5\nRemuez pour bien mélanger tous les ingrédients.\nÉTAPE 6\nVersez le porridge dans un bol et garnissez de tranches de banane supplémentaires, d'amandes effilées et d'un filet de miel si désiré.\nÉTAPE 7\nDégustez chaud !"]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
porridge_banane_amandes
def crepes_proteinees_vanille_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine","œufs","lait d'amande","protéine en poudre à la vanille","myrtilles","huile de coco"]
    ingredient = [0.5*multiplicateur_arrondi," tasse de flocons d'avoine",2*multiplicateur_arrondi," œufs",0.25*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.25*multiplicateur_arrondi," tasse de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi," cuillère à café d'huile de coco"]
    préparation=["ÉTAPE 1\nDans un blender, mélangez les flocons d'avoine, les œufs, le lait d'amande et la protéine en poudre à la vanille jusqu'à obtenir une pâte lisse.\nÉTAPE 2\nFaites chauffer une poêle antiadhésive à feu moyen et ajoutez un peu d'huile de coco pour graisser légèrement la poêle.\nÉTAPE 3\nVersez environ 1/4 tasse de pâte à crêpes dans la poêle chaude.\nÉTAPE 4\nDéposez quelques myrtilles sur le dessus de la crêpe.\nÉTAPE 5\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez la crêpe et faites cuire de l'autre côté jusqu'à ce qu'elle soit dorée.\nÉTAPE 6\nRépétez avec le reste de la pâte.\nÉTAPE 7\nServez les crêpes chaudes avec des myrtilles supplémentaires et du sirop d'érable si désiré.\nÉTAPE 8\nDégustez !"]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
crepes_proteinees_vanille_myrtilles
def pancakes_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine tout usage","levure chimique","sucre","sel","lait","œuf","beurre fondu","pépites de chocolat"]
    ingredient = [1*multiplicateur_arrondi," tasse de farine tout usage",2*multiplicateur_arrondi," cuillères à café de levure chimique",1*multiplicateur_arrondi," cuillère à soupe de sucre",0.5*multiplicateur_arrondi," cuillère à café de sel",0.75*multiplicateur_arrondi," tasse de lait",1*multiplicateur_arrondi," œuf",2*multiplicateur_arrondi," cuillères à soupe de beurre fondu",0.5*multiplicateur_arrondi," tasse de pépites de chocolat"]
    preparation=["ÉTAPE 1\nDans un grand bol, mélangez la farine, la levure chimique, le sucre et le sel.\nÉTAPE 2\nDans un autre bol, fouettez ensemble le lait, l'œuf et le beurre fondu.\nÉTAPE 3\nIncorporez les ingrédients liquides aux ingrédients secs et mélangez jusqu'à ce que la pâte soit homogène.\nÉTAPE 4\nAjoutez les pépites de chocolat et mélangez délicatement.\nÉTAPE 5\nFaites chauffer une poêle antiadhésive à feu moyen et graissez-la légèrement avec du beurre ou de l'huile.\nÉTAPE 6\nVersez environ 1/4 tasse de pâte par pancake sur la poêle chaude.\nÉTAPE 7\nLaissez cuire jusqu'à ce que des bulles se forment à la surface, puis retournez les pancakes et laissez cuire de l'autre côté jusqu'à ce qu'ils soient dorés.\nÉTAPE 8\nRépétez avec le reste de la pâte.\nÉTAPE 9\nServez chaud avec du sirop d'érable ou vos garnitures préférées.\nÉTAPE 10\nDégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,preparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
pancakes_pepites_chocolat
def muffins_proteines_bananes_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes","œufs","farine d'amande","protéine en poudre à la vanille","bicarbonate de soude","noix","miel"]
    ingredient = [2*multiplicateur_arrondi," bananes mûres écrasées",2*multiplicateur_arrondi," œufs",0.5*multiplicateur_arrondi," tasse de farine d'amande",1*multiplicateur_arrondi," cuillère à soupe de protéine en poudre à la vanille",0.5*multiplicateur_arrondi," cuillère à café de bicarbonate de soude",0.25*multiplicateur_arrondi," tasse de noix hachées",1*multiplicateur_arrondi," cuillère à soupe de miel"]
    preparation=["ÉTAPE 1\nPréchauffez votre four à 180°C (350°F) et tapissez un moule à muffins de caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélangez les bananes écrasées et les œufs jusqu'à ce que le mélange soit homogène.\nÉTAPE 3\nAjoutez la farine d'amande, la protéine en poudre à la vanille et le bicarbonate de soude, et mélangez bien.\nÉTAPE 4\nIncorporez les noix hachées et le miel dans la pâte à muffins.\nÉTAPE 5\nRépartissez la pâte dans les caissettes à muffins préparées, en les remplissant aux 3/4.\nÉTAPE 6\nEnfournez les muffins et laissez-les cuire pendant environ 20 à 25 minutes, ou jusqu'à ce qu'ils soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 7\nLaissez les muffins refroidir dans le moule pendant quelques minutes avant de les transférer sur une grille pour les laisser refroidir complètement.\nÉTAPE 8\nDégustez !"]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,preparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
muffins_proteines_bananes_noix
def smoothie_beurre_arachide_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes","lait d'amande","beurre d'arachide","miel","glace"]
    ingredient = [2*multiplicateur_arrondi," bananes",1*multiplicateur_arrondi," tasse de lait d'amande",1*multiplicateur_arrondi," cuillère à soupe de beurre d'arachide",1*multiplicateur_arrondi," cuillère à soupe de miel",1*multiplicateur_arrondi," tasse de glace"]
    preparation=["ÉTAPE 1\nPlacez tous les ingrédients dans un blender.\nÉTAPE 2\nMixez jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 3\nSi nécessaire, ajoutez plus de lait d'amande pour atteindre la consistance désirée.\nÉTAPE 4\nVersez dans un verre et dégustez immédiatement !"]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous=[ingredient_pour_algo,ingredient,preparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
smoothie_beurre_arachide_banane
tuplepetitdej_perte_de_poids=[bol_fromage_cottage_peche,bol_yogourt_graines_lin_kiwi,bowl_cake_proteine_vanille,bowl_cake_vanille_fruits_frais,bowl_flocons_avoine_fruits_noix,bowlcake_vanille_amandes,crepes_farine_avoine_fruits,crepes_farine_avoine_myrtilles,crepes_proteinees_farine_amande,crepes_proteinees_vanille_myrtilles,flocons_avoine_banane_cacahuete_chia,flocons_avoine_fruits_noix,flocons_avoine_pomme_noix,frittata_epinards_fromage,gaufres_proteinees_myrtilles,gaufres_vanille_myrtilles,muffin_oeufs_epinards_fromage,muffins_proteines_bananes_noix,oeufs_brouilles_legumes,oeufs_brouilles_legumes_fromage,oeufs_brouilles_saumon_fume_avocat,omelette_bacon_champignons,omelette_champignons_bacon,omelette_champignons_fromage,omelette_champignons_fromage_suisse,omelette_epinards_champignons_feta,omelette_epinards_feta,omelette_epinards_fromage,omelette_epinards_fromage_feta,omelette_epinards_poivrons_feta,omelette_epinards_saumon_fume,omelette_poivrons_fromage_cheddar,omelette_tomates_sechees_fromage_chevre,pain_grille_avocat_oeufs,pain_grille_avocats_oeufs_poches,pain_grille_ble_entier_beurre_amande_pomme,pain_grille_seigle_fromage_frais_figue,pain_perdu_fruits_rouges_sirop_derable,pain_perdu_myrtilles_sirop_derable,pain_perdu_myrtilles_sirop_erable,pancakes_banane_chocolat,pancakes_chocolat_noix,pancakes_myrtilles_sirop_derable,pancakes_patate_douce,pancakes_pepites_chocolat,pancakes_proteine_lactoserum_myrtilles,poelee_tofu_legumes,porridge_banane_amandes,porridge_fruits_rouges_graines_chia,porridge_quinoa_noix_cannelle,porridge_vanille_amandes,salade_avocat_tomate,salade_avocat_tomate_citron,salade_concombre_tomate_basilic,salade_fruits_menthe,sandwich_oeufs_avocat,smoothie_ananas_noix_coco,smoothie_avocat_epinards_baies,smoothie_avoine_myrtilles,smoothie_baies_banane_graines_chia,smoothie_baies_epinards,smoothie_baies_graines_lin,smoothie_baies_proteine,smoothie_banane_amandes,smoothie_banane_avocat_epinards,smoothie_banane_beurre_amande_epinards,smoothie_banane_chocolat_beurre_arachide,smoothie_bananes_epinards_beurre_arachide,smoothie_beurre_arachide_banane,smoothie_beurre_cacahuete_banane_flocons_avoine,smoothie_bowl_banane_beurre_amande,smoothie_bowl_mangue_noix_coco,smoothie_epinards_mangue,smoothie_epinards_mangue_banane,smoothie_fraises_banane_beurre_amande,smoothie_fruits_rouges_lait_amande,smoothie_fruits_rouges_proteine_lactoserum,smoothie_mangue_banane_chia,smoothie_mangue_noix_coco,smoothie_mangue_proteine_lactoserum,smoothie_proteine_banane_beurre_arachide,smoothie_proteine_mangue_noix_coco,smoothie_proteine_myrtilles_yaourt_grec,smoothie_proteine_vanille_yaourt_grec,smoothie_proteines_epinards_amandes,smoothie_proteines_vanille_amandes,smoothie_vert_avocat_banane,tartine_pain_complet_beurre_arachide_banane,tartine_pain_complet_fromage_cottage_fraises,toast_avocat_oeuf_plat,toast_avocat_oeufs_poches_piment_rouge,toasts_avocat_oeufs_poches,toasts_avocat_saumon,yaourt_grec_baies_chia,yaourt_grec_fruits_noix]
