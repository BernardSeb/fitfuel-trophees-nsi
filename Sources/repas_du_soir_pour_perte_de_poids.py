

def salade_poulet_grille_vinaigrette(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "vinaigre balsamique", "sel", "poivre", "ail", "jus de citron", "poulet", "laitue", "tomates cerises", "concombre"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", "sel", "poivre", "ail haché", "jus de citron", 1*multiplicateur_arrondi, "poitrine de poulet grillée", 2*multiplicateur_arrondi, "tasses de laitue", 1*multiplicateur_arrondi, "tasses de tomates cerises", 1*multiplicateur_arrondi, "concombre tranché"]
    préparation = ["ÉTAPE 1\nPréparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, le sel, le poivre, l'ail et le jus de citron dans un bol.\nÉTAPE 2\nCouper le poulet grillé en tranches.\nÉTAPE 3\nDans un grand bol, mélanger la laitue, les tomates cerises et le concombre.\nÉTAPE 4\nAjouter le poulet grillé au-dessus de la salade.\nÉTAPE 5\nArroser la salade de vinaigrette et mélanger doucement.\nÉTAPE 6\nServir immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_grille_vinaigrette
def saumon_cuit_four_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "sel", "poivre", "ail en poudre", "saumon", "asparagus", "poivrons", "courgettes", "oignon"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", "ail en poudre", 1*multiplicateur_arrondi, "filet de saumon", 1*multiplicateur_arrondi, "tasses d'asperges", 1*multiplicateur_arrondi, "tasses de poivrons", 1*multiplicateur_arrondi, "tasses de courgettes", 1*multiplicateur_arrondi, "oignon rouge"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nDans un bol, mélanger l'huile d'olive, le sel, le poivre et l'ail en poudre.\nÉTAPE 3\nDisposer le saumon sur une plaque de cuisson et badigeonner avec le mélange d'huile d'olive.\nÉTAPE 4\nDisposer les légumes coupés sur la plaque de cuisson autour du saumon.\nÉTAPE 5\nCuire au four pendant 15-20 minutes, jusqu'à ce que le saumon soit cuit et les légumes soient tendres.\nÉTAPE 6\nServir chaud."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

saumon_cuit_four_legumes_grilles
def soupe_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "oignon", "ail", "carottes", "céleri", "pommes de terre", "bouillon de légumes", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "carottes tranchées", 2*multiplicateur_arrondi, "branches de céleri tranchées", 2*multiplicateur_arrondi, "pommes de terre coupées en dés", 4*multiplicateur_arrondi, "tasses de bouillon de légumes", "sel", "poivre", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nDans une grande casserole, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon et l'ail et faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nAjouter les carottes, le céleri et les pommes de terre et faire sauter pendant quelques minutes.\nÉTAPE 4\nVerser le bouillon de légumes dans la casserole.\nÉTAPE 5\nPorter à ébullition, puis réduire le feu et laisser mijoter jusqu'à ce que les légumes soient tendres.\nÉTAPE 6\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 7\nSaupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

soupe_legumes
def poelee_crevettes_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "ail", "gingembre", "sauce soja", "miel", "crevettes", "brocoli", "poivrons", "carottes", "oignon"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "tasse de crevettes décortiquées", 1*multiplicateur_arrondi, "tasse de brocoli", 1*multiplicateur_arrondi, "tasse de poivrons", 1*multiplicateur_arrondi, "tasse de carottes en julienne", 1*multiplicateur_arrondi, "oignon rouge tranché"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'ail et le gingembre râpé et faire sauter jusqu'à ce qu'ils soient parfumés.\nÉTAPE 3\nAjouter la sauce soja et le miel dans la poêle et mélanger.\nÉTAPE 4\nAjouter les crevettes dans la poêle et cuire jusqu'à ce qu'elles soient roses.\nÉTAPE 5\nAjouter les légumes (brocoli, poivrons, carottes et oignon) dans la poêle et faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nServir chaud."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_legumes
def omelette_champignons_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "sel", "poivre", "oeufs", "champignons", "épinards", "oignon", "fromage (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre", 2*multiplicateur_arrondi, "œufs battus", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé (facultatif)"]
    préparation = ["ÉTAPE 1\nDans une poêle antiadhésive, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter les champignons et l'oignon dans la poêle et faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nAjouter les épinards dans la poêle et cuire jusqu'à ce qu'ils soient flétris.\nÉTAPE 4\nVerser les œufs battus sur les légumes dans la poêle.\nÉTAPE 5\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus et dorée en dessous.\nÉTAPE 6\nSi désiré, saupoudrer de fromage râpé sur l'omelette.\nÉTAPE 7\nPliez l'omelette en deux et servez chaud."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

omelette_champignons_epinards
def tofu_saute_brocoli_poivrons(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "tofu", "sauce soja", "miel", "ail", "brocoli", "poivrons", "oignon", "gingembre"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "tasse de tofu coupé en cubes", 2*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à café de miel", 2*multiplicateur_arrondi, "gousses d'ail hachées", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "tasse de poivrons en lanières", 1*multiplicateur_arrondi, "oignon émincé", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter le tofu dans la poêle et faire sauter jusqu'à ce qu'il soit doré sur tous les côtés.\nÉTAPE 3\nDans un bol, mélanger la sauce soja, le miel, l'ail haché et le gingembre râpé.\nÉTAPE 4\nAjouter le mélange de sauce soja au tofu dans la poêle et remuer pour bien enrober.\nÉTAPE 5\nAjouter le brocoli, les poivrons et l'oignon dans la poêle et faire sauter jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 6\nServir chaud."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_brocoli_poivrons
def courgettes_farcies_viande_maigre(multiplicateur_arrondi):
    ingredient_pour_algo = ["courgettes", "huile d'olive", "oignon", "ail", "viande hachée maigre", "tomates", "fromage râpé (facultatif)", "sel", "poivre", "herbes de Provence"]
    ingredient = [2*multiplicateur_arrondi, "courgettes moyennes", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail hachées", 1*multiplicateur_arrondi, "tasse de viande hachée maigre", 1*multiplicateur_arrondi, "tomates concassées en conserve", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé (facultatif)", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 180°C.\nÉTAPE 2\nCouper les courgettes en deux dans le sens de la longueur et retirer la chair avec une cuillère pour former des bateaux.\nÉTAPE 3\nDans une poêle, chauffer l'huile d'olive et faire revenir l'oignon et l'ail jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAjouter la viande hachée dans la poêle et cuire jusqu'à ce qu'elle soit dorée.\nÉTAPE 5\nAjouter les tomates concassées et la chair de courgette hachée dans la poêle et laisser mijoter pendant quelques minutes.\nÉTAPE 6\nAssaisonner avec du sel, du poivre et des herbes de Provence selon votre goût.\nÉTAPE 7\nFarcir les moitiés de courgettes avec le mélange de viande et de légumes.\nÉTAPE 8\nSaupoudrer de fromage râpé sur le dessus si désiré.\nÉTAPE 9\nCuire au four pendant 20-25 minutes jusqu'à ce que les courgettes soient tendres.\nÉTAPE 10\nServir chaud."]
    temps_de_préparation = 20
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

courgettes_farcies_viande_maigre
def salade_thon_haricots_verts_tomates(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots verts", "vinaigre balsamique", "miel", "moutarde de Dijon", "thon en conserve", "tomates cerises", "oignon rouge", "olives (facultatif)", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de haricots verts cuits", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "tasse de thon en conserve égoutté", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "poignée d'olives (facultatif)", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un bol, préparer la vinaigrette en mélangeant le vinaigre balsamique, le miel, la moutarde de Dijon, du sel et du poivre.\nÉTAPE 2\nDans un grand bol, mélanger les haricots verts cuits, le thon émietté, les tomates cerises coupées en deux, les tranches d'oignon rouge et les olives si désiré.\nÉTAPE 3\nArroser la salade de vinaigrette et mélanger doucement pour enrober tous les ingrédients.\nÉTAPE 4\nServir immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_verts_tomates
def poisson_papillote_herbes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de poisson blanc", "citron", "huile d'olive", "ail", "herbes fraîches (persil, thym, romarin)", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de poisson blanc", 1*multiplicateur_arrondi, "tranches de citron", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe d'herbes fraîches hachées (persil, thym, romarin)", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 180°C.\nÉTAPE 2\nPlacer chaque filet de poisson sur une feuille de papier sulfurisé.\nÉTAPE 3\nArroser les filets de poisson d'huile d'olive et frotter avec de l'ail émincé.\nÉTAPE 4\nAjouter des tranches de citron et des herbes fraîches sur les filets de poisson.\nÉTAPE 5\nSaler et poivrer selon votre goût.\nÉTAPE 6\nFermer hermétiquement les papillotes et les placer sur une plaque de cuisson.\nÉTAPE 7\nCuire au four pendant 15-20 minutes, jusqu'à ce que le poisson soit cuit à travers.\nÉTAPE 8\nServir chaud."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poisson_papillote_herbes
def quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "courgettes", "poivrons", "aubergine", "oignon", "ail", "huile d'olive", "sel", "poivre", "jus de citron", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "oignon rouge tranché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, puis ajouter le quinoa.\nÉTAPE 3\nRéduire le feu, couvrir et laisser mijoter pendant 15-20 minutes jusqu'à ce que le quinoa soit cuit et que toute l'eau soit absorbée.\nÉTAPE 4\nPendant ce temps, préchauffer le gril à feu moyen-élevé.\nÉTAPE 5\nDans un bol, mélanger les dés de courgette, de poivron, d'aubergine, d'oignon et d'ail avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 6\nGriller les légumes pendant 10-15 minutes, en les retournant de temps en temps, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 7\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés.\nÉTAPE 8\nArroser de jus de citron et saupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

quinoa_legumes_grilles
def wraps_dinde_legumes_croquants(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "escalopes de dinde", "poivrons", "carottes", "concombre", "laitue", "houmous (facultatif)", "jus de citron", "huile d'olive", "sel", "poivre", "herbes fraîches (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "escalope de dinde cuite et tranchée", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "concombre coupé en bâtonnets", 1*multiplicateur_arrondi, "tasse de laitue déchiquetée", 1*multiplicateur_arrondi, "cuillère à soupe de houmous (facultatif)", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'herbes fraîches hachées (facultatif)"]
    préparation = ["ÉTAPE 1\nDans une petite poêle, chauffer légèrement chaque tortilla pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler une cuillère à soupe de houmous (si désiré).\nÉTAPE 3\nDisposer des tranches d'escalope de dinde, des lanières de poivron, des carottes râpées, des bâtonnets de concombre et de la laitue déchiquetée sur chaque tortilla.\nÉTAPE 4\nArroser de jus de citron et d'huile d'olive sur les garnitures.\nÉTAPE 5\nAssaisonner avec du sel, du poivre et des herbes fraîches hachées si désiré.\nÉTAPE 6\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 7\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wraps_dinde_legumes_croquants
def poulet_grille_salade_chou_kale(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "chou kale", "huile d'olive", "citron", "miel", "moutarde de Dijon", "ail", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 4*multiplicateur_arrondi, "tasses de chou kale haché", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "gousses d'ail émincées", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill à feu moyen-élevé.\nÉTAPE 2\nAssaisonner les poitrines de poulet avec du sel et du poivre.\nÉTAPE 3\nGriller le poulet pendant 6-8 minutes de chaque côté, jusqu'à ce qu'il soit bien cuit et ne soit plus rose à l'intérieur.\nÉTAPE 4\nPendant ce temps, dans un grand bol, mélanger le chou kale avec de l'huile d'olive, du jus de citron, du miel, de la moutarde de Dijon et de l'ail émincé.\nÉTAPE 5\nMasser le chou kale avec les mains pendant quelques minutes pour le ramollir et l'attendrir.\nÉTAPE 6\nServir les poitrines de poulet grillées avec la salade de chou kale."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poulet_grille_salade_chou_kale
def buddha_bowl_legumes_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "patate douce", "brocoli", "pois chiches", "avocat", "carottes", "épinards", "huile d'olive", "sel", "poivre", "jus de citron", "graines de sésame (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "patate douce coupée en dés", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "avocat tranché", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "graines de sésame (facultatif)"]
    préparation = ["ÉTAPE 1\nCuire le quinoa selon les instructions sur l'emballage.\nÉTAPE 2\nPendant ce temps, préchauffer le four à 200°C.\nÉTAPE 3\nSur une plaque de cuisson, étaler les dés de patate douce et les fleurettes de brocoli en une seule couche.\nÉTAPE 4\nArroser d'huile d'olive, assaisonner avec du sel et du poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 5\nRôtir au four pendant 20-25 minutes, jusqu'à ce que les légumes soient dorés et tendres.\nÉTAPE 6\nDans un grand bol, assembler le quinoa cuit, les légumes rôtis, les pois chiches, l'avocat, la carotte râpée et les épinards frais.\nÉTAPE 7\nArroser de jus de citron et saupoudrer de graines de sésame si désiré avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_quinoa
def soupe_lentilles_corail_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles corail", "carottes", "oignon", "ail", "curry en poudre", "curcuma", "cumin", "lait de coco", "bouillon de légumes", "sel", "poivre", "coriandre fraîche (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles corail", 2*multiplicateur_arrondi, "carottes coupées en dés", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de poudre de curry", 1*multiplicateur_arrondi, "cuillère à café de curcuma", 1*multiplicateur_arrondi, "cuillère à café de cumin", 1*multiplicateur_arrondi, "tasse de lait de coco", 3*multiplicateur_arrondi, "tasses de bouillon de légumes", "sel", "poivre", "coriandre fraîche (facultatif)"]
    préparation = ["ÉTAPE 1\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon et l'ail hachés et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 3\nAjouter les carottes coupées en dés et cuire pendant quelques minutes.\nÉTAPE 4\nIncorporer les épices (curry, curcuma, cumin) et cuire pendant une minute de plus pour libérer les arômes.\nÉTAPE 5\nVerser le bouillon de légumes et le lait de coco dans la casserole, puis ajouter les lentilles corail.\nÉTAPE 6\nPorter à ébullition, puis réduire le feu et laisser mijoter pendant environ 20 minutes, jusqu'à ce que les lentilles et les légumes soient tendres.\nÉTAPE 7\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 8\nServir chaud, garni de coriandre fraîche si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

soupe_lentilles_corail_legumes
def salade_quinoa_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "avocat", "tomates cerises", "concombre", "oignon rouge", "coriandre fraîche", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "avocat mûr coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les dés d'avocat, les tomates cerises coupées en deux, le concombre coupé en dés et l'oignon rouge tranché.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nArroser la salade de quinoa et avocat avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients.\nÉTAPE 4\nSaupoudrer de coriandre fraîche hachée avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_avocat
def wrap_vegetarien_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "haricots noirs", "maïs", "avocat", "tomates", "laitue", "coriandre fraîche", "fromage râpé (facultatif)", "sel", "poivre", "jus de citron", "sauce piquante (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "avocat mûr coupé en tranches", 1*multiplicateur_arrondi, "tomates coupées en dés", 1*multiplicateur_arrondi, "tasse de laitue déchiquetée", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé (facultatif)", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sauce piquante (facultatif)"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, disposer une couche de haricots noirs cuits, de maïs, de tranches d'avocat, de dés de tomates, de laitue déchiquetée et de coriandre fraîche hachée.\nÉTAPE 3\nSaupoudrer de fromage râpé si désiré et assaisonner avec du sel et du poivre.\nÉTAPE 4\nArroser de jus de citron et ajouter de la sauce piquante si désiré.\nÉTAPE 5\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 6\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_haricots_noirs
def salade_crevettes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes décortiquées", "avocat", "tomates", "concombre", "oignon rouge", "jus de citron", "huile d'olive", "sel", "poivre", "coriandre fraîche"]
    ingredient = [200*multiplicateur_arrondi, "g de crevettes décortiquées cuites", 1*multiplicateur_arrondi, "avocat mûr coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les crevettes cuites, les dés d'avocat, les tomates cerises coupées en deux, les dés de concombre et l'oignon rouge tranché.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nArroser la salade de crevettes et avocat avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients.\nÉTAPE 4\nSaupoudrer de coriandre fraîche hachée avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_crevettes_avocat(1)
def salade_poulet_cesar_legere(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "laitue romaine", "tomates cerises", "croûtons de pain complet", "parmesan râpé", "jus de citron", "huile d'olive", "moutarde de Dijon", "ail", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 1*multiplicateur_arrondi, "tête de laitue romaine déchiquetée", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "tasse de croûtons de pain complet", 1*multiplicateur_arrondi, "cuillère à soupe de parmesan râpé", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "gousse d'ail émincée", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet, la laitue romaine déchiquetée, les tomates cerises coupées en deux et les croûtons de pain complet.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, la moutarde de Dijon, l'ail émincé, du sel et du poivre.\nÉTAPE 3\nArroser la salade de poulet césar avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients.\nÉTAPE 4\nSaupoudrer de parmesan râpé avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar_legere
def bol_riz_brun_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz brun", "courgettes", "poivrons", "oignon rouge", "champignons", "huile d'olive", "sel", "poivre", "herbes de Provence"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, étaler les dés de courgette, les lanières de poivron, les tranches d'oignon rouge et les champignons tranchés en une seule couche.\nÉTAPE 3\nArroser d'huile d'olive et saupoudrer d'herbes de Provence, de sel et de poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nRôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient tendres et légèrement dorés.\nÉTAPE 5\nDans un bol, servir le riz brun cuit avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_riz_brun_legumes_rotis

def sandwich_thon_legumes_croquants(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "pain complet", "avocat", "concombre", "carottes", "laitue", "mayonnaise légère", "moutarde de Dijon", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 2*multiplicateur_arrondi, "tranches de pain complet", 1*multiplicateur_arrondi, "avocat mûr écrasé", 1*multiplicateur_arrondi, "concombre coupé en fines tranches", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "feuille de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un petit bol, mélanger le thon égoutté avec la mayonnaise légère, la moutarde de Dijon, le jus de citron, du sel et du poivre.\nÉTAPE 2\nSur une tranche de pain complet, étaler l'avocat écrasé.\nÉTAPE 3\nDisposer les tranches de concombre, la carotte râpée et la feuille de laitue sur l'avocat.\nÉTAPE 4\nAjouter le mélange de thon sur les légumes et couvrir avec une autre tranche de pain complet.\nÉTAPE 5\nCouper le sandwich en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

sandwich_thon_legumes_croquants
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomates cerises", "concombre", "oignon rouge", "persil frais", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "tasse de haricots blancs en conserve égouttés et rincés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon égoutté, les haricots blancs égouttés, les tomates cerises coupées en deux, le concombre coupé en dés, l'oignon rouge tranché et le persil frais haché.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nArroser la salade de thon et haricots blancs avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def bol_lentilles_legumes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "avocat", "tomates cerises", "concombre", "oignon rouge", "coriandre fraîche", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "avocat mûr coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lentilles vertes cuites, les dés d'avocat, les tomates cerises coupées en deux, le concombre coupé en dés, l'oignon rouge tranché et la coriandre fraîche hachée.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nArroser le bol de lentilles aux légumes avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_lentilles_legumes_avocat
def wrap_poulet_grille_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "tortillas de blé entier", "avocat", "concombre", "carottes", "laitue", "mayonnaise légère", "moutarde de Dijon", "jus de citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en lanières", 2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "avocat mûr écrasé", 1*multiplicateur_arrondi, "concombre coupé en fines tranches", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "feuille de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler l'avocat écrasé.\nÉTAPE 3\nDisposer les lanières de poulet cuites, les tranches de concombre, la carotte râpée et la feuille de laitue sur l'avocat.\nÉTAPE 4\nDans un petit bol, mélanger la mayonnaise légère, la moutarde de Dijon et le jus de citron pour faire une sauce.\nÉTAPE 5\nAjouter la sauce sur les ingrédients du wrap.\nÉTAPE 6\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 7\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_grille_legumes
def bol_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "courgettes", "poivrons", "oignon rouge", "champignons", "huile d'olive", "sel", "poivre", "herbes de Provence"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, étaler les dés de courgette, les lanières de poivron, les tranches d'oignon rouge et les champignons tranchés en une seule couche.\nÉTAPE 3\nArroser d'huile d'olive et saupoudrer d'herbes de Provence, de sel et de poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nRôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient tendres et légèrement dorés.\nÉTAPE 5\nDans un bol, servir le quinoa cuit avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_quinoa_legumes_rotis
def wrap_vegetarien_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "aubergine", "courgettes", "poivrons", "oignon rouge", "champignons", "huile d'olive", "sel", "poivre", "origan séché"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "aubergine coupée en tranches", 1*multiplicateur_arrondi, "courgette coupée en tranches", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'origan séché"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill du four ou une poêle grill.\nÉTAPE 2\nBadigeonner les tranches d'aubergine, de courgette, de poivron et les rondelles d'oignon rouge avec de l'huile d'olive, du sel, du poivre et de l'origan séché.\nÉTAPE 3\nFaire griller les légumes pendant quelques minutes de chaque côté jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nChauffer légèrement chaque tortilla dans une poêle.\nÉTAPE 5\nSur chaque tortilla, disposer une portion de légumes grillés.\nÉTAPE 6\nEnrouler les tortillas en forme de wrap et couper en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_legumes_grilles
def salade_poulet_quinoa_legumes_croquants(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "quinoa", "concombre", "tomates cerises", "poivrons", "oignon rouge", "jus de citron", "huile d'olive", "sel", "poivre", "persil frais"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet cuites, le quinoa cuit, le concombre coupé en dés, les tomates cerises coupées en deux, les lanières de poivron et l'oignon rouge tranché.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nArroser la salade de poulet et quinoa aux légumes avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients.\nÉTAPE 4\nSaupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_quinoa_legumes_croquants
def salade_quinoa_legumes_mediterraneens(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "tomates cerises", "concombre", "poivrons", "oignon rouge", "olives noires", "feta", "jus de citron", "huile d'olive", "sel", "poivre", "origan séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "tasse d'olives noires dénoyautées", 1*multiplicateur_arrondi, "tasse de feta émiettée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'origan séché"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les tomates cerises coupées en deux, le concombre coupé en dés, les lanières de poivron, l'oignon rouge tranché, les olives noires dénoyautées et la feta émiettée.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel, du poivre et de l'origan séché.\nÉTAPE 3\nArroser la salade de quinoa aux légumes méditerranéens avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_mediterraneens
def bol_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "aubergine", "courgettes", "poivrons", "oignon rouge", "huile d'olive", "sel", "poivre", "origan séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'origan séché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, étaler les dés d'aubergine, de courgette, les lanières de poivron et les tranches d'oignon rouge en une seule couche.\nÉTAPE 3\nArroser d'huile d'olive et saupoudrer d'origan séché, de sel et de poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nRôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient tendres et légèrement dorés.\nÉTAPE 5\nDans un bol, servir les lentilles vertes cuites avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_lentilles_legumes_rotis
def wrap_dinde_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["escalopes de dinde", "tortillas de blé entier", "avocat", "tomates", "laitue", "mayonnaise légère", "moutarde de Dijon", "jus de citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "escalopes de dinde cuites et tranchées", 2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "avocat mûr écrasé", 1*multiplicateur_arrondi, "tomates tranchées", 1*multiplicateur_arrondi, "feuille de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler l'avocat écrasé.\nÉTAPE 3\nDisposer les tranches de dinde, les tranches de tomate et la feuille de laitue sur l'avocat.\nÉTAPE 4\nDans un petit bol, mélanger la mayonnaise légère, la moutarde de Dijon et le jus de citron pour faire une sauce.\nÉTAPE 5\nAjouter la sauce sur les ingrédients du wrap.\nÉTAPE 6\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 7\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_dinde_avocat
def salade_pois_chiches_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["pois chiches en conserve", "poivrons", "courgettes", "tomates cerises", "oignon rouge", "ail", "huile d'olive", "sel", "poivre", "cumin moulu"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pois chiches en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "poivrons coupés en lanières", 1*multiplicateur_arrondi, "courgettes coupées en tranches", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill du four ou une poêle grill.\nÉTAPE 2\nSur une plaque de cuisson, étaler les lanières de poivron, les tranches de courgette, les tomates cerises coupées en deux, l'oignon rouge tranché et l'ail émincé en une seule couche.\nÉTAPE 3\nArroser d'huile d'olive, saupoudrer de cumin moulu, de sel et de poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nFaire griller les légumes pendant quelques minutes de chaque côté jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 5\nDans un grand bol, mélanger les pois chiches égouttés avec les légumes grillés.\nÉTAPE 6\nServir la salade de pois chiches et légumes grillés immédiatement ou réfrigérer pour servir froid."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_pois_chiches_legumes_grilles
def bol_riz_brun_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz brun", "brocoli", "carottes", "poivrons", "champignons", "oignon", "ail", "huile d'olive", "sauce soja faible en sodium", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "oignon tranché finement", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame"]
    préparation = ["ÉTAPE 1\nFaire cuire le riz brun selon les instructions sur l'emballage.\nÉTAPE 2\nDans une poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 3\nAjouter l'oignon tranché et les gousses d'ail émincées et faire sauter jusqu'à ce qu'ils deviennent translucides.\nÉTAPE 4\nAjouter les carottes coupées en dés, les lanières de poivron et les fleurettes de brocoli dans la poêle et faire sauter pendant quelques minutes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 5\nAjouter les champignons tranchés et faire sauter jusqu'à ce qu'ils soient dorés.\nÉTAPE 6\nAjouter le riz cuit à la poêle et mélanger pour combiner tous les ingrédients.\nÉTAPE 7\nArroser de sauce soja faible en sodium et assaisonner avec du sel et du poivre, puis mélanger à nouveau.\nÉTAPE 8\nServir le bol de riz brun aux légumes sautés chaud, saupoudré de graines de sésame."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_riz_brun_legumes_sautes
def wrap_saumon_fume_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "saumon fumé", "avocat", "concombre", "roquette", "mayonnaise légère", "jus de citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "tranches de saumon fumé", 1*multiplicateur_arrondi, "avocat mûr écrasé", 1*multiplicateur_arrondi, "concombre coupé en lanières", 1*multiplicateur_arrondi, "poignée de roquette", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler l'avocat écrasé.\nÉTAPE 3\nDisposer les tranches de saumon fumé, les lanières de concombre et la roquette sur l'avocat.\nÉTAPE 4\nDans un petit bol, mélanger la mayonnaise légère et le jus de citron pour faire une sauce.\nÉTAPE 5\nAjouter la sauce sur les ingrédients du wrap.\nÉTAPE 6\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 7\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_saumon_fume_avocat
def salade_quinoa_haricots_noirs_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "haricots noirs en conserve", "maïs en conserve", "poivrons", "oignon rouge", "coriandre fraîche", "jus de lime", "huile d'olive", "sel", "poivre", "piment en poudre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de haricots noirs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de maïs en conserve, égoutté", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de lime frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de piment en poudre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les haricots noirs égouttés, le maïs égoutté, les dés de poivron et les tranches d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant la coriandre fraîche hachée, le jus de lime frais, l'huile d'olive, du sel, du poivre et du piment en poudre.\nÉTAPE 3\nArroser la salade de quinoa aux haricots noirs et légumes avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_haricots_noirs_legumes
def bol_riz_sauvage_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz sauvage", "courges butternut", "poivrons", "oignon rouge", "champignons", "huile d'olive", "sel", "poivre", "thym frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz sauvage cuit", 1*multiplicateur_arrondi, "courge butternut coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge tranché", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de thym frais haché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, étaler les dés de courge butternut, les lanières de poivron, les tranches d'oignon rouge et les champignons tranchés en une seule couche.\nÉTAPE 3\nArroser d'huile d'olive et saupoudrer de thym frais haché, de sel et de poivre, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nRôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient tendres et légèrement dorés.\nÉTAPE 5\nDans un bol, servir le riz sauvage cuit avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_riz_sauvage_legumes_rotis
def salade_poulet_quinoa_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "quinoa", "avocat", "tomates", "concombre", "laitue", "vinaigre balsamique", "huile d'olive", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "avocat mûr coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates coupées en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "feuilles de laitue déchirées", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet cuit, le quinoa cuit, les dés d'avocat, les dés de tomate, les dés de concombre et les feuilles de laitue déchirées.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique, l'huile d'olive, le miel, la moutarde de Dijon, du sel et du poivre.\nÉTAPE 3\nArroser la salade de poulet et quinoa à l'avocat avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_quinoa_avocat
def buddha_bowl_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "pois chiches en conserve", "brocoli", "carottes", "oignon rouge", "huile d'olive", "sel", "poivre", "paprika"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de pois chiches en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill du four.\nÉTAPE 2\nSur une plaque de cuisson, disposer les pois chiches égouttés, les fleurettes de brocoli, les rondelles de carotte et les quartiers d'oignon rouge.\nÉTAPE 3\nArroser d'huile d'olive, saupoudrer de sel, de poivre et de paprika, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nFaire griller au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient dorés et tendres.\nÉTAPE 5\nDans un bol, servir le quinoa cuit avec les légumes grillés sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_grilles
def bol_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "carottes", "chou-fleur", "oignon rouge", "huile d'olive", "sel", "poivre", "cumin moulu"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "tasse de chou-fleur en fleurettes", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, disposer les lentilles cuites, les rondelles de carotte, les fleurettes de chou-fleur et les quartiers d'oignon rouge.\nÉTAPE 3\nArroser d'huile d'olive, saupoudrer de sel, de poivre et de cumin moulu, puis mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nRôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient dorés et tendres.\nÉTAPE 5\nDans un bol, servir les lentilles cuites avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_lentilles_legumes_rotis
def wrap_vegetarien_mediterraneen(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "houmous", "poivrons grillés", "tomates séchées", "olives noires", "concombre", "feuilles de laitue", "huile d'olive", "vinaigre balsamique", "origan séché", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "cuillères à soupe de houmous", 1*multiplicateur_arrondi, "poivrons grillés, égouttés et coupés en lanières", 1*multiplicateur_arrondi, "cuillère à soupe de tomates séchées hachées", 1*multiplicateur_arrondi, "cuillère à soupe d'olives noires tranchées", 1*multiplicateur_arrondi, "concombre coupé en lanières", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de vinaigre balsamique", 1*multiplicateur_arrondi, "pincée d'origan séché", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler une cuillère à soupe de houmous.\nÉTAPE 3\nDisposer les lanières de poivrons grillés, les tomates séchées hachées, les olives noires tranchées, les lanières de concombre et les feuilles de laitue sur le houmous.\nÉTAPE 4\nDans un petit bol, mélanger l'huile d'olive, le vinaigre balsamique, l'origan séché, du sel et du poivre pour faire une vinaigrette.\nÉTAPE 5\nAjouter la vinaigrette sur les ingrédients du wrap.\nÉTAPE 6\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 7\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_mediterraneen
def salade_quinoa_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "épinards frais", "feta", "tomates cerises", "oignon rouge", "huile d'olive", "vinaigre balsamique", "miel", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse d'épinards frais, lavés et émincés", 1*multiplicateur_arrondi, "once de feta émiettée", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge tranché finement", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les épinards émincés, la feta émiettée, les tomates cerises coupées en deux et les tranches d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, le miel, du sel et du poivre.\nÉTAPE 3\nArroser la salade de quinoa aux épinards et feta avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_epinards_feta
def salade_lentilles_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "poivrons", "courgettes", "aubergines", "tomates cerises", "oignon rouge", "huile d'olive", "vinaigre balsamique", "sel", "poivre", "herbes de Provence"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en rondelles", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lentilles cuites, les lanières de poivron, les rondelles de courgette, les rondelles d'aubergine, les tomates cerises coupées en deux et les quartiers d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, du sel, du poivre et des herbes de Provence.\nÉTAPE 3\nArroser la salade de lentilles aux légumes grillés avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_grilles
def buddha_bowl_quinoa_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "pois chiches en conserve", "brocoli", "carottes", "poivrons", "oignon rouge", "huile d'olive", "sauce soja faible en sodium", "sel", "poivre", "curry en poudre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de pois chiches en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de curry en poudre"]
    préparation = ["ÉTAPE 1\nDans une poêle, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter les quartiers d'oignon rouge et les petits bouquets de brocoli dans la poêle et faire sauter pendant quelques minutes jusqu'à ce qu'ils commencent à ramollir.\nÉTAPE 3\nAjouter les rondelles de carotte, les lanières de poivron et les pois chiches égouttés dans la poêle, puis saupoudrer de curry en poudre et mélanger pour enrober uniformément les légumes.\nÉTAPE 4\nContinuer à faire sauter jusqu'à ce que tous les légumes soient tendres mais encore croquants.\nÉTAPE 5\nDans un bol, servir le quinoa cuit avec les légumes sautés sur le dessus.\nÉTAPE 6\nArroser avec la sauce soja faible en sodium, puis assaisonner avec du sel et du poivre selon votre goût."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_quinoa_legumes_sautes
def wrap_poulet_grille_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "poitrines de poulet", "poivrons", "courgettes", "aubergines", "épinards", "houmous", "huile d'olive", "sel", "poivre", "paprika"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en lanières", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "aubergine coupée en lanières", 1*multiplicateur_arrondi, "tasse d'épinards frais", 2*multiplicateur_arrondi, "cuillères à soupe de houmous", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, étaler une cuillère à soupe de houmous.\nÉTAPE 3\nDisposer les lanières de poulet cuit, les lanières de poivron, les lanières de courgette, les lanières d'aubergine et les épinards frais sur le houmous.\nÉTAPE 4\nArroser avec un filet d'huile d'olive et assaisonner avec du sel, du poivre et du paprika.\nÉTAPE 5\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 6\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_grille_legumes
def bowl_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "brocoli", "carottes", "poivrons", "oignon rouge", "champignons", "huile d'olive", "sel", "poivre", "ail en poudre", "thym séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1*multiplicateur_arrondi, "cuillère à café de thym séché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nSur une plaque de cuisson, disposer les petits bouquets de brocoli, les rondelles de carotte, les lanières de poivron et les quartiers d'oignon rouge.\nÉTAPE 3\nAjouter les champignons tranchés sur la plaque.\nÉTAPE 4\nArroser d'huile d'olive et saupoudrer d'ail en poudre, de thym séché, de sel et de poivre.\nÉTAPE 5\nFaire rôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient dorés et tendres.\nÉTAPE 6\nDans un bol, servir le quinoa cuit avec les légumes rôtis sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bowl_quinoa_legumes_rotis
def salade_poulet_legumes_verts(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "épinards", "brocoli", "concombre", "avocat", "tomates cerises", "huile d'olive", "vinaigre balsamique", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 2*multiplicateur_arrondi, "tasses d'épinards frais", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "avocat mûr coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet cuit, les épinards frais, les fleurettes de brocoli, les dés de concombre, les dés d'avocat et les tomates cerises coupées en deux.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, le miel, la moutarde de Dijon, du sel et du poivre.\nÉTAPE 3\nArroser la salade de poulet et légumes verts avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_legumes_verts
def sandwich_thon_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["pains pita", "thon en conserve", "poivrons", "courgettes", "aubergines", "oignon rouge", "huile d'olive", "sel", "poivre", "jus de citron", "herbes fraîches (persil, basilic)"]
    ingredient = [2*multiplicateur_arrondi, "pains pita de blé entier", 2*multiplicateur_arrondi, "onces de thon en conserve égoutté", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "aubergine coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'herbes fraîches hachées (persil, basilic)"]
    préparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nSur le gril, faire griller les lanières de poivron, de courgette, d'aubergine et les quartiers d'oignon rouge jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 3\nDans un bol, émietter le thon égoutté et arroser de jus de citron. Ajouter les herbes fraîches hachées, du sel et du poivre au goût, puis mélanger pour bien combiner.\nÉTAPE 4\nOuvrir les pains pita en deux et les garnir de thon émietté et de légumes grillés. Arroser d'un filet d'huile d'olive et refermer les pains pita.\nÉTAPE 5\nChauffer les sandwiches au thon et aux légumes grillés sur le gril pendant quelques minutes de chaque côté, jusqu'à ce qu'ils soient chauds et croustillants."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

sandwich_thon_legumes_grilles
def wrap_dinde_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "filets de dinde", "poivrons", "courgettes", "oignon rouge", "aubergines", "huile d'olive", "sel", "poivre", "paprika", "ail en poudre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "filets de dinde cuits et coupés en lanières", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "aubergine coupée en lanières", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement chaque tortilla dans une poêle pour les ramollir.\nÉTAPE 2\nSur chaque tortilla, disposer les lanières de dinde cuite, les lanières de poivron, les lanières de courgette, les quartiers d'oignon rouge et les lanières d'aubergine.\nÉTAPE 3\nArroser d'huile d'olive et assaisonner avec du sel, du poivre, du paprika et de l'ail en poudre.\nÉTAPE 4\nEnrouler fermement chaque tortilla en forme de wrap.\nÉTAPE 5\nCouper les wraps de dinde aux légumes grillés en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_dinde_legumes_grilles
def salade_thon_haricots_legumes_croquants(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "concombre", "tomates cerises", "poivrons", "oignon rouge", "vinaigre de vin rouge", "huile d'olive", "sel", "poivre", "persil frais"]
    ingredient = [2*multiplicateur_arrondi, "onces de thon en conserve égoutté", 1*multiplicateur_arrondi, "tasse de haricots blancs en conserve égouttés et rincés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon émietté, les haricots blancs égouttés, les dés de concombre, les tomates cerises coupées en deux, les lanières de poivron et les quartiers d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre de vin rouge, l'huile d'olive, du sel, du poivre et du persil frais haché.\nÉTAPE 3\nArroser la salade de thon aux haricots et légumes croquants avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_legumes_croquants
def bowl_riz_brun_legumes_sautés_tofu(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz brun", "tofu ferme", "brocoli", "carottes", "poivrons", "champignons", "sauce soja faible en sodium", "huile de sésame", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "tasse de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à café d'huile de sésame", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de graines de sésame"]
    préparation = ["ÉTAPE 1\nDans une poêle, faire chauffer l'huile de sésame à feu moyen.\nÉTAPE 2\nAjouter le tofu coupé en dés dans la poêle et faire sauter jusqu'à ce qu'il soit doré et croustillant.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter les petits bouquets de brocoli, les rondelles de carotte, les lanières de poivron et les champignons tranchés.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter le tofu réservé dans la poêle et arroser avec la sauce soja faible en sodium.\nÉTAPE 7\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 8\nServir le mélange de légumes sautés et de tofu sur le riz brun cuit, saupoudré de graines de sésame."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bowl_riz_brun_legumes_sautés_tofu
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles", "carottes", "poivrons", "oignon rouge", "champignons", "huile d'olive", "vinaigre balsamique", "miel", "moutarde de Dijon", "sel", "poivre", "herbes fraîches (persil, ciboulette)"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles cuites", 1*multiplicateur_arrondi, "carotte coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'herbes fraîches hachées (persil, ciboulette)"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lentilles cuites, les dés de carotte, les lanières de poivron, les quartiers d'oignon rouge et les champignons tranchés.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, le miel, la moutarde de Dijon, du sel, du poivre et les herbes fraîches hachées.\nÉTAPE 3\nArroser la salade de lentilles aux légumes rôtis avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def wraps_vegetariens_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "haricots noirs en conserve", "maïs", "poivrons", "avocat", "tomates", "coriandre fraîche", "jus de lime", "sel", "poivre", "paprika"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "tasse de haricots noirs égouttés et rincés", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tomates coupées en dés", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de lime", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika"]
    préparation = ["ÉTAPE 1\nSur chaque tortilla, étaler une couche de haricots noirs écrasés.\nÉTAPE 2\nDisposer le maïs, les dés de poivron, les tranches d'avocat et les dés de tomates sur les haricots noirs.\nÉTAPE 3\nSaupoudrer de coriandre fraîche hachée et arroser de jus de lime.\nÉTAPE 4\nAssaisonner avec du sel, du poivre et du paprika selon votre goût.\nÉTAPE 5\nEnrouler fermement chaque tortilla en forme de wrap et couper en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wraps_vegetariens_haricots_noirs
def saumon_grille_legumes_saison(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "asperges", "courgettes", "aubergines", "tomates cerises", "ail", "huile d'olive", "sel", "poivre", "jus de citron", "herbes fraîches (thym, romarin)"]
    ingredient = [2*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "tasse d'asperges, extrémités coupées", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "aubergine coupée en rondelles épaisses", 1*multiplicateur_arrondi, "tasse de tomates cerises", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'herbes fraîches (thym, romarin) hachées"]
    préparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nBadigeonner les filets de saumon avec un peu d'huile d'olive et assaisonner avec du sel, du poivre et du jus de citron.\nÉTAPE 3\nSur une plaque de cuisson, disposer les asperges, les rondelles de courgette et d'aubergine, les tomates cerises et l'ail émincé.\nÉTAPE 4\nArroser d'huile d'olive et assaisonner avec du sel, du poivre et des herbes fraîches hachées.\nÉTAPE 5\nPlacer les filets de saumon sur la plaque de cuisson avec les légumes.\nÉTAPE 6\nFaire griller au four pendant 10-12 minutes, ou jusqu'à ce que le saumon soit cuit et les légumes soient tendres.\nÉTAPE 7\nServir le saumon grillé avec les légumes de saison."]
    temps_de_préparation = 10
    temps_de_cuisson = 12
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_legumes_saison
def salade_quinoa_legumes_mediterraneens(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "concombres", "tomates", "poivrons", "oignon rouge", "olives noires", "persil frais", "menthe fraîche", "huile d'olive", "vinaigre balsamique", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomates coupées en dés", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tasse d'olives noires dénoyautées", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de menthe fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les dés de concombre, les dés de tomates, les dés de poivron, les quartiers d'oignon rouge et les olives noires dénoyautées.\nÉTAPE 2\nAjouter le persil frais haché et la menthe fraîche hachée dans le bol.\nÉTAPE 3\nPréparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre balsamique, du sel et du poivre dans un petit bol.\nÉTAPE 4\nArroser la salade de quinoa aux légumes méditerranéens avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_mediterraneens
def wrap_poulet_grille_legumes_mediterraneens(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "filets de poulet", "poivrons", "courgettes", "tomates", "oignon rouge", "ail", "origan", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "tomates coupées en tranches", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nSur chaque tortilla, disposer les lanières de poulet cuit, les lanières de poivron, les lanières de courgette, les tranches de tomates, les quartiers d'oignon rouge, les gousses d'ail émincées et saupoudrer d'origan séché.\nÉTAPE 2\nArroser d'huile d'olive et assaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 3\nEnrouler fermement chaque tortilla en forme de wrap et couper en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_grille_legumes_mediterraneens
def poelee_crevettes_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes", "brocoli", "carottes", "poivrons", "oignon", "ail", "huile d'olive", "sauce soja faible en sodium", "miel", "gingembre frais", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de crevettes décortiquées et déveinées", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon émincé", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter les crevettes décortiquées et déveinées dans la poêle et faire cuire jusqu'à ce qu'elles deviennent roses et opaques.\nÉTAPE 3\nRetirer les crevettes de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter le brocoli en fleurettes, les rondelles de carotte, les lanières de poivron, l'oignon émincé et les gousses d'ail émincées.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter les crevettes réservées dans la poêle et arroser avec la sauce soja faible en sodium, le miel et le gingembre frais râpé.\nÉTAPE 7\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 8\nServir la poêlée de crevettes aux légumes saupoudrée de graines de sésame."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_legumes
def buddha_bowl_pois_chiches_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["pois chiches", "quinoa", "avocat", "concombre", "carottes", "chou kale", "tomates cerises", "graines de courge", "huile d'olive", "jus de citron", "sel", "poivre", "cumin"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pois chiches cuits", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "concombre coupé en rondelles", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "feuilles de chou kale hachées", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "cuillère à soupe de graines de courge grillées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C et préparer une plaque de cuisson recouverte de papier sulfurisé.\nÉTAPE 2\nRincer et égoutter les pois chiches cuits, puis les étaler sur la plaque de cuisson.\nÉTAPE 3\nArroser les pois chiches d'huile d'olive, saupoudrer de cumin moulu, de sel et de poivre, puis mélanger pour bien enrober.\nÉTAPE 4\nFaire cuire au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les pois chiches soient dorés et croustillants.\nÉTAPE 5\nPendant ce temps, préparer les autres ingrédients : cuire le quinoa selon les instructions sur l'emballage et préparer les légumes.\nÉTAPE 6\nAssembler les bols en disposant le quinoa cuit, les pois chiches grillés, les tranches d'avocat, les rondelles de concombre, la carotte râpée, le chou kale haché et les tomates cerises coupées en deux.\nÉTAPE 7\nArroser le tout d'huile d'olive et de jus de citron, puis saupoudrer de graines de courge grillées."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_pois_chiches_grilles
def salade_thon_legumes_croquants(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "concombre", "carottes", "poivrons", "oignon rouge", "céleri", "persil frais", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tasse de céleri tranché", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, émietter le thon en conserve égoutté à la fourchette.\nÉTAPE 2\nAjouter les dés de concombre, la carotte râpée, les dés de poivron, les quartiers d'oignon rouge, le céleri tranché et le persil frais haché dans le bol avec le thon.\nÉTAPE 3\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, du sel et du poivre.\nÉTAPE 4\nArroser la salade de thon aux légumes croquants avec la vinaigrette et mélanger délicatement pour enrober tous les ingrédients."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_legumes_croquants
def wrap_vegetarien_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "haricots noirs en conserve", "avocat", "maïs", "poivron rouge", "oignon rouge", "coriandre fraîche", "jus de citron", "huile d'olive", "sel", "poivre", "piment en poudre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "tasse de haricots noirs égouttés et rincés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de piment en poudre"]
    préparation = ["ÉTAPE 1\nSur chaque tortilla, répartir les haricots noirs égouttés et rincés au centre.\nÉTAPE 2\nAjouter les tranches d'avocat, le maïs égoutté, les lanières de poivron rouge, les rondelles d'oignon rouge et la coriandre fraîche hachée sur les haricots noirs.\nÉTAPE 3\nArroser chaque tortilla avec du jus de citron et de l'huile d'olive, puis assaisonner avec du sel, du poivre et du piment en poudre selon votre goût.\nÉTAPE 4\nEnrouler fermement chaque tortilla pour former un wrap et couper en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_haricots_noirs
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "courge butternut", "poivrons", "oignon rouge", "ail", "huile d'olive", "vinaigre de cidre", "moutarde de Dijon", "sirop d'érable", "cumin", "paprika", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "tasse de courge butternut coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à café de sirop d'érable", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika fumé", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C et préparer une plaque de cuisson recouverte de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les dés de courge butternut, les lanières de poivron, les quartiers d'oignon rouge et les gousses d'ail émincées avec de l'huile d'olive, du vinaigre de cidre, de la moutarde de Dijon, du sirop d'érable, du cumin moulu et du paprika fumé.\nÉTAPE 3\nÉtaler les légumes sur la plaque de cuisson préparée et assaisonner avec du sel et du poivre.\nÉTAPE 4\nFaire rôtir au four pendant 25-30 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient dorés et tendres.\nÉTAPE 5\nDans un grand bol, mélanger les lentilles cuites avec les légumes rôtis et saupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def poke_bowl_saumon_legumes_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["saumon frais", "riz brun", "concombre", "carottes", "avocat", "edamame", "radis", "graines de sésame", "sauce soja faible en sodium", "huile de sésame", "vinaigre de riz", "miel", "gingembre frais", "ail"]
    ingredient = [1*multiplicateur_arrondi, "filet de saumon frais coupé en dés", 1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "carotte coupée en julienne", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tasse d'edamame décortiquées", 1*multiplicateur_arrondi, "radis tranchés", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à café d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de riz", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "gousse d'ail émincée"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de saumon frais avec la sauce soja faible en sodium, l'huile de sésame, le vinaigre de riz, le miel, le gingembre frais râpé et l'ail émincé.\nÉTAPE 2\nLaisser mariner le saumon au réfrigérateur pendant au moins 30 minutes.\nÉTAPE 3\nPendant ce temps, préparer les autres ingrédients : cuire le riz brun selon les instructions sur l'emballage et préparer les légumes.\nÉTAPE 4\nAssembler les bols en disposant le riz brun cuit, le saumon mariné, les dés de concombre, les carottes julienne, les tranches d'avocat, les edamame décortiquées et les radis tranchés.\nÉTAPE 5\nSaupoudrer de graines de sésame grillées avant de servir."]
    temps_de_préparation = 30
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poke_bowl_saumon_legumes_frais
def salade_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "brocoli", "poivrons", "courgette", "tomates cerises", "oignon rouge", "ail", "huile d'olive", "vinaigre de cidre", "sirop d'érable", "sel", "poivre", "thym frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à café de sirop d'érable", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de thym frais haché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C et préparer une plaque de cuisson recouverte de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les fleurettes de brocoli, les lanières de poivron, les dés de courgette, les tomates cerises coupées en deux, les quartiers d'oignon rouge et les gousses d'ail émincées avec de l'huile d'olive, du vinaigre de cidre, du sirop d'érable, du sel, du poivre et du thym frais haché.\nÉTAPE 3\nÉtaler les légumes sur la plaque de cuisson préparée et faire rôtir au four pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que les légumes soient tendres et légèrement dorés.\nÉTAPE 4\nDans un grand bol, mélanger le quinoa cuit avec les légumes rôtis avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_rotis
def bowl_crevettes_legumes_asiatiques(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes", "brocoli", "carottes", "poivrons", "oignon", "sauce soja faible en sodium", "miel", "huile de sésame", "gingembre frais", "ail", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de crevettes décortiquées et déveinées", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en julienne", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon émincé", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, chauffer un peu d'huile de sésame à feu moyen.\nÉTAPE 2\nAjouter les crevettes décortiquées et déveinées dans la poêle et faire cuire jusqu'à ce qu'elles deviennent roses et opaques.\nÉTAPE 3\nRetirer les crevettes de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter les fleurettes de brocoli, les carottes julienne, les lanières de poivron et l'oignon émincé.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter les crevettes réservées dans la poêle et arroser avec la sauce soja faible en sodium, le miel, l'huile de sésame, le gingembre frais râpé et l'ail émincé.\nÉTAPE 7\nMélanger tous les ingrédients dans la poêle jusqu'à ce qu'ils soient bien enrobés de sauce.\nÉTAPE 8\nServir le bowl de crevettes sautées aux légumes asiatiques saupoudré de graines de sésame grillées."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bowl_crevettes_legumes_asiatiques
def tofu_saute_curry_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "carottes", "poivrons", "oignon", "ail", "huile de coco", "curry en poudre", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "tasse de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en julienne", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon émincé", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco", 1*multiplicateur_arrondi, "cuillère à café de curry en poudre", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, faire chauffer l'huile de coco à feu moyen.\nÉTAPE 2\nAjouter les dés de tofu dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter les fleurettes de brocoli, les carottes julienne, les lanières de poivron, l'oignon émincé et l'ail émincé.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter le tofu réservé dans la poêle et saupoudrer de curry en poudre.\nÉTAPE 7\nAssaisonner avec du sel et du poivre selon votre goût et mélanger tous les ingrédients dans la poêle jusqu'à ce qu'ils soient bien enrobés de curry.\nÉTAPE 8\nServir le tofu sauté au curry avec des légumes garni de coriandre fraîche hachée."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_curry_legumes
def bowl_lentilles_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles", "aubergine", "courgette", "poivrons", "oignon rouge", "huile d'olive", "vinaigre de vin rouge", "herbes de Provence", "sel", "poivre", "feta (facultatif)"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles cuites", 1*multiplicateur_arrondi, "aubergine coupée en rondelles épaisses", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence", "sel", "poivre", 1*multiplicateur_arrondi, "feta émiettée (facultatif)"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill du four.\nÉTAPE 2\nDans un grand bol, mélanger les rondelles d'aubergine, les rondelles de courgette, les lanières de poivron et les quartiers d'oignon rouge avec de l'huile d'olive, du vinaigre de vin rouge, des herbes de Provence, du sel et du poivre.\nÉTAPE 3\nDisposer les légumes sur une plaque de cuisson et faire griller au four pendant 15-20 minutes, en retournant à mi-cuisson, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nDans un bol, disposer les lentilles cuites et les légumes grillés.\nÉTAPE 5\nGarnir de feta émiettée si désiré avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bowl_lentilles_legumes_grilles
def saumon_grille_asperges_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["saumon", "asperges", "quinoa", "huile d'olive", "citron", "ail en poudre", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "filet de saumon", 1*multiplicateur_arrondi, "bouquet d'asperges, bouts coupés", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "tranches de citron", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nPréchauffer le grill du four.\nÉTAPE 2\nAssaisonner le saumon avec du sel, du poivre et de l'ail en poudre.\nÉTAPE 3\nDans un plat allant au four, disposer le saumon et les asperges.\nÉTAPE 4\nArroser le saumon et les asperges d'huile d'olive et garnir de tranches de citron.\nÉTAPE 5\nFaire griller au four pendant 10-12 minutes, jusqu'à ce que le saumon soit cuit et les asperges soient tendres.\nÉTAPE 6\nPendant ce temps, faire cuire le quinoa selon les instructions sur l'emballage.\nÉTAPE 7\nServir le saumon grillé et les asperges avec le quinoa cuit.\nÉTAPE 8\nGarnir de persil frais haché avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 12
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_asperges_quinoa
def salade_thon_haricots_blancs_tomates(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomates", "oignon rouge", "persil frais", "huile d'olive", "vinaigre de vin rouge", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve égouttés et rincés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de vin rouge", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon émietté, les haricots blancs égouttés et rincés, les dés de tomate, les dés d'oignon rouge et le persil frais haché.\nÉTAPE 2\nArroser la salade d'huile d'olive et de vinaigre de vin rouge.\nÉTAPE 3\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nMélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de vinaigrette.\nÉTAPE 5\nServir la salade de thon aux haricots blancs et tomates immédiatement ou réfrigérer jusqu'au moment de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs_tomates
def wrap_legumes_grilles_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "poivrons", "courgette", "aubergine", "oignon rouge", "wrap de blé entier", "hummus", "feuilles de laitue", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "filet de poulet cuit et coupé en lanières", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "aubergine coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en lanières", 1*multiplicateur_arrondi, "wrap de blé entier", 1*multiplicateur_arrondi, "cuillère à soupe d'hummus", 1*multiplicateur_arrondi, "feuilles de laitue", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive"]
    préparation = ["ÉTAPE 1\nPréchauffer une poêle grill ou un gril.\nÉTAPE 2\nDans un bol, mélanger les lanières de poulet, de poivron, de courgette, d'aubergine et d'oignon rouge avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nFaire griller les légumes et le poulet jusqu'à ce qu'ils soient cuits et légèrement dorés.\nÉTAPE 4\nChauffer le wrap de blé entier légèrement sur le gril.\nÉTAPE 5\nÉtaler l'hummus sur le wrap chauffé.\nÉTAPE 6\nDisposer les feuilles de laitue sur l'hummus.\nÉTAPE 7\nAjouter les légumes grillés et le poulet sur les feuilles de laitue.\nÉTAPE 8\nEnrouler le wrap fermement et couper en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_legumes_grilles_poulet
def salade_quinoa_legumes_crevettes(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "crevettes", "concombre", "tomates cerises", "poivron", "avocat", "oignon rouge", "jus de citron", "huile d'olive", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "crevettes cuites et décortiquées", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "avocat coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les crevettes cuites et décortiquées, les dés de concombre, les tomates cerises coupées en deux, les dés de poivron, les dés d'avocat et les dés d'oignon rouge.\nÉTAPE 2\nArroser la salade de jus de citron et d'huile d'olive.\nÉTAPE 3\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nAjouter la coriandre fraîche hachée et mélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de vinaigrette.\nÉTAPE 5\nServir la salade de quinoa aux légumes et crevettes immédiatement ou réfrigérer jusqu'au moment de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_crevettes
def poelee_legumes_tofu_teriyaki(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "carottes", "poivrons", "oignon", "champignons", "sauce teriyaki", "huile d'olive", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en julienne", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en lanières", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "cuillère à soupe de sauce teriyaki", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter les dés de tofu dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter le brocoli, les carottes, les poivrons, l'oignon et les champignons.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter le tofu réservé dans la poêle et verser la sauce teriyaki.\nÉTAPE 7\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 8\nMélanger tous les ingrédients dans la poêle jusqu'à ce qu'ils soient bien enrobés de sauce teriyaki.\nÉTAPE 9\nServir la poêlée de légumes au tofu immédiatement, saupoudrée de graines de sésame."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poelee_legumes_tofu_teriyaki
def salade_poulet_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "poivrons", "courgette", "oignon rouge", "laitue", "vinaigrette balsamique", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "filet de poulet cuit et coupé en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette balsamique", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nPréchauffer une poêle grill ou un gril.\nÉTAPE 2\nDans un bol, mélanger les dés de poulet avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nFaire griller les dés de poulet jusqu'à ce qu'ils soient cuits et légèrement dorés.\nÉTAPE 4\nDans la même poêle ou sur le gril, faire griller les lanières de poivron, les rondelles de courgette et les quartiers d'oignon rouge jusqu'à ce qu'ils soient tendres et légèrement grillés.\nÉTAPE 5\nDans un grand bol, disposer les feuilles de laitue.\nÉTAPE 6\nAjouter les légumes grillés et les dés de poulet sur les feuilles de laitue.\nÉTAPE 7\nArroser la salade de vinaigrette balsamique.\nÉTAPE 8\nMélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de vinaigrette.\nÉTAPE 9\nServir la salade de poulet aux légumes grillés immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_legumes_grilles
def poisson_four_legumes_mediterraneens(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de poisson blanc", "tomates", "oignon rouge", "ail", "olives noires", "câpres", "huile d'olive", "jus de citron", "origan séché", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de poisson blanc", 1*multiplicateur_arrondi, "tomates coupées en quartiers", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "olives noires dénoyautées", 1*multiplicateur_arrondi, "cuillère à soupe de câpres", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nDans un plat allant au four, disposer les quartiers de tomates, les quartiers d'oignon rouge, les gousses d'ail émincées, les olives noires dénoyautées et les câpres.\nÉTAPE 3\nArroser les légumes d'huile d'olive et de jus de citron.\nÉTAPE 4\nSaupoudrer d'origan séché, de sel et de poivre selon votre goût.\nÉTAPE 5\nDéposer les filets de poisson sur les légumes dans le plat allant au four.\nÉTAPE 6\nArroser les filets de poisson d'un filet d'huile d'olive supplémentaire et de jus de citron.\nÉTAPE 7\nAssaisonner les filets de poisson avec du sel et du poivre selon votre goût.\nÉTAPE 8\nCuire au four préchauffé pendant 15-20 minutes, ou jusqu'à ce que le poisson soit cuit et les légumes soient tendres.\nÉTAPE 9\nServir le poisson au four avec les légumes méditerranéens immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

poisson_four_legumes_mediterraneens
def sandwich_chevre_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["fromage de chèvre", "poivrons", "courgette", "aubergine", "oignon rouge", "pain au levain", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranche de fromage de chèvre", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "aubergine coupée en rondelles épaisses", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles épaisses", 1*multiplicateur_arrondi, "tranche de pain au levain", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nPréchauffer une poêle grill ou un gril.\nÉTAPE 2\nDans un bol, mélanger les lanières de poivron, les rondelles de courgette, les rondelles d'aubergine et les rondelles d'oignon rouge avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nFaire griller les légumes jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nFaire griller les tranches de pain au levain sur le gril jusqu'à ce qu'elles soient légèrement croustillantes.\nÉTAPE 5\nÉtaler une tranche de fromage de chèvre sur une tranche de pain grillé.\nÉTAPE 6\nDisposer les légumes grillés sur le fromage de chèvre.\nÉTAPE 7\nCouvrir avec une autre tranche de pain grillé.\nÉTAPE 8\nPresser légèrement le sandwich pour faire adhérer les ingrédients.\nÉTAPE 9\nCouper le sandwich en deux avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

sandwich_chevre_legumes_grilles
def salade_lentilles_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "poivrons", "courgette", "oignon rouge", "tomates cerises", "feuilles de laitue", "vinaigrette au citron", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette au citron", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nPréchauffer une poêle grill ou un gril.\nÉTAPE 2\nDans un bol, mélanger les lentilles cuites avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nFaire griller les lanières de poivron, les rondelles de courgette et les quartiers d'oignon rouge jusqu'à ce qu'ils soient tendres et légèrement grillés.\nÉTAPE 4\nDans un grand bol, disposer les feuilles de laitue.\nÉTAPE 5\nAjouter les légumes grillés, les tomates cerises et les lentilles sur les feuilles de laitue.\nÉTAPE 6\nArroser la salade de vinaigrette au citron.\nÉTAPE 7\nMélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de vinaigrette.\nÉTAPE 8\nServir la salade de lentilles et légumes grillés immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_grilles
def wrap_vegetarien_haricots_noirs_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots noirs", "avocat", "maïs", "tomates", "oignon rouge", "wrap de blé entier", "coriandre fraîche", "sel", "poivre", "sauce piquante"]
    ingredient = [1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "tomates coupées en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "wrap de blé entier", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de sauce piquante"]
    préparation = ["ÉTAPE 1\nDans un bol, mélanger les haricots noirs cuits, les tranches d'avocat, le maïs égoutté, les dés de tomates et les dés d'oignon rouge.\nÉTAPE 2\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 3\nChauffer légèrement le wrap de blé entier sur une poêle ou au micro-ondes.\nÉTAPE 4\nÉtaler une cuillère à soupe de sauce piquante sur le wrap chauffé.\nÉTAPE 5\nDisposer le mélange de haricots noirs et de légumes sur le wrap.\nÉTAPE 6\nParsemer de coriandre fraîche hachée.\nÉTAPE 7\nEnrouler fermement le wrap et couper en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_haricots_noirs_avocat
def tofu_saute_legumes_sesame(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "carottes", "poivrons", "oignon", "sauce soja", "huile de sésame", "vinaigre de riz", "miel", "gingembre frais", "ail", "graines de sésame", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en julienne", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en lanières", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de riz", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter les dés de tofu dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire et faire sauter le brocoli, les carottes, les poivrons et les oignons jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 5\nPendant ce temps, préparer la sauce en mélangeant la sauce soja, l'huile de sésame, le vinaigre de riz, le miel, le gingembre râpé et l'ail émincé.\nÉTAPE 6\nAjouter le tofu réservé dans la poêle avec les légumes sautés.\nÉTAPE 7\nVerser la sauce sur le tofu et les légumes dans la poêle.\nÉTAPE 8\nMélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de sauce.\nÉTAPE 9\nSaupoudrer de graines de sésame avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_legumes_sesame
def salade_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivrons", "courgette", "oignon rouge", "tomates cerises", "épinards frais", "vinaigrette balsamique", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles épaisses", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette balsamique", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nDans un plat allant au four, disposer les lanières de poivron, les rondelles de courgette et les quartiers d'oignon rouge.\nÉTAPE 3\nArroser les légumes d'huile d'olive, de sel et de poivre.\nÉTAPE 4\nRôtir les légumes au four préchauffé pendant 20-25 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 5\nDans un grand bol, mélanger le quinoa cuit, les tomates cerises coupées en deux, les épinards frais et les légumes rôtis.\nÉTAPE 6\nArroser la salade de vinaigrette balsamique.\nÉTAPE 7\nMélanger tous les ingrédients jusqu'à ce qu'ils soient bien enrobés de vinaigrette.\nÉTAPE 8\nServir la salade de quinoa aux légumes rôtis immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_rotis
def wraps_dinde_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de dinde", "avocat", "feuilles de laitue", "tomates", "tortillas de blé", "mayonnaise légère", "moutarde", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de dinde cuit et coupé en lanières", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "tomates coupées en tranches", 1*multiplicateur_arrondi, "tortillas de blé", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nSur chaque tortilla de blé, étaler une cuillère à soupe de mayonnaise légère et une cuillère à café de moutarde.\nÉTAPE 2\nDisposer les lanières de dinde cuites, les tranches d'avocat, les feuilles de laitue et les tranches de tomates sur les tortillas.\nÉTAPE 3\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nEnrouler fermement les tortillas pour former des wraps.\nÉTAPE 5\nCouper les wraps en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wraps_dinde_avocat
def soupe_legumes_maison(multiplicateur_arrondi):
    ingredient_pour_algo = ["carottes", "céleri", "poireaux", "pommes de terre", "oignon", "ail", "bouillon de légumes", "thym frais", "feuilles de laurier", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "branche de céleri coupée en dés", 1*multiplicateur_arrondi, "poireau coupé en rondelles", 1*multiplicateur_arrondi, "pomme de terre coupée en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 1*multiplicateur_arrondi, "gousse d'ail émincée", 3*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "cuillère à café de thym frais", 2*multiplicateur_arrondi, "feuilles de laurier", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon et l'ail émincés dans la casserole et faire revenir jusqu'à ce qu'ils soient dorés et parfumés.\nÉTAPE 3\nAjouter les carottes, le céleri, le poireau et les pommes de terre coupés en dés dans la casserole.\nÉTAPE 4\nVerser le bouillon de légumes dans la casserole et porter à ébullition.\nÉTAPE 5\nRéduire le feu et laisser mijoter la soupe pendant 20-25 minutes, ou jusqu'à ce que les légumes soient tendres.\nÉTAPE 6\nRetirer la casserole du feu et retirer les feuilles de laurier.\nÉTAPE 7\nUtiliser un mixeur plongeant pour réduire la soupe en purée lisse.\nÉTAPE 8\nAssaisonner la soupe de sel, de poivre et de thym frais.\nÉTAPE 9\nServir la soupe de légumes maison chaude, garnie de feuilles de thym frais."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

soupe_legumes_maison
def salade_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "avocat", "concombre", "tomates", "feuilles de laitue", "vinaigrette légère", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "poitrine de poulet grillée et coupée en tranches", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomates coupées en dés", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les tranches de poulet grillées, les tranches d'avocat, les dés de concombre et les dés de tomates.\nÉTAPE 2\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 3\nDisposer les feuilles de laitue sur une assiette de service.\nÉTAPE 4\nPlacer le mélange de poulet, avocat et légumes sur les feuilles de laitue.\nÉTAPE 5\nArroser la salade de vinaigrette légère et d'huile d'olive.\nÉTAPE 6\nServir la salade de poulet grillé et avocat immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_avocat
def buddha_bowl_legumes_riz_complet(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz complet", "brocoli", "carottes", "pois chiches", "avocat", "graines de sésame", "vinaigrette au tahini", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz complet cuit", 1*multiplicateur_arrondi, "tasse de brocoli cuit en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette au tahini", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nDans un bol, disposer le riz complet cuit au fond du bol.\nÉTAPE 2\nDisposer le brocoli cuit, les rondelles de carotte, les pois chiches cuits et les tranches d'avocat sur le riz.\nÉTAPE 3\nSaupoudrer de graines de sésame sur les légumes.\nÉTAPE 4\nArroser le buddha bowl de vinaigrette au tahini et d'huile d'olive.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nServir le buddha bowl aux légumes et riz complet immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_riz_complet
def chili_vegetarien_haricots(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots rouges", "haricots noirs", "poivrons", "oignon", "ail", "tomates concassées en conserve", "piment en poudre", "cumin moulu", "paprika fumé", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "tasse de haricots rouges cuits et égouttés", 1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "tasse de tomates concassées en conserve", 1*multiplicateur_arrondi, "cuillère à café de piment en poudre", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika fumé", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    préparation = ["ÉTAPE 1\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon et l'ail émincés dans la casserole et faire revenir jusqu'à ce qu'ils soient dorés et parfumés.\nÉTAPE 3\nAjouter les poivrons coupés en dés dans la casserole et faire cuire jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nIncorporer les haricots rouges et noirs cuits, les tomates concassées en conserve, le piment en poudre, le cumin moulu et le paprika fumé dans la casserole.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nLaisser mijoter le chili végétarien pendant 15-20 minutes, en remuant de temps en temps.\nÉTAPE 7\nServir le chili végétarien aux haricots chaud, garni de coriandre fraîche hachée."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

chili_vegetarien_haricots
def wrap_vegetarien_pois_chiches(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé", "pois chiches", "avocat", "tomates", "concombre", "feuilles de laitue", "houmous", "sel", "poivre", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tomates coupées en tranches", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de houmous", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    préparation = ["ÉTAPE 1\nÉtaler une tortilla de blé sur une surface propre.\nÉTAPE 2\nÉtaler une cuillère à soupe de houmous sur la tortilla.\nÉTAPE 3\nDisposer les pois chiches cuits, les tranches d'avocat, les tranches de tomate, les dés de concombre et les feuilles de laitue sur la tortilla.\nÉTAPE 4\nAssaisonner avec du sel, du poivre et du jus de citron selon votre goût.\nÉTAPE 5\nEnrouler fermement la tortilla pour former un wrap.\nÉTAPE 6\nCouper le wrap en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_pois_chiches
def salade_lentilles_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles", "feta", "tomates cerises", "concombre", "oignon rouge", "persil frais", "vinaigrette au citron", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles cuites", 1*multiplicateur_arrondi, "feta émiettée", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette au citron", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lentilles cuites, la feta émiettée, les tomates cerises coupées en deux, les dés de concombre et les rondelles d'oignon rouge.\nÉTAPE 2\nAjouter le persil frais haché dans le bol et mélanger délicatement.\nÉTAPE 3\nArroser la salade de vinaigrette au citron et d'huile d'olive.\nÉTAPE 4\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 5\nServir la salade de lentilles et feta immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_feta
def tofu_sauté_legumes_sauce_soja(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "carottes", "poivrons", "oignon", "sauce soja", "huile de sésame", "vinaigre de riz", "miel", "gingembre frais", "ail", "graines de sésame", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "bloc de tofu ferme coupé en cubes", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carotte coupée en dés", 1*multiplicateur_arrondi, "poivron coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à café d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de riz", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans une poêle, chauffer une cuillère à soupe d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter le tofu coupé en cubes dans la poêle et faire sauter jusqu'à ce qu'il soit doré et croustillant.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire.\nÉTAPE 5\nAjouter l'oignon, l'ail et le gingembre dans la poêle et faire sauter jusqu'à ce qu'ils soient parfumés.\nÉTAPE 6\nIncorporer les carottes, le brocoli et les poivrons dans la poêle et faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 7\nAjouter le tofu réservé dans la poêle.\nÉTAPE 8\nVerser la sauce soja, l'huile de sésame, le vinaigre de riz et le miel dans la poêle et mélanger délicatement.\nÉTAPE 9\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 10\nSaupoudrer de graines de sésame avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

tofu_sauté_legumes_sauce_soja
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivrons", "courgettes", "oignon rouge", "tomates cerises", "épinards frais", "jus de citron", "huile d'olive", "sel", "poivre", "herbes de Provence"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "poivron coupé en lanières et grillé", 1*multiplicateur_arrondi, "courgette coupée en rondelles et grillée", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles et grillé", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'herbes de Provence"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa cuit, les poivrons grillés, les courgettes grillées, les oignons rouges grillés, les tomates cerises et les épinards frais.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron, l'huile d'olive, le sel, le poivre et les herbes de Provence.\nÉTAPE 3\nVerser la vinaigrette sur la salade de quinoa et légumes grillés et mélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 4\nServir la salade de quinoa aux légumes grillés immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def bowl_poulet_teriyaki_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "brocoli", "carottes", "pois mange-tout", "riz", "sauce teriyaki", "huile d'olive", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "filet de poulet coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli cuit", 1*multiplicateur_arrondi, "carotte coupée en rondelles", 1*multiplicateur_arrondi, "tasse de pois mange-tout", 1*multiplicateur_arrondi, "tasse de riz cuit", 1*multiplicateur_arrondi, "cuillère à soupe de sauce teriyaki", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame"]
    préparation = ["ÉTAPE 1\nDans une poêle, chauffer une cuillère à soupe d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter le poulet coupé en dés dans la poêle et faire cuire jusqu'à ce qu'il soit doré et cuit à travers.\nÉTAPE 3\nRetirer le poulet de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire.\nÉTAPE 5\nAjouter le brocoli cuit, les rondelles de carotte et les pois mange-tout dans la poêle et faire sauter jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nIncorporer le poulet réservé et le riz cuit dans la poêle.\nÉTAPE 7\nVerser la sauce teriyaki sur le mélange et mélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 8\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 9\nSaupoudrer de graines de sésame avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bowl_poulet_teriyaki_legumes
def salade_pates_pesto_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pâtes", "poivrons", "courgettes", "tomates cerises", "oignon rouge", "pesto", "jus de citron", "huile d'olive", "sel", "poivre", "parmesan râpé"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pâtes cuites", 1*multiplicateur_arrondi, "poivron coupé en lanières et grillé", 1*multiplicateur_arrondi, "courgette coupée en rondelles et grillée", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de pesto", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de parmesan râpé"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les pâtes cuites, les poivrons grillés, les courgettes grillées, les tomates cerises, et les rondelles d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant le pesto, le jus de citron, l'huile d'olive, le sel et le poivre.\nÉTAPE 3\nVerser la vinaigrette sur la salade de pâtes et légumes et mélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 4\nSaupoudrer de parmesan râpé avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_pates_pesto_legumes
def buddha_bowl_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz complet", "haricots noirs", "avocat", "maïs", "tomates cerises", "poivron rouge", "poivron jaune", "carotte", "épinards frais", "sauce tahini", "jus de citron", "sel", "poivre", "graines de sésame"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz complet cuit", 1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "avocat coupé en tranches", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune coupé en lanières", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de sauce tahini", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame"]
    préparation = ["ÉTAPE 1\nDans un grand bol, disposer le riz complet cuit comme base du Buddha Bowl.\nÉTAPE 2\nDisposer les haricots noirs cuits et égouttés, les tranches d'avocat, le maïs, les tomates cerises, les lanières de poivron rouge et jaune, la carotte râpée et les épinards frais sur le riz.\nÉTAPE 3\nDans un petit bol, préparer la sauce en mélangeant la sauce tahini, le jus de citron, le sel et le poivre.\nÉTAPE 4\nVerser la sauce sur le Buddha Bowl.\nÉTAPE 5\nSaupoudrer de graines de sésame avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_haricots_noirs
def sandwich_thon_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "thon en conserve", "avocat", "concombre", "tomates", "oignon rouge", "mayonnaise légère", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranche de pain complet", 1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "avocat écrasé", 1*multiplicateur_arrondi, "tranches de concombre", 1*multiplicateur_arrondi, "tranches de tomate", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nTartiner une tranche de pain complet avec la mayonnaise légère.\nÉTAPE 2\nDisposer le thon égoutté sur la tranche de pain.\nÉTAPE 3\nÉtaler l'avocat écrasé sur le thon.\nÉTAPE 4\nDisposer les tranches de concombre, les tranches de tomate et les rondelles d'oignon rouge sur l'avocat.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nTartiner une autre tranche de pain avec la moutarde de Dijon et placer sur le dessus pour former un sandwich.\nÉTAPE 7\nCouper le sandwich en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

sandwich_thon_legumes
def salade_pommes_terre_thon(multiplicateur_arrondi):
    ingredient_pour_algo = ["pommes de terre", "thon en conserve", "œufs", "haricots verts", "oignon rouge", "vinaigrette à la moutarde", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pommes de terre cuites et coupées en dés", 1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "œufs durs coupés en quartiers", 1*multiplicateur_arrondi, "tasse de haricots verts cuits et coupés en tronçons", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette à la moutarde", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    préparation = ["ÉTAPE 1\nDans un grand bol, mélanger les pommes de terre cuites, le thon égoutté, les œufs durs, les haricots verts et les rondelles d'oignon rouge.\nÉTAPE 2\nArroser la salade de vinaigrette à la moutarde et mélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 3\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nSaupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_pommes_terre_thon
def wrap_poulet_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "filet de poulet", "avocat", "concombre", "carotte", "feuilles de laitue", "mayonnaise légère", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé entier", 1*multiplicateur_arrondi, "filet de poulet cuit et tranché", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "concombre en lanières", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement la tortilla de blé entier pour la rendre plus souple.\nÉTAPE 2\nTartiner la tortilla avec la mayonnaise légère et la moutarde de Dijon.\nÉTAPE 3\nDisposer le filet de poulet tranché, les tranches d'avocat, les lanières de concombre, la carotte râpée et les feuilles de laitue sur la tortilla.\nÉTAPE 4\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 5\nEnrouler la tortilla fermement pour former un wrap.\nÉTAPE 6\nCouper le wrap en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_legumes
def salade_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "courgettes", "aubergines", "poivrons rouges", "oignon rouge", "tomates cerises", "épinards frais", "vinaigrette balsamique", "sel", "poivre", "origan séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette balsamique", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'origan séché"]
    préparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand bol, mélanger les rondelles de courgette, les dés d'aubergine, les lanières de poivron rouge et les rondelles d'oignon rouge avec un peu d'huile d'olive, du sel, du poivre et de l'origan séché.\nÉTAPE 3\nÉtaler les légumes sur une plaque de cuisson et rôtir au four pendant environ 20 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nDans un grand bol, mélanger le quinoa cuit, les légumes rôtis, les tomates cerises coupées en deux et les épinards frais.\nÉTAPE 5\nAssaisonner la salade avec la vinaigrette balsamique, du sel et du poivre selon votre goût.\nÉTAPE 6\nServir la salade de quinoa et légumes rôtis immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_rotis
def bol_riz_legumes_sautés(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz brun", "brocoli", "carottes", "pois mange-tout", "champignons", "oignon", "sauce soja légère", "huile de sésame", "ail", "gingembre frais", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "tasse de pois mange-tout coupés en deux", 1*multiplicateur_arrondi, "tasse de champignons tranchés", 1*multiplicateur_arrondi, "oignon coupé en lamelles", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja légère", 1*multiplicateur_arrondi, "cuillère à café d'huile de sésame", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nDans une poêle, chauffer un peu d'huile d'olive à feu moyen-vif.\nÉTAPE 2\nAjouter l'oignon coupé en lamelles et faire sauter jusqu'à ce qu'il soit translucide.\nÉTAPE 3\nAjouter l'ail émincé et le gingembre frais râpé dans la poêle et faire sauter pendant environ une minute, jusqu'à ce qu'ils soient parfumés.\nÉTAPE 4\nAjouter les carottes coupées en rondelles, les pois mange-tout coupés en deux, les champignons tranchés et les fleurettes de brocoli dans la poêle.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 6\nAjouter le riz cuit dans la poêle avec les légumes.\nÉTAPE 7\nVerser la sauce soja légère et l'huile de sésame sur le riz et les légumes, puis mélanger pour bien enrober tous les ingrédients.\nÉTAPE 8\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 9\nServir le bol de riz aux légumes sautés chaud."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

bol_riz_legumes_sautés
def salade_lentilles_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "poivrons rouges", "courgettes", "aubergines", "oignon rouge", "tomates cerises", "feuilles de basilic frais", "vinaigrette balsamique", "sel", "poivre", "huile d'olive"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "tomates cerises coupées en deux", 1*multiplicateur_arrondi, "feuilles de basilic frais", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette balsamique", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive"]
    préparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-vif.\nÉTAPE 2\nDans un bol, mélanger les lanières de poivron rouge, les rondelles de courgette, les dés d'aubergine et les rondelles d'oignon rouge avec un peu d'huile d'olive, du sel et du poivre.\nÉTAPE 3\nDisposer les légumes sur une plaque de cuisson et griller au four pendant environ 15 minutes, en retournant à mi-cuisson, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nDans un grand bol, mélanger les lentilles cuites, les légumes grillés, les tomates cerises coupées en deux et les feuilles de basilic frais.\nÉTAPE 5\nAssaisonner la salade avec la vinaigrette balsamique, du sel et du poivre selon votre goût.\nÉTAPE 6\nServir la salade de lentilles et de légumes grillés immédiatement."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_grilles
def wrap_tempeh_legumes_marines(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "tempeh", "poivrons rouges", "concombre", "carotte", "feuilles de laitue", "vinaigrette au citron vert", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé entier", 1*multiplicateur_arrondi, "tempeh coupé en tranches", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "concombre en lanières", 1*multiplicateur_arrondi, "carotte râpée", 1*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette au citron vert", "sel", "poivre"]
    préparation = ["ÉTAPE 1\nChauffer légèrement la tortilla de blé entier pour la rendre plus souple.\nÉTAPE 2\nDans une poêle, faire revenir les tranches de tempeh jusqu'à ce qu'elles soient dorées et croustillantes.\nÉTAPE 3\nTartiner la tortilla avec un peu de vinaigrette au citron vert.\nÉTAPE 4\nDisposer les tranches de tempeh, les lanières de poivron rouge, les lanières de concombre, la carotte râpée et les feuilles de laitue sur la tortilla.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nEnrouler la tortilla fermement pour former un wrap.\nÉTAPE 7\nCouper le wrap en deux avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_préparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous

wrap_tempeh_legumes_marines


tuple_du_soir_pour_perte_de_poids=[bol_lentilles_legumes_avocat,bol_lentilles_legumes_rotis,bol_quinoa_legumes_rotis,bol_riz_brun_legumes_rotis,bol_riz_brun_legumes_sautes,bol_riz_sauvage_legumes_rotis,bowl_crevettes_legumes_asiatiques,bowl_lentilles_legumes_grilles,bowl_poulet_teriyaki_legumes,bowl_quinoa_legumes_rotis,buddha_bowl_haricots_noirs,buddha_bowl_legumes_grilles,buddha_bowl_legumes_quinoa,buddha_bowl_legumes_riz_complet,buddha_bowl_pois_chiches_grilles,buddha_bowl_quinoa_legumes_sautes,chili_vegetarien_haricots,courgettes_farcies_viande_maigre,omelette_champignons_epinards,poelee_crevettes_legumes,poelee_legumes_tofu_teriyaki,poisson_four_legumes_mediterraneens,poisson_papillote_herbes,poke_bowl_saumon_legumes_frais,poulet_grille_salade_chou_kale,quinoa_legumes_grilles,salade_crevettes_avocat,salade_lentilles_feta,salade_lentilles_legumes_grilles,salade_lentilles_legumes_rotis,salade_pates_pesto_legumes,salade_pois_chiches_legumes_grilles,salade_pommes_terre_thon,salade_poulet_avocat,salade_poulet_cesar_legere,salade_poulet_grille_vinaigrette,salade_poulet_legumes_grilles,salade_poulet_legumes_verts,salade_poulet_quinoa_avocat,salade_poulet_quinoa_legumes_croquants,salade_quinoa_avocat,salade_quinoa_epinards_feta,salade_quinoa_haricots_noirs_legumes,salade_quinoa_legumes_crevettes,salade_quinoa_legumes_grilles,salade_quinoa_legumes_mediterraneens,salade_quinoa_legumes_rotis,salade_thon_haricots_blancs,salade_thon_haricots_blancs_tomates,salade_thon_haricots_legumes_croquants,salade_thon_haricots_verts_tomates,salade_thon_legumes_croquants,sandwich_chevre_legumes_grilles,sandwich_thon_legumes,sandwich_thon_legumes_croquants,sandwich_thon_legumes_grilles,saumon_cuit_four_legumes_grilles,saumon_grille_asperges_quinoa,saumon_grille_legumes_saison,soupe_legumes,soupe_legumes_maison,soupe_lentilles_corail_legumes,tofu_saute_brocoli_poivrons,tofu_saute_curry_legumes,tofu_saute_legumes_sesame,wrap_dinde_avocat,wrap_dinde_legumes_grilles,wrap_legumes_grilles_poulet,wrap_poulet_grille_legumes,wrap_poulet_grille_legumes_mediterraneens,wrap_poulet_legumes,wrap_saumon_fume_avocat,wrap_tempeh_legumes_marines,wrap_vegetarien_haricots_noirs,wrap_vegetarien_haricots_noirs_avocat,wrap_vegetarien_legumes_grilles,wrap_vegetarien_mediterraneen,wrap_vegetarien_pois_chiches,wraps_dinde_avocat,wraps_dinde_legumes_croquants,wraps_vegetariens_haricots_noirs]


