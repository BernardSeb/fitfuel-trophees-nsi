def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "bouillon de légumes", "poivron rouge", "courgette", "aubergine", "oignon rouge", "huile d'olive", "sel", "poivre", "jus de citron", "menthe fraîche", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 2*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "aubergine, coupée en rondelles", 1*multiplicateur_arrondi, "oignon rouge, coupé en lanières", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses de bouillon de légumes à ébullition. Ajouter le quinoa non cuit et laisser mijoter pendant environ 15 à 20 minutes, ou jusqu'à ce que le quinoa soit tendre et que tout le liquide soit absorbé. Retirer du feu et laisser reposer quelques minutes avant de l'égoutter.\nÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé. Badigeonner les rondelles de courgette, d'aubergine et les lanières de poivron rouge et d'oignon rouge avec de l'huile d'olive extra-vierge. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 3\nGriller les légumes préparés sur le gril préchauffé pendant environ 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient tendres et légèrement dorés. Retirer du gril et réserver.\nÉTAPE 4\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés. Assaisonner avec du jus de citron frais, de la menthe fraîche hachée et de la coriandre fraîche hachée. Mélanger délicatement pour combiner tous les ingrédients.\nÉTAPE 5\nServir la salade de quinoa aux légumes grillés immédiatement ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 20
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomates cerises", "concombre", "oignon rouge", "persil frais", "vinaigre de vin blanc", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "boîtes de thon en conserve dans de l'eau, égouttées", 2*multiplicateur_arrondi, "tasses de haricots blancs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 1*multiplicateur_arrondi, "concombre, coupé en dés", 1*multiplicateur_arrondi, "oignon rouge, haché finement", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de vin blanc", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, émietter le thon en conserve égoutté à la fourchette.\nÉTAPE 2\nAjouter les haricots blancs égouttés et rincés dans le bol avec le thon.\nÉTAPE 3\nAjouter les tomates cerises coupées en deux, les dés de concombre, l'oignon rouge haché finement et le persil frais haché.\nÉTAPE 4\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre de vin blanc avec de l'huile d'olive extra-vierge. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 5\nVerser la vinaigrette préparée sur la salade de thon et de haricots blancs dans le grand bol. Mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 6\nServir la salade de thon aux haricots blancs immédiatement ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def saumon_grille_quinoa_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "quinoa", "bouillon de légumes", "brocoli", "poivron rouge", "oignon rouge", "huile d'olive", "sel", "poivre", "jus de citron", "persil frais"]
    ingredient = [2*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "tasse de quinoa", 2*multiplicateur_arrondi, "tasses de bouillon de légumes", 2*multiplicateur_arrondi, "tasses de brocoli, coupées en petits bouquets", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en lanières", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses de bouillon de légumes à ébullition. Ajouter le quinoa non cuit et laisser mijoter pendant environ 15 à 20 minutes, ou jusqu'à ce que le quinoa soit tendre et que tout le liquide soit absorbé. Retirer du feu et laisser reposer quelques minutes avant de l'égoutter.\nÉTAPE 2\nPendant ce temps, préchauffer le gril à feu moyen-élevé. Badigeonner les filets de saumon avec un peu d'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût.\nÉTAPE 3\nGriller les filets de saumon préparés sur le gril préchauffé pendant environ 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits à travers et légèrement dorés sur les bords. Retirer du gril et réserver.\nÉTAPE 4\nDans une grande poêle, chauffer un peu d'huile d'olive à feu moyen. Ajouter les lanières de poivron rouge et d'oignon rouge et faire sauter pendant environ 5 minutes, ou jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 5\nAjouter les petits bouquets de brocoli dans la poêle avec les poivrons et les oignons. Faire sauter quelques minutes de plus jusqu'à ce que le brocoli soit tendre mais encore croquant.\nÉTAPE 6\nDans un grand bol, mélanger le quinoa cuit avec les légumes sautés. Assaisonner avec du jus de citron frais, du persil frais haché, du sel et du poivre au goût.\nÉTAPE 7\nServir les filets de saumon grillés sur un lit de quinoa aux légumes immédiatement."]
    temps_de_preparation = 20
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_quinoa_legumes
def wraps_poulet_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "tortillas de blé entier", "poivron rouge", "courgette", "oignon rouge", "huile d'olive", "sel", "poivre", "paprika", "cumin moulu", "ail en poudre", "yaourt grec nature", "jus de citron", "menthe fraîche", "coriandre fraîche", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet, coupés en lanières", 4*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "courgette, coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en lanières", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1*multiplicateur_arrondi, "tasse de yaourt grec nature", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol moyen, mélanger les lanières de poulet avec le paprika, le cumin moulu, l'ail en poudre, du sel et du poivre. Assurez-vous que les lanières de poulet sont bien enrobées d'épices.\nÉTAPE 2\nDans une grande poêle, chauffer 1 cuillère à soupe d'huile d'olive à feu moyen-élevé. Ajouter les lanières de poulet assaisonnées et cuire environ 5 à 7 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers. Retirer de la poêle et réserver.\nÉTAPE 3\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire. Ajouter les lanières de poivron rouge, de courgette et d'oignon rouge. Faire sauter pendant environ 5 minutes, ou jusqu'à ce que les légumes soient tendres mais encore croquants. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nDans un petit bol, mélanger le yaourt grec nature avec le jus de citron frais, la menthe fraîche hachée et la coriandre fraîche hachée. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 5\nPour assembler les wraps, répartir le mélange de poulet et de légumes grillés sur les tortillas de blé entier. Garnir de sauce au yaourt grec et rouler les tortillas fermement. Couper les wraps en deux et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_poulet_legumes_grilles
def poulet_teriyaki_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "sauce teriyaki", "ail", "gingembre frais", "brocoli", "carottes", "poivron rouge", "oignon", "huile de sésame", "sel", "poivre", "graines de sésame", "oignon vert"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet, coupés en lanières", 1*multiplicateur_arrondi, "tasse de sauce teriyaki", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", 2*multiplicateur_arrondi, "tasses de brocoli, coupées en petits bouquets", 2*multiplicateur_arrondi, "grosses carottes, coupées en tranches fines", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile de sésame", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées", 1*multiplicateur_arrondi, "oignon vert, tranché finement"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mariner les lanières de poulet avec la sauce teriyaki, l'ail émincé et le gingembre frais râpé pendant environ 15 à 20 minutes au réfrigérateur.\nÉTAPE 2\nDans une grande poêle ou un wok, chauffer un peu d'huile de sésame à feu moyen-élevé. Ajouter les lanières de poulet marinées et cuire pendant environ 5 à 7 minutes, ou jusqu'à ce qu'elles soient dorées et cuites à travers. Retirer le poulet de la poêle et réserver.\nÉTAPE 3\nDans la même poêle, ajouter un peu plus d'huile de sésame si nécessaire. Ajouter les bouquets de brocoli, les tranches de carottes, les lanières de poivron rouge et les tranches d'oignon. Faire sauter pendant environ 5 minutes, ou jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 4\nRemettre les lanières de poulet réservées dans la poêle avec les légumes sautés. Assaisonner avec du sel et du poivre au goût. Faire sauter pendant quelques minutes de plus pour réchauffer le poulet.\nÉTAPE 5\nServir le poulet teriyaki aux légumes sautés chaud, garni de graines de sésame grillées et d'oignons verts tranchés."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_teriyaki_legumes_sautes
def soupe_minestrone_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pâtes courtes", "tomates en conserve", "haricots rouges en conserve", "carottes", "céleri", "oignon", "ail", "épinards frais", "bouillon de légumes", "sel", "poivre", "origan séché", "basilic séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pâtes courtes non cuites", 2*multiplicateur_arrondi, "tasses de tomates en conserve, avec leur jus", 1*multiplicateur_arrondi, "tasse de haricots rouges en conserve, égouttés et rincés", 2*multiplicateur_arrondi, "carottes, coupées en dés", 2*multiplicateur_arrondi, "branches de céleri, coupées en dés", 1*multiplicateur_arrondi, "oignon, haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 4*multiplicateur_arrondi, "tasses d'épinards frais", 6*multiplicateur_arrondi, "tasses de bouillon de légumes", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", 1*multiplicateur_arrondi, "cuillère à café de basilic séché"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, porter une grande quantité d'eau à ébullition. Ajouter les pâtes courtes non cuites et cuire selon les instructions sur l'emballage. Égoutter et réserver.\nÉTAPE 2\nDans la même casserole, chauffer un peu d'huile d'olive à feu moyen. Ajouter l'oignon haché, les carottes coupées en dés et les branches de céleri coupées en dés. Faire sauter pendant quelques minutes jusqu'à ce que les légumes commencent à ramollir.\nÉTAPE 3\nAjouter les gousses d'ail émincées et cuire encore quelques minutes jusqu'à ce qu'elles deviennent parfumées.\nÉTAPE 4\nAjouter les tomates en conserve avec leur jus, les haricots rouges égouttés et rincés, le bouillon de légumes, l'origan séché et le basilic séché. Porter à ébullition, puis réduire le feu et laisser mijoter pendant environ 10 minutes.\nÉTAPE 5\nIncorporer les épinards frais dans la soupe et laisser mijoter encore quelques minutes jusqu'à ce qu'ils soient juste flétris. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 6\nServir la soupe minestrone chaude avec les pâtes cuites réservées."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_minestrone_legumes
def salade_poulet_cajun(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "poivron rouge", "maïs en conserve", "haricots noirs en conserve", "tomates cerises", "oignon rouge", "laitue", "avocat", "coriandre fraîche", "jus de citron", "huile d'olive", "sel", "poivre", "mélange d'épices cajun"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet désossés et sans peau", 1*multiplicateur_arrondi, "poivron rouge, coupé en dés", 1*multiplicateur_arrondi, "tasse de maïs en conserve, égoutté et rincé", 1*multiplicateur_arrondi, "tasse de haricots noirs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 4*multiplicateur_arrondi, "tasses de laitue, déchirées", 2*multiplicateur_arrondi, "avocats mûrs, coupés en tranches", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", "mélange d'épices cajun"]
    preparation = ["ÉTAPE 1\nAssaisonner les filets de poulet avec le mélange d'épices cajun, du sel et du poivre des deux côtés.\nÉTAPE 2\nDans une poêle grill ou une poêle antiadhésive, cuire les filets de poulet assaisonnés sur feu moyen-élevé pendant environ 6 à 7 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits à travers et dorés. Laisser refroidir légèrement avant de les trancher.\nÉTAPE 3\nDans un grand bol, mélanger les dés de poivron rouge, de maïs, de haricots noirs, de tomates cerises coupées en deux et d'oignon rouge tranché.\nÉTAPE 4\nAjouter la laitue déchirée dans le bol avec les légumes préparés. Ajouter les tranches d'avocat, les tranches de poulet cuites et tranchées, et la coriandre fraîche hachée.\nÉTAPE 5\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron frais avec de l'huile d'olive extra-vierge, du sel et du poivre. Verser la vinaigrette préparée sur la salade de poulet cajun et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 6\nServir la salade de poulet cajun immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cajun
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "courgette", "poivron rouge", "oignon rouge", "tomates cerises", "ail", "huile d'olive", "vinaigre balsamique", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes sèches", 1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", "sel", "poivre", "persil frais, haché"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter les lentilles vertes sèches, réduire le feu à doux, couvrir et laisser mijoter pendant 20 à 25 minutes, ou jusqu'à ce que les lentilles soient tendres mais encore légèrement croquantes. Égoutter les lentilles cuites et les rincer à l'eau froide. Laisser refroidir.\nÉTAPE 2\nPréchauffer le four à 200°C (400°F). Disposer les rondelles de courgettes, les lanières de poivron rouge, les tranches d'oignon rouge et les tomates cerises coupées en deux sur une plaque de cuisson tapissée de papier sulfurisé. Ajouter les gousses d'ail émincées sur les légumes. Arroser les légumes avec de l'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût.\nÉTAPE 3\nRôtir les légumes au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés, en les remuant à mi-cuisson pour une cuisson uniforme.\nÉTAPE 4\nDans un grand bol, mélanger les lentilles cuites avec les légumes rôtis préparés.\nÉTAPE 5\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique avec de l'huile d'olive extra-vierge, du sel et du poivre. Verser la vinaigrette préparée sur la salade de lentilles aux légumes rôtis et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 6\nServir la salade de lentilles aux légumes rôtis immédiatement, garnie de feuilles de persil frais hachées."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def saumon_grille_citron_herbes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "citron", "ail", "persil frais", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de saumon avec la peau", 1*multiplicateur_arrondi, "citron, coupé en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé. Assaisonner les filets de saumon avec du sel et du poivre des deux côtés.\nÉTAPE 2\nDisposer quelques rondelles de citron sur chaque filet de saumon assaisonné.\nÉTAPE 3\nDans un petit bol, mélanger l'ail émincé avec le persil frais haché et l'huile d'olive extra-vierge. Répartir ce mélange d'ail et de persil sur les filets de saumon, en les badigeonnant généreusement.\nÉTAPE 4\nGriller les filets de saumon assaisonnés sur la grille préchauffée du gril pendant environ 5 à 7 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits à travers et dorés.\nÉTAPE 5\nRetirer les filets de saumon grillés du gril et les laisser reposer pendant quelques minutes avant de les servir, garnis de rondelles de citron."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_citron_herbes
def poisson_papillote_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poisson blanc", "courgette", "carotte", "poivron rouge", "oignon rouge", "ail", "citron", "huile d'olive", "sel", "poivre", "herbes de Provence"]
    ingredient = [2*multiplicateur_arrondi, "filets de poisson blanc", 1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "carotte, coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "citron, tranché en rondelles", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", "herbes de Provence"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F). Découper quatre feuilles de papier sulfurisé d'une taille suffisante pour envelopper chaque filet de poisson et les légumes.\nÉTAPE 2\nPlacer chaque filet de poisson au centre d'une feuille de papier sulfurisé. Répartir les rondelles de courgette, de carotte, les lanières de poivron rouge, les tranches d'oignon rouge et les gousses d'ail émincées sur le poisson.\nÉTAPE 3\nArroser chaque filet de poisson et les légumes avec du jus de citron frais et de l'huile d'olive extra-vierge. Assaisonner avec du sel, du poivre et des herbes de Provence au goût.\nÉTAPE 4\nFermer chaque feuille de papier sulfurisé en papillote en pliant les bords ensemble et en les scellant hermétiquement.\nÉTAPE 5\nPlacer les papillotes sur une plaque de cuisson et cuire au four préchauffé pendant 15 à 20 minutes, ou jusqu'à ce que le poisson soit cuit à travers et que les légumes soient tendres."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_papillote_legumes
def wraps_laitue_thon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "avocat", "laitue feuille de chêne", "tomates", "oignon rouge", "mayonnaise légère", "moutarde de Dijon", "jus de citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "boîtes de thon en conserve", 2*multiplicateur_arrondi, "avocats mûrs, coupés en tranches", 8*multiplicateur_arrondi, "feuilles de laitue feuille de chêne", 2*multiplicateur_arrondi, "tomates, tranchées", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, émietter le thon en conserve égoutté avec une fourchette.\nÉTAPE 2\nAjouter les tranches d'avocat mûr sur le dessus du thon émietté dans le bol.\nÉTAPE 3\nSur chaque feuille de laitue feuille de chêne, ajouter une portion du mélange de thon et d'avocat, des tranches de tomate et des tranches d'oignon rouge.\nÉTAPE 4\nDans un petit bol, mélanger la mayonnaise légère avec la moutarde de Dijon et le jus de citron frais. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 5\nArroser chaque wrap de laitue avec la vinaigrette préparée et envelopper les côtés pour former des wraps. Servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_laitue_thon_avocat
def salade_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "courgette", "poivron rouge", "oignon rouge", "tomates cerises", "ail", "huile d'olive", "vinaigre balsamique", "sel", "poivre", "basilic frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa sec", 1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", "sel", "poivre", "basilic frais, haché"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter le quinoa sec, réduire le feu à doux, couvrir et laisser mijoter pendant 15 à 20 minutes, ou jusqu'à ce que le quinoa soit cuit et que les germes se détachent. Retirer du feu et laisser reposer pendant 5 minutes avant de déflorer avec une fourchette.\nÉTAPE 2\nPendant ce temps, préchauffer le four à 200°C (400°F). Disposer les rondelles de courgettes, les lanières de poivron rouge, les tranches d'oignon rouge et les tomates cerises coupées en deux sur une plaque de cuisson tapissée de papier sulfurisé. Ajouter les gousses d'ail émincées sur les légumes. Arroser les légumes avec de l'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût.\nÉTAPE 3\nRôtir les légumes au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés, en les remuant à mi-cuisson pour une cuisson uniforme.\nÉTAPE 4\nDans un grand bol, mélanger le quinoa cuit avec les légumes rôtis préparés.\nÉTAPE 5\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique avec de l'huile d'olive extra-vierge, du sel et du poivre. Verser la vinaigrette préparée sur la salade de quinoa aux légumes rôtis et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 6\nServir la salade de quinoa aux légumes rôtis immédiatement, garnie de feuilles de basilic frais hachées."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_rotis
def poulet_grille_citron_ail(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "citron", "ail", "huile d'olive", "sel", "poivre", "persil frais"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 1*multiplicateur_arrondi, "citron, coupé en rondelles", 4*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", "persil frais, haché"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé. Huiler légèrement la grille du gril pour éviter que le poulet ne colle.\nÉTAPE 2\nAssaisonner les poitrines de poulet désossées et sans peau avec du sel et du poivre des deux côtés.\nÉTAPE 3\nDisposer quelques rondelles de citron sur chaque poitrine de poulet assaisonnée.\nÉTAPE 4\nGriller les poitrines de poulet assaisonnées sur la grille préchauffée du gril pendant environ 6 à 7 minutes de chaque côté, ou jusqu'à ce qu'elles soient bien cuites à travers et dorées.\nÉTAPE 5\nRetirer les poitrines de poulet grillées du gril et les laisser reposer pendant quelques minutes avant de les servir, garnies de persil frais haché."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_grille_citron_ail
def salade_thon_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "courgette", "aubergine", "poivron rouge", "tomates cerises", "oignon rouge", "huile d'olive", "vinaigre balsamique", "sel", "poivre", "basilic frais"]
    ingredient = [2*multiplicateur_arrondi, "boîtes de thon en conserve", 1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "aubergine, coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", "sel", "poivre", "basilic frais, haché"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé. Disposer les rondelles de courgettes et d'aubergines, les lanières de poivron rouge et les tranches d'oignon rouge sur une plaque de cuisson tapissée de papier sulfurisé. Arroser les légumes avec de l'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût.\nÉTAPE 2\nGriller les légumes au gril préchauffé pendant 10 à 15 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés, en les retournant à mi-cuisson pour une cuisson uniforme.\nÉTAPE 3\nDans un grand bol, mélanger le thon en conserve égoutté avec les légumes grillés, les tomates cerises coupées en deux et le basilic frais haché.\nÉTAPE 4\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique avec de l'huile d'olive extra-vierge, du sel et du poivre. Verser la vinaigrette préparée sur la salade de thon aux légumes grillés et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 5\nServir la salade de thon aux légumes grillés immédiatement, garnie de feuilles de basilic frais hachées."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_legumes_grilles
def curry_pois_chiches_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["pois chiches en conserve", "épinards frais", "tomates concassées en conserve", "oignon jaune", "ail", "gingembre frais", "huile de coco", "poudre de curry", "curcuma moulu", "cumin moulu", "coriandre moulue", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "boîtes de pois chiches, égouttés et rincés", 4*multiplicateur_arrondi, "tasses d'épinards frais, lavés et équeutés", 1*multiplicateur_arrondi, "boîte de tomates concassées", 1*multiplicateur_arrondi, "oignon jaune, haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe d'huile de coco", 2*multiplicateur_arrondi, "cuillères à soupe de poudre de curry", 1*multiplicateur_arrondi, "cuillère à café de curcuma moulu", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de coriandre moulue", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande casserole ou une poêle, chauffer l'huile de coco à feu moyen. Ajouter les oignons hachés et faire sauter jusqu'à ce qu'ils soient tendres, environ 5 minutes.\nÉTAPE 2\nAjouter l'ail émincé et le gingembre râpé dans la casserole ou la poêle et faire cuire pendant 1 minute de plus, en remuant fréquemment.\nÉTAPE 3\nIncorporer les tomates concassées, les pois chiches égouttés et rincés, les épinards frais lavés et équeutés, la poudre de curry, le curcuma moulu, le cumin moulu, la coriandre moulue, du sel et du poivre dans la casserole ou la poêle avec les oignons sautés. Bien mélanger tous les ingrédients.\nÉTAPE 4\nPorter le mélange à ébullition, puis réduire le feu à doux et laisser mijoter pendant 10 à 15 minutes, en remuant de temps en temps, jusqu'à ce que les épinards soient flétris et que les saveurs soient bien mélangées.\nÉTAPE 5\nServir le curry de pois chiches et épinards chaud, garni de coriandre fraîche hachée, avec du riz basmati cuit à côté si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

curry_pois_chiches_epinards
def salade_poulet_fraises_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "fraises", "épinards frais", "oignon rouge", "amandes effilées", "vinaigre balsamique", "huile d'olive", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 2*multiplicateur_arrondi, "tasses de fraises fraîches, tranchées", 4*multiplicateur_arrondi, "tasses d'épinards frais, lavés et équeutés", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 1*multiplicateur_arrondi, "tasse d'amandes effilées, grillées", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", 3*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé. Assaisonner les poitrines de poulet désossées et sans peau avec du sel et du poivre des deux côtés.\nÉTAPE 2\nGriller les poitrines de poulet assaisonnées sur le gril préchauffé pendant 6 à 7 minutes de chaque côté, ou jusqu'à ce qu'elles soient cuites à travers et dorées. Retirer les poitrines de poulet grillées du gril et les laisser reposer pendant quelques minutes avant de les découper en tranches.\nÉTAPE 3\nDans un grand bol, mélanger les épinards frais lavés et équeutés avec les tranches de fraises fraîches, les tranches d'oignon rouge et les amandes effilées grillées.\nÉTAPE 4\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique, l'huile d'olive extra-vierge, le miel, la moutarde de Dijon, du sel et du poivre. Verser la vinaigrette préparée sur la salade de poulet aux fraises et épinards, et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 5\nRépartir les tranches de poulet grillé sur la salade préparée et servir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_fraises_epinards
def salade_quinoa_fruits_mer(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "crevettes décortiquées", "calmar", "poivron rouge", "oignon rouge", "persil frais", "citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa sec", 1*multiplicateur_arrondi, "tasse de crevettes décortiquées cuites et découpées", 1*multiplicateur_arrondi, "tasse de calmars cuits, coupés en rondelles", 1*multiplicateur_arrondi, "poivron rouge, coupé en petits dés", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais, haché", "citron", "huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter le quinoa sec, réduire le feu à doux, couvrir et laisser mijoter pendant 15 à 20 minutes, ou jusqu'à ce que le quinoa soit cuit et que les germes se détachent. Retirer du feu et laisser reposer pendant 5 minutes avant de déflorer avec une fourchette.\nÉTAPE 2\nDans un grand bol, mélanger le quinoa cuit avec les crevettes décortiquées cuites et découpées, les calmars cuits coupés en rondelles, les dés de poivron rouge et les tranches d'oignon rouge.\nÉTAPE 3\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron frais, l'huile d'olive extra-vierge, du sel et du poivre. Verser la vinaigrette préparée sur la salade de quinoa et de fruits de mer, et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 4\nRéfrigérer la salade de quinoa aux fruits de mer pendant au moins 30 minutes avant de servir, pour permettre aux saveurs de se marier. Garnir de persil frais haché avant de servir."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_fruits_mer
def saumon_grille_ail_citron(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "ail", "citron", "huile d'olive", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "citron, coupé en rondelles", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre", "persil frais, haché"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé. Huiler légèrement la grille du gril pour éviter que le poisson ne colle.\nÉTAPE 2\nBadigeonner les filets de saumon avec de l'huile d'olive extra-vierge et assaisonner généreusement de sel et de poivre des deux côtés.\nÉTAPE 3\nFrotter les filets de saumon avec les gousses d'ail émincées sur chaque côté.\nÉTAPE 4\nDisposer quelques rondelles de citron sur chaque filet de saumon.\nÉTAPE 5\nGriller les filets de saumon sur la grille préchauffée du gril pendant environ 4 à 5 minutes de chaque côté, ou jusqu'à ce qu'ils soient cuits à votre goût.\nÉTAPE 6\nRetirer les filets de saumon grillés du gril et les servir immédiatement, garnis de persil frais haché."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_ail_citron
def chili_vegetarien(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots rouges en conserve", "haricots noirs en conserve", "maïs en conserve", "tomates concassées en conserve", "poivron rouge", "oignon jaune", "ail", "huile d'olive", "poudre de chili", "cumin moulu", "paprika fumé", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "boîte de haricots rouges, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de haricots noirs, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de maïs, égoutté", 1*multiplicateur_arrondi, "boîte de tomates concassées", 1*multiplicateur_arrondi, "poivron rouge, coupé en dés", 1*multiplicateur_arrondi, "oignon jaune, haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika fumé", "sel", "poivre", "coriandre fraîche, hachée"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, chauffer l'huile d'olive extra-vierge à feu moyen. Ajouter les oignons hachés et le poivron rouge coupé en dés et faire sauter jusqu'à ce qu'ils soient tendres, environ 5 minutes.\nÉTAPE 2\nAjouter l'ail émincé dans la casserole et faire cuire pendant 1 minute de plus, en remuant fréquemment.\nÉTAPE 3\nIncorporer les tomates concassées, les haricots rouges égouttés et rincés, les haricots noirs égouttés et rincés, le maïs égoutté, la poudre de chili, le cumin moulu et le paprika fumé dans la casserole avec les légumes sautés. Assaisonner le chili avec du sel et du poivre au goût.\nÉTAPE 4\nPorter le mélange à ébullition, puis réduire le feu à doux et laisser mijoter pendant 15 à 20 minutes, en remuant de temps en temps.\nÉTAPE 5\nServir le chili végétarien chaud, garni de coriandre fraîche hachée."]
    temps_de_preparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_vegetarien
def wrap_vegetalien_legumes_marines(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "chou-fleur", "carottes", "concombre", "poivron rouge", "oignon rouge", "vinaigre de cidre", "sirop d'érable", "moutarde de Dijon", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "tasses de chou-fleur, coupées en petits bouquets", 1*multiplicateur_arrondi, "carottes, pelées et coupées en fines lanières", 1*multiplicateur_arrondi, "concombre, coupé en fines lanières", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre de pomme", 1*multiplicateur_arrondi, "cuillère à soupe de sirop d'érable pur", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les bouquets de chou-fleur avec les fines lanières de carottes, de concombre et de poivron rouge, ainsi que les tranches d'oignon rouge.\nÉTAPE 2\nDans un petit bol, préparer la marinade en mélangeant le vinaigre de cidre de pomme, le sirop d'érable pur, la moutarde de Dijon, de l'huile d'olive extra-vierge, du sel et du poivre.\nÉTAPE 3\nVerser la marinade préparée sur les légumes préparés et mélanger délicatement pour enrober tous les légumes de la marinade. Laisser mariner les légumes pendant au moins 30 minutes, en remuant de temps en temps.\nÉTAPE 4\nRéchauffer les tortillas de blé entier dans une poêle légèrement huilée à feu moyen pendant quelques secondes de chaque côté, juste pour les ramollir.\nÉTAPE 5\nRépartir les légumes marinés dans les tortillas réchauffées et les enrouler fermement pour former des wraps.\nÉTAPE 6\nCouper les wraps en deux diagonalement et les servir immédiatement."]
    temps_de_preparation = 20
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetalien_legumes_marines
def buddha_bowl_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "brocoli", "carottes", "poivron rouge", "oignon rouge", "huile d'olive", "vinaigre de cidre", "miel", "moutarde de Dijon", "ail", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa sec", 2*multiplicateur_arrondi, "tasses de brocoli, coupées en petits bouquets", 1*multiplicateur_arrondi, "carottes, pelées et coupées en bâtonnets", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre de pomme", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "gousses d'ail émincées", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter le quinoa sec, réduire le feu à doux, couvrir et laisser mijoter pendant 15 à 20 minutes, ou jusqu'à ce que le quinoa soit cuit et que les germes se détachent. Retirer du feu et laisser reposer pendant 5 minutes avant de déflorer avec une fourchette.\nÉTAPE 2\nPendant ce temps, préparer les légumes grillés. Préchauffer le gril à feu moyen. Disposer les bouquets de brocoli, les bâtonnets de carottes, les lanières de poivron rouge et les tranches d'oignon rouge sur une plaque de cuisson tapissée de papier sulfurisé. Arroser les légumes avec de l'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût. Griller les légumes au four préchauffé pendant 15 à 20 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 3\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre de cidre de pomme, le miel, la moutarde de Dijon et les gousses d'ail émincées. Assaisonner la vinaigrette avec du sel et du poivre au goût.\nÉTAPE 4\nRépartir le quinoa cuit et les légumes grillés dans des bols de service individuels. Arroser chaque bol avec la vinaigrette préparée et servir immédiatement."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_grilles
def buddha_bowl_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "brocoli", "carottes", "poivron rouge", "oignon rouge", "huile d'olive", "vinaigre de cidre", "miel", "moutarde de Dijon", "ail", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa sec", 2*multiplicateur_arrondi, "tasses de brocoli, coupées en petits bouquets", 1*multiplicateur_arrondi, "carottes, pelées et coupées en bâtonnets", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre de pomme", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 2*multiplicateur_arrondi, "gousses d'ail émincées", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter le quinoa sec, réduire le feu à doux, couvrir et laisser mijoter pendant 15 à 20 minutes, ou jusqu'à ce que le quinoa soit cuit et que les germes se détachent. Retirer du feu et laisser reposer pendant 5 minutes avant de déflorer avec une fourchette.\nÉTAPE 2\nPendant ce temps, préparer les légumes grillés. Préchauffer le gril à feu moyen. Disposer les bouquets de brocoli, les bâtonnets de carottes, les lanières de poivron rouge et les tranches d'oignon rouge sur une plaque de cuisson tapissée de papier sulfurisé. Arroser les légumes avec de l'huile d'olive extra-vierge et assaisonner avec du sel et du poivre au goût. Griller les légumes au four préchauffé pendant 15 à 20 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 3\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre de cidre de pomme, le miel, la moutarde de Dijon et les gousses d'ail émincées. Assaisonner la vinaigrette avec du sel et du poivre au goût.\nÉTAPE 4\nRépartir le quinoa cuit et les légumes grillés dans des bols de service individuels. Arroser chaque bol avec la vinaigrette préparée et servir immédiatement."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_grilles
def poelee_legumes_ete(multiplicateur_arrondi):
    ingredient_pour_algo = ["courgette", "aubergine", "tomates cerises", "oignon rouge", "ail", "huile d'olive", "thym frais", "basilic frais", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "courgette, coupée en rondelles", 1*multiplicateur_arrondi, "aubergine, coupée en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de thym frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de basilic frais haché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive extra-vierge à feu moyen. Ajouter les rondelles de courgette, les dés d'aubergine, les tranches d'oignon rouge et les gousses d'ail émincées dans la poêle chaude.\nÉTAPE 2\nFaire sauter les légumes, en remuant fréquemment, jusqu'à ce qu'ils soient tendres et dorés, environ 10 à 12 minutes.\nÉTAPE 3\nAjouter les tomates cerises coupées en deux dans la poêle avec les légumes sautés, et continuer à faire sauter pendant 2 à 3 minutes de plus, ou jusqu'à ce que les tomates soient juste ramollies.\nÉTAPE 4\nAssaisonner la poêlée de légumes d'été avec du sel et du poivre au goût, puis ajouter le thym frais haché et le basilic frais haché.\nÉTAPE 5\nMélanger délicatement les herbes fraîches avec les légumes sautés, puis retirer la poêle du feu.\nÉTAPE 6\nServir la poêlée de légumes d'été chauds, garnis de plus de thym et de basilic frais si désiré."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poelee_legumes_ete
def wrap_vegetarien_haricots_noirs(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "haricots noirs en conserve", "maïs en conserve", "poivron rouge", "oignon rouge", "coriandre fraîche", "avocat", "jus de lime", "poudre de chili", "cumin moulu", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "boîte de haricots noirs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de maïs en conserve, égoutté", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge, coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "avocat mûr, tranché", 1*multiplicateur_arrondi, "jus de lime frais", 1*multiplicateur_arrondi, "cuillère à café de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les haricots noirs égouttés et rincés avec le maïs égoutté, les lanières de poivron rouge, les tranches d'oignon rouge et la coriandre fraîche hachée. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 2\nDans un petit bol, écraser l'avocat mûr avec du jus de lime frais et assaisonner avec de la poudre de chili et du cumin moulu.\nÉTAPE 3\nTartiner chaque tortilla de blé entier avec la purée d'avocat épicée.\nÉTAPE 4\nRépartir le mélange de haricots noirs et de légumes sur les tortillas garnies d'avocat.\nÉTAPE 5\nRouler les tortillas garnies pour former des wraps serrés.\nÉTAPE 6\nCouper les wraps en deux diagonalement et les servir immédiatement, ou les envelopper individuellement dans du papier d'aluminium pour les emporter."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_haricots_noirs
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "courgette", "aubergine", "poivron rouge", "poivron jaune", "tomates cerises", "oignon rouge", "ail", "huile d'olive", "jus de citron", "menthe fraîche", "persil frais", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa sec", 1*multiplicateur_arrondi, "courgette, tranchée en longueur", 1*multiplicateur_arrondi, "aubergine, tranchée en longueur", 1*multiplicateur_arrondi, "poivron rouge, coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune, coupé en lanières", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 1*multiplicateur_arrondi, "oignon rouge, tranché en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition. Ajouter le quinoa et réduire le feu à doux. Couvrir et laisser mijoter pendant 15 à 20 minutes, ou jusqu'à ce que le quinoa soit cuit et que les germes se détachent. Retirer du feu et laisser reposer pendant 5 minutes avant de déflorer avec une fourchette.\nÉTAPE 2\nPendant ce temps, préparer les légumes grillés. Préchauffer le gril à feu moyen. Badigeonner les tranches de courgette et d'aubergine avec de l'huile d'olive et assaisonner avec du sel et du poivre. Griller les tranches de courgette et d'aubergine pendant 3 à 4 minutes de chaque côté, ou jusqu'à ce qu'elles soient tendres et marquées par le gril. Retirer du gril et laisser refroidir légèrement avant de les couper en morceaux.\nÉTAPE 3\nDans un grand bol à salade, mélanger le quinoa cuit avec les tranches de courgette et d'aubergine grillées, les lanières de poivron rouge et jaune, les tomates cerises coupées en deux et les rondelles d'oignon rouge.\nÉTAPE 4\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive extra-vierge, le jus de citron frais, l'ail émincé, la menthe fraîche hachée et le persil frais haché. Assaisonner la vinaigrette avec du sel et du poivre au goût.\nÉTAPE 5\nVerser la vinaigrette préparée sur la salade de quinoa et de légumes grillés, et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 6\nServir la salade de quinoa aux légumes grillés immédiatement, garnie de plus de menthe fraîche hachée et de persil frais haché si désiré."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def chili_vegetarien(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots rouges en conserve", "haricots noirs en conserve", "pois chiches en conserve", "tomates en dés en conserve", "oignon", "poivron rouge", "poivron vert", "maïs en conserve", "ail", "poudre de chili", "cumin moulu", "paprika", "origan séché", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de haricots rouges en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de haricots noirs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de pois chiches en conserve, égouttés et rincés", 2*multiplicateur_arrondi, "boîtes de tomates en dés en conserve", 1*multiplicateur_arrondi, "oignon moyen, haché", 1*multiplicateur_arrondi, "poivron rouge, haché", 1*multiplicateur_arrondi, "poivron vert, haché", 1*multiplicateur_arrondi, "boîte de maïs en conserve, égoutté", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande casserole ou une cocotte, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon haché, les poivrons rouges et verts hachés et les gousses d'ail émincées dans la casserole chaude, et faire sauter jusqu'à ce qu'ils soient tendres, environ 5 minutes.\nÉTAPE 3\nAjouter la poudre de chili, le cumin moulu, le paprika et l'origan séché dans la casserole avec les légumes sautés, et remuer pour bien enrober les légumes des épices.\nÉTAPE 4\nAjouter les tomates en dés en conserve, les haricots rouges, les haricots noirs, les pois chiches et le maïs égouttés dans la casserole avec les légumes et les épices.\nÉTAPE 5\nRemplir une des boîtes de tomates en dés vides avec de l'eau et ajouter l'eau dans la casserole pour diluer légèrement le mélange de chili.\nÉTAPE 6\nAssaisonner le chili végétarien avec du sel et du poivre au goût, puis laisser mijoter à feu doux pendant environ 20 à 25 minutes, en remuant de temps en temps, jusqu'à ce que les saveurs soient bien mélangées et que le chili épaississe légèrement.\nÉTAPE 7\nServir le chili végétarien chaud, garni de coriandre fraîche hachée, d'oignon rouge finement tranché ou de fromage râpé si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_vegetarien
def pois_chiches_rotis_epices(multiplicateur_arrondi):
    ingredient_pour_algo = ["pois chiches cuits", "huile d'olive", "paprika", "cumin moulu", "ail en poudre", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tasses de pois chiches cuits, égouttés et rincés", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).\nÉTAPE 2\nDans un bol, mélanger les pois chiches cuits égouttés et rincés avec l'huile d'olive extra-vierge, le paprika, le cumin moulu, l'ail en poudre, du sel et du poivre au goût.\nÉTAPE 3\nÉtaler les pois chiches assaisonnés sur une plaque de cuisson tapissée de papier sulfurisé en une seule couche.\nÉTAPE 4\nRôtir les pois chiches dans le four préchauffé pendant environ 30 à 35 minutes, en les remuant à mi-cuisson, jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 5\nRetirer les pois chiches rôtis du four et laisser refroidir légèrement avant de les servir comme collation ou garniture pour les salades."]
    temps_de_preparation = 10
    temps_de_cuisson = 35
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pois_chiches_rotis_epices
def salade_poulet_agrumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "orange", "pamplemousse", "citron vert", "salade verte", "oignon rouge", "huile d'olive", "miel", "moutarde de Dijon", "vinaigre de cidre", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet désossés et sans peau", 1*multiplicateur_arrondi, "orange, pelée à vif et coupée en quartiers", 1*multiplicateur_arrondi, "pamplemousse, pelé à vif et coupé en quartiers", 1*multiplicateur_arrondi, "citron vert, pelé à vif et coupé en quartiers", 2*multiplicateur_arrondi, "tasses de salade verte mélangée", 1*multiplicateur_arrondi, "oignon rouge finement tranché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive extra-vierge", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nAssaisonner les filets de poulet avec du sel et du poivre au goût.\nÉTAPE 3\nGriller les filets de poulet assaisonnés sur le gril préchauffé pendant environ 6 à 8 minutes de chaque côté, ou jusqu'à ce qu'ils soient bien cuits et dorés.\nÉTAPE 4\nRetirer les filets de poulet grillés du gril et laisser refroidir légèrement avant de les couper en tranches fines.\nÉTAPE 5\nDans un grand saladier, mélanger la salade verte mélangée avec les quartiers d'orange, de pamplemousse et de citron vert.\nÉTAPE 6\nAjouter les tranches d'oignon rouge finement tranché et les tranches de poulet grillé sur le dessus de la salade.\nÉTAPE 7\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive extra-vierge, le miel, la moutarde de Dijon et le vinaigre de cidre jusqu'à ce qu'ils soient bien combinés.\nÉTAPE 8\nVerser la vinaigrette préparée sur la salade de poulet grillé et d'agrumes, et mélanger délicatement pour enrober tous les ingrédients de la vinaigrette.\nÉTAPE 9\nServir la salade de poulet grillé aux agrumes immédiatement, garnie de graines de sésame grillées si désiré."]
    temps_de_preparation = 20
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_agrumes_grilles
def poisson_blanc_legumes_four(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poisson blanc", "brocoli", "carottes", "courgette", "tomates cerises", "ail", "citron", "huile d'olive", "thym frais", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de poisson blanc (comme la morue, le tilapia, ou le cabillaud)", 1*multiplicateur_arrondi, "tasse de fleurons de brocoli", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "tasse de tomates cerises, coupées en deux", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "citron en tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de thym frais haché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (thermostat 6-7).\nÉTAPE 2\nDans un grand plat allant au four, disposer les filets de poisson blanc au centre du plat.\nÉTAPE 3\nDisposer les fleurons de brocoli, les rondelles de carottes, les rondelles de courgette et les tomates cerises coupées en deux autour des filets de poisson.\nÉTAPE 4\nAjouter les gousses d'ail émincées sur les légumes, puis placer les tranches de citron sur les filets de poisson.\nÉTAPE 5\nArroser le tout avec de l'huile d'olive, et assaisonner avec du sel, du poivre et du thym frais haché.\nÉTAPE 6\nCuire au four préchauffé pendant environ 20 à 25 minutes, ou jusqu'à ce que le poisson soit opaque et les légumes soient tendres.\nÉTAPE 7\nServir le poisson blanc au four avec les légumes chauds immédiatement, accompagné de quartiers de citron frais si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_blanc_legumes_four
def tofu_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "poivrons", "carottes", "oignon", "ail", "sauce soja faible en sodium", "huile de sésame", "gingembre frais", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "paquet de tofu ferme, coupé en cubes", 1*multiplicateur_arrondi, "tasse de fleurons de brocoli", 1*multiplicateur_arrondi, "poivrons coupés en lanières", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "oignon coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer un peu d'huile de sésame à feu moyen-élevé.\nÉTAPE 2\nAjouter les cubes de tofu dans la poêle chaude, assaisonner avec du sel et du poivre au goût, et faire sauter jusqu'à ce qu'ils soient dorés et croustillants, environ 5 à 7 minutes.\nÉTAPE 3\nRetirer le tofu cuit de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile de sésame si nécessaire, puis ajouter les fleurons de brocoli, les poivrons coupés en lanières, les rondelles de carottes, les fines tranches d'oignon, les gousses d'ail émincées et le gingembre frais râpé.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants, environ 5 minutes.\nÉTAPE 6\nAjouter le tofu réservé dans la poêle avec les légumes sautés, puis verser la sauce soja faible en sodium dans la poêle.\nÉTAPE 7\nRemuer pour bien enrober tous les ingrédients de la sauce, et cuire encore 2 à 3 minutes jusqu'à ce que le tout soit bien chaud.\nÉTAPE 8\nServir le tofu sauté aux légumes immédiatement, accompagné de riz brun cuit si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tofu_legumes_sautes
def poulet_legumes_sautes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "poivrons", "brocoli", "carottes", "oignon", "ail", "gingembre frais", "sauce soja faible en sodium", "huile de sésame", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 1*multiplicateur_arrondi, "poivrons coupés en lanières", 1*multiplicateur_arrondi, "tasse de fleurons de brocoli", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "oignon coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja faible en sodium", 1*multiplicateur_arrondi, "cuillère à café d'huile de sésame", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer un peu d'huile de sésame à feu moyen-élevé.\nÉTAPE 2\nAjouter les lanières de poulet dans la poêle chaude, assaisonner avec du sel et du poivre au goût, et faire sauter jusqu'à ce qu'elles soient dorées et cuites à travers, environ 5 à 7 minutes.\nÉTAPE 3\nRetirer le poulet cuit de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile de sésame si nécessaire, puis ajouter les poivrons coupés en lanières, les fleurons de brocoli, les rondelles de carottes, les fines tranches d'oignon, les gousses d'ail émincées et le gingembre frais râpé.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants, environ 5 minutes.\nÉTAPE 6\nAjouter les lanières de poulet réservées dans la poêle avec les légumes sautés, puis verser la sauce soja faible en sodium et l'huile de sésame dans la poêle.\nÉTAPE 7\nRemuer pour bien enrober tous les ingrédients de la sauce, et cuire encore 2 à 3 minutes jusqu'à ce que le tout soit bien chaud.\nÉTAPE 8\nServir le poulet aux légumes sautés immédiatement, accompagné de riz brun cuit si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_legumes_sautes
def salade_fruits_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "mangue", "banane", "kiwi", "ananas", "raisins", "menthe fraîche", "jus de citron", "miel"]
    ingredient = [2*multiplicateur_arrondi, "tasses de fraises fraîches tranchées", 1*multiplicateur_arrondi, "mangue mûre coupée en dés", 2*multiplicateur_arrondi, "bananes tranchées", 2*multiplicateur_arrondi, "kiwis pelés et coupés en tranches", 1*multiplicateur_arrondi, "tasse d'ananas frais coupé en dés", 1*multiplicateur_arrondi, "tasse de raisins rouges sans pépins, coupés en deux", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à café de miel"]
    preparation = ["ÉTAPE 1\nDans un grand saladier, mélanger délicatement les fraises fraîches tranchées, les dés de mangue mûre, les tranches de banane, les tranches de kiwi, les dés d'ananas frais et les moitiés de raisins rouges sans pépins.\nÉTAPE 2\nAjouter la menthe fraîche hachée dans le saladier avec les fruits frais, et mélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 3\nDans un petit bol, préparer la vinaigrette en mélangeant le jus de citron frais et le miel jusqu'à ce qu'ils soient bien combinés.\nÉTAPE 4\nVerser la vinaigrette préparée sur la salade de fruits frais, et mélanger délicatement pour enrober les fruits de la vinaigrette.\nÉTAPE 5\nRéfrigérer la salade de fruits frais pendant au moins 30 minutes avant de servir pour permettre aux saveurs de se marier.\nÉTAPE 6\nServir la salade de fruits frais réfrigérée, garnie de feuilles de menthe fraîche si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_fruits_frais
def wraps_poulet_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "tortillas de blé entier", "poivrons", "courgette", "oignon rouge", "ail", "huile d'olive", "épinards frais", "yaourt grec", "citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "filets de poulet coupés en lanières", 4*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "poivrons coupés en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "tasses d'épinards frais lavés et égouttés", 1*multiplicateur_arrondi, "tasse de yaourt grec nature", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nDans un bol, mélanger les lanières de poulet avec de l'huile d'olive, de l'ail émincé, du sel et du poivre au goût.\nÉTAPE 3\nGriller les lanières de poulet marinées sur le gril préchauffé pendant environ 5 à 7 minutes de chaque côté, ou jusqu'à ce qu'elles soient bien cuites et légèrement dorées.\nÉTAPE 4\nPendant que le poulet grille, préparer les légumes en les coupant en lanières et en tranches fines.\nÉTAPE 5\nDans une poêle, chauffer un peu d'huile d'olive à feu moyen, puis faire sauter les lanières de poivrons, de courgettes, les tranches d'oignon rouge et l'ail émincé jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 6\nDans un petit bol, préparer la sauce en mélangeant le yaourt grec nature, le jus de citron frais, du sel et du poivre au goût.\nÉTAPE 7\nPour assembler les wraps, étaler une cuillère à soupe de sauce au yaourt sur chaque tortilla de blé entier, puis répartir les épinards frais lavés et égouttés sur chaque tortilla.\nÉTAPE 8\nAjouter une portion de lanières de poulet grillé et de légumes sautés sur chaque tortilla.\nÉTAPE 9\nEnrouler fermement les tortillas garnies pour former des wraps, et servir immédiatement."]
    temps_de_preparation = 20
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_poulet_legumes_grilles
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivrons", "aubergine", "courgette", "oignon rouge", "huile d'olive", "vinaigre balsamique", "miel", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cru", 1*multiplicateur_arrondi, "poivrons coupés en lanières", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide courante dans une passoire fine, puis égoutter.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition.\nÉTAPE 3\nAjouter le quinoa rincé dans l'eau bouillante, réduire le feu et laisser mijoter à feu doux pendant environ 15 minutes, ou jusqu'à ce que le quinoa soit tendre et que les germes se détachent.\nÉTAPE 4\nRetirer la casserole du feu, couvrir et laisser reposer le quinoa cuit pendant environ 5 minutes.\nÉTAPE 5\nPendant ce temps, préchauffer le gril à feu moyen-élevé.\nÉTAPE 6\nDans un grand bol, mélanger les lanières de poivrons, les dés d'aubergine, les rondelles de courgette et les fines tranches d'oignon rouge avec de l'huile d'olive, du sel et du poivre au goût.\nÉTAPE 7\nDisposer les légumes préparés sur une plaque de cuisson tapissée de papier sulfurisé, et griller au four pendant environ 10 à 15 minutes, ou jusqu'à ce que les légumes soient tendres et légèrement dorés, en les retournant de temps en temps pour une cuisson uniforme.\nÉTAPE 8\nDans un petit bol, préparer la vinaigrette en mélangeant le vinaigre balsamique, le miel, du sel et du poivre au goût.\nÉTAPE 9\nDans un grand saladier, mélanger le quinoa cuit, les légumes grillés et la vinaigrette préparée jusqu'à ce que tous les ingrédients soient bien enrobés.\nÉTAPE 10\nServir la salade de quinoa aux légumes grillés immédiatement, garnie de persil frais haché si désiré."]
    temps_de_preparation = 20
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def saumon_grille_asperges(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "asperges", "citron", "ail", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon avec la peau", 1*multiplicateur_arrondi, "bouquets d'asperges, bouts fibreux retirés", 1*multiplicateur_arrondi, "citron en tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nDans un bol, mélanger les bouquets d'asperges avec les tranches de citron, les gousses d'ail émincées, l'huile d'olive, du sel et du poivre au goût.\nÉTAPE 3\nBadigeonner légèrement les filets de saumon avec de l'huile d'olive, et assaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nPlacer les filets de saumon sur le gril préchauffé, côté peau vers le bas, et cuire pendant environ 3 à 4 minutes, ou jusqu'à ce que la peau soit croustillante et dorée.\nÉTAPE 5\nRetourner les filets de saumon sur le gril, et cuire pendant encore 3 à 4 minutes, ou jusqu'à ce que le saumon soit cuit à votre goût.\nÉTAPE 6\nPendant que le saumon grille, placer les bouquets d'asperges marinés sur le gril préchauffé, et cuire pendant environ 5 à 7 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés, en les retournant de temps en temps pour une cuisson uniforme.\nÉTAPE 7\nServir le saumon grillé avec les asperges chaudes immédiatement, accompagné de quartiers de citron supplémentaires si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_asperges
def soupe_legumes_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "carottes", "céleri", "oignon", "ail", "épinards frais", "bouillon de légumes", "tomates en dés", "thym", "persil frais", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cru", 2*multiplicateur_arrondi, "carottes coupées en rondelles", 2*multiplicateur_arrondi, "branches de céleri coupées en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "tasses d'épinards frais lavés et égouttés", 4*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "boîte de tomates en dés", 1*multiplicateur_arrondi, "cuillère à café de thym séché", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide courante dans une passoire fine, puis égoutter.\nÉTAPE 2\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 3\nAjouter les carottes coupées en rondelles, le céleri coupé en dés, l'oignon coupé en dés et les gousses d'ail émincées dans la casserole chaude.\nÉTAPE 4\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres, en remuant fréquemment.\nÉTAPE 5\nAjouter les épinards frais lavés et égouttés dans la casserole avec les légumes, et remuer jusqu'à ce qu'ils soient légèrement flétris.\nÉTAPE 6\nVerser le bouillon de légumes et les tomates en dés dans la casserole avec les légumes, et porter à ébullition.\nÉTAPE 7\nRéduire le feu et laisser mijoter la soupe pendant environ 15 à 20 minutes, ou jusqu'à ce que les légumes soient tendres et que les saveurs se mélangent.\nÉTAPE 8\nAjouter le quinoa rincé dans la soupe, et laisser mijoter encore 10 à 15 minutes, ou jusqu'à ce que le quinoa soit cuit.\nÉTAPE 9\nAssaisonner la soupe aux légumes et quinoa avec du sel et du poivre au goût, et garnir de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 35
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_legumes_quinoa
def salade_chou_frise_pois_chiches(multiplicateur_arrondi):
    ingredient_pour_algo = ["chou frisé", "pois chiches", "tomates cerises", "oignon rouge", "persil frais", "huile d'olive", "vinaigre de cidre", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tasses de chou frisé émincé", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le chou frisé émincé, les pois chiches cuits et égouttés, les tomates cerises coupées en deux, les fines tranches d'oignon rouge et le persil frais haché.\nÉTAPE 2\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre de cidre, le miel, la moutarde de Dijon, du sel et du poivre au goût.\nÉTAPE 3\nVerser la vinaigrette préparée sur la salade de chou frisé et pois chiches, et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 4\nLaisser reposer la salade au réfrigérateur pendant au moins 30 minutes avant de servir pour permettre aux saveurs de se marier.\nÉTAPE 5\nServir la salade de chou frisé aux pois chiches fraîche, garnie de graines de sésame grillées si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_chou_frise_pois_chiches
def poelee_crevettes_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes", "brocoli", "poivron rouge", "oignon", "ail", "huile d'olive", "sauce soja", "miel", "gingembre frais", "sésame", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "livre de crevettes décortiquées et déveinées", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen-vif.\nÉTAPE 2\nAjouter les crevettes décortiquées et déveinées dans la poêle chaude, et faire sauter jusqu'à ce qu'elles soient roses et opaques, en remuant de temps en temps pour une cuisson uniforme.\nÉTAPE 3\nRetirer les crevettes cuites de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire, puis ajouter les bouquets de brocoli, les lanières de poivron rouge et les tranches d'oignon.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants, en remuant fréquemment.\nÉTAPE 6\nAjouter les gousses d'ail émincées, la sauce soja, le miel et le gingembre frais râpé dans la poêle avec les légumes.\nÉTAPE 7\nRemettre les crevettes dans la poêle avec les légumes, et mélanger pour bien enrober tous les ingrédients de la sauce.\nÉTAPE 8\nAssaisonner avec du sel et du poivre au goût, et faire sauter pendant quelques minutes de plus pour permettre aux saveurs de se mélanger.\nÉTAPE 9\nServir la poêlée de crevettes aux légumes chaud immédiatement, garnie de graines de sésame grillées si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_legumes
def poulet_roti_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["cuisses de poulet", "pommes de terre", "carottes", "oignon", "ail", "huile d'olive", "thym", "romarin", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "cuisses de poulet avec la peau", 2*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 1*multiplicateur_arrondi, "carottes coupées en rondelles épaisses", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de thym séché", 1*multiplicateur_arrondi, "cuillère à café de romarin séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, disposer les cuisses de poulet avec la peau vers le haut.\nÉTAPE 3\nAjouter les quartiers de pommes de terre, les rondelles épaisses de carottes, les quartiers d'oignon et les gousses d'ail émincées autour du poulet dans le plat allant au four.\nÉTAPE 4\nArroser le poulet et les légumes avec de l'huile d'olive, du thym séché, du romarin séché, du sel et du poivre, et mélanger pour bien enrober.\nÉTAPE 5\nPlacer le plat au four préchauffé et rôtir le poulet et les légumes pendant environ 45 à 50 minutes, ou jusqu'à ce que le poulet soit bien cuit et que les légumes soient tendres et dorés, en les retournant de temps en temps pour une cuisson uniforme.\nÉTAPE 6\nServir le poulet rôti avec les légumes chauds immédiatement, garni de persil frais haché si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 50
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_legumes
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "patate douce", "poivron rouge", "oignon rouge", "ail", "épinards frais", "huile d'olive", "vinaigre de cidre", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes sèches", 1*multiplicateur_arrondi, "patate douce coupée en dés", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "tasses d'épinards frais lavés et égouttés", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, mélanger les lentilles vertes sèches, les dés de patate douce, les lanières de poivron rouge et les tranches d'oignon rouge.\nÉTAPE 3\nArroser les légumes avec de l'huile d'olive, du sel et du poivre, et mélanger pour bien enrober.\nÉTAPE 4\nPlacer le plat au four préchauffé et rôtir les légumes pendant environ 20 à 25 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés, en remuant de temps en temps pour une cuisson uniforme.\nÉTAPE 5\nPendant que les légumes rôtissent, préparer les lentilles vertes en les faisant cuire dans une casserole d'eau bouillante salée pendant environ 20 à 25 minutes, ou jusqu'à ce qu'elles soient tendres mais encore légèrement fermes.\nÉTAPE 6\nÉgoutter les lentilles cuites et les rincer à l'eau froide pour arrêter la cuisson, puis les égoutter à nouveau.\nÉTAPE 7\nDans un petit bol, préparer la vinaigrette en mélangeant l'huile d'olive, le vinaigre de cidre, le miel, la moutarde de Dijon, du sel et du poivre au goût.\nÉTAPE 8\nDans un grand bol, mélanger les lentilles cuites, les légumes rôtis, les épinards frais lavés et égouttés, et la vinaigrette préparée.\nÉTAPE 9\nServir la salade de lentilles aux légumes rôtis immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def tofu_saute_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "poivron rouge", "oignon", "ail", "sauce soja", "huile de sésame", "miel", "gingembre frais", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "paquet de tofu ferme, égoutté et coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de gingembre frais râpé", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer l'huile d'olive à feu moyen-vif.\nÉTAPE 2\nAjouter les dés de tofu dans la poêle chaude, et faire sauter jusqu'à ce qu'ils soient dorés et croustillants, en remuant de temps en temps pour une cuisson uniforme.\nÉTAPE 3\nRetirer le tofu cuit de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire, puis ajouter les bouquets de brocoli, les lanières de poivron rouge et les tranches d'oignon.\nÉTAPE 5\nFaire sauter les légumes jusqu'à ce qu'ils soient tendres mais encore croquants, en remuant fréquemment.\nÉTAPE 6\nAjouter les gousses d'ail émincées, la sauce soja, l'huile de sésame, le miel et le gingembre frais râpé dans la poêle avec les légumes.\nÉTAPE 7\nRemettre les dés de tofu dans la poêle avec les légumes, et mélanger pour bien enrober tous les ingrédients de la sauce.\nÉTAPE 8\nAssaisonner avec du sel et du poivre au goût, et faire sauter pendant quelques minutes de plus pour permettre aux saveurs de se mélanger.\nÉTAPE 9\nServir le tofu sauté aux légumes chaud immédiatement, garni de ciboulette fraîche si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_legumes
def saumon_four_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de saumon", "pommes de terre", "brocoli", "carottes", "citron", "ail", "huile d'olive", "thym", "romarin", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de saumon frais", 2*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "citron coupé en tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de thym séché", 1*multiplicateur_arrondi, "cuillère à café de romarin séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, disposer les quartiers de pommes de terre, les bouquets de brocoli et les rondelles de carottes.\nÉTAPE 3\nArroser les légumes avec de l'huile d'olive, du thym séché, du romarin séché, du sel et du poivre, et mélanger pour bien enrober.\nÉTAPE 4\nPlacer le filet de saumon frais sur les légumes dans le plat allant au four.\nÉTAPE 5\nAssaisonner le saumon avec du sel et du poivre au goût.\nÉTAPE 6\nDisposer quelques tranches de citron sur le saumon pour plus de saveur.\nÉTAPE 7\nCuire au four préchauffé pendant environ 15 à 20 minutes, ou jusqu'à ce que le saumon soit bien cuit et que les légumes soient tendres et dorés.\nÉTAPE 8\nServir le saumon cuit au four avec les légumes chauds, garni de persil frais haché si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_four_legumes
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "courgette", "aubergine", "oignon rouge", "tomates cerises", "ail", "persil", "vinaigrette légère", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cru", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide à l'aide d'une passoire fine.\nÉTAPE 2\nDans une casserole moyenne, porter 2 tasses d'eau à ébullition.\nÉTAPE 3\nAjouter le quinoa rincé dans la casserole avec l'eau bouillante, puis réduire le feu à doux.\nÉTAPE 4\nCouvrir la casserole et laisser mijoter le quinoa pendant environ 15 à 20 minutes, ou jusqu'à ce qu'il soit tendre et que toute l'eau soit absorbée.\nÉTAPE 5\nRetirer le quinoa cuit du feu et laisser reposer à couvert pendant 5 minutes avant de l'émietter à l'aide d'une fourchette.\nÉTAPE 6\nPendant que le quinoa cuit, préparer les légumes grillés en les coupant en morceaux de taille uniforme.\nÉTAPE 7\nFaire chauffer un gril à feu moyen-vif et griller les légumes jusqu'à ce qu'ils soient tendres et légèrement carbonisés, en les retournant de temps en temps pour une cuisson uniforme.\nÉTAPE 8\nDans un grand bol, mélanger le quinoa cuit, les légumes grillés, les tomates cerises coupées en deux, l'ail émincé, le persil frais haché, la vinaigrette légère et le jus de citron frais.\nÉTAPE 9\nAssaisonner la salade de quinoa aux légumes grillés avec du sel et du poivre au goût, et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 10\nServir la salade de quinoa aux légumes grillés immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 20
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def poisson_four_asperges(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de poisson blanc", "asperges", "citron", "huile d'olive", "ail", "persil", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de poisson blanc (cabillaud, tilapia, etc.)", 1*multiplicateur_arrondi, "bouquets d'asperges", 1*multiplicateur_arrondi, "citron coupé en tranches", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, disposer les filets de poisson blanc et les bouquets d'asperges.\nÉTAPE 3\nArroser le poisson et les asperges avec du jus de citron frais et de l'huile d'olive.\nÉTAPE 4\nAssaisonner avec de l'ail émincé, du persil frais haché, du sel et du poivre au goût.\nÉTAPE 5\nAjouter quelques tranches de citron sur le poisson et les asperges pour plus de saveur.\nÉTAPE 6\nCuire au four préchauffé pendant environ 15 à 20 minutes, ou jusqu'à ce que le poisson soit bien cuit et que les asperges soient tendres.\nÉTAPE 7\nServir le poisson cuit au four avec les asperges chauds, garni de quartiers de citron frais si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_four_asperges
def poulet_grille_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "pommes de terre", "courgette", "poivron rouge", "oignon", "ail", "huile d'olive", "thym", "romarin", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 2*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à café de thym séché", 1*multiplicateur_arrondi, "cuillère à café de romarin séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, mélanger les quartiers de pommes de terre, les rondelles de courgette, les lanières de poivron rouge, les quartiers d'oignon et les gousses d'ail émincées.\nÉTAPE 3\nArroser les légumes avec de l'huile d'olive, du thym séché, du romarin séché, du sel et du poivre, et mélanger pour bien enrober.\nÉTAPE 4\nPlacer les poitrines de poulet désossées et sans peau sur les légumes dans le plat allant au four.\nÉTAPE 5\nAssaisonner les poitrines de poulet avec du sel et du poivre au goût.\nÉTAPE 6\nCuire au four préchauffé pendant environ 25 à 30 minutes, ou jusqu'à ce que le poulet soit bien cuit et que les légumes soient tendres et dorés.\nÉTAPE 7\nServir le poulet grillé avec les légumes rôtis chauds, garni de persil frais haché si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_grille_legumes_rotis
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomate", "concombre", "oignon rouge", "persil", "vinaigrette légère", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve égouttés et rincés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon en conserve égoutté, les haricots blancs en conserve égouttés et rincés, les dés de tomate, les dés de concombre, les tranches d'oignon rouge et le persil frais haché.\nÉTAPE 2\nArroser la salade de thon aux haricots blancs avec de la vinaigrette légère et du jus de citron frais.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nMélanger délicatement tous les ingrédients pour bien enrober.\nÉTAPE 5\nServir la salade de thon aux haricots blancs immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def soupe_lentilles_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles", "carottes", "céleri", "oignon", "ail", "tomates en dés", "bouillon de légumes", "curcuma", "cumin", "paprika", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles sèches", 1*multiplicateur_arrondi, "carottes coupées en dés", 1*multiplicateur_arrondi, "céleri coupé en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "tasse de tomates en dés en conserve", 4*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "cuillère à café de curcuma", 1*multiplicateur_arrondi, "cuillère à café de cumin moulu", 1*multiplicateur_arrondi, "cuillère à café de paprika", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon coupé en dés et l'ail émincé dans la casserole chaude, et faire sauter pendant quelques minutes jusqu'à ce qu'ils soient dorés et parfumés.\nÉTAPE 3\nAjouter les carottes coupées en dés, le céleri coupé en dés et les lentilles sèches dans la casserole, et remuer pour bien les enrober d'huile.\nÉTAPE 4\nVerser les tomates en dés en conserve dans la casserole avec les légumes et les lentilles.\nÉTAPE 5\nAjouter le bouillon de légumes, le curcuma, le cumin et le paprika dans la casserole, et assaisonner avec du sel et du poivre au goût.\nÉTAPE 6\nPorter la soupe de lentilles aux légumes à ébullition, puis réduire le feu et laisser mijoter à feu doux pendant environ 30 à 40 minutes, ou jusqu'à ce que les lentilles et les légumes soient tendres.\nÉTAPE 7\nRectifier l'assaisonnement avec du sel et du poivre au besoin, et servir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 40
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_lentilles_legumes
def tofu_saute_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "brocoli", "poivron rouge", "carottes", "oignon", "ail", "gingembre frais", "sauce soja", "huile de sésame", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "paquet de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "oignon coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter le tofu coupé en dés dans la poêle chaude, et faire sauter pendant quelques minutes jusqu'à ce qu'il soit doré et croustillant.\nÉTAPE 3\nRetirer le tofu de la poêle et réserver.\nÉTAPE 4\nDans la même poêle, ajouter un peu plus d'huile d'olive si nécessaire, puis ajouter l'oignon coupé en dés, l'ail émincé et le gingembre frais râpé.\nÉTAPE 5\nFaire sauter l'oignon, l'ail et le gingembre pendant quelques minutes jusqu'à ce qu'ils soient tendres et parfumés.\nÉTAPE 6\nAjouter les carottes coupées en rondelles, les lanières de poivron rouge et les bouquets de brocoli dans la poêle, et continuer à faire sauter pendant quelques minutes de plus jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 7\nIncorporer le tofu sauté réservé dans la poêle avec les légumes, et arroser de sauce soja et d'huile de sésame.\nÉTAPE 8\nAssaisonner avec du sel et du poivre au goût, et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 9\nCuire le tofu sauté aux légumes pendant quelques minutes de plus, en remuant constamment, jusqu'à ce que tout soit bien chaud et que les saveurs soient bien mélangées.\nÉTAPE 10\nServir le tofu sauté aux légumes chaud, garni de graines de sésame grillées et de ciboulette fraîche hachée si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_legumes
def salade_crevettes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes décortiquées", "avocat", "concombre", "tomate cerise", "oignon rouge", "coriandre", "citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de crevettes décortiquées", 1*multiplicateur_arrondi, "avocat coupé en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "citron coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les crevettes décortiquées, les dés d'avocat, les dés de concombre, les tomates cerises coupées en deux, les tranches d'oignon rouge et la coriandre fraîche hachée.\nÉTAPE 2\nArroser la salade de crevettes et d'avocat avec du jus de citron frais et de l'huile d'olive.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nMélanger délicatement tous les ingrédients pour bien enrober.\nÉTAPE 5\nServir la salade de crevettes et d'avocat immédiatement, garnie de quartiers de citron supplémentaires si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_crevettes_avocat
def salade_lentilles_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles", "concombre", "tomate", "oignon rouge", "persil", "menthe", "vinaigrette légère", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles cuites et égouttées", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lentilles cuites et égouttées, les dés de concombre, les dés de tomate, les tranches d'oignon rouge, le persil frais haché et la menthe fraîche hachée.\nÉTAPE 2\nArroser la salade de lentilles et de légumes avec de la vinaigrette légère et du jus de citron frais.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nMélanger délicatement tous les ingrédients pour bien enrober.\nÉTAPE 5\nServir la salade de lentilles et de légumes immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes
def riz_saute_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz complet", "pois mange-tout", "carottes", "oignon", "poivron rouge", "ail", "gingembre frais", "sauce soja", "huile de sésame", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz complet cuit", 1*multiplicateur_arrondi, "tasse de pois mange-tout coupés en morceaux", 1*multiplicateur_arrondi, "carottes coupées en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une grande poêle ou un wok, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon coupé en dés et l'ail émincé, et faire sauter pendant quelques minutes jusqu'à ce qu'ils soient dorés et parfumés.\nÉTAPE 3\nAjouter les dés de carottes, les morceaux de pois mange-tout et les dés de poivron rouge dans la poêle, et continuer à faire sauter pendant quelques minutes jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 4\nIncorporer le riz cuit dans la poêle avec les légumes, et mélanger pour bien les enrober d'huile.\nÉTAPE 5\nAjouter le gingembre frais râpé, la sauce soja, l'huile de sésame, du sel et du poivre au riz sauté aux légumes, et mélanger pour bien répartir les assaisonnements.\nÉTAPE 6\nCuire le riz sauté aux légumes pendant quelques minutes de plus, en remuant constamment, jusqu'à ce qu'il soit chaud et que les saveurs soient bien mélangées.\nÉTAPE 7\nServir le riz sauté aux légumes chaud, garni de coriandre fraîche hachée si désiré."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

riz_saute_legumes
def salade_poulet_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "quinoa", "poivron rouge", "concombre", "tomate", "oignon rouge", "persil", "menthe", "huile d'olive", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "poitrines de poulet cuites et coupées en dés", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les dés de poulet cuites, le quinoa cuit, les dés de poivron rouge, les dés de concombre, les dés de tomate, les tranches d'oignon rouge, le persil frais haché et la menthe fraîche hachée.\nÉTAPE 2\nArroser la salade de poulet et de quinoa avec de l'huile d'olive et du jus de citron frais.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nMélanger délicatement tous les ingrédients pour bien enrober.\nÉTAPE 5\nServir la salade de poulet et de quinoa immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_quinoa
def wrap_vegetarien_pois_chiches(multiplicateur_arrondi):
    ingredient_pour_algo = ["pois chiches", "tortillas de blé entier", "avocat", "concombre", "tomate", "laitue", "hummus", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 4*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "concombre en tranches", 1*multiplicateur_arrondi, "tomate en tranches", 2*multiplicateur_arrondi, "tasses de laitue déchirée", 4*multiplicateur_arrondi, "cuillères à soupe d'hummus", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, écraser légèrement les pois chiches cuits et égouttés avec une fourchette.\nÉTAPE 2\nSur chaque tortilla de blé entier, étaler une cuillerée d'hummus sur toute la surface.\nÉTAPE 3\nRépartir les pois chiches écrasés, les tranches d'avocat, les tranches de concombre, les tranches de tomate et la laitue déchirée sur les tortillas.\nÉTAPE 4\nArroser chaque wrap avec un filet de jus de citron frais et assaisonner avec du sel et du poivre au goût.\nÉTAPE 5\nEnrouler fermement chaque tortilla pour former un wrap.\nÉTAPE 6\nCouper chaque wrap en deux et servir immédiatement, ou envelopper individuellement dans du papier d'aluminium pour emporter."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_pois_chiches
def poisson_papillote_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filet de poisson blanc", "courgette", "carottes", "poivron rouge", "oignon", "citron", "ail", "persil", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filet de poisson blanc (cabillaud, sole, tilapia, etc.)", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon coupé en rondelles", 1*multiplicateur_arrondi, "citron coupé en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nCouper des morceaux de papier d'aluminium assez grands pour envelopper chaque filet de poisson individuellement.\nÉTAPE 3\nAu centre de chaque morceau de papier d'aluminium, disposer les rondelles de courgette, les rondelles de carotte, les lanières de poivron rouge, les rondelles d'oignon, les rondelles de citron et l'ail émincé.\nÉTAPE 4\nDéposer un filet de poisson sur le dessus des légumes dans chaque papillote.\nÉTAPE 5\nArroser chaque filet de poisson d'huile d'olive, de jus de citron, et assaisonner avec du sel, du poivre et du persil frais haché.\nÉTAPE 6\nFermer hermétiquement les papillotes en pliant les bords et en scellant les bords ensemble.\nÉTAPE 7\nPlacer les papillotes sur une plaque de cuisson et cuire au four préchauffé pendant environ 15-20 minutes, ou jusqu'à ce que le poisson soit bien cuit et que les légumes soient tendres."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_papillote_legumes
def salade_quinoa_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "courgette", "aubergine", "oignon rouge", "tomate cerise", "ail", "persil", "huile d'olive", "vinaigre balsamique", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cru", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "aubergine coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 1*multiplicateur_arrondi, "tasse de tomates cerises", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, mélanger les dés de poivron rouge, de courgette, d'aubergine et les quartiers d'oignon rouge avec de l'ail émincé, du persil frais haché, de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nRôtir les légumes au four préchauffé pendant environ 25-30 minutes, en remuant à mi-cuisson, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nPendant ce temps, rincer le quinoa à l'eau froide et le faire cuire selon les instructions sur l'emballage.\nÉTAPE 5\nDans un grand bol, assembler le quinoa cuit, les légumes rôtis, les tomates cerises coupées en deux, et arroser de vinaigre balsamique.\nÉTAPE 6\nMélanger délicatement tous les ingrédients de la salade de quinoa aux légumes rôtis.\nÉTAPE 7\nServir la salade de quinoa tiède ou à température ambiante."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_rotis
def saumon_grille_asperges(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "asperges", "citron", "ail", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "bouquets d'asperges", 1*multiplicateur_arrondi, "citron coupé en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nDans un petit bol, mélanger l'ail émincé, l'huile d'olive, du sel et du poivre pour faire une marinade.\nÉTAPE 3\nBadigeonner les filets de saumon et les bouquets d'asperges avec la marinade à l'ail.\nÉTAPE 4\nPlacer les filets de saumon et les asperges sur le gril préchauffé.\nÉTAPE 5\nCuire le saumon pendant environ 4-5 minutes de chaque côté, ou jusqu'à ce qu'il soit bien cuit et légèrement doré.\nÉTAPE 6\nPendant ce temps, griller les asperges pendant environ 3-4 minutes de chaque côté, ou jusqu'à ce qu'elles soient tendres et légèrement carbonisées.\nÉTAPE 7\nServir les filets de saumon grillés avec les asperges grillées et des rondelles de citron pour arroser."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_asperges
def poulet_roti_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet entier", "pommes de terre", "carottes", "oignon", "ail", "thym", "romarin", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "poulet entier (environ 1,5 kg)", 4*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 4*multiplicateur_arrondi, "carottes coupées en morceaux", 1*multiplicateur_arrondi, "oignon coupé en quartiers", 4*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de thym frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de romarin frais haché", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nDans un grand plat allant au four, disposer les quartiers de pommes de terre, les morceaux de carottes, les quartiers d'oignon et l'ail émincé.\nÉTAPE 3\nDans un petit bol, mélanger le thym frais haché, le romarin frais haché, l'huile d'olive, du sel et du poivre pour faire une marinade.\nÉTAPE 4\nBadigeonner le poulet entier avec la marinade, en veillant à bien enrober la peau.\nÉTAPE 5\nPlacer le poulet mariné sur les légumes dans le plat allant au four.\nÉTAPE 6\nRôtir le poulet et les légumes au four préchauffé pendant environ 1 heure, ou jusqu'à ce que le poulet soit bien cuit et que les légumes soient tendres et dorés.\nÉTAPE 7\nRetirer le poulet et les légumes du four et laisser reposer le poulet pendant quelques minutes avant de le découper et de servir avec les légumes rôtis."]
    temps_de_preparation = 15
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_legumes
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomate", "concombre", "oignon rouge", "persil", "vinaigrette légère", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égouttée", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve égouttés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon en conserve émietté, les haricots blancs égouttés, les dés de tomate, les dés de concombre, les tranches d'oignon rouge et le persil frais haché.\nÉTAPE 2\nArroser la salade de thon et de haricots blancs avec de la vinaigrette légère.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nMélanger délicatement pour bien enrober tous les ingrédients.\nÉTAPE 5\nServir la salade de thon et de haricots blancs immédiatement, ou réfrigérer pendant quelques heures avant de servir pour permettre aux saveurs de se développer."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def bol_buddha_tofu_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu ferme", "quinoa", "brocoli", "carottes", "pois chiches", "avocat", "graines de sésame", "coriandre", "sauce soja", "vinaigre de riz", "miel", "gingembre", "ail", "huile de sésame", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de tofu ferme coupé en dés", 1*multiplicateur_arrondi, "tasse de quinoa cuit", 1*multiplicateur_arrondi, "tasse de fleurettes de brocoli cuites à la vapeur", 1*multiplicateur_arrondi, "carottes râpées", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "cuillère à soupe de graines de sésame grillées", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de riz", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une poêle, chauffer l'huile de sésame à feu moyen. Ajouter les dés de tofu et cuire jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 2\nDans un bol, assembler le quinoa cuit, les fleurettes de brocoli cuites, les carottes râpées, les pois chiches cuits, les tranches d'avocat et les dés de tofu dorés.\nÉTAPE 3\nDans un petit bol, fouetter ensemble la sauce soja, le vinaigre de riz, le miel, le gingembre râpé et l'ail émincé pour faire la vinaigrette.\nÉTAPE 4\nVerser la vinaigrette sur le bol de buddha et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 5\nGarnir de graines de sésame grillées et de coriandre fraîche hachée avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

bol_buddha_tofu_legumes
def wrap_dinde_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["escalopes de dinde", "tortillas de blé entier", "avocat", "tomate", "laitue", "mayonnaise légère", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "escalopes de dinde cuites et tranchées", 4*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "tomate en tranches", 2*multiplicateur_arrondi, "tasses de laitue déchirée", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nSur chaque tortilla de blé entier, étaler une fine couche de mayonnaise légère et de moutarde de Dijon.\nÉTAPE 2\nRépartir uniformément les tranches d'escalope de dinde, les tranches d'avocat, les tranches de tomate et la laitue déchirée sur les tortillas.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nEnrouler fermement chaque tortilla pour former un wrap.\nÉTAPE 5\nCouper chaque wrap en deux et servir immédiatement, ou envelopper individuellement dans du papier d'aluminium pour emporter."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_dinde_avocat
def salade_lentilles_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "courgette", "aubergine", "poivron rouge", "tomate cerise", "oignon rouge", "ail", "persil", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes cuites", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en tranches", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "tasse de tomates cerises", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nÉtaler les rondelles de courgette et les tranches d'aubergine sur une plaque de cuisson. Arroser d'huile d'olive et assaisonner avec du sel et du poivre au goût.\nÉTAPE 3\nRôtir les légumes au four préchauffé pendant environ 20-25 minutes, en retournant à mi-cuisson, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nDans un grand bol, mélanger les lentilles cuites, les légumes grillés, les tomates cerises, les tranches d'oignon rouge, l'ail émincé et le persil frais haché.\nÉTAPE 5\nArroser la salade de jus de citron frais et d'huile d'olive, puis mélanger pour bien enrober tous les ingrédients.\nÉTAPE 6\nServir la salade de lentilles et de légumes grillés tiède ou à température ambiante."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_grilles
def bol_burrito_vegetalien(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz brun", "haricots noirs", "maïs en conserve", "tomate", "avocat", "coriandre", "oignon rouge", "jus de lime", "piment jalapeño", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de riz brun cuit", 1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "avocat en tranches", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 1*multiplicateur_arrondi, "cuillère à soupe de jus de lime frais", 1*multiplicateur_arrondi, "piment jalapeño en rondelles", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, assembler le riz brun cuit, les haricots noirs cuits et égouttés, le maïs en conserve égoutté, les dés de tomate, les tranches d'avocat, la coriandre fraîche hachée et les tranches d'oignon rouge.\nÉTAPE 2\nArroser le bol avec du jus de lime frais et garnir de rondelles de piment jalapeño.\nÉTAPE 3\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nServir le bol de burrito végétalien tel quel ou avec une sauce au choix."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

bol_burrito_vegetalien
def tacos_vegetariens_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de maïs", "pois chiches", "poivron rouge", "épices à tacos", "avocat", "tomate", "oignon rouge", "coriandre", "jus de lime", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortillas de maïs", 1*multiplicateur_arrondi, "tasse de pois chiches cuits et égouttés", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "cuillère à café d'épices à tacos", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de lime frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans une poêle, chauffer un peu d'huile d'olive à feu moyen. Ajouter les pois chiches égouttés et les lanières de poivron rouge dans la poêle. Saupoudrer d'épices à tacos et mélanger pour bien enrober.\nÉTAPE 2\nCuire les pois chiches et les poivrons pendant environ 5-7 minutes, jusqu'à ce que les légumes soient tendres.\nÉTAPE 3\nDans un petit bol, écraser les tranches d'avocat à la fourchette avec du jus de lime frais, du sel et du poivre pour faire une guacamole.\nÉTAPE 4\nChauffer les tortillas de maïs selon les instructions sur l'emballage.\nÉTAPE 5\nRépartir les pois chiches cuits et les poivrons sur les tortillas chaudes.\nÉTAPE 6\nGarnir de tranches d'avocat, de dés de tomate, de tranches d'oignon rouge et de coriandre fraîche hachée.\nÉTAPE 7\nServir les tacos végétariens chauds avec une garniture de guacamole."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_vegetariens_avocat
def salade_poulet_fraises_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "fraises", "épinards frais", "oignon rouge", "noix", "vinaigre balsamique", "huile d'olive", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 1*multiplicateur_arrondi, "tasse de fraises tranchées", 2*multiplicateur_arrondi, "tasses d'épinards frais", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 1*multiplicateur_arrondi, "tasse de noix concassées", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les lanières de poulet cuit, les fraises tranchées, les épinards frais, les tranches d'oignon rouge et les noix concassées.\nÉTAPE 2\nDans un petit bol, fouetter ensemble le vinaigre balsamique, l'huile d'olive, le miel, la moutarde de Dijon, du sel et du poivre pour faire la vinaigrette.\nÉTAPE 3\nVerser la vinaigrette sur la salade de poulet et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 4\nServir frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_fraises_epinards
def wraps_poulet_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "tortillas de blé entier", "avocat", "tomate", "laitue", "carottes", "concombre", "oignon rouge", "houmous", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poulet cuits et coupés en lanières", 4*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "tomate en tranches", 2*multiplicateur_arrondi, "tasses de laitue déchirée", 1*multiplicateur_arrondi, "carottes râpées", 1*multiplicateur_arrondi, "concombre en lanières", 1*multiplicateur_arrondi, "oignon rouge en tranches fines", 1*multiplicateur_arrondi, "tasse de houmous", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nSur chaque tortilla de blé entier, étaler une cuillerée de houmous.\nÉTAPE 2\nRépartir uniformément les lanières de poulet cuit, les tranches d'avocat, les tranches de tomate, la laitue déchirée, les carottes râpées, les lanières de concombre et les tranches d'oignon rouge sur les tortillas.\nÉTAPE 3\nArroser chaque wrap de jus de citron frais et assaisonner avec du sel et du poivre au goût.\nÉTAPE 4\nEnrouler fermement chaque tortilla pour former un wrap.\nÉTAPE 5\nCouper chaque wrap en deux et servir immédiatement, ou envelopper individuellement dans du papier d'aluminium pour emporter."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_poulet_legumes
def salade_quinoa_haricots_noirs_mais(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "haricots noirs", "maïs en conserve", "poivron rouge", "coriandre", "avocat", "oignon rouge", "jus de lime", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "tasse de haricots noirs cuits et égouttés", 1*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "avocat coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe de jus de lime frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide et le cuire selon les instructions sur l'emballage. Égoutter et laisser refroidir.\nÉTAPE 2\nDans un grand bol, mélanger le quinoa cuit, les haricots noirs cuits et égouttés, le maïs égoutté, les dés de poivron rouge, la coriandre fraîche hachée, les dés d'avocat et les dés d'oignon rouge.\nÉTAPE 3\nDans un petit bol, fouetter ensemble le jus de lime frais, l'huile d'olive, du sel et du poivre pour faire la vinaigrette.\nÉTAPE 4\nVerser la vinaigrette sur la salade de quinoa et mélanger pour bien enrober tous les ingrédients.\nÉTAPE 5\nServir frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_haricots_noirs_mais
def poisson_papillote_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poisson blanc", "courgette", "poivron rouge", "oignon", "citron", "ail", "thym frais", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poisson blanc (cabillaud, bar, etc.)", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en rondelles", "tranches de citron", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "branches de thym frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nCouper quatre morceaux de papier sulfurisé d'environ 30 cm sur 30 cm.\nÉTAPE 3\nSur chaque morceau de papier sulfurisé, placer une portion de filet de poisson au centre.\nÉTAPE 4\nRépartir uniformément les rondelles de courgette, les lanières de poivron rouge et les rondelles d'oignon rouge sur les filets de poisson.\nÉTAPE 5\nAjouter quelques tranches de citron, de l'ail émincé et des branches de thym frais sur le dessus des légumes.\nÉTAPE 6\nArroser chaque papillote d'un filet d'huile d'olive et assaisonner avec du sel et du poivre au goût.\nÉTAPE 7\nFermer hermétiquement chaque papillote en repliant les bords et en les scellant.\nÉTAPE 8\nPlacer les papillotes sur une plaque de cuisson et cuire au four préchauffé pendant environ 15-20 minutes ou jusqu'à ce que le poisson soit cuit à travers et que les légumes soient tendres.\nÉTAPE 9\nServir chaud directement dans les papillotes."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_papillote_legumes
def poelee_crevettes_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes décortiquées", "brocoli", "poivron rouge", "carottes", "ail", "gingembre frais", "sauce soja", "huile de sésame", "sel", "poivre", "coriandre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de crevettes décortiquées", 1*multiplicateur_arrondi, "tasse de fleurettes de brocoli", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de sésame", "sel", "poivre", "coriandre fraîche"]
    preparation = ["ÉTAPE 1\nDans une grande poêle, chauffer un peu d'huile d'olive à feu moyen. Ajouter l'ail émincé et le gingembre râpé et cuire pendant une minute.\nÉTAPE 2\nAjouter les crevettes décortiquées à la poêle et cuire jusqu'à ce qu'elles soient roses et cuites à travers. Retirer les crevettes de la poêle et réserver.\nÉTAPE 3\nAjouter un peu plus d'huile d'olive à la poêle si nécessaire, puis ajouter les fleurettes de brocoli, les lanières de poivron rouge et les rondelles de carottes. Cuire en remuant fréquemment jusqu'à ce que les légumes soient tendres mais encore croquants.\nÉTAPE 4\nRemettre les crevettes dans la poêle, ajouter la sauce soja et l'huile de sésame, et mélanger pour bien enrober tous les ingrédients. Assaisonner avec du sel et du poivre au goût.\nÉTAPE 5\nServir la poêlée de crevettes et légumes chauds, garnie de coriandre fraîche."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_legumes
def poulet_curry_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "riz basmati", "oignon", "ail", "gingembre frais", "curry en poudre", "lait de coco", "bouillon de poulet", "sel", "poivre", "coriandre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poulet coupés en dés", 1*multiplicateur_arrondi, "tasse de riz basmati", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à café de gingembre frais râpé", 2*multiplicateur_arrondi, "cuillères à café de poudre de curry", 1*multiplicateur_arrondi, "tasse de lait de coco", 1*multiplicateur_arrondi, "tasse de bouillon de poulet", "sel", "poivre", "coriandre fraîche"]
    preparation = ["ÉTAPE 1\nRincer le riz basmati sous l'eau froide jusqu'à ce que l'eau soit claire. Égoutter et réserver.\nÉTAPE 2\nDans une grande poêle, chauffer un peu d'huile d'olive à feu moyen. Ajouter l'oignon et cuire jusqu'à ce qu'il soit translucide.\nÉTAPE 3\nAjouter l'ail émincé et le gingembre râpé à la poêle et cuire pendant une minute de plus.\nÉTAPE 4\nAjouter les dés de poulet à la poêle et cuire jusqu'à ce qu'ils soient dorés de tous les côtés.\nÉTAPE 5\nSaupoudrer la poudre de curry sur le poulet et mélanger pour bien enrober.\nÉTAPE 6\nVerser le lait de coco et le bouillon de poulet dans la poêle. Porter à ébullition, puis réduire le feu et laisser mijoter pendant environ 15-20 minutes jusqu'à ce que le poulet soit cuit et la sauce épaissie.\nÉTAPE 7\nPendant ce temps, cuire le riz basmati selon les instructions sur l'emballage.\nÉTAPE 8\nServir le poulet au curry chaud avec du riz basmati cuit, garni de coriandre fraîche."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_curry_riz_basmati
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "courge musquée", "betteraves", "oignon rouge", "épinards", "vinaigre balsamique", "huile d'olive", "miel", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes", 1*multiplicateur_arrondi, "tasse de courge musquée coupée en dés", 1*multiplicateur_arrondi, "tasse de betteraves coupées en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 2*multiplicateur_arrondi, "tasses d'épinards frais", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre balsamique", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer les lentilles sous l'eau froide et les faire cuire selon les instructions sur l'emballage. Égoutter et laisser refroidir.\nÉTAPE 2\nPréchauffer le four à 200°C (400°F).\nÉTAPE 3\nDans un grand bol, mélanger les dés de courge musquée, les dés de betteraves et les dés d'oignon rouge avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 4\nDisposer les légumes sur une plaque à pâtisserie tapissée de papier sulfurisé et rôtir au four pendant environ 25-30 minutes ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 5\nDans un petit bol, fouetter ensemble le vinaigre balsamique, le miel, la moutarde de Dijon, du sel et du poivre pour faire la vinaigrette.\nÉTAPE 6\nDans un grand bol, mélanger les lentilles cuites avec les légumes rôtis et les épinards frais.\nÉTAPE 7\nArroser la salade de vinaigrette balsamique et mélanger pour bien enrober les ingrédients.\nÉTAPE 8\nServir frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 30
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def saumon_grille_asperges_riz_complet(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de saumon", "asperges", "riz complet", "huile d'olive", "sel", "poivre", "citron"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "tasse d'asperges, bouts coupés", 1*multiplicateur_arrondi, "tasse de riz complet", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", "tranches de citron"]
    preparation = ["ÉTAPE 1\nCuire le riz complet selon les instructions sur l'emballage.\nÉTAPE 2\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 3\nBadigeonner les filets de saumon avec de l'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 4\nPlacer les filets de saumon et les asperges sur le gril et cuire pendant environ 4-5 minutes de chaque côté ou jusqu'à ce que le saumon soit cuit et les asperges soient tendres.\nÉTAPE 5\nServir le saumon grillé et les asperges avec du riz complet cuit.\nÉTAPE 6\nGarnir de tranches de citron avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_asperges_riz_complet
def salade_poulet_grille_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poulet", "avocat", "laitue romaine", "tomate", "concombre", "oignon rouge", "huile d'olive", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poulet", 1*multiplicateur_arrondi, "avocat, coupé en dés", 2*multiplicateur_arrondi, "tasses de laitue romaine déchirée", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en fines tranches", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nAssaisonner les filets de poulet avec du sel, du poivre et un peu d'huile d'olive.\nÉTAPE 2\nFaire cuire les filets de poulet sur un gril préchauffé à feu moyen pendant environ 6-7 minutes de chaque côté ou jusqu'à ce qu'ils soient bien cuits.\nÉTAPE 3\nDans un grand bol, mélanger la laitue romaine déchirée, les dés d'avocat, les dés de tomate, les dés de concombre et les tranches d'oignon rouge.\nÉTAPE 4\nDans un petit bol, fouetter ensemble l'huile d'olive, le jus de citron, du sel et du poivre pour faire la vinaigrette.\nÉTAPE 5\nTrancher les filets de poulet grillé et les ajouter à la salade.\nÉTAPE 6\nArroser la salade de vinaigrette et mélanger pour bien enrober les ingrédients.\nÉTAPE 7\nServir frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_grille_avocat
def buddha_bowl_legumes_grilles_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "courgette", "aubergine", "brocoli", "carottes", "huile d'olive", "sel", "poivre", "hummus"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en rondelles", 1*multiplicateur_arrondi, "tasse de fleurettes de brocoli", 1*multiplicateur_arrondi, "carottes coupées en rondelles", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "tasse de hummus"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, ajouter le quinoa, réduire le feu et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre et toute l'eau soit absorbée.\nÉTAPE 3\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 4\nBadigeonner les rondelles de courgette, d'aubergine et les fleurettes de brocoli avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 5\nGriller les légumes sur le gril pendant environ 4-5 minutes de chaque côté ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nDans des bols individuels, répartir le quinoa cuit, les légumes grillés et les rondelles de carottes.\nÉTAPE 7\nAjouter une cuillerée de hummus sur le dessus de chaque bol avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

buddha_bowl_legumes_grilles_quinoa
def tacos_poisson_salsa_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["filets de poisson blanc", "tortillas de maïs", "avocat", "tomate", "oignon rouge", "coriandre", "piment vert", "jus de citron vert", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "filets de poisson blanc (tilapia, mahi-mahi, ou autre)", 4*multiplicateur_arrondi, "tortillas de maïs", 1*multiplicateur_arrondi, "avocat mûr, coupé en dés", 1*multiplicateur_arrondi, "tomate, coupée en dés", 1*multiplicateur_arrondi, "oignon rouge, coupé en dés", 2*multiplicateur_arrondi, "cuillères à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "piment vert, épépiné et haché finement", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron vert frais", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger l'avocat coupé en dés, la tomate coupée en dés, l'oignon rouge coupé en dés, la coriandre fraîche hachée, le piment vert haché, le jus de citron vert frais, l'huile d'olive, le sel et le poivre pour faire la salsa à l'avocat.\nÉTAPE 2\nAssaisonner les filets de poisson blanc avec du sel et du poivre.\nÉTAPE 3\nFaire chauffer un peu d'huile d'olive dans une poêle à feu moyen-élevé. Ajouter les filets de poisson et les faire cuire pendant environ 3-4 minutes de chaque côté ou jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 4\nFaire chauffer les tortillas de maïs selon les instructions sur l'emballage.\nÉTAPE 5\nAssembler les tacos en plaçant les filets de poisson cuits sur les tortillas chaudes et en garnissant de salsa à l'avocat.\nÉTAPE 6\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tacos_poisson_salsa_avocat
def soupe_minestrone_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pâtes", "haricots blancs en conserve", "carottes", "céleri", "oignon", "ail", "tomates en conserve", "bouillon de légumes", "épinards frais", "persil frais", "sel", "poivre", "parmesan râpé"]
    ingredient = [1*multiplicateur_arrondi, "tasse de pâtes non cuites (coquillettes, macaroni, ou autre)", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve, égouttés et rincés", 2*multiplicateur_arrondi, "carottes coupées en dés", 2*multiplicateur_arrondi, "branches de céleri coupées en dés", 1*multiplicateur_arrondi, "oignon coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "boîte de tomates en conserve, hachées", 4*multiplicateur_arrondi, "tasses de bouillon de légumes", 2*multiplicateur_arrondi, "tasses d'épinards frais hachés", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", "sel", "poivre", "parmesan râpé"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, faire cuire les pâtes selon les instructions sur l'emballage. Égoutter et réserver.\nÉTAPE 2\nDans la même casserole, chauffer un peu d'huile d'olive à feu moyen. Ajouter les carottes, le céleri, l'oignon et l'ail, et faire revenir pendant quelques minutes jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nAjouter les tomates en conserve, les haricots blancs, le bouillon de légumes, le sel et le poivre. Porter à ébullition, puis réduire le feu et laisser mijoter pendant environ 15-20 minutes.\nÉTAPE 4\nAjouter les épinards frais et le persil haché à la soupe, et laisser cuire encore quelques minutes jusqu'à ce que les épinards soient flétris.\nÉTAPE 5\nServir la soupe minestrone chaude, garnie de pâtes cuites et de parmesan râpé."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_minestrone_legumes
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "poivron jaune", "courgette", "oignon rouge", "huile d'olive", "vinaigre de vin rouge", "sel", "poivre", "jus de citron", "persil"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "oignon rouge coupé en lanières", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de vin rouge", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, ajouter le quinoa, réduire le feu et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre et toute l'eau soit absorbée.\nÉTAPE 3\nPendant ce temps, préchauffer le gril à feu moyen-élevé.\nÉTAPE 4\nBadigeonner les lanières de poivron rouge, de poivron jaune, de courgette et d'oignon rouge avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 5\nGriller les légumes sur le gril pendant environ 4-5 minutes de chaque côté ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés.\nÉTAPE 7\nDans un petit bol, fouetter ensemble l'huile d'olive, le vinaigre de vin rouge, le sel, le poivre et le jus de citron pour faire la vinaigrette.\nÉTAPE 8\nVerser la vinaigrette sur la salade de quinoa et légumes et mélanger pour bien enrober les ingrédients.\nÉTAPE 9\nServir frais, garni de persil frais haché."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def wrap_vegetarien_legumes_grilles_houmous(multiplicateur_arrondi):
    ingredient_pour_algo = ["wrap de blé entier", "poivron rouge", "courgette", "aubergine", "houmous", "feta", "olives noires", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "wrap de blé entier", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "courgette coupée en lanières", 1*multiplicateur_arrondi, "aubergine coupée en lanières", 1*multiplicateur_arrondi, "tasse de houmous", 1*multiplicateur_arrondi, "tasse de feta émiettée", 1*multiplicateur_arrondi, "tasse d'olives noires dénoyautées", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nBadigeonner les lanières de poivron rouge, de courgette et d'aubergine avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nGriller les légumes sur le gril pendant environ 4-5 minutes de chaque côté ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nChauffer les wraps de blé entier selon les instructions sur l'emballage.\nÉTAPE 5\nÉtaler une généreuse couche de houmous sur chaque wrap chaud.\nÉTAPE 6\nRépartir les légumes grillés, la feta émiettée et les olives noires sur le houmous.\nÉTAPE 7\nEnrouler fermement les wraps et les couper en deux avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_vegetarien_legumes_grilles_houmous
def poulet_roti_pommes_terre_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet entier", "pommes de terre", "carottes", "oignon", "huile d'olive", "ail", "thym", "romarin", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "poulet entier (environ 1,5 à 2 kg)", 4*multiplicateur_arrondi, "pommes de terre coupées en quartiers", 4*multiplicateur_arrondi, "carottes pelées et coupées en morceaux", 1*multiplicateur_arrondi, "oignon rouge coupé en quartiers", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 4*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à café de thym frais", 2*multiplicateur_arrondi, "cuillères à café de romarin frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nFrotter le poulet avec de l'huile d'olive, de l'ail émincé, du thym, du romarin, du sel et du poivre.\nÉTAPE 3\nDisposer les quartiers de pommes de terre, les carottes et les quartiers d'oignon autour du poulet dans un plat allant au four.\nÉTAPE 4\nArroser les légumes avec le reste d'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 5\nRôtir au four pendant environ 1 heure et demie à 2 heures ou jusqu'à ce que le poulet soit doré et bien cuit et que les légumes soient tendres et dorés.\nÉTAPE 6\nLaisser reposer le poulet pendant quelques minutes avant de le découper.\nÉTAPE 7\nServir le poulet rôti avec les pommes de terre et les légumes rôtis chauds."]
    temps_de_preparation = 15
    temps_de_cuisson = 90
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_pommes_terre_legumes_rotis
def salade_thon_haricots_blancs(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "tomates cerises", "concombre", "oignon rouge", "persil frais", "vinaigre de vin rouge", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon en conserve émietté avec les haricots blancs égouttés, les tomates cerises coupées en deux, le concombre coupé en dés, l'oignon rouge coupé en dés et le persil frais haché.\nÉTAPE 2\nDans un petit bol, fouetter ensemble le vinaigre de vin rouge, l'huile d'olive, le sel et le poivre pour faire la vinaigrette.\nÉTAPE 3\nVerser la vinaigrette sur la salade de thon et haricots blancs et mélanger pour bien enrober les ingrédients.\nÉTAPE 4\nServir frais."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_blancs
def saumon_poele_riz_basmati_brocoli(multiplicateur_arrondi):
    ingredient_pour_algo = ["saumon", "riz basmati", "eau", "sel", "brocoli", "huile d'olive", "ail", "jus de citron", "persil"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "tasse de riz basmati", 2*multiplicateur_arrondi, "tasses d'eau", "sel", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nDans une casserole, porter l'eau à ébullition, ajouter le riz basmati et le sel, réduire le feu, couvrir et laisser mijoter pendant 15-20 minutes ou jusqu'à ce que le riz soit tendre et toute l'eau soit absorbée.\nÉTAPE 2\nPendant ce temps, chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter l'ail émincé et faire revenir pendant 1 minute.\nÉTAPE 4\nAjouter les filets de saumon dans la poêle et cuire pendant environ 4-5 minutes de chaque côté ou jusqu'à ce que le saumon soit cuit à votre goût.\nÉTAPE 5\nPendant les dernières minutes de cuisson du saumon, ajouter les fleurettes de brocoli dans la poêle et les faire cuire jusqu'à ce qu'elles soient tendres mais encore croquantes.\nÉTAPE 6\nServir le saumon et le brocoli chauds sur le riz basmati cuit.\nÉTAPE 7\nArroser de jus de citron frais et garnir de persil frais haché avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_poele_riz_basmati_brocoli
def brochettes_crevettes_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes", "poivron rouge", "poivron jaune", "courgette", "oignon rouge", "huile d'olive", "ail", "sel", "poivre", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "livre de crevettes décortiquées et déveinées", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "poivron jaune coupé en dés", 1*multiplicateur_arrondi, "courgette coupée en dés", 1*multiplicateur_arrondi, "oignon rouge coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nEnfiler les crevettes décortiquées sur des brochettes en bois ou en métal, en alternant avec les dés de poivron rouge, de poivron jaune, de courgette et d'oignon rouge.\nÉTAPE 3\nDans un petit bol, mélanger l'huile d'olive, l'ail émincé, le sel, le poivre et le jus de citron pour faire la marinade.\nÉTAPE 4\nBadigeonner les brochettes avec la marinade.\nÉTAPE 5\nGriller les brochettes sur le gril pendant environ 4-5 minutes de chaque côté ou jusqu'à ce que les crevettes soient roses et opaques et les légumes soient tendres et légèrement dorés.\nÉTAPE 6\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

brochettes_crevettes_legumes_grilles
def salade_poulet_cesar_legere(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "laitue romaine", "tomates cerises", "concombre", "parmesan râpé", "vinaigre de vin rouge", "jus de citron", "moutarde de Dijon", "ail", "huile d'olive", "sel", "poivre", "croûtons de blé entier"]
    ingredient = [1*multiplicateur_arrondi, "poitrines de poulet cuites et tranchées", 1*multiplicateur_arrondi, "têtes de laitue romaine, hachées", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tasse de parmesan râpé", 2*multiplicateur_arrondi, "cuillères à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "gousse d'ail émincée", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "tasse de croûtons de blé entier"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger la laitue romaine, les tomates cerises et le concombre.\nÉTAPE 2\nDans un petit bol, fouetter ensemble le vinaigre de vin rouge, le jus de citron, la moutarde de Dijon, l'ail, l'huile d'olive, le sel et le poivre pour faire la vinaigrette.\nÉTAPE 3\nAjouter les poitrines de poulet cuites et tranchées à la salade.\nÉTAPE 4\nVerser la vinaigrette sur la salade et mélanger pour bien enrober les ingrédients.\nÉTAPE 5\nSaupoudrer de parmesan râpé et de croûtons de blé entier avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_cesar_legere
def saumon_grille_asperges_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["saumon", "asperges", "quinoa", "eau", "sel", "poivre", "huile d'olive", "ail", "citron", "persil"]
    ingredient = [1*multiplicateur_arrondi, "filets de saumon", 1*multiplicateur_arrondi, "bouquet d'asperges, extrémités coupées", 1*multiplicateur_arrondi, "tasse de quinoa", 2*multiplicateur_arrondi, "tasses d'eau", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "citron coupé en quartiers", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter l'eau à ébullition, ajouter le quinoa, réduire le feu et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre.\nÉTAPE 3\nPendant ce temps, préchauffer le gril à feu moyen-élevé.\nÉTAPE 4\nBadigeonner les filets de saumon avec de l'huile d'olive, de l'ail émincé, du sel et du poivre.\nÉTAPE 5\nGriller le saumon sur le gril pendant environ 4-5 minutes de chaque côté ou jusqu'à ce qu'il soit cuit à votre goût.\nÉTAPE 6\nPendant les dernières minutes de cuisson du saumon, ajouter les asperges sur le gril et les faire cuire jusqu'à ce qu'elles soient tendres et légèrement dorées.\nÉTAPE 7\nServir le saumon grillé avec les asperges et le quinoa cuit.\nÉTAPE 8\nGarnir de quartiers de citron et de persil frais haché."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_grille_asperges_quinoa
def chili_vegetarien_haricots(multiplicateur_arrondi):
    ingredient_pour_algo = ["haricots rouges en conserve", "haricots noirs en conserve", "maïs en conserve", "tomates en dés", "oignon", "poivron rouge", "poivron vert", "ail", "piment jalapeño", "bouillon de légumes", "poudre de chili", "cumin", "paprika", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "boîte de haricots rouges, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de haricots noirs, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de maïs, égoutté", 1*multiplicateur_arrondi, "boîte de tomates en dés", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "poivron rouge coupé en dés", 1*multiplicateur_arrondi, "poivron vert coupé en dés", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "piment jalapeño haché (facultatif)", 2*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "cuillère à soupe de poudre de chili", 1*multiplicateur_arrondi, "cuillère à café de cumin en poudre", 1*multiplicateur_arrondi, "cuillère à café de paprika", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    preparation = ["ÉTAPE 1\nDans une grande casserole, chauffer un peu d'huile d'olive à feu moyen.\nÉTAPE 2\nAjouter l'oignon, les poivrons, l'ail et le piment jalapeño haché (si utilisé) et faire sauter jusqu'à ce que les légumes soient tendres.\nÉTAPE 3\nAjouter les haricots rouges, les haricots noirs, le maïs, les tomates en dés, le bouillon de légumes et les épices (poudre de chili, cumin, paprika, sel et poivre) dans la casserole.\nÉTAPE 4\nPorter à ébullition, puis réduire le feu et laisser mijoter pendant environ 20-25 minutes.\nÉTAPE 5\nRectifier l'assaisonnement selon votre goût.\nÉTAPE 6\nServir chaud, garni de coriandre fraîche hachée."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_vegetarien_haricots
def soupe_legumes_lentilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles corail", "eau", "carotte", "céleri", "oignon", "ail", "pomme de terre", "tomates en dés", "bouillon de légumes", "sel", "poivre", "curcuma", "cumin", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles corail", 4*multiplicateur_arrondi, "tasses d'eau", 1*multiplicateur_arrondi, "carotte coupée en dés", 1*multiplicateur_arrondi, "côte de céleri coupée en dés", 1*multiplicateur_arrondi, "oignon haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "pomme de terre coupée en dés", 1*multiplicateur_arrondi, "tasse de tomates en dés (fraîches ou en conserve)", 2*multiplicateur_arrondi, "tasses de bouillon de légumes", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de curcuma", 1*multiplicateur_arrondi, "cuillère à café de cumin en poudre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    preparation = ["ÉTAPE 1\nRincer les lentilles corail sous l'eau froide.\nÉTAPE 2\nDans une grande casserole, porter l'eau à ébullition, ajouter les lentilles corail et réduire le feu.\nÉTAPE 3\nAjouter les carottes, le céleri, l'oignon, l'ail, la pomme de terre, les tomates en dés et le bouillon de légumes dans la casserole.\nÉTAPE 4\nAssaisonner avec du sel, du poivre, du curcuma et du cumin.\nÉTAPE 5\nLaisser mijoter pendant environ 20-25 minutes ou jusqu'à ce que les lentilles et les légumes soient tendres.\nÉTAPE 6\nAjouter plus d'eau si nécessaire pour obtenir la consistance désirée.\nÉTAPE 7\nRectifier l'assaisonnement selon votre goût.\nÉTAPE 8\nServir chaud, garni de coriandre fraîche hachée."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_legumes_lentilles
def wrap_poulet_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "wrap de blé entier", "poivron rouge", "poivron jaune", "courgette", "aubergine", "huile d'olive", "sel", "poivre", "hummus"]
    ingredient = [1*multiplicateur_arrondi, "filets de poulet cuits et tranchés", 1*multiplicateur_arrondi, "wrap de blé entier", 1*multiplicateur_arrondi, "poivron rouge en lanières", 1*multiplicateur_arrondi, "poivron jaune en lanières", 1*multiplicateur_arrondi, "courgette en lanières", 1*multiplicateur_arrondi, "aubergine en lanières", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'hummus"]
    preparation = ["ÉTAPE 1\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 2\nBadigeonner les lanières de poivron rouge, de poivron jaune, de courgette et d'aubergine avec de l'huile d'olive et assaisonner avec du sel et du poivre.\nÉTAPE 3\nGriller les légumes sur le gril pendant environ 5-7 minutes de chaque côté ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 4\nPendant ce temps, chauffer les wraps de blé entier selon les instructions sur l'emballage.\nÉTAPE 5\nÉtaler une cuillère à soupe d'hummus sur chaque wrap.\nÉTAPE 6\nDisposer les tranches de poulet cuit sur le hummus.\nÉTAPE 7\nAjouter les légumes grillés sur le poulet.\nÉTAPE 8\nEnrouler les wraps fermement et couper en deux avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_legumes_grilles
def salade_lentilles_feta_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "eau", "sel", "poivre", "concombre", "tomate", "oignon rouge", "feta", "huile d'olive", "vinaigre de vin rouge", "origan séché"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes", 2*multiplicateur_arrondi, "tasses d'eau", "sel", "poivre", 1*multiplicateur_arrondi, "concombre coupé en dés", 1*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "oignon rouge haché", 1*multiplicateur_arrondi, "tasse de feta émiettée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de vin rouge", 1*multiplicateur_arrondi, "cuillère à café d'origan séché"]
    preparation = ["ÉTAPE 1\nRincer les lentilles sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter l'eau à ébullition, ajouter les lentilles et cuire à feu moyen pendant 20-25 minutes ou jusqu'à ce que les lentilles soient tendres.\nÉTAPE 3\nÉgoutter les lentilles et les rincer à l'eau froide.\nÉTAPE 4\nDans un grand bol, mélanger les lentilles cuites avec le concombre, la tomate, l'oignon rouge et la feta émiettée.\nÉTAPE 5\nDans un petit bol, mélanger l'huile d'olive, le vinaigre de vin rouge, l'origan séché, le sel et le poivre pour faire la vinaigrette.\nÉTAPE 6\nVerser la vinaigrette sur la salade de lentilles et mélanger doucement pour bien enrober les ingrédients.\nÉTAPE 7\nServir la salade de lentilles froide ou à température ambiante."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_feta_legumes
def poelee_crevettes_quinoa_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["crevettes", "quinoa", "eau", "sel", "poivre", "huile d'olive", "ail", "brocoli", "pois mange-tout", "poivron rouge", "carotte", "sauce soja", "miel", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "livre de crevettes décortiquées et déveinées", 1*multiplicateur_arrondi, "tasse de quinoa", 2*multiplicateur_arrondi, "tasses d'eau", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 1*multiplicateur_arrondi, "tasse de pois mange-tout", 1*multiplicateur_arrondi, "poivron rouge en dés", 1*multiplicateur_arrondi, "carotte en dés", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter l'eau à ébullition, ajouter le quinoa, réduire le feu et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre.\nÉTAPE 3\nPendant ce temps, chauffer l'huile d'olive dans une grande poêle à feu moyen.\nÉTAPE 4\nAjouter l'ail émincé et faire revenir pendant 1 minute.\nÉTAPE 5\nAjouter les crevettes et cuire jusqu'à ce qu'elles soient roses et opaques.\nÉTAPE 6\nRetirer les crevettes de la poêle et réserver.\nÉTAPE 7\nDans la même poêle, ajouter le brocoli, les pois mange-tout, le poivron rouge et la carotte.\nÉTAPE 8\nFaire sauter les légumes pendant quelques minutes jusqu'à ce qu'ils soient tendres mais encore croquants.\nÉTAPE 9\nAjouter les crevettes cuites et le quinoa cuit aux légumes dans la poêle.\nÉTAPE 10\nArroser la poêlée de sauce soja, de miel et de jus de citron.\nÉTAPE 11\nMélanger le tout et cuire pendant 2-3 minutes de plus.\nÉTAPE 12\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poelee_crevettes_quinoa_legumes
def poulet_roti_patates_brocoli(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet", "patates douces", "brocoli", "huile d'olive", "sel", "poivre", "paprika", "ail en poudre", "thym séché"]
    ingredient = [1*multiplicateur_arrondi, "poulet entier", 2*multiplicateur_arrondi, "patates douces en cubes", 1*multiplicateur_arrondi, "tête de brocoli en fleurettes", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café de paprika", 1*multiplicateur_arrondi, "cuillère à café d'ail en poudre", 1*multiplicateur_arrondi, "cuillère à café de thym séché"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nDans un grand bol, mélanger les cubes de patates douces avec de l'huile d'olive, du sel, du poivre, du paprika, de l'ail en poudre et du thym séché.\nÉTAPE 3\nDisposer les cubes de patates douces sur une plaque de cuisson et cuire au four pendant environ 20 minutes.\nÉTAPE 4\nPendant ce temps, préparer le poulet en le frottant avec de l'huile d'olive, du sel, du poivre, du paprika, de l'ail en poudre et du thym séché.\nÉTAPE 5\nAjouter le poulet sur la plaque de cuisson avec les patates douces et cuire au four pendant environ 45-60 minutes ou jusqu'à ce que le poulet soit bien cuit et que les patates douces soient tendres.\nÉTAPE 6\nEnviron 10 minutes avant la fin de la cuisson, ajouter les fleurettes de brocoli sur la plaque de cuisson pour les faire rôtir avec le poulet et les patates douces.\nÉTAPE 7\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 60
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_roti_patates_brocoli
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "eau", "sel", "poivre", "poivron rouge", "courgette", "aubergine", "tomates cerises", "huile d'olive", "vinaigre balsamique", "ail", "persil"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 2*multiplicateur_arrondi, "tasses d'eau", "sel", "poivre", 1*multiplicateur_arrondi, "poivron rouge en dés", 1*multiplicateur_arrondi, "courgette en dés", 1*multiplicateur_arrondi, "aubergine en dés", 1*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nRincer le quinoa sous l'eau froide.\nÉTAPE 2\nDans une casserole, porter l'eau à ébullition, ajouter le quinoa et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre.\nÉTAPE 3\nPendant ce temps, préchauffer le gril à feu moyen-élevé.\nÉTAPE 4\nDans un bol, mélanger les dés de poivron rouge, de courgette et d'aubergine avec l'huile d'olive, le vinaigre balsamique, l'ail, le sel et le poivre.\nÉTAPE 5\nGriller les légumes mariner pendant environ 8-10 minutes ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nDans un grand saladier, mélanger le quinoa cuit avec les légumes grillés et les tomates cerises.\nÉTAPE 7\nAssaisonner avec du sel, du poivre et du persil frais haché.\nÉTAPE 8\nServir chaud ou froid."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def boulettes_dinde_sauce_tomate(multiplicateur_arrondi):
    ingredient_pour_algo = ["dindeviande hachée", "oignon", "ail", "persil", "parmesan râpé", "œuf", "chapelure", "huile d'olive", "tomates en dés", "ail", "origan", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "livre de dinde hachée", 1*multiplicateur_arrondi, "oignon finement haché", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de parmesan râpé", 1*multiplicateur_arrondi, "œuf battu", 1*multiplicateur_arrondi, "tasse de chapelure", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "boîte de tomates en dés", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger la dinde hachée, l'oignon, l'ail, le persil, le parmesan râpé, l'œuf battu, la chapelure, le sel et le poivre.\nÉTAPE 2\nFormer des boulettes de viande avec le mélange.\nÉTAPE 3\nDans une grande poêle, chauffer l'huile d'olive à feu moyen.\nÉTAPE 4\nFaire dorer les boulettes de viande de tous les côtés jusqu'à ce qu'elles soient bien cuites.\nÉTAPE 5\nDans une autre casserole, chauffer un peu d'huile d'olive et faire revenir l'ail jusqu'à ce qu'il soit doré.\nÉTAPE 6\nAjouter les tomates en dés et l'origan, et laisser mijoter pendant environ 10 minutes.\nÉTAPE 7\nAssaisonner la sauce tomate avec du sel et du poivre selon votre goût.\nÉTAPE 8\nServir les boulettes de viande avec la sauce tomate chaude."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

boulettes_dinde_sauce_tomate
def salade_thon_haricots_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["thon en conserve", "haricots blancs en conserve", "poivron rouge", "tomate", "oignon rouge", "persil", "vinaigrette légère"]
    ingredient = [1*multiplicateur_arrondi, "boîte de thon en conserve", 1*multiplicateur_arrondi, "boîte de haricots blancs en conserve, égouttés et rincés", 1*multiplicateur_arrondi, "poivron rouge en dés", 1*multiplicateur_arrondi, "tomate en dés", 1*multiplicateur_arrondi, "oignon rouge émincé", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigrette légère"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le thon égoutté, les haricots blancs égouttés, le poivron rouge, la tomate, l'oignon rouge et le persil.\nÉTAPE 2\nArroser la salade de vinaigrette légère et mélanger doucement.\nÉTAPE 3\nServir frais."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_thon_haricots_legumes
def saumon_vapeur_riz_brun(multiplicateur_arrondi):
    ingredient_pour_algo = ["saumon", "riz brun", "eau", "sel", "poivre", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "filet de saumon", 1*multiplicateur_arrondi, "tasse de riz brun", 2*multiplicateur_arrondi, "tasses d'eau", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    preparation = ["ÉTAPE 1\nCuire le riz brun selon les instructions sur l'emballage.\nÉTAPE 2\nAssaisonner le saumon avec du sel, du poivre et du jus de citron.\nÉTAPE 3\nDans une grande casserole, porter l'eau à ébullition.\nÉTAPE 4\nPlacer le saumon dans un panier vapeur au-dessus de l'eau bouillante.\nÉTAPE 5\nCouvrir et laisser cuire à la vapeur pendant environ 8-10 minutes, ou jusqu'à ce que le saumon soit cuit.\nÉTAPE 6\nServir le saumon chaud sur le riz brun cuit."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

saumon_vapeur_riz_brun
def omelette_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs", "lait", "sel", "poivre", "huile d'olive", "oignon", "poivron", "tomate", "épinards", "fromage râpé (facultatif)"]
    ingredient = [2*multiplicateur_arrondi, "oeufs", 1*multiplicateur_arrondi, "cuillère à soupe de lait", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "poivron en dés", 1*multiplicateur_arrondi, "tomate en dés", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé (facultatif)"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les oeufs avec le lait, le sel et le poivre.\nÉTAPE 2\nChauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter l'oignon et le poivron et faire cuire jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nAjouter les tomates et les épinards et cuire jusqu'à ce que les épinards soient flétris.\nÉTAPE 5\nVerser le mélange d'oeufs sur les légumes dans la poêle.\nÉTAPE 6\nCuire jusqu'à ce que les bords commencent à prendre, puis soulever délicatement les bords avec une spatule pour laisser couler les oeufs non cuits vers le bas.\nÉTAPE 7\nSaupoudrer de fromage râpé (si désiré) et cuire jusqu'à ce que les oeufs soient complètement cuits.\nÉTAPE 8\nPliez l'omelette en deux et servez-la chaude."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_legumes
def chili_vegetarien(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "oignon", "poivron rouge", "poivron vert", "carotte", "céleri", "ail", "piment chili en poudre", "cumin en poudre", "haricots rouges", "haricots noirs", "tomates en dés", "bouillon de légumes", "sel", "poivre", "coriandre fraîche"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "poivron rouge en dés", 1*multiplicateur_arrondi, "poivron vert en dés", 1*multiplicateur_arrondi, "carotte en dés", 1*multiplicateur_arrondi, "branche de céleri hachée", 2*multiplicateur_arrondi, "gousses d'ail émincées", 1*multiplicateur_arrondi, "cuillère à soupe de piment chili en poudre", 1*multiplicateur_arrondi, "cuillère à café de cumin en poudre", 1*multiplicateur_arrondi, "boîte de haricots rouges, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de haricots noirs, égouttés et rincés", 1*multiplicateur_arrondi, "boîte de tomates en dés", 2*multiplicateur_arrondi, "tasses de bouillon de légumes", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une grande casserole à feu moyen.\nÉTAPE 2\nAjouter l'oignon, les poivrons, la carotte, le céleri et l'ail, et faire revenir pendant environ 5 minutes ou jusqu'à ce que les légumes soient tendres.\nÉTAPE 3\nAjouter le piment chili en poudre et le cumin en poudre et cuire pendant 1 minute de plus.\nÉTAPE 4\nAjouter les haricots, les tomates en dés et le bouillon de légumes.\nÉTAPE 5\nPorter à ébullition, puis réduire le feu et laisser mijoter pendant environ 20 minutes.\nÉTAPE 6\nAssaisonner de sel et de poivre selon votre goût.\nÉTAPE 7\nServir chaud, garni de coriandre fraîche."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chili_vegetarien
def wrap_laitue_dinde_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["feuilles de laitue", "dinde tranchée", "tomate", "concombre", "avocat", "mayonnaise légère", "moutarde", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "feuilles de laitue", 2*multiplicateur_arrondi, "tranches de dinde", 1*multiplicateur_arrondi, "tomate en tranches", 1*multiplicateur_arrondi, "concombre en tranches", 1*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à café de moutarde", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDisposer les feuilles de laitue sur une assiette.\nÉTAPE 2\nTartiner les tranches de dinde avec la moutarde et la mayonnaise légère.\nÉTAPE 3\nDisposer les tranches de dinde sur les feuilles de laitue.\nÉTAPE 4\nAjouter les tranches de tomate, de concombre et d'avocat sur la dinde.\nÉTAPE 5\nSaler et poivrer selon votre goût.\nÉTAPE 6\nEnrouler les feuilles de laitue autour des ingrédients pour former des wraps.\nÉTAPE 7\nServir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_laitue_dinde_legumes
def soupe_legumes_maison(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "oignon", "carotte", "céleri", "ail", "bouillon de légumes", "tomates en dés", "thym", "laurier", "sel", "poivre", "persil"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "oignon haché", 1*multiplicateur_arrondi, "carotte en dés", 1*multiplicateur_arrondi, "branche de céleri hachée", 2*multiplicateur_arrondi, "gousses d'ail émincées", 4*multiplicateur_arrondi, "tasses de bouillon de légumes", 1*multiplicateur_arrondi, "boîte de tomates en dés", 1*multiplicateur_arrondi, "cuillère à café de thym séché", 1*multiplicateur_arrondi, "feuille de laurier", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de persil frais haché"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une grande casserole à feu moyen.\nÉTAPE 2\nAjouter l'oignon, la carotte, le céleri et l'ail, et faire revenir pendant environ 5 minutes ou jusqu'à ce que les légumes commencent à ramollir.\nÉTAPE 3\nAjouter le bouillon de légumes, les tomates en dés, le thym, le laurier, le sel et le poivre.\nÉTAPE 4\nPorter à ébullition, puis réduire le feu et laisser mijoter pendant environ 20 minutes ou jusqu'à ce que les légumes soient tendres.\nÉTAPE 5\nRetirer le thym et la feuille de laurier.\nÉTAPE 6\nServir chaud, garni de persil frais."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

soupe_legumes_maison
def poisson_four_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "sel", "poivre", "jus de citron", "filets de poisson", "courgette", "poivron rouge", "oignon rouge", "asperges"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "filet de poisson", 1*multiplicateur_arrondi, "courgette", 1*multiplicateur_arrondi, "poivron rouge", 1*multiplicateur_arrondi, "oignon rouge", 1*multiplicateur_arrondi, "asperges"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C.\nÉTAPE 2\nDans un bol, mélanger l'huile d'olive, le sel, le poivre et le jus de citron.\nÉTAPE 3\nBadigeonner les filets de poisson avec ce mélange.\nÉTAPE 4\nCouper les légumes en morceaux et les disposer sur une plaque de cuisson.\nÉTAPE 5\nDéposer les filets de poisson sur les légumes.\nÉTAPE 6\nCuire au four pendant 20-25 minutes ou jusqu'à ce que le poisson soit cuit et les légumes soient tendres.\nÉTAPE 7\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poisson_four_legumes_grilles
def salade_poulet_grille_vinaigrette(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "vinaigre balsamique", "miel", "moutarde de Dijon", "ail", "sel", "poivre", "poitrine de poulet", "laitue", "tomate", "concombre", "oignon rouge"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre balsamique", 1*multiplicateur_arrondi, "cuillère à café de miel", 1*multiplicateur_arrondi, "cuillère à café de moutarde de Dijon", 1*multiplicateur_arrondi, "gousse d'ail émincée", "sel", "poivre", 1*multiplicateur_arrondi, "poitrine de poulet grillée", 1*multiplicateur_arrondi, "tasse de laitue", 1*multiplicateur_arrondi, "tomate en quartiers", 1*multiplicateur_arrondi, "concombre en rondelles", 1*multiplicateur_arrondi, "oignon rouge émincé"]
    preparation = ["ÉTAPE 1\nDans un petit bol, mélanger l'huile d'olive, le vinaigre balsamique, le miel, la moutarde de Dijon, l'ail, le sel et le poivre pour faire la vinaigrette.\nÉTAPE 2\nDans un grand bol, mélanger la laitue, les tomates, les concombres et les oignons rouges.\nÉTAPE 3\nFaire griller les poitrines de poulet jusqu'à ce qu'elles soient cuites, puis les trancher.\nÉTAPE 4\nAjouter le poulet à la salade.\nÉTAPE 5\nArroser la salade de vinaigrette et mélanger doucement.\nÉTAPE 6\nServir immédiatement."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_poulet_grille_vinaigrette
tuplerepaspourmidi_perte_de_poids=[bol_buddha_tofu_legumes,bol_burrito_vegetalien,boulettes_dinde_sauce_tomate,brochettes_crevettes_legumes_grilles,buddha_bowl_legumes_grilles,buddha_bowl_legumes_grilles_quinoa,chili_vegetarien,chili_vegetarien_haricots,curry_pois_chiches_epinards,omelette_legumes,poelee_crevettes_legumes,poelee_crevettes_quinoa_legumes,poelee_legumes_ete,pois_chiches_rotis_epices,poisson_blanc_legumes_four,poisson_four_asperges,poisson_four_legumes_grilles,poisson_papillote_legumes,poulet_curry_riz_basmati,poulet_grille_citron_ail,poulet_grille_legumes_rotis,poulet_legumes_sautes,poulet_roti_legumes,poulet_roti_patates_brocoli,poulet_roti_pommes_terre_legumes_rotis,poulet_teriyaki_legumes_sautes,riz_saute_legumes,salade_chou_frise_pois_chiches,salade_crevettes_avocat,salade_fruits_frais,salade_lentilles_feta_legumes,salade_lentilles_legumes,salade_lentilles_legumes_grilles,salade_lentilles_legumes_rotis,salade_poulet_agrumes_grilles,salade_poulet_cajun,salade_poulet_cesar_legere,salade_poulet_fraises_epinards,salade_poulet_grille_avocat,salade_poulet_grille_vinaigrette,salade_poulet_quinoa,salade_quinoa_fruits_mer,salade_quinoa_haricots_noirs_mais,salade_quinoa_legumes_grilles,salade_quinoa_legumes_rotis,salade_thon_haricots_blancs,salade_thon_haricots_legumes,salade_thon_legumes_grilles,saumon_four_legumes,saumon_grille_ail_citron,saumon_grille_asperges,saumon_grille_asperges_quinoa,saumon_grille_asperges_riz_complet,saumon_grille_citron_herbes,saumon_grille_quinoa_legumes,saumon_poele_riz_basmati_brocoli,saumon_vapeur_riz_brun,soupe_legumes_lentilles,soupe_legumes_maison,soupe_legumes_quinoa,soupe_lentilles_legumes,soupe_minestrone_legumes,tacos_poisson_salsa_avocat,tacos_vegetariens_avocat,tofu_legumes_sautes,tofu_saute_legumes,wrap_dinde_avocat,wrap_laitue_dinde_legumes,wrap_poulet_legumes_grilles,wrap_vegetalien_legumes_marines,wrap_vegetarien_haricots_noirs,wrap_vegetarien_legumes_grilles_houmous,wrap_vegetarien_pois_chiches,wraps_laitue_thon_avocat,wraps_poulet_legumes,wraps_poulet_legumes_grilles]
