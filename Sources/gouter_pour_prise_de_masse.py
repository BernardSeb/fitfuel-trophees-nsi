def barre_proteinee(multiplicateur_arrondi):
    ingredient_pour_algo = ["beurre de cacahuète", "miel", "protéine en poudre", "flocons d'avoine", "pépites de chocolat", "lait d'amande"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe de beurre de cacahuète", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "mesure de protéine en poudre", 1*multiplicateur_arrondi, "tasse de flocons d'avoine", 1*multiplicateur_arrondi, "tasse de pépites de chocolat", 1*multiplicateur_arrondi, "tasse de lait d'amande"]
    preparation = ["ÉTAPE 1\nMélanger le beurre de cacahuète et le miel dans une casserole à feu doux jusqu'à ce que le mélange soit homogène.\nÉTAPE 2\nRetirer du feu et ajouter la protéine en poudre, les flocons d'avoine et les pépites de chocolat.\nÉTAPE 3\nMélanger jusqu'à ce que tous les ingrédients soient bien combinés.\nÉTAPE 4\nVerser le lait d'amande et remuer jusqu'à obtenir une pâte.\nÉTAPE 5\nPresser le mélange dans un moule rectangulaire et réfrigérer pendant au moins une heure.\nÉTAPE 6\nCouper en barres avant de servir."]
    temps_de_preparation = 10
    temps_de_repos = 60
    temps_total = temps_de_preparation + temps_de_repos
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_repos, temps_total]
    return tous

barre_proteinee
def melange_noix_fruits_secs(multiplicateur_arrondi):
    ingredient_pour_algo = ["amandes", "noix de cajou", "noisettes", "raisins secs", "cranberries séchées", "abricots secs"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse d'amandes", 0.5*multiplicateur_arrondi, "tasse de noix de cajou", 0.5*multiplicateur_arrondi, "tasse de noisettes", 0.5*multiplicateur_arrondi, "tasse de raisins secs", 0.25*multiplicateur_arrondi, "tasse de cranberries séchées", 0.25*multiplicateur_arrondi, "tasse d'abricots secs"]
    preparation = ["ÉTAPE 1\nMélanger tous les ingrédients dans un grand bol.\nÉTAPE 2\nTransférer dans des petits sacs refermables pour une portion facile à emporter."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

melange_noix_fruits_secs
def avocat_ecrase_pain_complet(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "citron", "sel", "poivre", "pain complet tranché"]
    ingredient = [1*multiplicateur_arrondi, "avocat mûr", 0.5*multiplicateur_arrondi, "citron", "sel", "poivre", 2*multiplicateur_arrondi, "tranches de pain complet"]
    preparation = ["ÉTAPE 1\nCouper l'avocat en deux, retirer le noyau et la peau.\nÉTAPE 2\nÉcraser la chair d'avocat à la fourchette dans un bol.\nÉTAPE 3\nPresser le citron sur l'avocat écrasé et assaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nFaire griller les tranches de pain complet.\nÉTAPE 5\nÉtaler l'avocat écrasé sur les tranches de pain grillées.\nÉTAPE 6\nServir immédiatement."]
    temps_de_preparation = 10
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

avocat_ecrase_pain_complet
def flocons_avoine_proteines(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "protéine en poudre", "lait", "miel", "fruits frais"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de flocons d'avoine", 1*multiplicateur_arrondi, "mesure de protéine en poudre", 1*multiplicateur_arrondi, "tasse de lait", "cuillère à soupe de miel", "fruits frais pour garnir"]
    preparation = ["ÉTAPE 1\nDans une casserole, mélanger les flocons d'avoine, la protéine en poudre et le lait.\nÉTAPE 2\nFaire chauffer à feu moyen jusqu'à ce que les flocons d'avoine soient tendres et que le mélange épaississe.\nÉTAPE 3\nRetirer du feu et ajouter le miel, mélanger jusqu'à ce qu'il soit bien incorporé.\nÉTAPE 4\nServir chaud garni de fruits frais."]
    temps_de_preparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

flocons_avoine_proteines
def concombre_houmous(multiplicateur_arrondi):
    ingredient_pour_algo = ["concombre", "houmous", "paprika"]
    ingredient = [1*multiplicateur_arrondi, "concombre", 2*multiplicateur_arrondi, "cuillères à soupe de houmous", "paprika pour saupoudrer"]
    preparation = ["ÉTAPE 1\nCouper le concombre en tranches épaisses.\nÉTAPE 2\nDisposer les tranches de concombre sur une assiette de service.\nÉTAPE 3\nAjouter une cuillerée de houmous sur chaque tranche de concombre.\nÉTAPE 4\nSaupoudrer de paprika pour garnir."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

concombre_houmous
def yaourt_grec_graines_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "graines de chia", "miel", "fruits frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de graines de chia", "miel au goût", "fruits frais pour garnir"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger le yaourt grec avec les graines de chia.\nÉTAPE 2\nAjouter du miel au goût et bien mélanger.\nÉTAPE 3\nLaisser reposer au réfrigérateur pendant au moins 30 minutes pour que les graines de chia gonflent.\nÉTAPE 4\nServir avec des fruits frais."]
    temps_de_preparation = 5
    temps_de_repos = 30
    temps_total = temps_de_preparation + temps_de_repos
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_repos, temps_total]
    return tous

yaourt_grec_graines_chia
def riz_poulet_grille(multiplicateur_arrondi):
    ingredient_pour_algo = ["riz complet", "poulet", "huile d'olive", "sel", "poivre", "épices au choix"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de riz complet", 1*multiplicateur_arrondi, "poitrine de poulet", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre", "épices au choix"]
    preparation = ["ÉTAPE 1\nFaire cuire le riz complet selon les instructions sur l'emballage.\nÉTAPE 2\nAssaisonner la poitrine de poulet avec du sel, du poivre et des épices au choix.\nÉTAPE 3\nFaire chauffer l'huile d'olive dans une poêle à feu moyen-élevé.\nÉTAPE 4\nFaire griller le poulet jusqu'à ce qu'il soit bien cuit et doré des deux côtés.\nÉTAPE 5\nCouper le poulet en tranches.\nÉTAPE 6\nServir le poulet grillé sur le riz complet cuit."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

riz_poulet_grille
def chips_patates_douces(multiplicateur_arrondi):
    ingredient_pour_algo = ["patates douces", "huile d'olive", "sel", "paprika"]
    ingredient = [1*multiplicateur_arrondi, "patate douce", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "paprika"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C (400°F).\nÉTAPE 2\nLaver et éplucher les patates douces.\nÉTAPE 3\nCouper les patates douces en tranches fines à l'aide d'une mandoline ou d'un couteau bien aiguisé.\nÉTAPE 4\nDans un bol, mélanger les tranches de patates douces avec l'huile d'olive, le sel et le paprika.\nÉTAPE 5\nDisposer les tranches de patates douces sur une plaque à pâtisserie recouverte de papier sulfurisé en une seule couche.\nÉTAPE 6\nCuire au four pendant 15 à 20 minutes, en retournant à mi-cuisson, jusqu'à ce que les chips soient dorées et croustillantes.\nÉTAPE 7\nLaisser refroidir avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

chips_patates_douces
def fromage_blanc_baies(multiplicateur_arrondi):
    ingredient_pour_algo = ["fromage blanc", "baies", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de fromage blanc", 0.5*multiplicateur_arrondi, "tasse de baies mélangées", "miel au goût"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger le fromage blanc avec les baies mélangées.\nÉTAPE 2\nAjouter du miel au goût et mélanger doucement pour incorporer.\nÉTAPE 3\nServir frais."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

fromage_blanc_baies
def pain_complet_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "beurre d'arachide"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet", 2*multiplicateur_arrondi, "cuillères à soupe de beurre d'arachide"]
    preparation = ["ÉTAPE 1\nFaire griller les tranches de pain complet si désiré.\nÉTAPE 2\nÉtaler uniformément le beurre d'arachide sur les tranches de pain.\nÉTAPE 3\nAssembler en sandwich si désiré ou servir ouvert."]
    temps_de_preparation = 2
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

pain_complet_beurre_arachide
def tofu_saute_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu", "légumes", "huile d'olive", "sauce soja"]
    ingredient = [1*multiplicateur_arrondi, "bloc de tofu ferme", 1*multiplicateur_arrondi, "tasse de légumes coupés en dés (ex : poivrons, champignons, brocoli)", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja"]
    preparation = ["ÉTAPE 1\nPresser le tofu pour en retirer l'excès d'eau, puis le couper en dés.\nÉTAPE 2\nChauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter les dés de tofu et faire sauter jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 4\nAjouter les légumes coupés en dés et faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 5\nAjouter la sauce soja et mélanger pour enrober les ingrédients.\nÉTAPE 6\nServir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_legumes
def laitue_thon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["laitue", "thon", "avocat", "tomate", "vinaigrette"]
    ingredient = [1*multiplicateur_arrondi, "tête de laitue", 1*multiplicateur_arrondi, "boîte de thon égoutté", 1*multiplicateur_arrondi, "avocat mûr", 1*multiplicateur_arrondi, "tomate coupée en dés", "vinaigrette au choix"]
    preparation = ["ÉTAPE 1\nLaver et sécher les feuilles de laitue, puis les disposer sur une assiette de service.\nÉTAPE 2\nÉgoutter le thon et l'émietter sur les feuilles de laitue.\nÉTAPE 3\nCouper l'avocat en tranches et les disposer sur le thon.\nÉTAPE 4\nAjouter les dés de tomate sur le dessus.\nÉTAPE 5\nArroser de vinaigrette au choix.\nÉTAPE 6\nServir frais."]
    temps_de_preparation = 10
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

laitue_thon_avocat
def pain_complet_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "beurre d'amande"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet", 2*multiplicateur_arrondi, "cuillères à soupe de beurre d'amande"]
    preparation = ["ÉTAPE 1\nFaire griller les tranches de pain complet si désiré.\nÉTAPE 2\nÉtaler uniformément le beurre d'amande sur les tranches de pain.\nÉTAPE 3\nAssembler en sandwich si désiré ou servir ouvert."]
    temps_de_preparation = 2
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

pain_complet_beurre_amande
def tofu_saute_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tofu", "légumes", "huile d'olive", "sauce soja"]
    ingredient = [1*multiplicateur_arrondi, "bloc de tofu ferme", 1*multiplicateur_arrondi, "tasse de légumes coupés en dés (ex : poivrons, champignons, brocoli)", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de sauce soja"]
    preparation = ["ÉTAPE 1\nPresser le tofu pour en retirer l'excès d'eau, puis le couper en dés.\nÉTAPE 2\nChauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter les dés de tofu et faire sauter jusqu'à ce qu'ils soient dorés et croustillants.\nÉTAPE 4\nAjouter les légumes coupés en dés et faire sauter jusqu'à ce qu'ils soient tendres.\nÉTAPE 5\nAjouter la sauce soja et mélanger pour enrober les ingrédients.\nÉTAPE 6\nServir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

tofu_saute_legumes
def laitue_thon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["laitue", "thon", "avocat", "tomate", "vinaigrette"]
    ingredient = [1*multiplicateur_arrondi, "tête de laitue", 1*multiplicateur_arrondi, "boîte de thon égoutté", 1*multiplicateur_arrondi, "avocat mûr", 1*multiplicateur_arrondi, "tomate coupée en dés", "vinaigrette au choix"]
    preparation = ["ÉTAPE 1\nLaver et sécher les feuilles de laitue, puis les disposer sur une assiette de service.\nÉTAPE 2\nÉgoutter le thon et l'émietter sur les feuilles de laitue.\nÉTAPE 3\nCouper l'avocat en tranches et les disposer sur le thon.\nÉTAPE 4\nAjouter les dés de tomate sur le dessus.\nÉTAPE 5\nArroser de vinaigrette au choix.\nÉTAPE 6\nServir frais."]
    temps_de_preparation = 10
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

laitue_thon_avocat
def smoothie_fruits_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "fruits surgelés", "jus de fruits"]
    ingredient = [1*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "tasse de fruits surgelés (ex : banane, fraises, mangue)", 0.5*multiplicateur_arrondi, "tasse de jus de fruits (ex : jus d'orange, jus de pomme)"]
    preparation = ["ÉTAPE 1\nDans un blender, mettre le yaourt grec, les fruits surgelés et le jus de fruits.\nÉTAPE 2\nMixer jusqu'à obtention d'une texture lisse et homogène.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

smoothie_fruits_yaourt_grec
def barres_noix_miel(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix mélangées", "miel", "flocons d'avoine", "beurre"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de noix mélangées concassées (ex : noix de cajou, amandes, noix)", 0.25*multiplicateur_arrondi, "tasse de miel", 0.5*multiplicateur_arrondi, "tasse de flocons d'avoine", 2*multiplicateur_arrondi, "cuillères à soupe de beurre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (350°F) et tapisser un moule carré de papier parchemin.\nÉTAPE 2\nDans une casserole, faire fondre le miel et le beurre à feu doux jusqu'à ce que le mélange soit lisse.\nÉTAPE 3\nDans un bol, mélanger les noix mélangées concassées et les flocons d'avoine.\nÉTAPE 4\nVerser le mélange de miel et de beurre sur les ingrédients secs et bien mélanger.\nÉTAPE 5\nPresser le mélange dans le moule préparé et lisser le dessus.\nÉTAPE 6\nCuire au four pendant 20 à 25 minutes, ou jusqu'à ce que les barres soient dorées sur les bords.\nÉTAPE 7\nLaisser refroidir complètement avant de découper en barres."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_noix_miel
def pudding_chia_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["graines de chia", "lait d'amande", "sirop d'érable", "extrait de vanille"]
    ingredient = [2*multiplicateur_arrondi, "cuillères à soupe de graines de chia", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de sirop d'érable", 0.5*multiplicateur_arrondi, "cuillère à café d'extrait de vanille"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les graines de chia, le lait d'amande, le sirop d'érable et l'extrait de vanille.\nÉTAPE 2\nBien mélanger pour s'assurer que les graines de chia sont complètement immergées dans le liquide.\nÉTAPE 3\nPlacer au réfrigérateur pendant au moins 2 heures, ou toute la nuit, jusqu'à ce que le mélange ait épaissi et pris une consistance de pudding.\nÉTAPE 4\nRemuer avant de servir et garnir de fruits frais si désiré."]
    temps_de_preparation = 5
    temps_de_repos = 120
    temps_total = temps_de_preparation + temps_de_repos
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_repos, temps_total]
    return tous

pudding_chia_vanille
def muffins_banane_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes mûres", "œufs", "beurre fondu", "sucre", "farine", "levure chimique", "sel", "pépites de chocolat"]
    ingredient = [2*multiplicateur_arrondi, "bananes mûres écrasées", 2*multiplicateur_arrondi, "œufs battus", 0.5*multiplicateur_arrondi, "tasse de beurre fondu", 1*multiplicateur_arrondi, "tasse de sucre", 1.5*multiplicateur_arrondi, "tasse de farine tout usage", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", 0.5*multiplicateur_arrondi, "cuillère à café de sel", 1*multiplicateur_arrondi, "tasse de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (350°F) et tapisser un moule à muffins de caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger les bananes écrasées, les œufs battus et le beurre fondu.\nÉTAPE 3\nAjouter le sucre et bien mélanger.\nÉTAPE 4\nIncorporer la farine, la levure chimique et le sel jusqu'à ce que le mélange soit homogène.\nÉTAPE 5\nAjouter les pépites de chocolat et mélanger délicatement.\nÉTAPE 6\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 7\nCuire au four pendant 20 à 25 minutes, ou jusqu'à ce que les muffins soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 8\nLaisser refroidir avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_banane_chocolat
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "légumes", "huile d'olive", "vinaigre", "sel", "poivre", "épices au choix"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "tasse de légumes grillés (ex : poivrons, courgettes, oignons)", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", "sel", "poivre", "épices au choix"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire fine.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition.\nÉTAPE 3\nAjouter le quinoa, réduire le feu et laisser mijoter pendant 15 minutes, ou jusqu'à ce que toute l'eau soit absorbée et que le quinoa soit tendre.\nÉTAPE 4\nRetirer du feu et laisser reposer à couvert pendant 5 minutes.\nÉTAPE 5\nDans un bol, mélanger les légumes grillés avec le quinoa cuit.\nÉTAPE 6\nAjouter l'huile d'olive, le vinaigre de cidre, le sel, le poivre et les épices au choix.\nÉTAPE 7\nMélanger délicatement pour bien enrober les ingrédients.\nÉTAPE 8\nServir chaud ou froid."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def batonnets_celeri_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["céleri", "beurre d'arachide"]
    ingredient = [1*multiplicateur_arrondi, "tige de céleri coupée en bâtonnets", 2*multiplicateur_arrondi, "cuillères à soupe de beurre d'arachide"]
    preparation = ["ÉTAPE 1\nLaver et sécher la tige de céleri, puis la couper en bâtonnets.\nÉTAPE 2\nTartiner les bâtonnets de céleri avec le beurre d'arachide.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

batonnets_celeri_beurre_arachide
def smoothie_epinards_banane_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards frais", "banane", "beurre d'arachide", "lait d'amande", "graines de chia"]
    ingredient = [1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "banane", 1*multiplicateur_arrondi, "cuillère à soupe de beurre d'arachide", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de graines de chia"]
    preparation = ["ÉTAPE 1\nMettre les épinards, la banane coupée, le beurre d'arachide, le lait d'amande et les graines de chia dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et homogène.\nÉTAPE 3\nAjuster la consistance en ajoutant plus de lait d'amande si nécessaire.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

smoothie_epinards_banane_arachide
def yogourt_grec_miel_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["yogourt grec", "miel", "noix mélangées"]
    ingredient = [1*multiplicateur_arrondi, "tasse de yogourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 0.5*multiplicateur_arrondi, "tasse de noix mélangées (hachées)"]
    preparation = ["ÉTAPE 1\nVerser le yogourt grec dans un bol.\nÉTAPE 2\nArroser de miel sur le dessus du yogourt.\nÉTAPE 3\nSaupoudrer les noix mélangées hachées sur le yogourt.\nÉTAPE 4\nMélanger légèrement avant de servir."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

yogourt_grec_miel_noix
def toast_avocat_oeuf_poché(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "avocat", "œuf", "vinaigre", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranche de pain complet", 0.5*multiplicateur_arrondi, "avocat écrasé", 1*multiplicateur_arrondi, "œuf", 1*multiplicateur_arrondi, "cuillère à café de vinaigre (pour pocher l'œuf)", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nFaire toaster le pain jusqu'à ce qu'il soit croustillant.\nÉTAPE 2\nFaire chauffer de l'eau dans une casserole jusqu'à frémissement, ajouter le vinaigre.\nÉTAPE 3\nCasser l'œuf dans un ramequin, puis le glisser délicatement dans l'eau frémissante. Laisser pocher pendant 3-4 minutes pour un jaune coulant.\nÉTAPE 4\nÉtaler l'avocat écrasé sur le toast, assaisonner de sel et de poivre.\nÉTAPE 5\nPlacer délicatement l'œuf poché sur l'avocat. Assaisonner à nouveau selon le goût."]
    temps_de_preparation = 10
    temps_de_cuisson = 4
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

toast_avocat_oeuf_poché
def galettes_quinoa_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "carotte", "courgette", "oignon", "œufs", "farine", "sel", "poivre"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de quinoa cuit", 0.5*multiplicateur_arrondi, "carotte râpée", 0.5*multiplicateur_arrondi, "courgette râpée", 0.25*multiplicateur_arrondi, "oignon finement haché", 1*multiplicateur_arrondi, "œuf", 2*multiplicateur_arrondi, "cuillères à soupe de farine", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nMélanger le quinoa cuit, les légumes râpés, l'oignon haché, l'œuf, la farine, le sel et le poivre dans un grand bol.\nÉTAPE 2\nChauffer une poêle antiadhésive à feu moyen et ajouter un peu d'huile.\nÉTAPE 3\nFormer des petites galettes avec le mélange et les faire cuire pendant 3-4 minutes de chaque côté, jusqu'à ce qu'elles soient dorées et croustillantes.\nÉTAPE 4\nServir chaud."]
    temps_de_preparation = 15
    temps_de_cuisson = 8
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

galettes_quinoa_legumes
def porridge_avoine_banane_cacahuete(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait", "banane", "beurre de cacahuète", "miel"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de flocons d'avoine", 1*multiplicateur_arrondi, "tasse de lait", 0.5*multiplicateur_arrondi, "banane tranchée", 1*multiplicateur_arrondi, "cuillère à soupe de beurre de cacahuète", 1*multiplicateur_arrondi, "cuillère à café de miel"]
    preparation = ["ÉTAPE 1\nDans une casserole, mélanger les flocons d'avoine et le lait. Porter à ébullition, puis réduire le feu et laisser mijoter jusqu'à ce que l'avoine soit tendre.\nÉTAPE 2\nAjouter la banane tranchée, le beurre de cacahuète et le miel au porridge. Bien mélanger.\nÉTAPE 3\nServir chaud."]
    temps_de_preparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

porridge_avoine_banane_cacahuete
def smoothie_proteine_chocolat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande", "poudre de protéine au chocolat", "banane", "beurre d'amande", "graines de chia"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "scoop de poudre de protéine au chocolat", 1*multiplicateur_arrondi, "banane", 1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de graines de chia"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une texture lisse et homogène.\nÉTAPE 3\nAjuster la texture en ajoutant plus de lait d'amande si nécessaire.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

smoothie_proteine_chocolat_banane
def toast_avocat_oeuf_poché(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "avocat", "œuf", "vinaigre", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranche de pain complet", 0.5*multiplicateur_arrondi, "avocat", 1*multiplicateur_arrondi, "œuf", 1*multiplicateur_arrondi, "cuillère à café de vinaigre", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nFaire toaster le pain jusqu'à ce qu'il soit croustillant.\nÉTAPE 2\nÉcraser l'avocat et étaler sur le pain toasté. Assaisonner avec du sel et du poivre.\nÉTAPE 3\nPorter une casserole d'eau à ébullition, ajouter le vinaigre. Réduire le feu pour que l'eau frémisse. Casser l'œuf dans un ramequin, puis le verser délicatement dans l'eau frémissante. Laisser pocher pendant 3-4 minutes pour un jaune coulant.\nÉTAPE 4\nRetirer l'œuf avec une écumoire et l'égoutter sur du papier absorbant. Placer l'œuf poché sur le toast à l'avocat.\nÉTAPE 5\nServir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 4
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

toast_avocat_oeuf_poché
def muffins_myrtilles_proteines(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine", "myrtilles", "œuf", "lait d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de farine d'avoine", 0.5*multiplicateur_arrondi, "tasse de poudre de protéine", 0.5*multiplicateur_arrondi, "tasse de myrtilles", 1*multiplicateur_arrondi, "œuf", 0.75*multiplicateur_arrondi, "tasse de lait d'amande", 2*multiplicateur_arrondi, "cuillères à soupe de miel"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (350°F). Graisser légèrement les moules à muffins.\nÉTAPE 2\nDans un grand bol, mélanger la farine d'avoine, la poudre de protéine, les myrtilles, l'œuf, le lait d'amande et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 3\nRépartir la pâte dans les moules à muffins.\nÉTAPE 4\nCuire au four pendant 20-25 minutes ou jusqu'à ce que les muffins soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 5\nLaisser refroidir avant de démouler."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_myrtilles_proteines
def bowl_quinoa_poulet_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poitrine de poulet", "brocoli", "carotte", "poivron", "huile d'olive", "sel", "poivre"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de quinoa cuit", 100*multiplicateur_arrondi, "g de poitrine de poulet cuite", 0.5*multiplicateur_arrondi, "tasse de brocoli en fleurettes", 0.5*multiplicateur_arrondi, "carotte coupée en dés", 0.5*multiplicateur_arrondi, "poivron coupé en dés", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger le quinoa cuit, les légumes coupés et le poulet.\nÉTAPE 2\nAssaisonner avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 3\nMélanger bien et servir."]
    temps_de_preparation = 10
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

bowl_quinoa_poulet_legumes
def smoothie_banane_beurre_cacahuete(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "beurre de cacahuète", "lait d'amande", "poudre de protéine", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane mûre", 2*multiplicateur_arrondi, "cuillères à soupe de beurre de cacahuète", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "scoop de poudre de protéine de vanille", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à l'obtention d'une consistance lisse et homogène.\nÉTAPE 3\nVerser dans un grand verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

smoothie_banane_beurre_cacahuete
def wrap_dinde_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortilla complète", "tranche de dinde", "avocat", "laitue", "tomate", "mayonnaise légère"]
    ingredient = [1*multiplicateur_arrondi, "tortilla complète", 2*multiplicateur_arrondi, "tranches de dinde", 0.5*multiplicateur_arrondi, "avocat écrasé", 1*multiplicateur_arrondi, "feuille de laitue", 0.5*multiplicateur_arrondi, "tomate coupée en dés", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère"]
    preparation = ["ÉTAPE 1\nÉtaler l'avocat écrasé sur la tortilla.\nÉTAPE 2\nAjouter les tranches de dinde, la laitue, les dés de tomate et la mayonnaise.\nÉTAPE 3\nRouler la tortilla fermement.\nÉTAPE 4\nCouper le wrap en deux et servir."]
    temps_de_preparation = 10
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

wrap_dinde_avocat
def salade_quinoa_saumon(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "saumon cuit", "concombre", "tomate cerise", "feta", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de quinoa cuit", 100*multiplicateur_arrondi, "g de saumon cuit", 0.5*multiplicateur_arrondi, "concombre coupé en dés", 0.5*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 0.25*multiplicateur_arrondi, "tasse de feta émiettée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger le quinoa, le saumon émietté, le concombre, les tomates cerises, et la feta.\nÉTAPE 2\nAssaisonner avec le jus de citron, l'huile d'olive, du sel et du poivre. Mélanger délicatement.\nÉTAPE 3\nServir frais."]
    temps_de_preparation = 15
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_total]
    return tous

salade_quinoa_saumon
def muffins_proteines_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine vanille", "poudre à lever", "sel", "banane", "oeufs", "lait d'amande", "miel", "myrtilles"]
    ingredient = [1.5*multiplicateur_arrondi, "tasses de farine d'avoine", 0.5*multiplicateur_arrondi, "tasse de poudre de protéine de vanille", 1*multiplicateur_arrondi, "cuillère à café de poudre à lever", "une pincée de sel", 1*multiplicateur_arrondi, "banane écrasée", 2*multiplicateur_arrondi, "oeufs", 0.75*multiplicateur_arrondi, "tasse de lait d'amande", 2*multiplicateur_arrondi, "cuillères à soupe de miel", 0.5*multiplicateur_arrondi, "tasse de myrtilles"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C (350°F) et préparer un moule à muffins avec des caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la farine d'avoine, la poudre de protéine, la poudre à lever, et le sel.\nÉTAPE 3\nDans un autre bol, mélanger la banane écrasée, les oeufs, le lait d'amande, et le miel jusqu'à ce que le mélange soit homogène.\nÉTAPE 4\nIncorporer les ingrédients humides aux ingrédients secs et mélanger jusqu'à ce que tout soit juste combiné. Ajouter délicatement les myrtilles.\nÉTAPE 5\nRépartir la pâte dans les moules à muffins et cuire au four pendant 20-25 minutes, ou jusqu'à ce qu'un cure-dent inséré dans un muffin en ressorte propre.\nÉTAPE 6\nLaisser refroidir avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_proteines_myrtilles
def toast_avocat_oeuf_poche(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranche de pain complet", "avocat", "oeuf", "vinaigre", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranche de pain complet", 0.5*multiplicateur_arrondi, "avocat écrasé", 1*multiplicateur_arrondi, "oeuf", "vinaigre pour l'eau de pochage", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nFaire griller le pain jusqu'à ce qu'il soit croustillant.\nÉTAPE 2\nFaire chauffer de l'eau dans une casserole et ajouter un peu de vinaigre. Porter à ébullition légère.\nÉTAPE 3\nCasser l'oeuf dans un petit ramequin puis le glisser délicatement dans l'eau frémissante. Laisser pocher pendant 3-4 minutes pour un jaune encore coulant.\nÉTAPE 4\nÉtaler l'avocat écrasé sur le toast, placer l'oeuf poché dessus, saler et poivrer."]
    temps_de_preparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

toast_avocat_oeuf_poche
def pancakes_banane_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine complète", "levure chimique", "sel", "lait", "oeuf", "huile de coco", "banane", "noix", "sirop d'érable"]
    ingredient = [1*multiplicateur_arrondi, "tasse de farine complète", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", "une pincée de sel", 1*multiplicateur_arrondi, "tasse de lait", 1*multiplicateur_arrondi, "oeuf", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco fondue", 1*multiplicateur_arrondi, "banane écrasée", 0.25*multiplicateur_arrondi, "tasse de noix hachées", "sirop d'érable pour servir"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger la farine, la levure chimique, et le sel.\nÉTAPE 2\nDans un autre bol, battre l'oeuf avec le lait et l'huile de coco fondue. Ajouter la banane écrasée.\nÉTAPE 3\nIncorporer les ingrédients humides aux ingrédients secs et mélanger jusqu'à ce que le tout soit juste combiné. Incorporer délicatement les noix hachées.\nÉTAPE 4\nChauffer une poêle légèrement huilée à feu moyen. Verser des louchées de pâte pour former des pancakes. Cuire jusqu'à ce que des bulles apparaissent sur la surface, puis retourner et cuire jusqu'à ce que le pancake soit doré.\nÉTAPE 5\nServir chaud avec du sirop d'érable."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pancakes_banane_noix
def smoothie_proteine_beurre_cacahuete_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande", "poudre de protéine de vanille", "banane", "beurre de cacahuète", "glace"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lait d'amande", 2*multiplicateur_arrondi, "cuillères à soupe de poudre de protéine de vanille", 1*multiplicateur_arrondi, "banane congelée", 2*multiplicateur_arrondi, "cuillères à soupe de beurre de cacahuète", "glace"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans le blender.\nÉTAPE 2\nMixer à haute vitesse jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjuster la consistance en ajoutant plus de lait d'amande si nécessaire.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_proteine_beurre_cacahuete_banane
def bol_quinoa_fruits_secs_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa cuit", "amandes", "noix de pécan", "abricots secs", "miel", "lait d'amande"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 0.25*multiplicateur_arrondi, "tasse d'amandes hachées", 0.25*multiplicateur_arrondi, "tasse de noix de pécan hachées", 0.25*multiplicateur_arrondi, "tasse d'abricots secs hachés", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 0.5*multiplicateur_arrondi, "tasse de lait d'amande"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger le quinoa cuit avec les amandes, noix de pécan, et abricots secs.\nÉTAPE 2\nArroser de miel et ajouter le lait d'amande.\nÉTAPE 3\nMélanger bien et servir. Peut être dégusté chaud ou froid."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous
bol_quinoa_fruits_secs_noix
def omelette_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "fromage râpé", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "œufs", 1*multiplicateur_arrondi, "poignée d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le sel et le poivre.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter les épinards frais dans la poêle et les faire cuire jusqu'à ce qu'ils soient fanés.\nÉTAPE 4\nVerser les œufs battus dans la poêle.\nÉTAPE 5\nSaupoudrer le fromage râpé sur les œufs.\nÉTAPE 6\nLaisser cuire jusqu'à ce que les bords commencent à se décoller de la poêle, puis replier l'omelette en deux.\nÉTAPE 7\nContinuer à cuire jusqu'à ce que l'omelette soit bien prise.\nÉTAPE 8\nServir chaud."]
    temps_de_preparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_fromage
def omelette_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "fromage râpé", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "œufs", 1*multiplicateur_arrondi, "poignée d'épinards frais", 1*multiplicateur_arrondi, "cuillère à soupe de fromage râpé", 1*multiplicateur_arrondi, "cuillère à café d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, battre les œufs avec le sel et le poivre.\nÉTAPE 2\nFaire chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 3\nAjouter les épinards frais dans la poêle et les faire cuire jusqu'à ce qu'ils soient fanés.\nÉTAPE 4\nVerser les œufs battus dans la poêle.\nÉTAPE 5\nSaupoudrer le fromage râpé sur les œufs.\nÉTAPE 6\nLaisser cuire jusqu'à ce que les bords commencent à se décoller de la poêle, puis replier l'omelette en deux.\nÉTAPE 7\nContinuer à cuire jusqu'à ce que l'omelette soit bien prise.\nÉTAPE 8\nServir chaud."]
    temps_de_preparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

omelette_epinards_fromage
def barres_proteinees_maison(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine de vanille", "beurre d'arachide", "miel", "pépites de chocolat"]
    ingredient = [1*multiplicateur_arrondi, "tasse de flocons d'avoine", 0.5*multiplicateur_arrondi, "tasse de poudre de protéine de vanille", 0.5*multiplicateur_arrondi, "tasse de beurre d'arachide", 0.25*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les flocons d'avoine et la poudre de protéine de vanille.\nÉTAPE 2\nDans une casserole, chauffer le beurre d'arachide et le miel à feu doux jusqu'à ce qu'ils soient bien mélangés.\nÉTAPE 3\nVerser le mélange de beurre d'arachide sur les flocons d'avoine et mélanger jusqu'à ce que tous les ingrédients soient bien combinés.\nÉTAPE 4\nIncorporer les pépites de chocolat.\nÉTAPE 5\nPresser le mélange dans un plat carré tapissé de papier parchemin et mettre au réfrigérateur pendant au moins une heure.\nÉTAPE 6\nDécouper en barres et servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_proteinees_maison
def toast_avocat_saumon_fume(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet", "avocat", "saumon fumé", "citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tranches de pain complet", 0.5*multiplicateur_arrondi, "avocat écrasé", 50*multiplicateur_arrondi, "g de saumon fumé", "jus de citron", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nFaire griller les tranches de pain jusqu'à ce qu'elles soient croustillantes.\nÉTAPE 2\nÉtaler l'avocat écrasé sur les tranches de pain grillé.\nÉTAPE 3\nDéposer des tranches de saumon fumé sur l'avocat.\nÉTAPE 4\nArroser de jus de citron, saler et poivrer selon votre goût."]
    temps_de_preparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

toast_avocat_saumon_fume
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "yaourt grec", "banane", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de mangue congelée", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 0.5*multiplicateur_arrondi, "banane", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait de coco si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_coco
def barres_energetiques_fruits_secs(multiplicateur_arrondi):
    ingredient_pour_algo = ["dattes", "noix de cajou", "noix", "amandes", "raisins secs", "cannelle"]
    ingredient = [1*multiplicateur_arrondi, "tasse de dattes dénoyautées", 0.5*multiplicateur_arrondi, "tasse de noix de cajou", 0.25*multiplicateur_arrondi, "tasse de noix", 0.25*multiplicateur_arrondi, "tasse d'amandes", 0.25*multiplicateur_arrondi, "tasse de raisins secs", "une pincée de cannelle"]
    preparation = ["ÉTAPE 1\nDans un robot culinaire, mélanger les dattes, les noix de cajou, les noix, les amandes, les raisins secs et la cannelle.\nÉTAPE 2\nPulser jusqu'à ce que les ingrédients soient bien combinés et que le mélange commence à former une pâte.\nÉTAPE 3\nPresser le mélange dans un plat carré tapissé de papier parchemin.\nÉTAPE 4\nRéfrigérer pendant au moins une heure, puis couper en barres avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_energetiques_fruits_secs
def yaourt_grec_fruits_granola(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "fruits frais", "granola"]
    ingredient = [1*multiplicateur_arrondi, "tasse de yaourt grec", 0.5*multiplicateur_arrondi, "tasse de fruits frais coupés en dés", 0.25*multiplicateur_arrondi, "tasse de granola"]
    preparation = ["ÉTAPE 1\nDans un bol, verser le yaourt grec.\nÉTAPE 2\nAjouter les fruits frais coupés en dés sur le yaourt.\nÉTAPE 3\nSaupoudrer de granola sur le dessus.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_fruits_granola
def smoothie_chocolat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "lait d'amande", "poudre de cacao non sucré", "poudre de protéine de vanille", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane congelée", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de poudre de cacao non sucré", 1*multiplicateur_arrondi, "scoop de poudre de protéine de vanille", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_chocolat_banane
def sandwich_poulet_grille_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "poitrine de poulet grillée", "tomate", "laitue", "avocat", "moutarde"]
    ingredient = [1*multiplicateur_arrondi, "tranche de pain complet", 1*multiplicateur_arrondi, "poitrine de poulet grillée tranchée", 2*multiplicateur_arrondi, "tranches de tomate", 2*multiplicateur_arrondi, "feuilles de laitue", 0.5*multiplicateur_arrondi, "avocat en tranches", 1*multiplicateur_arrondi, "cuillère à café de moutarde"]
    preparation = ["ÉTAPE 1\nTartiner une tranche de pain complet avec de la moutarde.\nÉTAPE 2\nDisposer les tranches de poulet grillé sur le pain.\nÉTAPE 3\nAjouter les tranches de tomate, les feuilles de laitue et les tranches d'avocat.\nÉTAPE 4\nRecouvrir avec une autre tranche de pain complet.\nÉTAPE 5\nCouper en deux et servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_grille_legumes
def barres_proteinees_beurre_cacahuete(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine de vanille", "beurre de cacahuète", "miel", "pépites de chocolat"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de flocons d'avoine", 0.5*multiplicateur_arrondi, "tasse de poudre de protéine de vanille", 0.5*multiplicateur_arrondi, "tasse de beurre de cacahuète", 0.25*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nDans un grand bol, mélanger les flocons d'avoine et la poudre de protéine de vanille.\nÉTAPE 2\nDans une casserole, chauffer le beurre de cacahuète et le miel à feu doux jusqu'à ce qu'ils soient bien mélangés.\nÉTAPE 3\nVerser le mélange de beurre de cacahuète sur les flocons d'avoine et mélanger jusqu'à ce que tous les ingrédients soient bien combinés.\nÉTAPE 4\nIncorporer les pépites de chocolat.\nÉTAPE 5\nPresser le mélange dans un plat carré tapissé de papier parchemin et mettre au réfrigérateur pendant au moins une heure.\nÉTAPE 6\nDécouper en barres et servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_proteinees_beurre_cacahuete
def smoothie_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "épinards frais", "lait d'amande", "banane", "miel"]
    ingredient = [0.5*multiplicateur_arrondi, "avocat mûr", 1*multiplicateur_arrondi, "poignée d'épinards frais", 1*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "banane congelée", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_avocat_epinards
def wrap_thon_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortilla de blé complet", "thon en conserve", "mayonnaise légère", "carotte", "concombre", "poivron rouge"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé complet", 1*multiplicateur_arrondi, "boîte de thon en conserve", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 0.5*multiplicateur_arrondi, "carotte râpée", 0.5*multiplicateur_arrondi, "concombre en dés", 0.25*multiplicateur_arrondi, "poivron rouge en dés"]
    preparation = ["ÉTAPE 1\nÉtaler la mayonnaise légère sur la tortilla de blé complet.\nÉTAPE 2\nDisposer le thon égoutté sur la tortilla.\nÉTAPE 3\nAjouter les carottes râpées, les dés de concombre et de poivron rouge sur le thon.\nÉTAPE 4\nRouler la tortilla fermement.\nÉTAPE 5\nCouper en deux et servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_thon_legumes
def muffins_proteines_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'amande", "poudre de protéine de vanille", "levure chimique", "œufs", "yaourt grec", "miel", "myrtilles fraîches"]
    ingredient = [1*multiplicateur_arrondi, "tasse de farine d'amande", 0.5*multiplicateur_arrondi, "tasse de poudre de protéine de vanille", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", 2*multiplicateur_arrondi, "œufs", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 0.25*multiplicateur_arrondi, "tasse de miel", 0.5*multiplicateur_arrondi, "tasse de myrtilles fraîches"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et préparer un moule à muffins avec des caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la farine d'amande, la poudre de protéine de vanille et la levure chimique.\nÉTAPE 3\nDans un autre bol, battre les œufs, puis ajouter le yaourt grec et le miel et bien mélanger.\nÉTAPE 4\nIncorporer les ingrédients liquides dans les ingrédients secs et remuer jusqu'à obtenir une pâte homogène.\nÉTAPE 5\nAjouter les myrtilles fraîches et mélanger délicatement.\nÉTAPE 6\nRépartir la pâte dans les moules à muffins.\nÉTAPE 7\nCuire au four pendant environ 20-25 minutes, ou jusqu'à ce qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 8\nLaisser refroidir avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_proteines_myrtilles
def bol_smoothie_vanille_fruits_rouges(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "fraise", "myrtilles", "lait d'amande", "poudre de protéine de vanille", "graines de chia", "granola"]
    ingredient = [1*multiplicateur_arrondi, "banane congelée", 1*multiplicateur_arrondi, "tasse de fraises congelées", 0.5*multiplicateur_arrondi, "tasse de myrtilles congelées", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "scoop de poudre de protéine de vanille", 1*multiplicateur_arrondi, "cuillère à soupe de graines de chia", 0.25*multiplicateur_arrondi, "tasse de granola"]
    preparation = ["ÉTAPE 1\nMettre les bananes, les fraises, les myrtilles, le lait d'amande et la poudre de protéine de vanille dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser le smoothie dans un bol.\nÉTAPE 4\nGarnir de graines de chia et de granola.\nÉTAPE 5\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

bol_smoothie_vanille_fruits_rouges
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "aubergine", "courgette", "tomates cerises", "huile d'olive", "vinaigre de cidre", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa cuit", 0.5*multiplicateur_arrondi, "poivron rouge coupé en dés", 0.5*multiplicateur_arrondi, "aubergine coupée en dés", 0.5*multiplicateur_arrondi, "courgette coupée en dés", 0.25*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nFaire cuire le quinoa selon les instructions sur l'emballage et laisser refroidir.\nÉTAPE 2\nPréchauffer le gril à feu moyen-élevé.\nÉTAPE 3\nDans un bol, mélanger les dés de poivron rouge, d'aubergine et de courgette avec l'huile d'olive, le vinaigre de cidre, le sel et le poivre.\nÉTAPE 4\nPlacer les légumes sur une plaque de cuisson et griller pendant 8-10 minutes, en remuant de temps en temps, jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 5\nDans un grand bol, mélanger le quinoa cuit avec les légumes grillés et les tomates cerises.\nÉTAPE 6\nRectifier l'assaisonnement selon votre goût.\nÉTAPE 7\nServir chaud ou froid."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def smoothie_ananas_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["ananas", "lait de coco", "yaourt grec", "miel", "noix de coco râpée"]
    ingredient = [1*multiplicateur_arrondi, "tasse d'ananas frais coupé en dés", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de noix de coco râpée"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait de coco si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_ananas_noix_coco
def oeufs_brouilles_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "brocoli", "poivron rouge", "oignon", "ail", "huile d'olive", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "œufs", 0.5*multiplicateur_arrondi, "tasse de brocoli coupé en petits bouquets", 0.25*multiplicateur_arrondi, "tasse de poivron rouge coupé en dés", 0.25*multiplicateur_arrondi, "oignon moyen haché", 1*multiplicateur_arrondi, "gousse d'ail émincée", 1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nChauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2\nAjouter l'oignon et l'ail et faire revenir jusqu'à ce qu'ils soient translucides.\nÉTAPE 3\nAjouter le brocoli et le poivron rouge et faire cuire jusqu'à ce qu'ils soient tendres.\nÉTAPE 4\nBattre les œufs dans un bol et les verser dans la poêle avec les légumes.\nÉTAPE 5\nRemuer doucement les œufs jusqu'à ce qu'ils soient brouillés et cuits à votre goût.\nÉTAPE 6\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 7\nServir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

oeufs_brouilles_legumes
def wrap_haricots_noirs_salsa(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortilla de blé", "haricots noirs", "maïs", "tomate", "oignon rouge", "coriandre fraîche", "jus de citron vert", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé", 0.5*multiplicateur_arrondi, "tasse de haricots noirs cuits", 0.25*multiplicateur_arrondi, "tasse de maïs en conserve égoutté", 0.25*multiplicateur_arrondi, "tasse de tomate coupée en dés", 0.25*multiplicateur_arrondi, "tasse d'oignon rouge haché", 1*multiplicateur_arrondi, "cuillère à soupe de coriandre fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron vert", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un bol, mélanger les haricots noirs, le maïs, la tomate, l'oignon rouge, la coriandre, le jus de citron vert, le sel et le poivre pour faire la salsa.\nÉTAPE 2\nRéchauffer les tortillas de blé selon les instructions sur l'emballage.\nÉTAPE 3\nÉtaler une portion de la salsa sur chaque tortilla.\nÉTAPE 4\nRouler les tortillas en wrap et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_haricots_noirs_salsa
def smoothie_mangue_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "banane", "lait d'amande", "yaourt grec", "miel", "graines de chia", "granola"]
    ingredient = [1*multiplicateur_arrondi, "mangue mûre coupée en dés", 1*multiplicateur_arrondi, "banane congelée", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de graines de chia", 0.25*multiplicateur_arrondi, "tasse de granola"]
    preparation = ["ÉTAPE 1\nMettre la mangue, la banane, le lait d'amande, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser le smoothie dans un bol.\nÉTAPE 4\nGarnir de graines de chia et de granola.\nÉTAPE 5\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_banane
def wraps_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortilla de blé", "poitrine de poulet", "avocat", "tomate", "feuilles de laitue", "mayonnaise légère", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortilla de blé", 0.5*multiplicateur_arrondi, "poitrine de poulet cuite et coupée en lanières", 1*multiplicateur_arrondi, "avocat mûr, tranché", 0.25*multiplicateur_arrondi, "tomate coupée en dés", 2*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 0.5*multiplicateur_arrondi, "cuillère à soupe de jus de citron", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nChauffer les tortillas de blé selon les instructions sur l'emballage.\nÉTAPE 2\nTartiner chaque tortilla avec de la mayonnaise légère.\nÉTAPE 3\nDisposer les lanières de poulet, les tranches d'avocat, les dés de tomate et les feuilles de laitue sur chaque tortilla.\nÉTAPE 4\nArroser de jus de citron et assaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 5\nEnrouler les tortillas fermement et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_poulet_avocat
def bol_smoothie_fraise_pastèque(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "pastèque", "lait de coco", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de fraises fraîches coupées en dés", 0.5*multiplicateur_arrondi, "tasse de pastèque coupée en dés", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre les fraises, la pastèque, le lait de coco, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser le smoothie dans un bol.\nÉTAPE 4\nGarnir de fruits frais supplémentaires si désiré.\nÉTAPE 5\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

bol_smoothie_fraise_pastèque
def smoothie_banane_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "beurre d'amande", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane congelée", 1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre la banane, le beurre d'amande, le lait d'amande, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_banane_beurre_amande
def mini_pizzas_thon_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["pâte à pizza", "thon en conserve", "tomate", "poivron rouge", "oignon rouge", "fromage râpé", "origan séché", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "pâte à pizza pré-cuite", 0.25*multiplicateur_arrondi, "boîte de thon en conserve égoutté", 0.25*multiplicateur_arrondi, "tomate coupée en tranches", 0.25*multiplicateur_arrondi, "poivron rouge coupé en dés", 0.25*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 0.25*multiplicateur_arrondi, "tasse de fromage râpé", 1*multiplicateur_arrondi, "cuillère à café d'origan séché", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C et préparer une plaque à pâtisserie recouverte de papier sulfurisé.\nÉTAPE 2\nÉtaler la pâte à pizza sur la plaque à pâtisserie.\nÉTAPE 3\nRépartir le thon émietté, les tranches de tomate, les dés de poivron rouge et les rondelles d'oignon rouge sur la pâte.\nÉTAPE 4\nSaupoudrer de fromage râpé et d'origan séché.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nCuire au four pendant environ 12-15 minutes, ou jusqu'à ce que la croûte soit dorée et croustillante.\nÉTAPE 7\nCouper en petits morceaux et servir chaud."]
    temps_de_preparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

mini_pizzas_thon_legumes
def barres_proteinees_amandes_dattes(multiplicateur_arrondi):
    ingredient_pour_algo = ["amandes", "dattes", "flocons d'avoine", "protéine en poudre à la vanille", "miel", "huile de coco"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse d'amandes", 0.5*multiplicateur_arrondi, "tasse de dattes dénoyautées", 0.5*multiplicateur_arrondi, "tasse de flocons d'avoine", 0.25*multiplicateur_arrondi, "tasse de protéine en poudre à la vanille", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco"]
    preparation = ["ÉTAPE 1\nMettre les amandes, les dattes, les flocons d'avoine, la protéine en poudre, le miel et l'huile de coco dans un robot culinaire.\nÉTAPE 2\nMixer jusqu'à ce que le mélange soit bien combiné et qu'il forme une pâte collante.\nÉTAPE 3\nPresser fermement le mélange dans un plat rectangulaire tapissé de papier sulfurisé.\nÉTAPE 4\nRéfrigérer pendant au moins 1 heure, puis couper en barres.\nÉTAPE 5\nConserver les barres au réfrigérateur jusqu'au moment de les servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_proteinees_amandes_dattes
def smoothie_proteine_vanille_framboises(multiplicateur_arrondi):
    ingredient_pour_algo = ["protéine en poudre à la vanille", "framboises surgelées", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "scoop de protéine en poudre à la vanille", 1*multiplicateur_arrondi, "tasse de framboises surgelées", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre la protéine en poudre à la vanille, les framboises surgelées, le lait d'amande, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_proteine_vanille_framboises
def muffins_patate_douce_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce", "farine d'amande", "œufs", "miel", "huile de coco", "levure chimique", "cannelle", "pépites de chocolat noir"]
    ingredient = [1*multiplicateur_arrondi, "patate douce cuite et écrasée", 1*multiplicateur_arrondi, "tasse de farine d'amande", 2*multiplicateur_arrondi, "œufs", 0.25*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse d'huile de coco fondue", 1*multiplicateur_arrondi, "cuillère à café de levure chimique", 1*multiplicateur_arrondi, "cuillère à café de cannelle", 0.5*multiplicateur_arrondi, "tasse de pépites de chocolat noir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et préparer un moule à muffins avec des caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la patate douce écrasée, la farine d'amande, les œufs, le miel, l'huile de coco fondue, la levure chimique et la cannelle jusqu'à obtenir une pâte homogène.\nÉTAPE 3\nIncorporer délicatement les pépites de chocolat noir dans la pâte.\nÉTAPE 4\nRépartir la pâte dans les caissettes à muffins.\nÉTAPE 5\nCuire au four pendant environ 20-25 minutes, ou jusqu'à ce qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 6\nLaisser refroidir avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_patate_douce_chocolat
def sandwich_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain de blé entier", "poitrine de poulet", "avocat", "tomate", "feuilles de laitue", "mayonnaise légère", "moutarde de Dijon", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain de blé entier", 0.5*multiplicateur_arrondi, "poitrine de poulet grillée et tranchée", 0.5*multiplicateur_arrondi, "avocat mûr, tranché", 0.25*multiplicateur_arrondi, "tomate coupée en tranches", 2*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nTartiner une tranche de pain de blé entier avec de la mayonnaise légère et l'autre tranche avec de la moutarde de Dijon.\nÉTAPE 2\nDisposer les tranches de poulet grillé sur une des tranches de pain.\nÉTAPE 3\nAjouter les tranches d'avocat, de tomate et les feuilles de laitue sur le poulet.\nÉTAPE 4\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 5\nCouvrir avec l'autre tranche de pain.\nÉTAPE 6\nCouper en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

sandwich_poulet_avocat
def smoothie_myrtilles_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles surgelées", "protéine en poudre à la vanille", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de myrtilles surgelées", 1*multiplicateur_arrondi, "scoop de protéine en poudre à la vanille", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre les myrtilles surgelées, la protéine en poudre à la vanille, le lait d'amande, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_myrtilles_vanille
def barres_cereales_noix_fruits_secs(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "noix de cajou", "amandes", "dattes", "miel", "huile de coco", "cannelle", "sel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de flocons d'avoine", 0.25*multiplicateur_arrondi, "tasse de noix de cajou", 0.25*multiplicateur_arrondi, "tasse d'amandes", 0.5*multiplicateur_arrondi, "tasse de dattes dénoyautées", 0.25*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse d'huile de coco fondue", 1*multiplicateur_arrondi, "cuillère à café de cannelle", "pincée de sel"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et préparer un plat rectangulaire tapissé de papier sulfurisé.\nÉTAPE 2\nDans un robot culinaire, mélanger les flocons d'avoine, les noix de cajou, les amandes, les dattes, le miel, l'huile de coco, la cannelle et une pincée de sel jusqu'à obtenir une pâte collante.\nÉTAPE 3\nPresser fermement le mélange dans le plat préparé.\nÉTAPE 4\nCuire au four pendant environ 20-25 minutes, ou jusqu'à ce que les barres soient dorées sur les bords.\nÉTAPE 5\nLaisser refroidir avant de couper en barres."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_cereales_noix_fruits_secs
def salade_quinoa_legumes_grilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "poivron rouge", "aubergine", "courgette", "oignon rouge", "tomates cerises", "huile d'olive", "jus de citron", "sel", "poivre", "feuilles de basilic frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 0.5*multiplicateur_arrondi, "poivron rouge coupé en dés", 0.5*multiplicateur_arrondi, "aubergine coupée en dés", 0.5*multiplicateur_arrondi, "courgette coupée en dés", 0.25*multiplicateur_arrondi, "oignon rouge coupé en dés", 0.25*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 2*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", "sel", "poivre", "feuilles de basilic frais pour garnir"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, puis ajouter le quinoa.\nÉTAPE 3\nRéduire le feu à doux, couvrir et laisser mijoter pendant 15-20 minutes, ou jusqu'à ce que le quinoa soit tendre et que toute l'eau soit absorbée.\nÉTAPE 4\nPendant ce temps, préparer les légumes en les coupant en dés et en les assaisonnant avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 5\nFaire griller les légumes au barbecue ou dans une poêle grillée jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nDans un grand bol, mélanger le quinoa cuit et les légumes grillés.\nÉTAPE 7\nAjouter le jus de citron et ajuster l'assaisonnement avec du sel et du poivre si nécessaire.\nÉTAPE 8\nGarnir de feuilles de basilic frais avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_grilles
def smoothie_mangue_noix_de_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "yaourt grec", "miel", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "mangue coupée en dés", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    preparation = ["ÉTAPE 1\nMettre la mangue, le lait de coco, le yaourt grec, le miel et le jus de citron dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait de coco si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_de_coco
def smoothie_mangue_noix_de_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "yaourt grec", "miel", "jus de citron"]
    ingredient = [1*multiplicateur_arrondi, "mangue coupée en dés", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron"]
    preparation = ["ÉTAPE 1\nMettre la mangue, le lait de coco, le yaourt grec, le miel et le jus de citron dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait de coco si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_de_coco
def barres_energetiques_noix_miel(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "noix de pécan", "amandes", "miel", "huile de coco", "extrait de vanille", "sel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de flocons d'avoine", 0.25*multiplicateur_arrondi, "tasse de noix de pécan hachées", 0.25*multiplicateur_arrondi, "tasse d'amandes hachées", 0.25*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse d'huile de coco fondue", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille", "pincée de sel"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et préparer un plat rectangulaire tapissé de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les flocons d'avoine, les noix de pécan, les amandes, le miel, l'huile de coco fondue, l'extrait de vanille et une pincée de sel jusqu'à obtenir une pâte collante.\nÉTAPE 3\nPresser fermement le mélange dans le plat préparé.\nÉTAPE 4\nCuire au four pendant environ 20-25 minutes, ou jusqu'à ce que les barres soient dorées sur les bords.\nÉTAPE 5\nLaisser refroidir avant de couper en barres."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_energetiques_noix_miel
def wrap_crevettes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "crevettes cuites", "avocat", "tomate", "oignon rouge", "laitue", "mayonnaise légère", "jus de citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 0.25*multiplicateur_arrondi, "tasse de crevettes cuites décortiquées", 0.5*multiplicateur_arrondi, "avocat mûr, tranché", 0.25*multiplicateur_arrondi, "tasse de tomate coupée en dés", 0.25*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 2*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nÉtaler une tortilla de blé entier sur une surface de travail propre.\nÉTAPE 2\nDisposer les crevettes cuites, les tranches d'avocat, les dés de tomate, les rondelles d'oignon rouge et les feuilles de laitue sur la tortilla.\nÉTAPE 3\nArroser de jus de citron et assaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 4\nEnrouler fermement la tortilla pour former un wrap.\nÉTAPE 5\nCouper en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_crevettes_avocat
def smoothie_fraises_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises surgelées", "banane", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de fraises surgelées", 1*multiplicateur_arrondi, "banane coupée en tranches et congelée", 0.5*multiplicateur_arrondi, "tasse de lait d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nMettre les fraises surgelées, la banane congelée, le lait d'amande, le yaourt grec et le miel dans un blender.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_fraises_banane
def salade_lentilles_legumes_rotis(multiplicateur_arrondi):
    ingredient_pour_algo = ["lentilles vertes", "carottes", "betteraves", "poivron rouge", "oignon rouge", "ail", "huile d'olive", "vinaigre de cidre", "moutarde de Dijon", "sel", "poivre", "persil frais"]
    ingredient = [1*multiplicateur_arrondi, "tasse de lentilles vertes", 0.5*multiplicateur_arrondi, "tasse de carottes coupées en dés", 0.5*multiplicateur_arrondi, "tasse de betteraves coupées en dés", 0.25*multiplicateur_arrondi, "tasse de poivron rouge coupé en dés", 0.25*multiplicateur_arrondi, "tasse d'oignon rouge coupé en dés", 1*multiplicateur_arrondi, "gousse d'ail émincée", 2*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive", 1*multiplicateur_arrondi, "cuillère à soupe de vinaigre de cidre", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", "sel", "poivre", "persil frais pour garnir"]
    preparation = ["ÉTAPE 1\nRincer les lentilles à l'eau froide dans une passoire.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, puis ajouter les lentilles.\nÉTAPE 3\nRéduire le feu à doux, couvrir et laisser mijoter pendant 15-20 minutes, ou jusqu'à ce que les lentilles soient tendres mais encore légèrement croquantes.\nÉTAPE 4\nPendant ce temps, préparer les légumes en les coupant en dés et en les assaisonnant avec de l'huile d'olive, du sel et du poivre.\nÉTAPE 5\nFaire rôtir les légumes au four préchauffé à 200°C pendant environ 20-25 minutes, ou jusqu'à ce qu'ils soient tendres et légèrement dorés.\nÉTAPE 6\nDans un grand bol, mélanger les lentilles cuites et les légumes rôtis.\nÉTAPE 7\nDans un petit bol, fouetter ensemble l'ail émincé, l'huile d'olive, le vinaigre de cidre, la moutarde de Dijon, du sel et du poivre pour préparer la vinaigrette.\nÉTAPE 8\nVerser la vinaigrette sur la salade de lentilles et mélanger pour bien enrober.\nÉTAPE 9\nGarnir de persil frais avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 45
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_lentilles_legumes_rotis
def smoothie_proteine_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "beurre d'amande", "protéine en poudre à la vanille", "lait d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane mûre", 2*multiplicateur_arrondi, "cuillères à soupe de beurre d'amande", 1*multiplicateur_arrondi, "scoop de protéine en poudre à la vanille", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nPeler la banane et la couper en morceaux.\nÉTAPE 2\nDans un blender, mélanger les morceaux de banane, le beurre d'amande, la protéine en poudre, le lait d'amande et le miel.\nÉTAPE 3\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 4\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_proteine_beurre_amande
def muffins_pepites_chocolat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine tout usage", "levure chimique", "sel", "bananes mûres", "œufs", "sucre", "beurre fondu", "lait", "extrait de vanille", "pépites de chocolat"]
    ingredient = [1.5*multiplicateur_arrondi, "tasses de farine tout usage", 1*multiplicateur_arrondi, "cuillère à soupe de levure chimique", 0.5*multiplicateur_arrondi, "cuillère à café de sel", 1.5*multiplicateur_arrondi, "bananes mûres écrasées", 2*multiplicateur_arrondi, "œufs battus", 1*multiplicateur_arrondi, "tasse de sucre", 0.5*multiplicateur_arrondi, "tasse de beurre fondu", 0.5*multiplicateur_arrondi, "tasse de lait", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille", 1*multiplicateur_arrondi, "tasse de pépites de chocolat"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 200°C et graisser un moule à muffins ou le garnir de caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la farine, la levure chimique et le sel.\nÉTAPE 3\nDans un autre bol, mélanger les bananes écrasées, les œufs battus, le sucre, le beurre fondu, le lait et l'extrait de vanille.\nÉTAPE 4\nIncorporer les ingrédients liquides aux ingrédients secs et mélanger jusqu'à ce que la pâte soit homogène.\nÉTAPE 5\nAjouter les pépites de chocolat et mélanger délicatement.\nÉTAPE 6\nRépartir la pâte dans les moules à muffins.\nÉTAPE 7\nCuire au four préchauffé pendant environ 20 minutes, ou jusqu'à ce que les muffins soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 8\nLaisser refroidir avant de déguster."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_pepites_chocolat_banane
def wrap_poulet_grille_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "poitrine de poulet", "poivron rouge", "poivron jaune", "oignon rouge", "laitue", "guacamole", "sauce piquante"]
    ingredient = [2*multiplicateur_arrondi, "tortillas de blé entier", 1*multiplicateur_arrondi, "poitrine de poulet cuite et tranchée", 0.5*multiplicateur_arrondi, "poivron rouge coupé en lanières", 0.5*multiplicateur_arrondi, "poivron jaune coupé en lanières", 0.25*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 2*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de guacamole", 1*multiplicateur_arrondi, "cuillère à soupe de sauce piquante"]
    preparation = ["ÉTAPE 1\nÉtaler une tortilla de blé entier sur une surface de travail propre.\nÉTAPE 2\nDisposer les tranches de poulet, les lanières de poivron rouge et jaune, les rondelles d'oignon rouge et les feuilles de laitue sur la tortilla.\nÉTAPE 3\nAjouter une cuillerée de guacamole et de sauce piquante selon votre goût.\nÉTAPE 4\nEnrouler fermement la tortilla pour former un wrap.\nÉTAPE 5\nCouper en deux et servir immédiatement."]
    temps_de_preparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_poulet_grille_legumes
def smoothie_myrtilles_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles surgelées", "banane", "protéine en poudre à la vanille", "lait d'amande"]
    ingredient = [1*multiplicateur_arrondi, "tasse de myrtilles surgelées", 1*multiplicateur_arrondi, "banane mûre", 1*multiplicateur_arrondi, "scoop de protéine en poudre à la vanille", 1*multiplicateur_arrondi, "tasse de lait d'amande"]
    preparation = ["ÉTAPE 1\nDans un blender, ajouter les myrtilles surgelées, la banane, la protéine en poudre à la vanille et le lait d'amande.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_myrtilles_vanille
def barres_cereales_fruits_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "amandes", "noix de cajou", "abricots secs", "raisins secs", "miel", "beurre d'amande", "vanille"]
    ingredient = [1.5*multiplicateur_arrondi, "tasses de flocons d'avoine", 0.5*multiplicateur_arrondi, "tasse d'amandes hachées", 0.5*multiplicateur_arrondi, "tasse de noix de cajou hachées", 0.5*multiplicateur_arrondi, "tasse d'abricots secs hachés", 0.5*multiplicateur_arrondi, "tasse de raisins secs", 0.5*multiplicateur_arrondi, "tasse de miel", 0.5*multiplicateur_arrondi, "tasse de beurre d'amande", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et tapisser un moule carré de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les flocons d'avoine, les amandes, les noix de cajou, les abricots secs et les raisins secs.\nÉTAPE 3\nDans une casserole, chauffer le miel et le beurre d'amande à feu doux jusqu'à ce que le mélange soit homogène.\nÉTAPE 4\nRetirer du feu et ajouter l'extrait de vanille.\nÉTAPE 5\nVerser le mélange de miel et de beurre d'amande sur les ingrédients secs et mélanger jusqu'à ce que tout soit bien enrobé.\nÉTAPE 6\nPresser fermement le mélange dans le moule préparé.\nÉTAPE 7\nCuire au four pendant environ 20-25 minutes, ou jusqu'à ce que les barres soient dorées sur le dessus.\nÉTAPE 8\nLaisser refroidir complètement avant de couper en barres."]
    temps_de_preparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_cereales_fruits_noix
def salade_quinoa_legumes_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "concombre", "tomates cerises", "poivron rouge", "oignon rouge", "persil frais", "menthe fraîche", "jus de citron", "huile d'olive", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tasse de quinoa", 1*multiplicateur_arrondi, "concombre coupé en dés", 0.5*multiplicateur_arrondi, "tasse de tomates cerises coupées en deux", 0.5*multiplicateur_arrondi, "tasse de poivron rouge coupé en dés", 0.25*multiplicateur_arrondi, "tasse d'oignon rouge coupé en dés", 2*multiplicateur_arrondi, "cuillères à soupe de persil frais haché", 2*multiplicateur_arrondi, "cuillères à soupe de menthe fraîche hachée", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire.\nÉTAPE 2\nDans une casserole, porter 2 tasses d'eau à ébullition, puis ajouter le quinoa.\nÉTAPE 3\nRéduire le feu à doux, couvrir et laisser mijoter pendant 15 minutes, ou jusqu'à ce que le quinoa soit tendre et que toute l'eau soit absorbée.\nÉTAPE 4\nTransférer le quinoa cuit dans un grand bol et laisser refroidir légèrement.\nÉTAPE 5\nAjouter le concombre, les tomates cerises, le poivron rouge, l'oignon rouge, le persil frais et la menthe fraîche au quinoa.\nÉTAPE 6\nDans un petit bol, fouetter ensemble le jus de citron, l'huile d'olive, du sel et du poivre pour préparer la vinaigrette.\nÉTAPE 7\nVerser la vinaigrette sur la salade de quinoa et mélanger pour bien enrober.\nÉTAPE 8\nRéfrigérer pendant au moins 30 minutes avant de servir pour permettre aux saveurs de se marier."]
    temps_de_preparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

salade_quinoa_legumes_frais
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "banane", "yaourt à la noix de coco", "lait de coco", "miel"]
    ingredient = [1*multiplicateur_arrondi, "mangue pelée et coupée en dés", 1*multiplicateur_arrondi, "banane mûre", 1*multiplicateur_arrondi, "yaourt à la noix de coco", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, ajouter les dés de mangue, la banane, le yaourt à la noix de coco, le lait de coco et le miel.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_coco
def barres_energetiques_dattes_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["dattes", "amandes", "noix de cajou", "cacao en poudre", "huile de coco", "sel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de dattes dénoyautées", 0.5*multiplicateur_arrondi, "tasse d'amandes", 0.5*multiplicateur_arrondi, "tasse de noix de cajou", 2*multiplicateur_arrondi, "cuillères à soupe de cacao en poudre", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco fondue", "pincée de sel"]
    preparation = ["ÉTAPE 1\nDans un robot culinaire, mixer les dattes, les amandes, les noix de cajou, le cacao en poudre, l'huile de coco et le sel jusqu'à ce que le mélange forme une pâte collante.\nÉTAPE 2\nTapisser un moule carré de papier sulfurisé et y presser fermement le mélange de dattes et de noix.\nÉTAPE 3\nRéfrigérer pendant au moins une heure, puis couper en barres avant de servir."]
    temps_de_preparation = 15
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_energetiques_dattes_noix
def poulet_grille_legumes_riz_basmati(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "poivron rouge", "poivron jaune", "oignon rouge", "ail", "huile d'olive", "sel", "poivre", "paprika", "riz basmati"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "poivron jaune coupé en lanières", 0.5*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre", "paprika", 1*multiplicateur_arrondi, "tasse de riz basmati cuit"]
    preparation = ["ÉTAPE 1\nPréchauffer le grill à feu moyen-élevé.\nÉTAPE 2\nDans un bol, mélanger les poitrines de poulet avec l'huile d'olive, le sel, le poivre et le paprika.\nÉTAPE 3\nGriller les poitrines de poulet pendant environ 6-8 minutes de chaque côté, ou jusqu'à ce qu'elles soient bien cuites et dorées.\nÉTAPE 4\nPendant ce temps, faire chauffer une poêle à feu moyen et y faire revenir les poivrons, l'oignon rouge et l'ail dans un peu d'huile d'olive jusqu'à ce qu'ils soient tendres.\nÉTAPE 5\nServir le poulet grillé avec les légumes sautés et le riz basmati cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_grille_legumes_riz_basmati
def smoothie_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "épinards frais", "banane", "jus de citron", "lait d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "avocat mûr, pelé et dénoyauté", 1*multiplicateur_arrondi, "tasse d'épinards frais", 1*multiplicateur_arrondi, "banane mûre", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, mélanger l'avocat, les épinards, la banane, le jus de citron, le lait d'amande et le miel.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_avocat_epinards
def barres_proteinees_noix_pecan_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de pécan", "amandes", "flocons d'avoine", "protéine en poudre à la vanille", "miel", "huile de coco", "cannelle"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de noix de pécan hachées", 0.5*multiplicateur_arrondi, "tasse d'amandes hachées", 1*multiplicateur_arrondi, "tasse de flocons d'avoine", 1*multiplicateur_arrondi, "scoop de protéine en poudre à la vanille", 0.5*multiplicateur_arrondi, "tasse de miel", 1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco fondue", 1*multiplicateur_arrondi, "cuillère à café de cannelle"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et tapisser un moule carré de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les noix de pécan, les amandes, les flocons d'avoine, la protéine en poudre, le miel, l'huile de coco fondue et la cannelle jusqu'à ce que le mélange soit bien combiné.\nÉTAPE 3\nPresser fermement le mélange dans le moule préparé.\nÉTAPE 4\nCuire au four préchauffé pendant environ 15-20 minutes, ou jusqu'à ce que les barres soient dorées sur le dessus.\nÉTAPE 5\nLaisser refroidir complètement avant de couper en barres."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_proteinees_noix_pecan_cannelle
def poulet_legumes_grilles_quinoa(multiplicateur_arrondi):
    ingredient_pour_algo = ["poitrines de poulet", "courgette", "aubergine", "poivron rouge", "oignon rouge", "ail", "huile d'olive", "sel", "poivre", "quinoa"]
    ingredient = [2*multiplicateur_arrondi, "poitrines de poulet désossées et sans peau", 1*multiplicateur_arrondi, "courgette coupée en rondelles", 1*multiplicateur_arrondi, "aubergine coupée en rondelles", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 0.5*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 2*multiplicateur_arrondi, "gousses d'ail émincées", 2*multiplicateur_arrondi, "cuillères à soupe d'huile d'olive", "sel", "poivre", 1*multiplicateur_arrondi, "tasse de quinoa cuit"]
    preparation = ["ÉTAPE 1\nPréchauffer le grill à feu moyen-élevé.\nÉTAPE 2\nDans un bol, mélanger les poitrines de poulet avec l'huile d'olive, le sel et le poivre.\nÉTAPE 3\nGriller les poitrines de poulet pendant environ 6-8 minutes de chaque côté, ou jusqu'à ce qu'elles soient bien cuites et dorées.\nÉTAPE 4\nPendant ce temps, badigeonner les rondelles de courgette, d'aubergine, de poivron rouge et d'oignon rouge d'huile d'olive et les faire griller jusqu'à ce qu'elles soient tendres et légèrement dorées.\nÉTAPE 5\nServir le poulet grillé avec les légumes grillés et le quinoa cuit."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poulet_legumes_grilles_quinoa
def muffins_proteines_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'amande", "poudre protéinée au chocolat", "bicarbonate de soude", "sel", "œufs", "banane mûre", "yaourt grec", "huile de coco fondue", "miel", "vanille", "pépites de chocolat noir"]
    ingredient = [1*multiplicateur_arrondi, "tasse de farine d'amande", 0.5*multiplicateur_arrondi, "tasse de poudre protéinée au chocolat", 0.5*multiplicateur_arrondi, "cuillère à café de bicarbonate de soude", "pincée de sel", 2*multiplicateur_arrondi, "œufs", 1*multiplicateur_arrondi, "banane mûre écrasée", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 0.25*multiplicateur_arrondi, "tasse d'huile de coco fondue", 0.25*multiplicateur_arrondi, "tasse de miel", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille", 0.5*multiplicateur_arrondi, "tasse de pépites de chocolat noir"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et tapisser un moule à muffins de caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la farine d'amande, la poudre protéinée, le bicarbonate de soude et le sel.\nÉTAPE 3\nDans un autre bol, battre les œufs, puis ajouter la banane écrasée, le yaourt grec, l'huile de coco fondue, le miel et la vanille.\nÉTAPE 4\nVerser le mélange liquide dans le mélange sec et mélanger jusqu'à ce que tout soit bien incorporé.\nÉTAPE 5\nAjouter les pépites de chocolat et mélanger à nouveau.\nÉTAPE 6\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 7\nCuire au four préchauffé pendant environ 20 minutes, ou jusqu'à ce qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 8\nLaisser refroidir dans le moule pendant quelques minutes, puis transférer sur une grille pour refroidir complètement."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffins_proteines_chocolat
def smoothie_framboise_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["framboises surgelées", "amandes", "banane", "lait d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de framboises surgelées", 0.25*multiplicateur_arrondi, "tasse d'amandes", 1*multiplicateur_arrondi, "banane mûre", 1*multiplicateur_arrondi, "tasse de lait d'amande", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, mélanger les framboises, les amandes, la banane, le lait d'amande et le miel.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_framboise_amande
def wraps_dinde_legumes_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "poitrines de dinde", "poivron rouge", "concombre", "avocat", "laitue", "mayonnaise légère", "moutarde de Dijon", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortillas de blé entier", 2*multiplicateur_arrondi, "poitrines de dinde cuites et tranchées", 1*multiplicateur_arrondi, "poivron rouge coupé en lanières", 1*multiplicateur_arrondi, "concombre coupé en lanières", 1*multiplicateur_arrondi, "avocat mûr, tranché", 2*multiplicateur_arrondi, "feuilles de laitue", 1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise légère", 1*multiplicateur_arrondi, "cuillère à soupe de moutarde de Dijon", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un petit bol, mélanger la mayonnaise légère, la moutarde de Dijon, le jus de citron, du sel et du poivre pour préparer la sauce.\nÉTAPE 2\nÉtaler chaque tortilla avec une couche de sauce préparée.\nÉTAPE 3\nDisposer les tranches de dinde sur les tortillas, puis répartir les lanières de poivron rouge, de concombre, les tranches d'avocat et les feuilles de laitue.\nÉTAPE 4\nRouler chaque tortilla fermement pour former un wrap.\nÉTAPE 5\nCouper chaque wrap en deux avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wraps_dinde_legumes_avocat
def smoothie_myrtilles_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles surgelées", "banane", "beurre d'amande", "yaourt grec", "lait d'amande"]
    ingredient = [1*multiplicateur_arrondi, "tasse de myrtilles surgelées", 1*multiplicateur_arrondi, "banane mûre", 1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 0.5*multiplicateur_arrondi, "tasse de lait d'amande"]
    preparation = ["ÉTAPE 1\nDans un blender, mélanger les myrtilles, la banane, le beurre d'amande, le yaourt grec et le lait d'amande.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_myrtilles_beurre_amande
def barres_energetiques_noix_macadamia_miel(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de macadamia", "amandes", "noix de coco râpée", "miel", "huile de coco", "vanille", "sel"]
    ingredient = [0.5*multiplicateur_arrondi, "tasse de noix de macadamia hachées", 0.5*multiplicateur_arrondi, "tasse d'amandes hachées", 0.25*multiplicateur_arrondi, "tasse de noix de coco râpée", 0.5*multiplicateur_arrondi, "tasse de miel", 0.25*multiplicateur_arrondi, "tasse d'huile de coco fondue", 1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille", "pincée de sel"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et tapisser un moule carré de papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les noix de macadamia, les amandes, la noix de coco râpée, le miel, l'huile de coco fondue, la vanille et le sel.\nÉTAPE 3\nPresser fermement le mélange dans le moule préparé.\nÉTAPE 4\nCuire au four préchauffé pendant environ 15-20 minutes, ou jusqu'à ce que les barres soient dorées sur le dessus.\nÉTAPE 5\nLaisser refroidir complètement avant de couper en barres."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

barres_energetiques_noix_macadamia_miel
def wrap_saumon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tortillas de blé entier", "saumon fumé", "avocat", "concombre", "oignon rouge", "crème fraîche légère", "aneth frais", "jus de citron", "sel", "poivre"]
    ingredient = [1*multiplicateur_arrondi, "tortillas de blé entier", 100*multiplicateur_arrondi, "g de saumon fumé", 1*multiplicateur_arrondi, "avocat mûr, tranché", 0.5*multiplicateur_arrondi, "concombre coupé en lanières", 0.25*multiplicateur_arrondi, "oignon rouge coupé en rondelles", 1*multiplicateur_arrondi, "cuillère à soupe de crème fraîche légère", 1*multiplicateur_arrondi, "cuillère à café d'aneth frais haché", 1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais", "sel", "poivre"]
    preparation = ["ÉTAPE 1\nDans un petit bol, mélanger la crème fraîche légère, l'aneth frais, le jus de citron, du sel et du poivre pour préparer la sauce.\nÉTAPE 2\nÉtaler chaque tortilla avec une couche de sauce préparée.\nÉTAPE 3\nDisposer le saumon fumé sur les tortillas, puis répartir les tranches d'avocat, les lanières de concombre et les rondelles d'oignon rouge.\nÉTAPE 4\nRouler chaque tortilla fermement pour former un wrap.\nÉTAPE 5\nCouper chaque wrap en deux avant de servir."]
    temps_de_preparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

wrap_saumon_avocat
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue congelée", "banane", "lait de coco", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "tasse de mangue congelée", 1*multiplicateur_arrondi, "banane mûre", 0.5*multiplicateur_arrondi, "tasse de lait de coco", 0.5*multiplicateur_arrondi, "tasse de yaourt grec", 1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    preparation = ["ÉTAPE 1\nDans un blender, mélanger la mangue, la banane, le lait de coco, le yaourt grec et le miel.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_mangue_noix_coco

tuplegouter_prise_de_mas=[avocat_ecrase_pain_complet,barre_proteinee,barres_cereales_fruits_noix,barres_cereales_noix_fruits_secs,barres_energetiques_dattes_noix,barres_energetiques_fruits_secs,barres_energetiques_noix_macadamia_miel,barres_energetiques_noix_miel,barres_noix_miel,barres_proteinees_amandes_dattes,barres_proteinees_beurre_cacahuete,barres_proteinees_maison,barres_proteinees_noix_pecan_cannelle,batonnets_celeri_beurre_arachide,bol_quinoa_fruits_secs_noix,bowl_quinoa_poulet_legumes,chips_patates_douces,concombre_houmous,flocons_avoine_proteines,fromage_blanc_baies,galettes_quinoa_legumes,laitue_thon_avocat,melange_noix_fruits_secs,mini_pizzas_thon_legumes,muffins_banane_chocolat,muffins_myrtilles_proteines,muffins_patate_douce_chocolat,muffins_pepites_chocolat_banane,muffins_proteines_chocolat,muffins_proteines_myrtilles,oeufs_brouilles_legumes,omelette_epinards_fromage,pain_complet_beurre_amande,pain_complet_beurre_arachide,pancakes_banane_noix,porridge_avoine_banane_cacahuete,poulet_grille_legumes_riz_basmati,poulet_legumes_grilles_quinoa,pudding_chia_vanille,riz_poulet_grille,salade_lentilles_legumes_rotis,salade_quinoa_legumes_frais,salade_quinoa_legumes_grilles,salade_quinoa_saumon,sandwich_poulet_avocat,sandwich_poulet_grille_legumes,smoothie_ananas_noix_coco,smoothie_avocat_epinards,smoothie_banane_beurre_amande,smoothie_banane_beurre_cacahuete,smoothie_chocolat_banane,smoothie_epinards_banane_arachide,smoothie_fraises_banane,smoothie_framboise_amande,smoothie_fruits_yaourt_grec,smoothie_mangue_banane,smoothie_mangue_noix_coco,smoothie_mangue_noix_de_coco,smoothie_myrtilles_beurre_amande,smoothie_myrtilles_vanille,smoothie_proteine_beurre_amande,smoothie_proteine_beurre_cacahuete_banane,smoothie_proteine_chocolat_banane,smoothie_proteine_vanille_framboises,toast_avocat_oeuf_poche,toast_avocat_saumon_fume,tofu_saute_legumes,wrap_crevettes_avocat,wrap_dinde_avocat,wrap_haricots_noirs_salsa,wraps_dinde_legumes_avocat]