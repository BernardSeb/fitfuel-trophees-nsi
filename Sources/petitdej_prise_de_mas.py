
def omelette_courgette_tomate (multiplicateur_arrondi):
    ingredient_pour_algo = ["huile de sésame","vinaigre","sel","poivre","poivre","tomate","courgette","oignon","oeufs","sauce soja"]
    ingredient = [1*multiplicateur_arrondi,"cuillère a café d'huile de sésame", 1*multiplicateur_arrondi," cuillère a soupe de vinaigre de framboise","sel","poivre",1*multiplicateur_arrondi," tomate",1*multiplicateur_arrondi," courgette",1*multiplicateur_arrondi," oignon moyen",2*multiplicateur_arrondi," oeufs",1*multiplicateur_arrondi," cuillére à soup de sauce soja"] 
    préparation=["ÉTAPE 1\n Dans une poêle anti-adhésive mélanger le vinaigre, la sauce et l'huile, puis faites chauffer doucement.\nÉTAPE 2 \nCouper tomate, courgette et oignon en petits dés.\nÉTAPE 3\nFaire revenir les dés de courgette et d'oignon dans la poêle.\nÉTAPE 4\nQuand ils sont un peu colorés ajouter les dés de tomate.\nÉTAPE 5\nPendant ce temps séparer les jaunes des blancs.\nÉTAPE 6\nSaler et poivrer les jaunes en les fouettant.\nÉTAPE 7\nMonter les blancs en neige et les incorporer aux jaunes toujours en soulevant avec une spatule en bois.\nÉTAPE 8\nVerser le mélange sur les légumes tout en mélangeant délicatement.\nÉTAPE 9\nLaisser cuire quelques minutes et mélanger à nouveau (2-3 fois en tout).\nÉTAPE 10\nServir aussitôt."]
    temps_de_préparation = 15
    temps_de_cuisson= 10
    temps_total = temps_de_cuisson+temps_de_préparation
    tous=[ingredient_pour_algo,ingredient,préparation,temps_de_préparation,temps_de_cuisson,temps_total]
    return tous
def omelette_legumes_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["huile d'olive", "sel", "poivre", "œufs", "lait", "tomate", "poivron", "oignon", "fromage râpé"]
    ingredient = [1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "poivre",3*multiplicateur_arrondi, "œufs", 1*multiplicateur_arrondi, "cuillère à soupe de lait",1*multiplicateur_arrondi, "tomate",1*multiplicateur_arrondi, "poivron",1*multiplicateur_arrondi, "oignon moyen",50*multiplicateur_arrondi, "g de fromage râpé"]
    préparation = ["ÉTAPE 1\n Battre les œufs avec le lait, saler et poivrer.\nÉTAPE 2 \nCouper la tomate, le poivron et l'oignon en petits dés.\nÉTAPE 3\nChauffer l'huile dans une poêle et y faire revenir les légumes.\nÉTAPE 4\nVerser le mélange d'œufs sur les légumes.\nÉTAPE 5\nParsemer de fromage râpé.\nÉTAPE 6\nCuire à feu moyen jusqu'à ce que les œufs soient pris.\nÉTAPE 7\nPlier l'omelette en deux et servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 8
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_proteine_fruits_yaourt(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "banane", "fraises", "protéine en poudre", "lait d'amande", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de yaourt grec",1*multiplicateur_arrondi, "banane",100*multiplicateur_arrondi, "g de fraises",1*multiplicateur_arrondi, "scoop de protéine en poudre",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Mettre tous les ingrédients dans un blender.\nÉTAPE 2 \nMixer jusqu'à l'obtention d'une consistance lisse et homogène.\nÉTAPE 3\nServir immédiatement."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_banane_avoine(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "banane", "œufs", "lait", "levure chimique", "miel", "huile pour la cuisson"]
    ingredient = [100*multiplicateur_arrondi, "g de flocons d'avoine",2*multiplicateur_arrondi, "bananes mûres", 2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait",1*multiplicateur_arrondi, "cuillère à café de levure chimique",2*multiplicateur_arrondi, "cuillères à soupe de miel","huile pour la cuisson"]
    préparation = ["ÉTAPE 1\n Mixer les flocons d'avoine pour obtenir une farine.\nÉTAPE 2 \nDans un bol, écraser les bananes et les mélanger avec les œufs battus, le lait, le miel et la levure.\nÉTAPE 3\nIncorporer la farine d'avoine et bien mélanger.\nÉTAPE 4\nChauffer une poêle légèrement huilée.\nÉTAPE 5\nVerser une petite louche de pâte pour chaque pancake.\nÉTAPE 6\nCuire jusqu'à ce que des bulles apparaissent, puis retourner pour cuire l'autre côté.\nÉTAPE 7\nServir chaud."]
    temps_de_préparation = 15
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bagel_saumon_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["bagel", "fromage frais", "tranches de saumon fumé", "câpres", "oignon rouge", "roquette"]
    ingredient = [1*multiplicateur_arrondi, "bagel, coupé en deux",30*multiplicateur_arrondi, "g de fromage frais",2*multiplicateur_arrondi, "tranches de saumon fumé",1*multiplicateur_arrondi, "cuillère à soupe de câpres",2*multiplicateur_arrondi, "tranches d'oignon rouge",1*multiplicateur_arrondi, "poignée de roquette"]
    préparation = ["ÉTAPE 1\n Toaster légèrement les deux moitiés du bagel.\nÉTAPE 2 \nÉtaler le fromage frais sur chaque moitié du bagel.\nÉTAPE 3\nAjouter sur une moitié les tranches de saumon fumé, les câpres, l'oignon rouge et la roquette.\nÉTAPE 4\nRecouvrir avec l'autre moitié du bagel et servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_fruits_secs_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait", "miel", "fruits secs", "noix mélangées", "cannelle"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",250*multiplicateur_arrondi, "ml de lait",1*multiplicateur_arrondi, "cuillère à soupe de miel",30*multiplicateur_arrondi, "g de fruits secs",15*multiplicateur_arrondi, "g de noix mélangées, hachées",1*multiplicateur_arrondi, "pincée de cannelle"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter à ébullition le lait et les flocons d'avoine.\nÉTAPE 2 \nRéduire le feu et laisser mijoter, en remuant de temps en temps, jusqu'à ce que le porridge épaississe.\nÉTAPE 3\nRetirer du feu et ajouter le miel, la cannelle, les fruits secs et les noix.\nÉTAPE 4\nServir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def sandwich_poulet_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "blanc de poulet", "avocat", "laitue", "tomate", "mayonnaise", "moutarde"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet", 1*multiplicateur_arrondi, "blanc de poulet grillé et tranché",1*multiplicateur_arrondi, "avocat, en tranches",2*multiplicateur_arrondi, "feuilles de laitue",2*multiplicateur_arrondi, "tranches de tomate",1*multiplicateur_arrondi, "cuillère à soupe de mayonnaise", 1*multiplicateur_arrondi, "cuillère à café de moutarde"]
    préparation = ["ÉTAPE 1\n Toaster les tranches de pain complet.\nÉTAPE 2 \nÉtaler la mayonnaise et la moutarde sur une tranche de pain.\nÉTAPE 3\nDisposer la laitue, les tranches de tomate, l'avocat et le blanc de poulet sur la tranche de pain.\nÉTAPE 4\nCouvrir avec l'autre tranche de pain. Servir immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_bowl_proteines_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande", "poudre de protéine", "banane", "baies mélangées", "graines de chia", "flocons d'avoine", "noix", "miel"]
    ingredient = [200*multiplicateur_arrondi, "ml de lait d'amande",30*multiplicateur_arrondi, "g de poudre de protéine (goût au choix)",1*multiplicateur_arrondi, "banane",50*multiplicateur_arrondi, "g de baies mélangées (frais ou surgelés)",10*multiplicateur_arrondi, "g de graines de chia",30*multiplicateur_arrondi, "g de flocons d'avoine",15*multiplicateur_arrondi, "g de noix (au choix), hachées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, combiner le lait d'amande, la poudre de protéine, la banane, les baies et les flocons d'avoine. Mixer jusqu'à obtention d'une consistance lisse.\nÉTAPE 2 \nVerser le mélange dans un bol.\nÉTAPE 3\nGarnir avec des graines de chia, des noix hachées et un filet de miel.\nÉTAPE 4\nServir immédiatement."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_proteinees_aux_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine", "oeuf", "lait d'amande", "banane", "fruits frais", "sirop d'érable"]
    ingredient = [100*multiplicateur_arrondi, "g de farine d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (goût au choix)",2*multiplicateur_arrondi, "oeufs",150*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "banane, écrasée",100*multiplicateur_arrondi, "g de fruits frais (au choix)", 2*multiplicateur_arrondi, "cuillères à soupe de sirop d'érable"]
    préparation = ["ÉTAPE 1\n Dans un grand bol, mélanger la farine d'avoine, la poudre de protéine, les oeufs, le lait d'amande et la banane écrasée jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nChauffer une poêle antiadhésive à feu moyen. Verser une louche de pâte et cuire jusqu'à ce que des bulles apparaissent à la surface. Retourner et cuire l'autre côté.\nÉTAPE 3\nRépéter avec le reste de la pâte.\nÉTAPE 4\nServir les crêpes garnies de fruits frais et arrosées de sirop d'érable."]
    temps_de_préparation = 15
    temps_de_cuisson = 15
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def oeufs_brouilles_saumon_fume(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs", "saumon fumé", "crème fraîche", "ciboulette", "beurre", "pain complet", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "oeufs",50*multiplicateur_arrondi, "g de saumon fumé, coupé en lanières",1*multiplicateur_arrondi, "cuillère à soupe de crème fraîche",1*multiplicateur_arrondi, "cuillère à soupe de ciboulette hachée",1*multiplicateur_arrondi, "noisette de beurre",2*multiplicateur_arrondi, "tranches de pain complet, toastées","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Battre les oeufs en omelette, ajouter la crème fraîche, la ciboulette, le sel et le poivre.\nÉTAPE 2 \nFaire fondre le beurre dans une poêle à feu moyen. Verser le mélange d'oeufs et cuire en remuant constamment, jusqu'à ce que les oeufs soient juste pris mais encore baveux.\nÉTAPE 3\nRetirer du feu et incorporer délicatement les lanières de saumon fumé.\nÉTAPE 4\nServir immédiatement sur les tranches de pain toasté."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def toast_avocat_oeuf_poché(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "avocat", "oeuf", "jus de citron", "paprika", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet",1*multiplicateur_arrondi, "avocat mûr",1*multiplicateur_arrondi, "oeuf",1*multiplicateur_arrondi, "cuillère à café de jus de citron",1*multiplicateur_arrondi, "pincée de paprika","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Faire toaster les tranches de pain.\nÉTAPE 2 \nÉcraser l'avocat avec le jus de citron, le sel et le poivre, puis étaler sur les toasts.\nÉTAPE 3\nPocher l'oeuf dans de l'eau frémissante avec un peu de vinaigre pendant 3-4 minutes ou jusqu'à ce que le blanc soit ferme mais que le jaune reste coulant.\nÉTAPE 4\nDéposer l'œuf poché sur le toast à l'avocat, saupoudrer de paprika, et servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_banane_beurre_cacahuete(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes", "oeufs", "beurre de cacahuète", "levure chimique", "flocons d'avoine", "miel", "huile de coco"]
    ingredient = [2*multiplicateur_arrondi, "bananes mûres",2*multiplicateur_arrondi, "oeufs",2*multiplicateur_arrondi, "cuillères à soupe de beurre de cacahuète",1*multiplicateur_arrondi, "cuillère à café de levure chimique",50*multiplicateur_arrondi, "g de flocons d'avoine",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco pour la cuisson"]
    préparation = ["ÉTAPE 1\n Dans un bol, écraser les bananes et ajouter les oeufs, le beurre de cacahuète, la levure chimique, et les flocons d'avoine. Mélanger jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nChauffer une poêle avec un peu d'huile de coco à feu moyen.\nÉTAPE 3\nVerser une petite quantité de pâte pour former chaque pancake. Cuire jusqu'à ce que des bulles apparaissent à la surface, puis retourner pour cuire l'autre côté.\nÉTAPE 4\nServir chaud avec un filet de miel sur le dessus."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowl_quinoa_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "lait de coco", "miel", "banane", "baies", "graines de chia", "amandes"]
    ingredient = [50*multiplicateur_arrondi, "g de quinoa",100*multiplicateur_arrondi, "ml de lait de coco",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "banane, tranchée",50*multiplicateur_arrondi, "g de baies (au choix)",1*multiplicateur_arrondi, "cuillère à soupe de graines de chia",10*multiplicateur_arrondi, "g d'amandes, hachées"]
    préparation = ["ÉTAPE 1\n Rincer le quinoa à l'eau froide, puis le cuire dans le lait de coco jusqu'à absorption complète du liquide.\nÉTAPE 2 \nUne fois cuit, ajouter le miel et mélanger.\nÉTAPE 3\nDisposer le quinoa dans un bol et garnir avec les tranches de banane, les baies, les graines de chia, et les amandes hachées.\nÉTAPE 4\nServir immédiatement ou laisser refroidir pour une dégustation plus fraîche."]
    temps_de_préparation = 5
    temps_de_cuisson = 20
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_bowl_proteine(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande", "poudre de protéine", "banane", "baies", "graines de chia", "flocons d'avoine", "beurre d'amande"]
    ingredient = [200*multiplicateur_arrondi, "ml de lait d'amande",30*multiplicateur_arrondi, "g de poudre de protéine (parfum au choix)",1*multiplicateur_arrondi, "banane congelée",50*multiplicateur_arrondi, "g de baies (au choix)",1*multiplicateur_arrondi, "cuillère à soupe de graines de chia",30*multiplicateur_arrondi, "g de flocons d'avoine",1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande"]
    préparation = ["ÉTAPE 1\n Dans un blender, combiner le lait d'amande, la poudre de protéine, la banane congelée, et les flocons d'avoine. Mixer jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nVerser le mélange dans un bol.\nÉTAPE 3\nGarnir avec les baies, les graines de chia, et le beurre d'amande.\nÉTAPE 4\nServir immédiatement pour un petit déjeuner riche en protéines et énergisant."]
    temps_de_préparation = 10
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffins_oeufs_legumes(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs", "épinards", "tomates cerise", "fromage feta", "sel", "poivre"]
    ingredient = [6*multiplicateur_arrondi, "oeufs",30*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de tomates cerise, coupées en deux",30*multiplicateur_arrondi, "g de fromage feta, émietté","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C (350°F). Graisser légèrement les moules à muffins.\nÉTAPE 2 \nDans un bol, battre les oeufs avec du sel et du poivre.\nÉTAPE 3\nRépartir les épinards, les tomates cerise, et le fromage feta dans les moules.\nÉTAPE 4\nVerser les oeufs battus sur le mélange de légumes et fromage.\nÉTAPE 5\nCuire au four pendant 20-25 minutes ou jusqu'à ce que les muffins soient fermes et dorés.\nÉTAPE 6\nLaisser refroidir avant de démouler et servir."]
    temps_de_préparation = 15
    temps_de_cuisson = 25
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_quinoa_noix_fruits_secs(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa", "lait", "miel", "cannelle", "noix", "fruits secs"]
    ingredient = [50*multiplicateur_arrondi, "g de quinoa",250*multiplicateur_arrondi, "ml de lait (au choix)",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "cuillère à café de cannelle",20*multiplicateur_arrondi, "g de noix, hachées",30*multiplicateur_arrondi, "g de fruits secs (au choix)"]
    préparation = ["ÉTAPE 1\n Rincer le quinoa à l'eau froide puis le mettre dans une casserole avec le lait. Porter à ébullition, réduire le feu et laisser mijoter jusqu'à ce que le quinoa soit tendre et que le lait soit absorbé, environ 15 minutes.\nÉTAPE 2 \nAjouter le miel et la cannelle au quinoa cuit et bien mélanger.\nÉTAPE 3\nServir le porridge dans un bol, garnir avec les noix hachées et les fruits secs.\nÉTAPE 4\nDéguster chaud pour un petit déjeuner nourrissant et énergétique."]
    temps_de_préparation = 5
    temps_de_cuisson = 15
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def oeufs_brouilles_epinards_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs", "épinards", "fromage râpé", "sel", "poivre", "huile d'olive"]
    ingredient = [3*multiplicateur_arrondi, "oeufs",50*multiplicateur_arrondi, "g d'épinards frais",30*multiplicateur_arrondi, "g de fromage râpé","sel", "au goût","poivre", "au goût",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive"]
    préparation = ["ÉTAPE 1\n Faire chauffer l'huile d'olive dans une poêle à feu moyen.\nÉTAPE 2 \nAjouter les épinards dans la poêle et faire cuire jusqu'à ce qu'ils soient ramollis.\nÉTAPE 3\nDans un bol, battre les oeufs avec du sel et du poivre.\nÉTAPE 4\nVerser les oeufs battus dans la poêle avec les épinards.\nÉTAPE 5\nFaire cuire en remuant constamment jusqu'à ce que les oeufs soient pris mais encore moelleux.\nÉTAPE 6\nSaupoudrer de fromage râpé sur les oeufs brouillés et laisser fondre légèrement.\nÉTAPE 7\nServir chaud."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_avoine_baies(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait", "baies (fraises, framboises, myrtilles)", "banane", "miel"]
    ingredient = [30*multiplicateur_arrondi, "g de flocons d'avoine",250*multiplicateur_arrondi, "ml de lait (au choix)",100*multiplicateur_arrondi, "g de baies (fraises, framboises, myrtilles, au choix)",1*multiplicateur_arrondi, "banane",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les flocons d'avoine, le lait, les baies, la banane et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nVerser dans un grand verre ou un bol.\nÉTAPE 3\nDécorer avec des baies supplémentaires si désiré.\nÉTAPE 4\nDéguster immédiatement pour un petit déjeuner rapide et énergétique."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_proteinees_vanille_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine de blé", "poudre de protéine (saveur vanille)", "oeufs", "lait", "huile de coco", "fruits frais", "sirop d'érable"]
    ingredient = [100*multiplicateur_arrondi, "g de farine de blé",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille)",2*multiplicateur_arrondi, "oeufs",150*multiplicateur_arrondi, "ml de lait",1*multiplicateur_arrondi, "cuillère à soupe d'huile de coco, fondue",100*multiplicateur_arrondi, "g de fruits frais (au choix)",2*multiplicateur_arrondi, "cuillères à soupe de sirop d'érable"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine de blé, la poudre de protéine, les oeufs et le lait jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nChauffer une poêle antiadhésive à feu moyen. Ajouter un peu d'huile de coco fondue.\nÉTAPE 3\nVerser une louche de pâte dans la poêle et étaler pour former une crêpe. Cuire jusqu'à ce que des bulles apparaissent à la surface, puis retourner et cuire l'autre côté.\nÉTAPE 4\nRépéter avec le reste de la pâte.\nÉTAPE 5\nServir les crêpes chaudes avec des fruits frais et un filet de sirop d'érable."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_proteine_epinards_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "banane", "poudre de protéine (saveur vanille ou non aromatisée)", "lait d'amande", "beurre d'amande", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g d'épinards frais",1*multiplicateur_arrondi, "banane",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",250*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les épinards, la banane, la poudre de protéine, le lait d'amande, le beurre d'amande et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour une texture plus épaisse et fraîche.\nÉTAPE 3\nVerser dans un grand verre et servir immédiatement pour profiter de ce petit déjeuner riche en protéines et en énergie."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["oeufs", "champignons", "fromage râpé", "beurre", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "oeufs",50*multiplicateur_arrondi, "g de champignons, tranchés",30*multiplicateur_arrondi, "g de fromage râpé (au choix)",1*multiplicateur_arrondi, "cuillère à café de beurre","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Faire fondre le beurre dans une poêle antiadhésive à feu moyen.\nÉTAPE 2 \nAjouter les champignons tranchés dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 3\nBattre les oeufs dans un bol avec du sel et du poivre.\nÉTAPE 4\nVerser les oeufs battus dans la poêle avec les champignons.\nÉTAPE 5\nLaisser cuire jusqu'à ce que les bords commencent à se solidifier, puis ajouter le fromage râpé sur un côté des oeufs.\nÉTAPE 6\nPlier l'omelette en deux et laisser cuire jusqu'à ce que le fromage soit fondu et les oeufs bien cuits.\nÉTAPE 7\nServir chaud avec une garniture de votre choix."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_bowl_baies_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies (fraises, framboises, myrtilles)", "banane", "lait d'amande", "flocons d'avoine", "amandes effilées", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g de baies (fraises, framboises, myrtilles, au choix)",1*multiplicateur_arrondi, "banane",150*multiplicateur_arrondi, "ml de lait d'amande",30*multiplicateur_arrondi, "g de flocons d'avoine",10*multiplicateur_arrondi, "g d'amandes effilées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les baies, la banane, le lait d'amande, les flocons d'avoine et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nVerser le smoothie dans un bol.\nÉTAPE 3\nGarnir de tranches de banane, d'amandes effilées et de baies supplémentaires si désiré.\nÉTAPE 4\nDéguster immédiatement pour un petit déjeuner riche en fibres et en saveurs."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffins_flocons_avoine_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait", "oeufs", "huile de coco", "miel", "pépites de chocolat"]
    ingredient = [100*multiplicateur_arrondi, "g de flocons d'avoine",150*multiplicateur_arrondi, "ml de lait (au choix)",2*multiplicateur_arrondi, "oeufs",2*multiplicateur_arrondi, "cuillères à soupe d'huile de coco, fondue",2*multiplicateur_arrondi, "cuillères à soupe de miel",50*multiplicateur_arrondi, "g de pépites de chocolat (noir ou au lait)"]
    préparation = [ "ÉTAPE 1\n Préchauffer le four à 180°C (350°F). Graisser légèrement un moule à muffins.\nÉTAPE 2 \nDans un bol, mélanger les flocons d'avoine, le lait, les oeufs, l'huile de coco fondue et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 3\nIncorporer les pépites de chocolat dans la pâte à muffins.\nÉTAPE 4\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 5\nCuire au four préchauffé pendant 20-25 minutes ou jusqu'à ce que les muffins soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 6\nLaisser refroidir légèrement avant de démouler et de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 25
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_myrtilles_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine", "levure chimique", "sel", "oeufs", "lait", "yaourt grec", "myrtilles", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g de farine",1*multiplicateur_arrondi, "cuillère à café de levure chimique","sel", "au goût",2*multiplicateur_arrondi, "oeufs",150*multiplicateur_arrondi, "ml de lait",100*multiplicateur_arrondi, "g de yaourt grec",50*multiplicateur_arrondi, "g de myrtilles",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un grand bol, mélanger la farine, la levure chimique et une pincée de sel.\nÉTAPE 2 \nAjouter les oeufs, le lait et le yaourt grec au mélange de farine et bien mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 3\nChauffer une poêle antiadhésive à feu moyen et verser une louche de pâte dans la poêle.\nÉTAPE 4\nDisposer quelques myrtilles sur le dessus de la crêpe.\nÉTAPE 5\nCuire jusqu'à ce que des bulles apparaissent à la surface, puis retourner et cuire l'autre côté.\nÉTAPE 6\nRépéter avec le reste de la pâte.\nÉTAPE 7\nServir chaud avec un filet de miel."]
    temps_de_préparation = 10
    temps_de_cuisson = 15
    temps_total = temps_de_cuisson + temps_de_préparation
def bowlcake_vanille_fruits_rouges(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille)", "yaourt grec", "banane", "oeuf", "fruits rouges (fraises, framboises, myrtilles)", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille)",100*multiplicateur_arrondi, "g de yaourt grec",1*multiplicateur_arrondi, "banane écrasée",1*multiplicateur_arrondi, "oeuf",50*multiplicateur_arrondi, "g de fruits rouges (fraises, framboises, myrtilles)",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, le yaourt grec, la banane écrasée et l'oeuf jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les fruits rouges dans la pâte à bowlcake.\nÉTAPE 3\nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 4\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 5\nArroser de miel avant de déguster ce délicieux petit déjeuner riche en protéines et en saveurs."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pain_perdu_proteine_cannelle_sirop_derable(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet ou aux céréales", "poudre de protéine (saveur vanille)", "oeufs", "lait", "cannelle", "sirop d'érable"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet ou aux céréales",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille)",2*multiplicateur_arrondi, "oeufs",100*multiplicateur_arrondi, "ml de lait",1*multiplicateur_arrondi, "cuillère à café de cannelle",2*multiplicateur_arrondi, "cuillères à soupe de sirop d'érable"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la poudre de protéine, les oeufs, le lait et la cannelle jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nTremper les tranches de pain dans le mélange de protéines et les retourner pour bien les imbiber.\nÉTAPE 3\nChauffer une poêle antiadhésive à feu moyen et y faire cuire les tranches de pain jusqu'à ce qu'elles soient dorées des deux côtés.\nÉTAPE 4\nServir chaud avec un filet de sirop d'érable."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_proteines_banane_beurre_cacahuete(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "banane", "oeufs", "lait", "beurre de cacahuète", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",1*multiplicateur_arrondi, "banane écrasée",2*multiplicateur_arrondi, "oeufs",50*multiplicateur_arrondi, "ml de lait",1*multiplicateur_arrondi, "cuillère à soupe de beurre de cacahuète",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, la banane écrasée, les oeufs et le lait jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nChauffer une poêle antiadhésive à feu moyen.\nÉTAPE 3\nVerser une petite quantité de pâte dans la poêle et étaler pour former un pancake.\nÉTAPE 4\nCuire jusqu'à ce que des bulles apparaissent à la surface, puis retourner et cuire l'autre côté.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir chaud avec une cuillère à soupe de beurre de cacahuète et un filet de miel."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_mangue_lait_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "mangue, pelée et coupée en dés",200*multiplicateur_arrondi, "ml de lait de coco",100*multiplicateur_arrondi, "ml de yaourt grec",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la mangue, le lait de coco, le yaourt grec et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour une texture plus épaisse et fraîche.\nÉTAPE 3\nVerser dans un grand verre ou un bol.\nÉTAPE 4\nDécorer avec des tranches de mangue ou de la noix de coco râpée si désiré.\nÉTAPE 5\nDéguster immédiatement pour profiter de ce smoothie rafraîchissant et nourrissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_bowl_acai_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["pulpe d'açaï", "banane", "baies surgelées (fraises, myrtilles, framboises)", "lait d'amande", "graines de chia", "granola", "fruits frais (fraises, myrtilles, kiwi)", "noix de coco râpée"]
    ingredient = [100*multiplicateur_arrondi, "g de pulpe d'açaï",1*multiplicateur_arrondi, "banane congelée",50*multiplicateur_arrondi, "g de baies surgelées (fraises, myrtilles, framboises)",150*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de graines de chia",2*multiplicateur_arrondi, "cuillères à soupe de granola","fruits frais (fraises, myrtilles, kiwi), pour garnir","noix de coco râpée, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la pulpe d'açaï, la banane congelée, les baies surgelées et le lait d'amande jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nVerser le smoothie dans un bol.\nÉTAPE 3\nParsemer de graines de chia, de granola, de fruits frais et de noix de coco râpée.\nÉTAPE 4\nDéguster immédiatement avec une cuillère pour profiter de ce délicieux smoothie bowl énergisant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_champignons(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "champignons", "fromage râpé", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de champignons, tranchés",30*multiplicateur_arrondi, "g de fromage râpé (au choix)",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Faire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 2 \nAjouter les épinards et les champignons dans la poêle et faire cuire jusqu'à ce qu'ils soient tendres.\nÉTAPE 3\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 4\nVerser les œufs battus dans la poêle avec les épinards et les champignons.\nÉTAPE 5\nLaisser cuire jusqu'à ce que les œufs soient pris.\nÉTAPE 6\nSaupoudrer de fromage râpé sur un côté des œufs.\nÉTAPE 7\nPlier l'omelette en deux et laisser cuire encore quelques instants jusqu'à ce que le fromage soit fondu.\nÉTAPE 8\nServir chaud avec une salade ou du pain grillé."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_vert_epinards_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards frais", "banane", "yaourt grec", "lait d'amande", "miel", "graines de lin"]
    ingredient = [50*multiplicateur_arrondi, "g d'épinards frais",1*multiplicateur_arrondi, "banane",100*multiplicateur_arrondi, "g de yaourt grec",150*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "cuillère à soupe de graines de lin"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les épinards, la banane, le yaourt grec, le lait d'amande et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nAjouter les graines de lin et mélanger brièvement pour les incorporer.\nÉTAPE 3\nVerser dans un verre et déguster immédiatement ce délicieux smoothie vert riche en nutriments."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_proteine_pommes_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "lait", "pomme", "cannelle", "miel", "amandes effilées"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",200*multiplicateur_arrondi, "ml de lait (de votre choix)",1*multiplicateur_arrondi, "pomme, pelée, épépinée et coupée en dés","cannelle", "au goût",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "cuillère à soupe d'amandes effilées, grillées"]
    préparation = ["ÉTAPE 1\n Dans une casserole, mélanger les flocons d'avoine, la poudre de protéine, le lait, les dés de pomme, la cannelle et le miel.\nÉTAPE 2 \nPorter à ébullition, puis réduire le feu et laisser mijoter pendant environ 5 minutes, en remuant de temps en temps, jusqu'à ce que le porridge épaississe et que les pommes soient tendres.\nÉTAPE 3\nVerser dans un bol et garnir d'amandes effilées grillées avant de servir.\nÉTAPE 4\nDéguster chaud pour un petit déjeuner réconfortant et riche en protéines."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_banane_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "banane", "œufs", "lait", "pépites de chocolat", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",1*multiplicateur_arrondi, "banane écrasée",2*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "ml de lait",2*multiplicateur_arrondi, "cuillères à soupe de pépites de chocolat",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, la banane écrasée, les œufs, le lait et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les pépites de chocolat dans la pâte à bowlcake.\nÉTAPE 3\nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 4\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 5\nArroser de miel avant de déguster ce délicieux petit déjeuner gourmand."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_mangue_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "épinards", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [1*multiplicateur_arrondi, "mangue, pelée et coupée en dés",50*multiplicateur_arrondi, "g d'épinards frais",150*multiplicateur_arrondi, "ml de lait d'amande",100*multiplicateur_arrondi, "ml de yaourt grec",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la mangue, les épinards, le lait d'amande, le yaourt grec et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour une texture plus fraîche.\nÉTAPE 3\nVerser dans un grand verre ou un bol.\nÉTAPE 4\nDéguster immédiatement ce smoothie vert et rafraîchissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_proteines_vanille_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "extrait de vanille", "myrtilles", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille",50*multiplicateur_arrondi, "g de myrtilles",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les flocons d'avoine, la poudre de protéine, les œufs, le lait d'amande et l'extrait de vanille jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nIncorporer délicatement les myrtilles dans la pâte.\nÉTAPE 3\nChauffer une poêle antiadhésive à feu moyen et y verser la pâte à pancakes en portions.\nÉTAPE 4\nCuire les pancakes pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et que des bulles apparaissent à la surface.\nÉTAPE 5\nServir chaud avec un filet de miel."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_banane_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "beurre d'amande", "lait d'amande", "miel", "amandes effilées"]
    ingredient = [1*multiplicateur_arrondi, "banane congelée",2*multiplicateur_arrondi, "cuillères à soupe de beurre d'amande",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel","amandes effilées, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la banane congelée, le beurre d'amande, le lait d'amande et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nVerser dans un grand verre ou un bol.\nÉTAPE 3\nGarnir d'amandes effilées avant de servir.\nÉTAPE 4\nDéguster immédiatement pour un petit déjeuner riche en protéines et en bons gras."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_tomates_sechees_fromage_chevre(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "tomates séchées", "fromage de chèvre", "huile d'olive", "sel", "poivre", "basilic frais"]
    ingredient = [3*multiplicateur_arrondi, "œufs",2*multiplicateur_arrondi, "tomates séchées, hachées",50*multiplicateur_arrondi, "g de fromage de chèvre, émietté",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût","basilic frais, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nChauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nVerser les œufs battus dans la poêle chaude.\nÉTAPE 4\nAjouter les tomates séchées et le fromage de chèvre émietté sur un côté des œufs.\nÉTAPE 5\nLaisser cuire jusqu'à ce que les œufs soient pris et que le fromage soit fondu.\nÉTAPE 6\nPlier l'omelette en deux et laisser cuire encore quelques instants.\nÉTAPE 7\nServir chaud, garni de basilic frais."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "épinards", "banane", "lait d'amande", "jus de citron", "miel"]
    ingredient = [1*multiplicateur_arrondi, "avocat, pelé et dénoyauté",50*multiplicateur_arrondi, "g d'épinards frais",1*multiplicateur_arrondi, "banane",150*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de jus de citron",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger l'avocat, les épinards, la banane, le lait d'amande, le jus de citron et le miel jusqu'à obtenir une consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour une texture plus fraîche.\nÉTAPE 3\nVerser dans un grand verre ou un bol.\nÉTAPE 4\nDéguster immédiatement ce smoothie vert riche en nutriments."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "fromage suisse", "beurre", "sel", "poivre", "ciboulette"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g de champignons, tranchés",50*multiplicateur_arrondi, "g de fromage suisse, râpé",1*multiplicateur_arrondi, "cuillère à soupe de beurre","sel", "au goût","poivre", "au goût","ciboulette fraîche, hachée, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans une poêle, faire fondre le beurre à feu moyen.\nÉTAPE 2 \nAjouter les champignons tranchés dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 3\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les champignons.\nÉTAPE 5\nSaupoudrer le fromage suisse râpé sur les œufs.\nÉTAPE 6\nCuire l'omelette jusqu'à ce que les œufs soient pris et que le fromage soit fondu.\nÉTAPE 7\nSaupoudrer de ciboulette fraîche hachée avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_vanille_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "extrait de vanille", "pépites de chocolat", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille",2*multiplicateur_arrondi, "cuillères à soupe de pépites de chocolat",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, les œufs, le lait d'amande, l'extrait de vanille et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les pépites de chocolat dans la pâte à bowlcake.\nÉTAPE 3\nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 4\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 5\nArroser de miel avant de déguster ce délicieux petit déjeuner gourmand."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_fraises_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "fraises", "amandes effilées", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",150*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de fraises, tranchées","amandes effilées, pour garnir",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et réduire le feu à doux.\nÉTAPE 3\nLaisser mijoter pendant environ 5 minutes, en remuant de temps en temps, jusqu'à ce que le mélange épaississe.\nÉTAPE 4\nRetirer du feu et laisser reposer quelques minutes.\nÉTAPE 5\nVerser le porridge dans un bol et garnir de fraises tranchées, d'amandes effilées et d'un filet de miel.\nÉTAPE 6\nServir chaud et déguster ce petit déjeuner réconfortant."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "fromage feta", "huile d'olive", "sel", "poivre", "origan séché"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de fromage feta, émietté",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût","origan séché, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans une poêle, faire chauffer l'huile d'olive à feu moyen.\nÉTAPE 2 \nAjouter les épinards frais dans la poêle et faire cuire jusqu'à ce qu'ils soient fanés.\nÉTAPE 3\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les épinards.\nÉTAPE 5\nSaupoudrer le fromage feta émietté sur les œufs.\nÉTAPE 6\nCuire l'omelette jusqu'à ce que les œufs soient pris et que le fromage soit fondu.\nÉTAPE 7\nSaupoudrer d'origan séché avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_proteines_chocolat_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur chocolat ou non aromatisée)", "œufs", "lait d'amande", "amandes effilées", "pépites de chocolat", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur chocolat ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande","amandes effilées, pour garnir",2*multiplicateur_arrondi, "cuillères à soupe de pépites de chocolat",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, les œufs, le lait d'amande et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les pépites de chocolat dans la pâte à pancakes.\nÉTAPE 3\nChauffer une poêle antiadhésive à feu moyen et y verser la pâte à pancakes en portions.\nÉTAPE 4\nCuire les pancakes pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et que des bulles apparaissent à la surface.\nÉTAPE 5\nServir chaud, garni d'amandes effilées et accompagné de miel."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_bowl_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "banane", "lait de coco", "flocons de noix de coco", "granola", "fruits frais (optionnel)"]
    ingredient = [1*multiplicateur_arrondi, "mangue, pelée et coupée en dés",1*multiplicateur_arrondi, "banane, congelée et tranchée",100*multiplicateur_arrondi, "ml de lait de coco","flocons de noix de coco, pour garnir","granola, pour garnir","fruits frais, pour garnir (optionnel)"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la mangue, la banane et le lait de coco jusqu'à consistance lisse.\nÉTAPE 2 \nVerser le smoothie dans un bol.\nÉTAPE 3\nGarnir de flocons de noix de coco, de granola et de fruits frais si désiré.\nÉTAPE 4\nServir immédiatement et déguster ce délicieux smoothie bowl rafraîchissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def oeufs_brouilles_epinards_fromage_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "fromage cheddar", "beurre", "sel", "poivre", "persil frais"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de fromage cheddar, râpé",1*multiplicateur_arrondi, "cuillère à soupe de beurre","sel", "au goût","poivre", "au goût","persil frais, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans une poêle, faire fondre le beurre à feu moyen.\nÉTAPE 2 \nAjouter les épinards frais dans la poêle et faire cuire jusqu'à ce qu'ils soient fanés.\nÉTAPE 3\nDans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les épinards.\nÉTAPE 5\nSaupoudrer le fromage cheddar râpé sur les œufs.\nÉTAPE 6\nRemuer doucement jusqu'à ce que les œufs soient pris et que le fromage soit fondu.\nÉTAPE 7\nSaupoudrer de persil frais haché avant de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_banane_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "banane", "lait d'amande", "pépites de chocolat", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",1*multiplicateur_arrondi, "banane, écrasée",100*multiplicateur_arrondi, "ml de lait d'amande",2*multiplicateur_arrondi, "cuillères à soupe de pépites de chocolat",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, les œufs, la banane écrasée, le lait d'amande et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les pépites de chocolat dans la pâte à bowlcake.\nÉTAPE 3\nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 4\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 5\nArroser de miel avant de déguster ce délicieux petit déjeuner gourmand."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_myrtilles_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "myrtilles", "amandes effilées", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",150*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de myrtilles","amandes effilées, pour garnir",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et réduire le feu à doux.\nÉTAPE 3\nLaisser mijoter pendant environ 5 minutes, en remuant de temps en temps, jusqu'à ce que le mélange épaississe.\nÉTAPE 4\nRetirer du feu et laisser reposer quelques minutes.\nÉTAPE 5\nVerser le porridge dans un bol et garnir de myrtilles fraîches et d'amandes effilées.\nÉTAPE 6\nArroser de miel avant de déguster ce petit déjeuner délicieusement nourrissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_myrtilles_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine", "œufs", "lait", "myrtilles", "yaourt grec", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait",50*multiplicateur_arrondi, "g de myrtilles",50*multiplicateur_arrondi, "g de yaourt grec",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine, les œufs et le lait jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nChauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nCuire chaque crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées.\nÉTAPE 4\nRépéter l'opération avec le reste de la pâte.\nÉTAPE 5\nGarnir chaque crêpe de yaourt grec, de myrtilles fraîches et de miel.\nÉTAPE 6\nPlier les crêpes en deux et servir chaud."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_beurre_cacahuete_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "beurre de cacahuète", "pépites de chocolat", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de beurre de cacahuète",2*multiplicateur_arrondi, "cuillères à soupe de pépites de chocolat",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, les œufs, le lait d'amande, le beurre de cacahuète et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nIncorporer les pépites de chocolat dans la pâte à bowlcake.\nÉTAPE 3\nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 4\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 5\nArroser de miel avant de déguster ce délicieux petit déjeuner gourmand."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_proteine_vanille_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["protéine en poudre (saveur vanille)", "lait d'amande", "amandes", "banane", "miel"]
    ingredient = [30*multiplicateur_arrondi, "g de protéine en poudre (saveur vanille)",250*multiplicateur_arrondi, "ml de lait d'amande",15*multiplicateur_arrondi, "g d'amandes",1*multiplicateur_arrondi, "banane",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la protéine en poudre, le lait d'amande, les amandes, la banane et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner protéiné et énergisant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def toasts_avocat_oeufs(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "avocat", "pain complet", "sel", "poivre", "piment rouge en flocons", "jus de citron"]
    ingredient = [2*multiplicateur_arrondi, "œufs",1*multiplicateur_arrondi, "avocat, pelé et écrasé",2*multiplicateur_arrondi, "tranches de pain complet, grillées","sel", "au goût","poivre", "au goût","piment rouge en flocons, pour garnir","jus de citron, pour arroser"]
    préparation = ["ÉTAPE 1\n Faire cuire les œufs selon la préférence (œufs au plat, œufs pochés, etc.).\nÉTAPE 2 \nPendant ce temps, griller les tranches de pain complet.\nÉTAPE 3\nÉtaler l'avocat écrasé sur les tranches de pain grillé.\nÉTAPE 4\nDisposer les œufs cuits sur l'avocat.\nÉTAPE 5\nAssaisonner avec du sel, du poivre et des flocons de piment rouge.\nÉTAPE 6\nArroser d'un filet de jus de citron avant de servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_patate_douce_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce", "œufs", "lait d'amande", "farine d'avoine", "levure chimique", "cannelle en poudre", "sel", "miel"]
    ingredient = [1*multiplicateur_arrondi, "patate douce, cuite et écrasée",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de farine d'avoine",1*multiplicateur_arrondi, "cuillère à café de levure chimique",1*multiplicateur_arrondi, "cuillère à café de cannelle en poudre","sel", "au goût","miel, pour garnir"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la patate douce écrasée, les œufs et le lait d'amande.\nÉTAPE 2 \nAjouter la farine d'avoine, la levure chimique, la cannelle en poudre et une pincée de sel.\nÉTAPE 3\nMélanger jusqu'à ce que la pâte soit homogène.\nÉTAPE 4\nChauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à pancakes.\nÉTAPE 5\nCuire chaque pancake pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés.\nÉTAPE 6\nServir chaud, garni de miel et de plus de cannelle si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_fraises_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "banane", "lait d'amande", "yaourt grec", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g de fraises, équeutées et coupées en morceaux",1*multiplicateur_arrondi, "banane, congelée et tranchée",150*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de yaourt grec",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les fraises, la banane, le lait d'amande, le yaourt grec et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner rafraîchissant et nutritif."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "fromage feta", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de fromage feta, émietté",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les épinards frais dans la poêle et faire cuire jusqu'à ce qu'ils soient fanés.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les épinards.\nÉTAPE 5\nAjouter le fromage feta émietté sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servez-la chaude."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_pomme_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "pomme", "cannelle en poudre", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "pomme, pelée et coupée en dés",1*multiplicateur_arrondi, "cuillère à café de cannelle en poudre",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, les œufs, le lait d'amande, la pomme coupée en dés, la cannelle en poudre et le miel jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 3\nCuire au micro-ondes pendant 2-3 minutes ou jusqu'à ce que le bowlcake soit bien gonflé et cuit.\nÉTAPE 4\nArroser de miel avant de déguster ce délicieux petit déjeuner gourmand."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_mangue_noix_de_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "yaourt à la noix de coco", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de mangue, pelée et coupée en morceaux",100*multiplicateur_arrondi, "ml de lait de coco",50*multiplicateur_arrondi, "g de yaourt à la noix de coco",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger la mangue, le lait de coco, le yaourt à la noix de coco et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner exotique et rafraîchissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage_suisse(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "fromage suisse", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g de champignons, tranchés",50*multiplicateur_arrondi, "g de fromage suisse, râpé",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les champignons tranchés dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les champignons.\nÉTAPE 5\nAjouter le fromage suisse râpé sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servez-la chaude."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_baies_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "baies mélangées (fraises, myrtilles, framboises)", "cannelle en poudre", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",150*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de baies mélangées (fraises, myrtilles, framboises)",1*multiplicateur_arrondi, "cuillère à café de cannelle en poudre",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et la cannelle en poudre dans la casserole.\nÉTAPE 3\nLaisser mijoter pendant environ 5 minutes, en remuant de temps en temps, jusqu'à ce que le mélange épaississe.\nÉTAPE 4\nRetirer du feu et laisser reposer quelques minutes.\nÉTAPE 5\nVerser le porridge dans un bol et garnir de baies mélangées.\nÉTAPE 6\nArroser de miel avant de déguster ce petit déjeuner réconfortant et nutritif."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_myrtilles_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["myrtilles", "épinards", "banane", "lait d'amande", "miel"]
    ingredient = [100*multiplicateur_arrondi, "g de myrtilles fraîches ou congelées",50*multiplicateur_arrondi, "g d'épinards frais",1*multiplicateur_arrondi, "banane",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les myrtilles, les épinards, la banane, le lait d'amande et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner énergétique et riche en antioxydants."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def toasts_saumon_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet", "avocat", "saumon fumé", "citron", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet, grillées",1*multiplicateur_arrondi, "avocat, pelé et tranché",50*multiplicateur_arrondi, "g de saumon fumé",1*multiplicateur_arrondi, "citron, coupé en quartiers","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Faire griller les tranches de pain complet.\nÉTAPE 2 \nÉtaler les tranches d'avocat sur les toasts grillés.\nÉTAPE 3\nDisposer les tranches de saumon fumé sur l'avocat.\nÉTAPE 4\nPresser un peu de jus de citron sur chaque toast.\nÉTAPE 5\nAssaisonner avec du sel et du poivre au goût.\nÉTAPE 6\nServir immédiatement ces délicieux toasts au saumon et à l'avocat."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_proteinees_vanille_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "myrtilles", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de myrtilles",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine d'avoine, la poudre de protéine, les œufs et le lait d'amande jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nDisposer quelques myrtilles sur la crêpe.\nÉTAPE 4\nCuire la crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elle soit dorée.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir les crêpes chaudes, garnies de miel et de plus de myrtilles si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_ananas_noix_de_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["ananas", "lait de coco", "yaourt à la noix de coco", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g d'ananas frais ou congelé, coupé en morceaux",100*multiplicateur_arrondi, "ml de lait de coco",50*multiplicateur_arrondi, "g de yaourt à la noix de coco",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger l'ananas, le lait de coco, le yaourt à la noix de coco et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner exotique et rafraîchissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage_cheddar(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "fromage cheddar", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g de champignons, tranchés",50*multiplicateur_arrondi, "g de fromage cheddar, râpé",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les champignons tranchés dans la poêle et faire cuire jusqu'à ce qu'ils soient dorés.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les champignons.\nÉTAPE 5\nAjouter le fromage cheddar râpé sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servez-la chaude."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_proteines_vanille_framboises(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "framboises", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de framboises",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine d'avoine, la poudre de protéine, les œufs et le lait d'amande jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à pancakes.\nÉTAPE 3\nDisposer quelques framboises sur chaque pancake.\nÉTAPE 4\nCuire les pancakes pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir les pancakes chauds, garnis de miel et de plus de framboises si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_banane_beurre_arachide(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "beurre d'arachide", "lait d'amande", "flocons d'avoine", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane",1*multiplicateur_arrondi, "cuillère à soupe de beurre d'arachide",200*multiplicateur_arrondi, "ml de lait d'amande",20*multiplicateur_arrondi, "g de flocons d'avoine",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter la banane, le beurre d'arachide, le lait d'amande, les flocons d'avoine et le miel.\nÉTAPE 2 \nMixer jusqu'à obtention d'une consistance lisse et crémeuse.\nÉTAPE 3\nAjouter des glaçons si désiré pour une texture plus épaisse et froide.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner nutritif et énergétique."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def toasts_oeufs_brouilles_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["tranches de pain complet", "œufs", "épinards", "fromage râpé", "sel", "poivre"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet, grillées",3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",20*multiplicateur_arrondi, "g de fromage râpé","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Faire griller les tranches de pain complet.\nÉTAPE 2 \nDans une poêle, faire cuire les œufs brouillés jusqu'à ce qu'ils soient pris mais encore légèrement baveux.\nÉTAPE 3\nAjouter les épinards frais à la poêle et mélanger jusqu'à ce qu'ils soient flétris.\nÉTAPE 4\nRépartir les œufs brouillés et les épinards sur les tranches de pain grillées.\nÉTAPE 5\nSaupoudrer de fromage râpé et assaisonner avec du sel et du poivre au goût.\nÉTAPE 6\nServir chaud et déguster ces délicieux toasts aux œufs brouillés et aux épinards."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffins_myrtilles_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œufs", "lait d'amande", "myrtilles", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine d'avoine",30*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de myrtilles",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C (350°F) et chemiser un moule à muffins de caissettes en papier ou en silicone.\nÉTAPE 2 \nDans un bol, mélanger la farine d'avoine, la poudre de protéine, les œufs et le lait d'amande jusqu'à obtenir une pâte homogène.\nÉTAPE 3\nIncorporer délicatement les myrtilles à la pâte.\nÉTAPE 4\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 5\nCuire au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce que les muffins soient dorés et qu'un cure-dent inséré au centre en ressorte propre.\nÉTAPE 6\nLaisser refroidir légèrement avant de servir et déguster ces délicieux muffins aux myrtilles et à la vanille."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_fromage_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards", "fromage feta", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de fromage feta, émietté",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les épinards frais dans la poêle et faire cuire jusqu'à ce qu'ils soient flétris.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les épinards.\nÉTAPE 5\nAjouter le fromage feta émietté sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servez-la chaude."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_pommes_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "pomme", "miel", "cannelle"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "pomme, pelée et coupée en dés",1*multiplicateur_arrondi, "cuillère à soupe de miel",1*multiplicateur_arrondi, "cuillère à café de cannelle en poudre"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et baisser le feu.\nÉTAPE 3\nRemuer régulièrement et laisser mijoter pendant 5 à 7 minutes, ou jusqu'à ce que le porridge épaississe.\nÉTAPE 4\nRetirer du feu et incorporer les dés de pomme, le miel et la cannelle.\nÉTAPE 5\nRemuer pour bien mélanger et laisser reposer quelques minutes pour permettre aux pommes de ramollir légèrement.\nÉTAPE 6\nServir chaud et savourer ce délicieux porridge aux pommes et à la cannelle."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_fraises_graines_chia(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "graines de chia", "lait d'amande", "miel"]
    ingredient = [150*multiplicateur_arrondi, "g de fraises fraîches ou congelées",1*multiplicateur_arrondi, "cuillère à soupe de graines de chia",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les fraises, les graines de chia, le lait d'amande et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner frais et revitalisant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_banane_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes", "œufs", "flocons d'avoine", "noix concassées", "miel"]
    ingredient = [2*multiplicateur_arrondi, "bananes mûres, écrasées",4*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g de flocons d'avoine",30*multiplicateur_arrondi, "g de noix concassées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les bananes écrasées, les œufs, les flocons d'avoine et les noix concassées jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nCuire chaque crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.\nÉTAPE 4\nRépéter avec le reste de la pâte.\nÉTAPE 5\nServir les crêpes chaudes, garnies de miel et de tranches de banane si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_epinards_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "mangue", "yaourt grec", "lait d'amande", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g d'épinards frais",1*multiplicateur_arrondi, "mangue, pelée et coupée en morceaux",100*multiplicateur_arrondi, "g de yaourt grec",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, mélanger les épinards frais, les morceaux de mangue, le yaourt grec, le lait d'amande et le miel jusqu'à consistance lisse.\nÉTAPE 2 \nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 3\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner vert et fruité."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def bowlcake_vanille_pepites_chocolat(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "poudre de protéine (saveur vanille ou non aromatisée)", "œuf", "banane écrasée", "lait d'amande", "pépites de chocolat"]
    ingredient = [40*multiplicateur_arrondi, "g de flocons d'avoine",20*multiplicateur_arrondi, "g de poudre de protéine (saveur vanille ou non aromatisée)",1*multiplicateur_arrondi, "œuf",1*multiplicateur_arrondi, "banane écrasée",100*multiplicateur_arrondi, "ml de lait d'amande",20*multiplicateur_arrondi, "g de pépites de chocolat"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger les flocons d'avoine, la poudre de protéine, l'œuf, la banane écrasée, le lait d'amande et les pépites de chocolat jusqu'à obtenir une pâte homogène.\nÉTAPE 2 \nVerser la pâte dans un bol allant au micro-ondes.\nÉTAPE 3\nFaire cuire au micro-ondes pendant 2 à 3 minutes, ou jusqu'à ce que le bowlcake soit gonflé et cuit à travers.\nÉTAPE 4\nLaisser refroidir légèrement avant de déguster ce délicieux bowlcake à la vanille et aux pépites de chocolat."]
    temps_de_préparation = 5
    temps_de_cuisson = 3
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_patate_douce_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce", "œufs", "farine d'amande", "cannelle", "levure chimique"]
    ingredient = [1*multiplicateur_arrondi, "patate douce, cuite et écrasée",2*multiplicateur_arrondi, "œufs",30*multiplicateur_arrondi, "g de farine d'amande","une pincée de cannelle","une pincée de levure chimique"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la patate douce écrasée avec les œufs.\nÉTAPE 2 \nAjouter la farine d'amande, la cannelle et la levure chimique, et bien mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 3\nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à pancakes.\nÉTAPE 4\nCuire chaque pancake pendant environ 2 à 3 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir les pancakes chauds avec du sirop d'érable ou du miel si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "fromage râpé", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g de champignons, tranchés",30*multiplicateur_arrondi, "g de fromage râpé",1*multiplicateur_arrondi, "cuillère à soupe d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les champignons tranchés dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et cuits.\nÉTAPE 4\nVerser les œufs battus dans la poêle chaude avec les champignons.\nÉTAPE 5\nSaupoudrer de fromage râpé sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage fondu.\nÉTAPE 7\nPliez l'omelette en deux et servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_avocat_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "banane", "lait d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "avocat mûr, pelé et dénoyauté",1*multiplicateur_arrondi, "banane mûre, pelée et coupée en morceaux",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter l'avocat pelé et dénoyauté, la banane pelée et coupée en morceaux, le lait d'amande et le miel.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit crémeux.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner riche en nutriments et en saveurs."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffins_myrtilles_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["bananes mûres", "œufs", "farine d'amande", "levure chimique", "myrtilles"]
    ingredient = [2*multiplicateur_arrondi, "bananes mûres, écrasées",3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "g de farine d'amande",1*multiplicateur_arrondi, "cuillère à café de levure chimique",100*multiplicateur_arrondi, "g de myrtilles fraîches ou surgelées"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C (350°F) et chemiser un moule à muffins de caissettes en papier.\nÉTAPE 2 \nDans un bol, mélanger les bananes écrasées avec les œufs.\nÉTAPE 3\nAjouter la farine d'amande et la levure chimique, et bien mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 4\nIncorporer délicatement les myrtilles à la pâte.\nÉTAPE 5\nRépartir la pâte dans les caissettes à muffins préparées.\nÉTAPE 6\nCuire au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce que les muffins soient dorés et cuits à travers.\nÉTAPE 7\nLaisser refroidir légèrement avant de démouler et de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_pastèque_fraises(multiplicateur_arrondi):
    ingredient_pour_algo = ["pastèque", "fraises", "jus de citron", "miel"]
    ingredient = [200*multiplicateur_arrondi, "g de pastèque, sans pépins et coupée en dés",100*multiplicateur_arrondi, "g de fraises, équeutées et coupées en morceaux",1*multiplicateur_arrondi, "cuillère à soupe de jus de citron frais",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter les dés de pastèque, les morceaux de fraises, le jus de citron et le miel.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner rafraîchissant et délicieux."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_sarrasin_fromage_frais(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine de sarrasin", "œufs", "lait d'amande", "fromage frais", "sel", "poivre"]
    ingredient = [50*multiplicateur_arrondi, "g de farine de sarrasin",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de fromage frais","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine de sarrasin avec les œufs et le lait d'amande jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nCuire chaque crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.\nÉTAPE 4\nRépéter avec le reste de la pâte.\nÉTAPE 5\nServir les crêpes chaudes, garnies de fromage frais et assaisonnées de sel et de poivre."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "fromage feta", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais, lavés et hachés",30*multiplicateur_arrondi, "g de fromage feta, émietté",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans un bol, battre les œufs avec du sel et du poivre.\nÉTAPE 2 \nFaire chauffer l'huile d'olive dans une poêle antiadhésive à feu moyen.\nÉTAPE 3\nAjouter les épinards hachés dans la poêle et faire sauter jusqu'à ce qu'ils soient flétris.\nÉTAPE 4\nVerser les œufs battus sur les épinards dans la poêle chaude.\nÉTAPE 5\nAjouter le fromage feta émietté sur les œufs.\nÉTAPE 6\nCuire jusqu'à ce que les œufs soient pris et le fromage feta fondu.\nÉTAPE 7\nPliez l'omelette en deux et servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_banane_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "banane", "amandes effilées", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "banane mûre, écrasée",20*multiplicateur_arrondi, "g d'amandes effilées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et la banane écrasée dans la casserole et remuer pour bien mélanger.\nÉTAPE 3\nLaisser mijoter pendant 3 à 5 minutes, en remuant de temps en temps, jusqu'à ce que le porridge épaississe.\nÉTAPE 4\nRetirer du feu et verser le porridge dans un bol de service.\nÉTAPE 5\nGarnir d'amandes effilées et de miel avant de servir chaud."]
    temps_de_préparation = 2
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "lait de coco", "jus d'orange", "noix de coco râpée"]
    ingredient = [1*multiplicateur_arrondi, "mangue mûre, pelée et coupée en morceaux",200*multiplicateur_arrondi, "ml de lait de coco",100*multiplicateur_arrondi, "ml de jus d'orange frais",1*multiplicateur_arrondi, "cuillère à soupe de noix de coco râpée"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter les morceaux de mangue, le lait de coco, le jus d'orange et la noix de coco râpée.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner tropical et rafraîchissant."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_avocat_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "épinards frais", "banane", "lait d'amande"]
    ingredient = [1*multiplicateur_arrondi, "avocat mûr, pelé et dénoyauté",50*multiplicateur_arrondi, "g d'épinards frais, lavés",1*multiplicateur_arrondi, "banane mûre, pelée",200*multiplicateur_arrondi, "ml de lait d'amande"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter l'avocat pelé et dénoyauté, les épinards frais lavés, la banane pelée et le lait d'amande.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter des glaçons si désiré pour obtenir une texture plus épaisse et froide.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner riche en nutriments et en saveurs."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffins_carottes_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["carottes", "œufs", "farine d'amande", "levure chimique", "noix"]
    ingredient = [2*multiplicateur_arrondi, "carottes râpées",3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "g de farine d'amande",1*multiplicateur_arrondi, "cuillère à café de levure chimique",50*multiplicateur_arrondi, "g de noix concassées"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C (350°F) et chemiser un moule à muffins de caissettes en papier.\nÉTAPE 2 \nDans un bol, mélanger les carottes râpées avec les œufs.\nÉTAPE 3\nAjouter la farine d'amande et la levure chimique, et bien mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 4\nIncorporer délicatement les noix concassées à la pâte.\nÉTAPE 5\nRépartir la pâte dans les caissettes à muffins préparées.\nÉTAPE 6\nCuire au four préchauffé pendant 20 à 25 minutes, ou jusqu'à ce que les muffins soient dorés et cuits à travers.\nÉTAPE 7\nLaisser refroidir légèrement avant de démouler et de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_farine_coco_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine de coco", "œufs", "lait de coco", "banane", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine de coco",3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait de coco",1*multiplicateur_arrondi, "banane mûre, écrasée",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine de coco avec les œufs et le lait de coco jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nCuire chaque crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.\nÉTAPE 4\nRépéter avec le reste de la pâte.\nÉTAPE 5\nServir les crêpes chaudes, garnies de banane écrasée et de miel avant de servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_baies_vanille(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies mélangées surgelées", "lait d'amande", "yaourt grec", "extrait de vanille"]
    ingredient = [150*multiplicateur_arrondi, "g de baies mélangées surgelées",200*multiplicateur_arrondi, "ml de lait d'amande",100*multiplicateur_arrondi, "g de yaourt grec",1*multiplicateur_arrondi, "cuillère à café d'extrait de vanille"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter les baies mélangées surgelées, le lait d'amande, le yaourt grec et l'extrait de vanille.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner riche en antioxydants et en saveurs."]
    temps_de_préparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def porridge_pommes_noix_pecan(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "pommes", "noix de pécan", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "pomme, pelée et coupée en dés",20*multiplicateur_arrondi, "g de noix de pécan, concassées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine et les dés de pomme dans la casserole et remuer pour bien mélanger.\nÉTAPE 3\nLaisser mijoter pendant 3 à 5 minutes, en remuant de temps en temps, jusqu'à ce que le porridge épaississe.\nÉTAPE 4\nRetirer du feu et verser le porridge dans un bol de service.\nÉTAPE 5\nGarnir de noix de pécan concassées et de miel avant de servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def crepes_avoine_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine d'avoine", "œufs", "lait d'amande", "myrtilles", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine d'avoine",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la farine d'avoine avec les œufs et le lait d'amande jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à crêpes.\nÉTAPE 3\nDisposer quelques myrtilles sur chaque crêpe dans la poêle.\nÉTAPE 4\nCuire chaque crêpe pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'elles soient dorées et cuites à travers.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir les crêpes chaudes, garnies de myrtilles supplémentaires et de miel avant de servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_framboises_menthe(multiplicateur_arrondi):
    ingredient_pour_algo = ["framboises surgelées", "banane", "feuilles de menthe fraîche", "jus d'orange", "yaourt grec"]
    ingredient = [150*multiplicateur_arrondi, "g de framboises surgelées",1*multiplicateur_arrondi, "banane congelée, coupée en morceaux",4*multiplicateur_arrondi, "feuilles de menthe fraîche",100*multiplicateur_arrondi, "ml de jus d'orange",100*multiplicateur_arrondi, "g de yaourt grec"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter les framboises surgelées, la banane congelée, les feuilles de menthe fraîche, le jus d'orange et le yaourt grec.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter plus de jus d'orange si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nVerser le smoothie dans un verre et garnir de quelques framboises fraîches et de feuilles de menthe avant de servir."]
    temps_de_préparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_patate_douce_noix_pecan(multiplicateur_arrondi):
    ingredient_pour_algo = ["patate douce", "œufs", "farine d'amande", "levure chimique", "noix de pécan"]
    ingredient = [1*multiplicateur_arrondi, "patate douce moyenne, cuite et écrasée",3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "g de farine d'amande",1*multiplicateur_arrondi, "cuillère à café de levure chimique",50*multiplicateur_arrondi, "g de noix de pécan concassées"]
    préparation = ["ÉTAPE 1\n Dans un bol, mélanger la patate douce écrasée avec les œufs.\nÉTAPE 2 \nAjouter la farine d'amande et la levure chimique, et bien mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 3\nIncorporer délicatement les noix de pécan concassées à la pâte.\nÉTAPE 4\nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à pancakes.\nÉTAPE 5\nCuire chaque pancake pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 6\nRépéter avec le reste de la pâte.\nÉTAPE 7\nServir les pancakes chauds, garnis de noix de pécan supplémentaires et de sirop d'érable si désiré."]
    temps_de_préparation = 10
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_fromage(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "fromage suisse", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "g de champignons, tranchés",50*multiplicateur_arrondi, "g de fromage suisse, râpé",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans une poêle antiadhésive, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2 \nAjouter les champignons tranchés dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 3\nBattre les œufs dans un bol et verser sur les champignons dans la poêle chaude.\nÉTAPE 4\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus et légèrement dorée en dessous.\nÉTAPE 5\nSaupoudrer le fromage suisse râpé sur la moitié de l'omelette.\nÉTAPE 6\nPlier l'omelette en deux et cuire encore quelques instants pour faire fondre le fromage.\nÉTAPE 7\nAssaisonner avec du sel et du poivre au goût avant de servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_mangue_noix_coco(multiplicateur_arrondi):
    ingredient_pour_algo = ["mangue", "banane", "lait de coco", "noix de coco râpée", "jus d'orange"]
    ingredient = [1*multiplicateur_arrondi, "mangue mûre, pelée et dénoyautée",1*multiplicateur_arrondi, "banane mûre, pelée",200*multiplicateur_arrondi, "ml de lait de coco",2*multiplicateur_arrondi, "cuillères à soupe de noix de coco râpée",100*multiplicateur_arrondi, "ml de jus d'orange"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter la mangue pelée et dénoyautée, la banane pelée, le lait de coco, la noix de coco râpée et le jus d'orange.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter plus de jus d'orange si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nVerser le smoothie dans un verre et garnir de noix de coco râpée supplémentaire avant de servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_epinards_feta(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "épinards frais", "fromage feta", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "g d'épinards frais",50*multiplicateur_arrondi, "g de fromage feta, émietté",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans une poêle antiadhésive, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2 \nAjouter les épinards frais dans la poêle et faire sauter jusqu'à ce qu'ils soient légèrement fanés.\nÉTAPE 3\nBattre les œufs dans un bol et verser sur les épinards dans la poêle chaude.\nÉTAPE 4\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus et légèrement dorée en dessous.\nÉTAPE 5\nSaupoudrer le fromage feta émietté sur la moitié de l'omelette.\nÉTAPE 6\nPlier l'omelette en deux et cuire encore quelques instants pour faire fondre le fromage.\nÉTAPE 7\nAssaisonner avec du sel et du poivre au goût avant de servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def pancakes_sarrasin_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine de sarrasin", "œufs", "lait d'amande", "myrtilles", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de farine de sarrasin",2*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "ml de lait d'amande",50*multiplicateur_arrondi, "g de myrtilles fraîches ou surgelées",1*multiplicateur_arrondi, "cuillère à soupe de miel"]
    préparation = [
        "ÉTAPE 1\n Dans un bol, mélanger la farine de sarrasin avec les œufs et le lait d'amande jusqu'à obtenir une pâte lisse.\nÉTAPE 2 \nFaire chauffer une poêle antiadhésive à feu moyen et y verser une louche de pâte à pancakes.\nÉTAPE 3\nDisposer quelques myrtilles sur chaque pancake dans la poêle.\nÉTAPE 4\nCuire chaque pancake pendant environ 2 minutes de chaque côté, ou jusqu'à ce qu'ils soient dorés et cuits à travers.\nÉTAPE 5\nRépéter avec le reste de la pâte.\nÉTAPE 6\nServir les pancakes chauds, garnis de myrtilles supplémentaires et de miel avant de servir."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def smoothie_banane_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["banane", "épinards frais", "lait d'amande", "beurre d'amande", "miel"]
    ingredient = [1*multiplicateur_arrondi, "banane mûre, pelée",50*multiplicateur_arrondi, "g d'épinards frais",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "cuillère à soupe de beurre d'amande",1*multiplicateur_arrondi, "cuillère à café de miel"]
    préparation = ["ÉTAPE 1\n Dans un blender, ajouter la banane pelée, les épinards frais, le lait d'amande, le beurre d'amande et le miel.\nÉTAPE 2 \nMixer jusqu'à ce que tous les ingrédients soient bien mélangés et le smoothie soit lisse.\nÉTAPE 3\nAjouter plus de lait d'amande si nécessaire pour atteindre la consistance désirée.\nÉTAPE 4\nVerser le smoothie dans un verre et déguster immédiatement pour un petit déjeuner nutritif et délicieux."]
    temps_de_préparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def flocons_avoine_pomme_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "lait d'amande", "pomme", "cannelle", "miel"]
    ingredient = [50*multiplicateur_arrondi, "g de flocons d'avoine",200*multiplicateur_arrondi, "ml de lait d'amande",1*multiplicateur_arrondi, "pomme, pelée et coupée en dés",1*multiplicateur_arrondi, "cuillère à café de cannelle",1*multiplicateur_arrondi, "cuillère à café de miel"]
    préparation = ["ÉTAPE 1\n Dans une casserole, porter le lait d'amande à ébullition.\nÉTAPE 2 \nAjouter les flocons d'avoine, les dés de pomme et la cannelle dans la casserole et remuer pour bien mélanger.\nÉTAPE 3\nLaisser mijoter pendant 3 à 5 minutes, en remuant de temps en temps, jusqu'à ce que le mélange épaississe.\nÉTAPE 4\nRetirer du feu et verser le porridge dans un bol de service.\nÉTAPE 5\nArroser de miel avant de servir chaud, saupoudré de cannelle supplémentaire si désiré."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def omelette_champignons_epinards(multiplicateur_arrondi):
    ingredient_pour_algo = ["œufs", "champignons", "épinards frais", "fromage râpé", "huile d'olive", "sel", "poivre"]
    ingredient = [3*multiplicateur_arrondi, "œufs",100*multiplicateur_arrondi, "g de champignons, tranchés",50*multiplicateur_arrondi, "g d'épinards frais",30*multiplicateur_arrondi, "g de fromage râpé",1*multiplicateur_arrondi, "cuillère à café d'huile d'olive","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Dans une poêle antiadhésive, chauffer l'huile d'olive à feu moyen.\nÉTAPE 2 \nAjouter les champignons tranchés dans la poêle et faire sauter jusqu'à ce qu'ils soient dorés et tendres.\nÉTAPE 3\nAjouter les épinards frais dans la poêle et faire sauter jusqu'à ce qu'ils soient légèrement fanés.\nÉTAPE 4\nBattre les œufs dans un bol et verser sur les champignons et les épinards dans la poêle chaude.\nÉTAPE 5\nCuire l'omelette jusqu'à ce qu'elle soit prise sur le dessus et légèrement dorée en dessous.\nÉTAPE 6\nSaupoudrer le fromage râpé sur la moitié de l'omelette.\nÉTAPE 7\nPlier l'omelette en deux et cuire encore quelques instants pour faire fondre le fromage.\nÉTAPE 8\nAssaisonner avec du sel et du poivre au goût avant de servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def toast_avocat_oeuf(multiplicateur_arrondi):
    ingredient_pour_algo = ["pain complet", "avocat", "œufs", "sel", "poivre", "piment rouge en flocons", "jus de citron"]
    ingredient = [2*multiplicateur_arrondi, "tranches de pain complet",1*multiplicateur_arrondi, "avocat mûr",2*multiplicateur_arrondi, "œufs","sel", "au goût","poivre", "au goût",1*multiplicateur_arrondi, "pincée de piment rouge en flocons",1*multiplicateur_arrondi, "cuillère à café de jus de citron"]
    préparation = ["ÉTAPE 1\n Faire griller les tranches de pain complet jusqu'à ce qu'elles soient dorées et croustillantes.\nÉTAPE 2 \nPendant ce temps, écraser l'avocat dans un bol avec une fourchette et assaisonner de sel, de poivre et de jus de citron.\nÉTAPE 3\nFaire cuire les œufs selon votre préférence (poêchés, au plat, etc.).\nÉTAPE 4\nÉtaler la purée d'avocat sur les tranches de pain grillé.\nÉTAPE 5\nPlacer les œufs cuits sur le dessus de l'avocat.\nÉTAPE 6\nSaupoudrer de piment rouge en flocons et servir chaud."]
    temps_de_préparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
def muffin_epinard_fromage_feta(multiplicateur_arrondi):
    
    
    
    ingredient_pour_algo = ["épinards frais", "œufs", "lait", "farine", "fromage feta", "levure chimique", "sel", "poivre"]
    ingredient = [50*multiplicateur_arrondi, "g d'épinards frais, hachés",2*multiplicateur_arrondi, "œufs",50*multiplicateur_arrondi, "ml de lait",50*multiplicateur_arrondi, "g de farine",30*multiplicateur_arrondi, "g de fromage feta émietté",1*multiplicateur_arrondi, "cuillère à café de levure chimique","sel", "au goût","poivre", "au goût"]
    préparation = ["ÉTAPE 1\n Préchauffer le four à 180°C et graisser un moule à muffins.\nÉTAPE 2 \nDans un bol, battre les œufs avec le lait.\nÉTAPE 3\nAjouter la farine et la levure chimique, puis mélanger jusqu'à obtenir une pâte lisse.\nÉTAPE 4\nIncorporer les épinards hachés et le fromage feta émietté dans la pâte.\nÉTAPE 5\nAssaisonner avec du sel et du poivre selon votre goût.\nÉTAPE 6\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 7\nCuire au four préchauffé pendant environ 20 minutes, ou jusqu'à ce que les muffins soient dorés et fermes au toucher.\nÉTAPE 8\nLaisser refroidir légèrement avant de démouler et de servir."]
    temps_de_préparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_cuisson + temps_de_préparation
    tous = [ingredient_pour_algo, ingredient, préparation, temps_de_préparation, temps_de_cuisson, temps_total]
    return tous
tuplepetitdej_prise_de_mas=[bagel_saumon_fromage,bowl_quinoa_fruits,bowlcake_banane_pepites_chocolat,bowlcake_beurre_cacahuete_pepites_chocolat,bowlcake_pomme_cannelle,bowlcake_vanille_fruits_rouges,bowlcake_vanille_pepites_chocolat,crepes_avoine_myrtilles,crepes_banane_noix,crepes_farine_coco_banane,crepes_myrtilles_yaourt_grec,crepes_proteinees_aux_fruits,crepes_proteinees_vanille_fruits,crepes_proteinees_vanille_myrtilles,crepes_sarrasin_fromage_frais,flocons_avoine_pomme_cannelle,muffin_epinard_fromage_feta,muffins_carottes_noix,muffins_flocons_avoine_chocolat,muffins_myrtilles_banane,muffins_myrtilles_vanille,muffins_oeufs_legumes,oeufs_brouilles_epinards_fromage,oeufs_brouilles_epinards_fromage_cheddar,oeufs_brouilles_saumon_fume,omelette_champignons_epinards,omelette_champignons_fromage,omelette_champignons_fromage_cheddar,omelette_champignons_fromage_suisse,omelette_courgette_tomate,omelette_epinards_champignons,omelette_epinards_feta,omelette_epinards_fromage_feta,omelette_legumes_fromage,omelette_tomates_sechees_fromage_chevre,pain_perdu_proteine_cannelle_sirop_derable,pancakes_banane_avoine,pancakes_banane_beurre_cacahuete,pancakes_patate_douce_cannelle,pancakes_patate_douce_noix_pecan,pancakes_proteines_banane_beurre_cacahuete,pancakes_proteines_chocolat_amandes,pancakes_proteines_vanille_framboises,pancakes_proteines_vanille_myrtilles,pancakes_sarrasin_myrtilles,porridge_baies_cannelle,porridge_banane_amandes,porridge_fraises_amandes,porridge_fruits_secs_noix,porridge_myrtilles_amandes,porridge_pommes_cannelle,porridge_pommes_noix_pecan,porridge_proteine_pommes_cannelle,porridge_quinoa_noix_fruits_secs,sandwich_poulet_avocat,smoothie_ananas_noix_de_coco,smoothie_avocat_banane,smoothie_avocat_epinards,smoothie_avoine_baies,smoothie_baies_vanille,smoothie_banane_beurre_amande,smoothie_banane_beurre_arachide,smoothie_banane_epinards,smoothie_bowl_acai_chia,smoothie_bowl_baies_amande,smoothie_bowl_mangue_noix_coco,smoothie_bowl_proteine,smoothie_bowl_proteines_fruits,smoothie_epinards_mangue,smoothie_fraises_banane,smoothie_fraises_graines_chia,smoothie_framboises_menthe,smoothie_mangue_epinards,smoothie_mangue_lait_coco,smoothie_mangue_noix_coco,smoothie_mangue_noix_de_coco,smoothie_myrtilles_epinards]