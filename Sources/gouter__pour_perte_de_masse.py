def fruit_frais_entier(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruit"]
    ingredient = ["1 fruit frais entier"]
    preparation = ["Pas de préparation nécessaire. Simplement laver et couper le fruit si désiré."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

fruit_frais_entier
def concombre_houmous(multiplicateur_arrondi):
    ingredient_pour_algo = ["concombre", "houmous"]
    ingredient = ["1 concombre", "2 cuillères à soupe de houmous"]
    preparation = ["ÉTAPE 1\nLaver le concombre et le couper en tranches.\nÉTAPE 2\nServir avec du houmous."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

concombre_houmous
def yaourt_grec_baies(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "baies"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de baies mélangées (fraises, myrtilles, framboises, etc.)"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les baies mélangées sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_baies
def oeuf_dur(multiplicateur_arrondi):
    ingredient_pour_algo = ["œuf"]
    ingredient = ["1 œuf"]
    preparation = ["ÉTAPE 1\nFaire bouillir de l'eau dans une casserole.\nÉTAPE 2\nPlacer l'œuf dans l'eau bouillante et cuire pendant 8 à 10 minutes.\nÉTAPE 3\nRetirer l'œuf de l'eau bouillante et le plonger dans de l'eau glacée pour arrêter la cuisson.\nÉTAPE 4\nPeler l'œuf une fois refroidi et servir."]
    temps_de_preparation = 5
    temps_de_cuisson = 10
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

oeuf_dur
def lait_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande"]
    ingredient = ["1 verre de lait d'amande non sucré"]
    preparation = ["Pas de préparation nécessaire. Servir frais."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

lait_amande
def pomme_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["pomme", "beurre d'amande"]
    ingredient = ["1 pomme", "2 cuillères à soupe de beurre d'amande"]
    preparation = ["ÉTAPE 1\nLaver la pomme et la couper en tranches.\nÉTAPE 2\nÉtaler le beurre d'amande sur les tranches de pomme.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pomme_beurre_amande
def flocons_avoine_fruits(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "fruits"]
    ingredient = ["0.5 tasse de flocons d'avoine", "1/2 tasse de fruits frais ou surgelés"]
    preparation = ["ÉTAPE 1\nDans une casserole, faire chauffer de l'eau ou du lait selon les préférences.\nÉTAPE 2\nAjouter les flocons d'avoine et cuire selon les instructions sur l'emballage.\nÉTAPE 3\nTransférer dans un bol et garnir de fruits."]
    temps_de_preparation = 2
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

flocons_avoine_fruits
def carottes_yaourt_grec(multiplicateur_arrondi):
    ingredient_pour_algo = ["carottes", "yaourt grec"]
    ingredient = ["Carottes coupées en bâtonnets", "1/2 tasse de yaourt grec nature"]
    preparation = ["ÉTAPE 1\nÉplucher les carottes et les couper en bâtonnets.\nÉTAPE 2\nServir les bâtonnets de carottes avec du yaourt grec comme trempette."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

carottes_yaourt_grec
def quinoa_cuit(multiplicateur_arrondi):
    ingredient_pour_algo = ["quinoa"]
    ingredient = ["1/2 tasse de quinoa cuit"]
    preparation = ["ÉTAPE 1\nRincer le quinoa à l'eau froide dans une passoire fine.\nÉTAPE 2\nDans une casserole, porter 1 tasse d'eau à ébullition.\nÉTAPE 3\nAjouter le quinoa rincé à l'eau bouillante.\nÉTAPE 4\nRéduire le feu, couvrir et laisser mijoter pendant 15 minutes ou jusqu'à ce que le quinoa soit tendre.\nÉTAPE 5\nRetirer du feu et laisser reposer pendant 5 minutes avant de servir."]
    temps_de_preparation = 5
    temps_de_cuisson = 15
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

quinoa_cuit
def poignee_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix mélangées"]
    ingredient = ["Une poignée de noix mélangées"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix
def rondelles_kiwi(multiplicateur_arrondi):
    ingredient_pour_algo = ["kiwi"]
    ingredient = ["2 kiwis, coupés en rondelles"]
    preparation = ["ÉTAPE 1\nÉplucher les kiwis et les couper en rondelles.\nÉTAPE 2\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

rondelles_kiwi
def poivron_guacamole(multiplicateur_arrondi):
    ingredient_pour_algo = ["poivron rouge", "guacamole"]
    ingredient = ["1 poivron rouge, coupé en tranches", "1/4 tasse de guacamole"]
    preparation = ["ÉTAPE 1\nLaver le poivron rouge et le couper en tranches.\nÉTAPE 2\nServir avec du guacamole comme trempette."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poivron_guacamole
def smoothie_vert(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "banane", "lait d'amande"]
    ingredient = ["1 tasse d'épinards frais", "1 banane congelée", "1/2 tasse de lait d'amande non sucré"]
    preparation = ["ÉTAPE 1\nMettre tous les ingrédients dans un mixeur.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_vert
oeuf_dur
def popcorn_non_sale(multiplicateur_arrondi):
    ingredient_pour_algo = ["maïs à éclater"]
    ingredient = ["1/2 tasse de grains de maïs à éclater"]
    preparation = ["ÉTAPE 1\nFaire chauffer une grande casserole à feu moyen avec un couvercle.\nÉTAPE 2\nAjouter les grains de maïs dans la casserole.\nÉTAPE 3\nCouvrir la casserole et secouer doucement pendant que les grains éclatent.\nÉTAPE 4\nRetirer du feu dès que les éclats de maïs ralentissent.\nÉTAPE 5\nTransférer dans un bol et servir."]
    temps_de_preparation = 5
    temps_de_cuisson = 5
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

popcorn_non_sale
def smoothie_avocat(multiplicateur_arrondi):
    ingredient_pour_algo = ["avocat", "lait d'amande", "miel"]
    ingredient = ["1 avocat mûr", "1 tasse de lait d'amande non sucré", "1 cuillère à café de miel (facultatif)"]
    preparation = ["ÉTAPE 1\nRetirer le noyau de l'avocat et prélever la chair.\nÉTAPE 2\nDans un mixeur, ajouter la chair de l'avocat, le lait d'amande et le miel (si désiré).\nÉTAPE 3\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 4\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_avocat
def mini_wrap_poulet(multiplicateur_arrondi):
    ingredient_pour_algo = ["poulet grillé", "laitue", "tortilla de blé entier"]
    ingredient = ["50g de poulet grillé, coupé en lanières", "1 grande feuille de laitue", "1 tortilla de blé entier"]
    preparation = ["ÉTAPE 1\nÉtaler la tortilla de blé entier.\nÉTAPE 2\nDisposer la feuille de laitue sur la tortilla.\nÉTAPE 3\nAjouter les lanières de poulet grillé sur la laitue.\nÉTAPE 4\nEnrouler la tortilla fermement autour des ingrédients.\nÉTAPE 5\nCouper en mini wraps et servir."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

mini_wrap_poulet
def muffin_grains_entiers(multiplicateur_arrondi):
    ingredient_pour_algo = ["farine de blé entier", "sucre de coco", "œufs", "yaourt grec", "banane", "huile de coco"]
    ingredient = ["1 tasse de farine de blé entier", "1/2 tasse de sucre de coco", "2 œufs", "1/2 tasse de yaourt grec", "1 banane écrasée", "1/4 tasse d'huile de coco fondue"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 180°C et préparer un moule à muffins avec des caissettes en papier.\nÉTAPE 2\nDans un grand bol, mélanger la farine de blé entier et le sucre de coco.\nÉTAPE 3\nDans un autre bol, battre les œufs, puis ajouter le yaourt grec, la banane écrasée et l'huile de coco fondue.\nÉTAPE 4\nIncorporer le mélange liquide dans le mélange sec et mélanger jusqu'à ce que la pâte soit homogène.\nÉTAPE 5\nRépartir la pâte dans les moules à muffins préparés.\nÉTAPE 6\nCuire au four préchauffé pendant 20-25 minutes ou jusqu'à ce qu'un cure-dent inséré en ressorte propre.\nÉTAPE 7\nLaisser refroidir avant de déguster."]
    temps_de_preparation = 15
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

muffin_grains_entiers
def granola_maison(multiplicateur_arrondi):
    ingredient_pour_algo = ["flocons d'avoine", "noix", "miel", "huile de coco", "cannelle"]
    ingredient = ["1 tasse de flocons d'avoine", "1/2 tasse de noix mélangées", "2 cuillères à soupe de miel", "2 cuillères à soupe d'huile de coco fondue", "1/2 cuillère à café de cannelle"]
    preparation = ["ÉTAPE 1\nPréchauffer le four à 160°C et préparer une plaque de cuisson avec du papier sulfurisé.\nÉTAPE 2\nDans un grand bol, mélanger les flocons d'avoine, les noix, le miel, l'huile de coco fondue et la cannelle jusqu'à ce que tout soit bien enrobé.\nÉTAPE 3\nÉtaler le mélange sur la plaque de cuisson préparée en une couche uniforme.\nÉTAPE 4\nCuire au four préchauffé pendant 20-25 minutes, en remuant à mi-cuisson, jusqu'à ce que le granola soit doré et croustillant.\nÉTAPE 5\nLaisser refroidir complètement avant de transférer dans un récipient hermétique."]
    temps_de_preparation = 10
    temps_de_cuisson = 20
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

granola_maison
def poignee_graines_citrouille(multiplicateur_arrondi):
    ingredient_pour_algo = ["graines de citrouille"]
    ingredient = ["Une poignée de graines de citrouille"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_graines_citrouille
def yaourt_grec_noix_baies(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "noix", "baies"]
    ingredient = ["1 tasse de yaourt grec nature", "1/4 tasse de noix mélangées", "1/2 tasse de baies mélangées (fraises, myrtilles, framboises, etc.)"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les noix mélangées et les baies mélangées sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_noix_baies
def yaourt_grec_pomme_cannelle(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "pomme", "cannelle"]
    ingredient = ["1 tasse de yaourt grec nature", "1 pomme, coupée en tranches", "1/2 cuillère à café de cannelle"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les tranches de pomme sur le yaourt.\nÉTAPE 3\nSaupoudrer de cannelle sur les tranches de pomme.\nÉTAPE 4\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_pomme_cannelle
def yaourt_grec_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "mangue"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de mangue coupée en dés"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux de mangue sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_mangue
def melange_baies_fraiches(multiplicateur_arrondi):
    ingredient_pour_algo = ["baies"]
    ingredient = ["1 tasse de mélange de baies fraîches (fraises, myrtilles, framboises, etc.)"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

melange_baies_fraiches
def fruits_rouges(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits rouges"]
    ingredient = ["1 tasse de fruits rouges mélangés (fraises, framboises, myrtilles, etc.)"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

fruits_rouges
def yaourt_grec_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "banane"]
    ingredient = ["1 tasse de yaourt grec nature", "1 banane, coupée en tranches"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les tranches de banane sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_banane
def melange_fruits_secs(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits secs"]
    ingredient = ["1/4 tasse de mélange de fruits secs (abricots secs, raisins secs, pruneaux, etc.)"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

melange_fruits_secs
def poignee_noix_cajou(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de cajou"]
    ingredient = ["Une poignée de noix de cajou"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix_cajou
def smoothie_fraises_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["fraises", "banane", "lait d'amande"]
    ingredient = ["1 tasse de fraises fraîches ou congelées", "1 banane mûre", "1/2 tasse de lait d'amande non sucré"]
    preparation = ["ÉTAPE 1\nMettre les fraises, la banane et le lait d'amande dans un mixeur.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_fraises_banane
def legumes_houmous(multiplicateur_arrondi):
    ingredient_pour_algo = ["légumes croquants", "houmous"]
    ingredient = ["Une poignée de légumes croquants (carottes, concombres, céleris, etc.)", "2 cuillères à soupe de houmous"]
    preparation = ["ÉTAPE 1\nLaver et couper les légumes en bâtonnets ou en rondelles.\nÉTAPE 2\nServir avec du houmous comme trempette."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

legumes_houmous
def yaourt_grec_baies_goji(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "baies de goji"]
    ingredient = ["1 tasse de yaourt grec nature", "2 cuillères à soupe de baies de goji séchées"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nSaupoudrer les baies de goji sur le yaourt.\nÉTAPE 3\nLaisser reposer quelques minutes pour réhydrater légèrement les baies de goji avant de servir."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_baies_goji
def smoothie_epinards_banane(multiplicateur_arrondi):
    ingredient_pour_algo = ["épinards", "banane", "lait d'amande"]
    ingredient = ["1 tasse d'épinards frais ou congelés", "1 banane mûre", "1/2 tasse de lait d'amande non sucré"]
    preparation = ["ÉTAPE 1\nMettre les épinards, la banane et le lait d'amande dans un mixeur.\nÉTAPE 2\nMixer jusqu'à obtenir une consistance lisse et crémeuse.\nÉTAPE 3\nVerser dans un verre et servir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

smoothie_epinards_banane
def poignee_noix_grenoble(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de Grenoble"]
    ingredient = ["Une poignée de noix de Grenoble"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix_grenoble
def yaourt_grec_myrtilles(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "myrtilles"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de myrtilles fraîches ou congelées"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les myrtilles sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_myrtilles
def melange_fruits_secs_noix(multiplicateur_arrondi):
    ingredient_pour_algo = ["fruits secs", "noix"]
    ingredient = ["1/4 tasse de mélange de fruits secs (abricots secs, raisins secs, pruneaux, etc.)", "Une poignée de noix mélangées"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

melange_fruits_secs_noix
def pomme_beurre_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["pomme", "beurre d'amande"]
    ingredient = ["1 pomme, coupée en tranches", "2 cuillères à soupe de beurre d'amande"]
    preparation = ["ÉTAPE 1\nLaver et couper la pomme en tranches.\nÉTAPE 2\nTartiner chaque tranche de pomme avec du beurre d'amande.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 5
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

pomme_beurre_amande
def yaourt_grec_framboises(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "framboises"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de framboises fraîches ou congelées"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les framboises sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_framboises
def yaourt_grec_peche(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "pêche"]
    ingredient = ["1 tasse de yaourt grec nature", "1 pêche, coupée en morceaux"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux de pêche sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_peche
def yaourt_grec_ananas(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "ananas"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse d'ananas frais coupé en dés"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux d'ananas sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_ananas
def yaourt_grec_mangue(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "mangue"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de mangue fraîche coupée en dés"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux de mangue sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_mangue
def yaourt_grec_kiwi(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "kiwi"]
    ingredient = ["1 tasse de yaourt grec nature", "1 kiwi, pelé et coupé en dés"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux de kiwi sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_kiwi
def poignee_noix_macadamia(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de macadamia"]
    ingredient = ["Une poignée de noix de macadamia"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix_macadamia
def yaourt_grec_fraise(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "fraise"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de fraises fraîches coupées en morceaux"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les morceaux de fraise sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_fraise
def poignee_noix_pecan(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de pécan"]
    ingredient = ["Une poignée de noix de pécan"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix_pecan
def yaourt_grec_cerise(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "cerise"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de cerises fraîches dénoyautées"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les cerises dénoyautées sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_cerise
def poignee_noix_cajou(multiplicateur_arrondi):
    ingredient_pour_algo = ["noix de cajou"]
    ingredient = ["Une poignée de noix de cajou"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_noix_cajou
def yaourt_grec_framboise(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "framboise"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de framboises fraîches"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les framboises sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_framboise
def poignee_amandes(multiplicateur_arrondi):
    ingredient_pour_algo = ["amandes"]
    ingredient = ["Une poignée d'amandes"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

poignee_amandes
def bol_lait_amande(multiplicateur_arrondi):
    ingredient_pour_algo = ["lait d'amande"]
    ingredient = ["1 bol de lait d'amande non sucré"]
    preparation = ["Pas de préparation nécessaire. Servir tel quel."]
    temps_de_preparation = 0
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

bol_lait_amande
def yaourt_grec_myrtille(multiplicateur_arrondi):
    ingredient_pour_algo = ["yaourt grec", "myrtille"]
    ingredient = ["1 tasse de yaourt grec nature", "1/2 tasse de myrtilles fraîches"]
    preparation = ["ÉTAPE 1\nMettre le yaourt grec dans un bol.\nÉTAPE 2\nAjouter les myrtilles sur le yaourt.\nÉTAPE 3\nServir immédiatement."]
    temps_de_preparation = 2
    temps_de_cuisson = 0
    temps_total = temps_de_preparation + temps_de_cuisson
    tous = [ingredient_pour_algo, ingredient, preparation, temps_de_preparation, temps_de_cuisson, temps_total]
    return tous

yaourt_grec_myrtille

tuplegouter_perte_de_poids=[bol_lait_amande,carottes_yaourt_grec,concombre_houmous,flocons_avoine_fruits,fruit_frais_entier,fruits_rouges,granola_maison,lait_amande,legumes_houmous,melange_baies_fraiches,melange_fruits_secs,melange_fruits_secs_noix,mini_wrap_poulet,muffin_grains_entiers,oeuf_dur,poignee_amandes,poignee_graines_citrouille,poignee_noix,poignee_noix_cajou,poignee_noix_grenoble,poignee_noix_macadamia,poignee_noix_pecan,poivron_guacamole,pomme_beurre_amande,popcorn_non_sale,quinoa_cuit,rondelles_kiwi,smoothie_avocat,smoothie_epinards_banane,smoothie_fraises_banane,smoothie_vert,yaourt_grec_ananas,yaourt_grec_baies,yaourt_grec_baies_goji,yaourt_grec_banane,yaourt_grec_cerise,yaourt_grec_fraise,yaourt_grec_framboise,yaourt_grec_framboises,yaourt_grec_kiwi,yaourt_grec_mangue,yaourt_grec_myrtille,yaourt_grec_myrtilles,yaourt_grec_noix_baies,yaourt_grec_peche,yaourt_grec_pomme_cannelle]
